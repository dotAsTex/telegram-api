﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Caliburn.Micro;
using Telegram.Api.Extensions;
using Telegram.Api.Helpers;
using Telegram.Api.Services;
using Telegram.Api.TL;
using Telegram.Api.WindowsPhone;
using Action = System.Action;
using TransportType = Telegram.Api.Services.TransportType;

namespace Telegram.Api.Transport
{
    public class TcpTransportResult
    {
        public SocketError Error { get; set; }

        public Exception Exception { get; set; }

        public SocketAsyncOperation Operation { get; set; }
        
        public TcpTransportResult(SocketAsyncOperation operation, SocketError error)
        {
            Operation = operation;
            Error = error;
        }

        public TcpTransportResult(SocketAsyncOperation operation, Exception exception)
        {
            Operation = operation;
            Exception = exception;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Operation=" + Operation);
            sb.AppendLine("Error=" + Error);
            sb.AppendLine("Exception=" + Exception);

            return sb.ToString();
        }
    }
    [Serializable]
    public class TcpTransport : ITransport
    {
        public int Id { get; protected set; }

        private readonly object _previousMessageRoot = new object();

        public long PreviousMessageId;
        public TcpTransport()
        {

        }
        public TLLong GenerateMessageId(bool checkPreviousMessageId = false)
        {
            var clientDelta = ClientTicksDelta;
            // serverTime = clientTime + clientDelta
            var now = DateTime.Now;
            //var unixTime = (long)Utils.DateTimeToUnixTimestamp(now) << 32;

            var unixTime = (long)(Utils.DateTimeToUnixTimestamp(now) * 4294967296) + clientDelta; //2^32
            long correctUnixTime;

            var addingTicks = 4 - (unixTime % 4);
            if ((unixTime % 4) == 0)
            {
                correctUnixTime = unixTime;
            }
            else
            {
                correctUnixTime = unixTime + addingTicks;
            }

            // check with previous messageId

            lock (_previousMessageRoot)
            {
                if (PreviousMessageId != 0 && checkPreviousMessageId)
                {
                    correctUnixTime = Math.Max(PreviousMessageId + 4, correctUnixTime);
                }
                PreviousMessageId = correctUnixTime;
            }

            // refactor this:
            // addTicks = 4 - (unixTime % 4)
            // fixedUnixTime = unixTime + addTicks
            // max(fixedUnixTime, previousMessageId + 4)

            //if ((unixTime % 4) == 0)
            //{
            //    correctUnixTime = unixTime;
            //}
            //else
            //{
            //    for (int i = 0; i < 300; i++)
            //    {
            //        var temp = unixTime - i;
            //        if ((temp % 4) == 0)
            //        {
            //            correctUnixTime = unixTime;
            //            break;
            //        }
            //    }
            //}
            //TLUtils.WriteLine("TLMessage ID: " + correctUnixTime);
            //TLUtils.WriteLine("MessageId % 4 =" + (correctUnixTime % 4));
            //TLUtils.WriteLine("Corresponding time: " + Utils.UnixTimestampToDateTime(correctUnixTime >> 32));

            if (correctUnixTime == 0)
                throw new Exception("Bad message id");

            return new TLLong(correctUnixTime);
        }


        #region NonEncryptedHistory

        private readonly object _nonEncryptedHistoryRoot = new object();

        private readonly Dictionary<long, HistoryItem> _nonEncryptedHistory = new Dictionary<long, HistoryItem>();

        public void EnqueueNonEncryptedItem(HistoryItem item)
        {
            lock (_nonEncryptedHistoryRoot)
            {
                _nonEncryptedHistory[item.Hash] = item;
            }
        }

        public HistoryItem DequeueFirstNonEncryptedItem()
        {
            lock (_nonEncryptedHistoryRoot)
            {
                var item = _nonEncryptedHistory.Values.FirstOrDefault();
                if (item != null)
                {
                    _nonEncryptedHistory.Remove(item.Hash);
                }

                return item;
            }
        }

        public void RemoveNonEncryptedItem(HistoryItem item)
        {
            lock (_nonEncryptedHistoryRoot)
            {
#if LOG_REGISTRATION
                var info = new StringBuilder();
                info.AppendLine("Remove NonEncryptedItem: " + item.Caption + " " + item.Hash);
                info.AppendLine("Items before: " + _nonEncryptedHistory.Count);
                foreach (var historyItem in _nonEncryptedHistory.Values)
                {
                    info.AppendLine(historyItem.Caption + " " + historyItem.Hash);
                }             
#endif

                _nonEncryptedHistory.Remove(item.Hash);
#if LOG_REGISTRATION
                info.AppendLine("Items after: " + _nonEncryptedHistory.Count);
                foreach (var historyItem in _nonEncryptedHistory.Values)
                {
                    info.AppendLine(historyItem.Caption + " " + historyItem.Hash);
                }
                TLUtils.WriteLog(info.ToString());
#endif

            }
        }

        public void ClearNonEncryptedHistory(Exception e = null)
        {
            lock (_nonEncryptedHistoryRoot)
            {
                var error = new StringBuilder();
                error.Append("ClearNonEncryptedHistory " + _nonEncryptedHistory.Count);
                if (e != null)
                {
                    error.AppendLine(e.ToString());
                }

#if LOG_REGISTRATION
                TLUtils.WriteLog(error.ToString());
#endif

                foreach (var historyItem in _nonEncryptedHistory)
                {
                    //var error = new StringBuilder();
                    //error.AppendLine("Clear NonEncrypted History: ");
                    //if (e != null)
                    //{
                    //    error.AppendLine(e.ToString());
                    //}
#if LOG_REGISTRATION
                    TLUtils.WriteLog("ClearNonEncryptedHistoryItem " + historyItem.Value.Caption);
#endif
                    historyItem.Value.FaultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString(error.ToString()) });
                }

                _nonEncryptedHistory.Clear();
            }
        }

        public string PrintNonEncryptedHistory()
        {
            var sb = new StringBuilder();

            lock (_nonEncryptedHistoryRoot)
            {
                sb.AppendLine("NonEncryptedHistory items:");
                foreach (var historyItem in _nonEncryptedHistory.Values)
                {
                    sb.AppendLine(historyItem.Caption + " msgId " + historyItem.Hash);
                }
            }

            return sb.ToString();
        }
        #endregion

        private readonly object _syncRoot = new object();

        public object SyncRoot { get { return _syncRoot; } }

        public override string ToString()
        {
            return string.Format("{0}) {1}:{2} (AuthKey {3})\n  Salt {4}\n  SessionId {5} TicksDelta {6}", DCId, Host, Port, AuthKey != null, Salt, SessionId, ClientTicksDelta);
        }

        public int DCId { get; set; }

        public byte[] AuthKey { get; set; }
        public TLLong SessionId { get; set; }
        public TLLong Salt { get; set; }
        public int SequenceNumber { get; set; }
        public long ClientTicksDelta { get; set; }

        public bool Initiated { get; set; }

        public bool Initialized { get; set; }

        public bool IsInitializing { get; set; }

        public bool IsAuthorized { get; set; }

        public bool IsAuthorizing { get; set; }

        public string Host { get { return _host; } }

        public int Port { get { return _port; } }

        public TransportType Type { get { return TransportType.Tcp; } }

        private readonly string _host;

        private readonly int _port;

        /// <summary>
        /// Socket used for connection to the server, sending and receiving data
        /// </summary>
        private readonly Socket _socket;

        /// <summary>
        /// Default _buffer size that should be used with the same value in both server and client
        /// </summary>
        private const int BufferSize = 60;//1024 * 5;//20042;

        /// <summary>
        /// Buffer used to store bytes received
        /// </summary>
        private readonly byte[] _buffer;

        /// <summary>
        /// Bytes received in current receiving operation
        /// </summary>
        private int _bytesReceived;

        private readonly SocketAsyncEventArgs _listener = new SocketAsyncEventArgs();

        private IEventAggregator _eventAggregator;

        public TcpTransport(string host, int port)
        {
            _host = host;
            _port = port;
            _buffer = new byte[BufferSize];

            //ConnectAsync(host, port, null);

            var random = new Random();
            Id = random.Next(0, 255);

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            
            //var ni = _socket.GetCurrentNetworkInterface();
            //NetworkCharacteristics ch = ni.Characteristics;
            //ni.InterfaceSubtype

            // Store the received _buffer in a class variable
            _listener.SetBuffer(_buffer, 0, _buffer.Length);
            // Set the event handler for received data
            _listener.Completed += OnReceived;
        }

        private void OnConnected(SocketAsyncEventArgs args, Action callback = null, Action<TcpTransportResult> faultCallback = null)
        {
            try
            {
                if (args.SocketError != SocketError.Success)
                {
                    faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Connect, args.SocketError));
                }
                else
                {
                    //if (!_onceFirstBytesReceived)
                    //{
                    //    _onceFirstBytesReceived = true;
                        RaiseConnectedAsync(EventArgs.Empty);
                    //}
#if LOG_TCP
                    TLUtils.WriteLog(string.Format("TCP Connected to: {0}:{1}", _host, _port));
#endif
                    BeginRead();

                    var sendArgs = new SocketAsyncEventArgs();
                    sendArgs.SetBuffer(new byte[] { 0xef }, 0, 1);
                    sendArgs.Completed += (o, e) => callback.SafeInvoke();

                    try
                    {
                        var result = _socket.SendAsync(sendArgs);
                    }
                    catch (ObjectDisposedException ex)
                    {

                        faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Send, ex));
                        //TLUtils.WriteLine("SendAsync on disposed socket", LogSeverity.Error);
                    }
                }

            }
            catch (Exception ex)
            {
                faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Connect, ex));
            }

        }

        private void BeginRead()
        {
#if LOG_TCP
            TLUtils.WriteLog("TCP BeginRead");
#endif
            try
            {
                if (_socket != null)
                {
                    if (_socket.Connected)
                    {
#if DEBUG
                        for (var i = 0; i < _buffer.Length; i++)
                        {
                            _buffer[i] = 0x0;
                        }
#endif
                        // Start receiving data asynchronously
                        try
                        {
#if LOG_TCP
                            TLUtils.WriteLog("TCP BeginRead ReceiveAsync");
#endif
                            _socket.ReceiveAsync(_listener);
                        }
                        catch (ObjectDisposedException ex)
                        {
#if LOG_TCP
                            TLUtils.WriteLog("TCP BeginRead ReceiveAsync Exception: \n" + ex);
#endif
                            //TLUtils.WriteLine("ReceiveAsync on disposed socket", LogSeverity.Error);
                        }
                    }
                    else
                    {
#if LOG_TCP
                        TLUtils.WriteLog("TCP BeginRead Socket is not connected");
#endif
                        //throw new Exception("Socket is not connected");
                    }
                }
                else
                {
#if LOG_TCP
                    TLUtils.WriteLog("TCP BeginRead _socket==null throw NullReferenceException");
#endif
                    throw new NullReferenceException("Socket is null");
                    //ConnectAsync();
                }
            }
            catch (Exception e)
            {
#if LOG_TCP
                TLUtils.WriteLog(string.Format("TCP BeginRead Exception:\n", e));
#endif
                TLUtils.WriteException(e);
                //throw;
            }
            
        }

        private static int GetLength(byte[] bytes, int position, out int bytesRead)
        {
            if (bytes.Length <= position)
            {
                bytesRead = 0;
                return 0;
            }

            int shortLength;
            if (bytes[position] == 0x7F)
            {
                var lengthBytes = bytes.SubArray(1 + position, 3);
                shortLength = BitConverter.ToInt32(lengthBytes.Concat(new byte[] { 0x00 }).ToArray(), 0);
                bytesRead = 4;
            }
            else
            {
                shortLength = bytes[0];
                bytesRead = 1;
            }

            return shortLength * 4;
        }

        readonly MemoryStream _stream = new MemoryStream();

        private int _packetLength = 0;

        private int _packetLengthBytesRead = 0;

        private byte[] _previousTail = new byte[0];

        private bool _usePreviousTail;

        public WindowsPhone.Tuple<int, int, int> GetCurrentPacketInfo()
        {
            //_listener.Status

            return new WindowsPhone.Tuple<int, int, int>(_packetLengthBytesRead, _packetLength, _bytesReceived);
        }

        public string GetTransportInfo()
        {
            var info = new StringBuilder();
            info.AppendLine("TCP transport");
            info.AppendLine(string.Format("Socket {0}:{1}, Connected={2}, Ttl={3}, HashCode={4}", _host, _port, _socket.Connected, _socket.Ttl, _socket.GetHashCode()));
            info.AppendLine(string.Format("Listener LastOperation={0}, SocketError={1}, RemoteEndPoint={2}, SocketHash={3}", _listener.LastOperation, _listener.SocketError, _listener.RemoteEndPoint, _listener.ConnectSocket != null ? _listener.ConnectSocket.GetHashCode().ToString() : "null"));
            info.AppendLine(string.Format("FirstReceiveTime={0}", _firstReceiveTime.GetValueOrDefault().ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)));
            info.AppendLine(string.Format("LastReceiveTime={0}, BytesReceived={1}", _lastReceiveTime.GetValueOrDefault().ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture), _bytesReceived));
            info.AppendLine(string.Format("FirstSendTime={0}", _firstSendTime.GetValueOrDefault().ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)));
            info.AppendLine(string.Format("LastSendTime={0}", _lastSendTime.GetValueOrDefault().ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)));
            info.AppendLine(string.Format("PacketLength={0}, PacketBytesRead={1}", _packetLength, _stream.Length));

            return info.ToString();
        }

        //private bool _onceFirstBytesReceived;

        private void OnReceived(object sender, SocketAsyncEventArgs e)
        {
            var socket = sender as Socket;
            if (socket == null || socket != _socket)
            {
#if LOG_TCP
                TLUtils.WriteLog("TCP OnReceived return socket==null: " + (socket == null) + " socket!=_socket: " + (socket != _socket));
#endif
                return;
            }
            var now = DateTime.Now;

            if (!_firstReceiveTime.HasValue)
            {
                _firstReceiveTime = now;
            }
            // Make sure that receiving was successful
            if (e.SocketError == SocketError.Success)
            {
                
                if (e.BytesTransferred > 0)
                {
                    _lastReceiveTime = now;
                }

                // Increase the count of received bytes in the current receiving operation
                _bytesReceived += e.BytesTransferred;
                if (_packetLength == 0)
                {
                    byte[] buffer;

                    if (_usePreviousTail)
                    {
                        _usePreviousTail = false;
                        buffer = TLUtils.Combine(_previousTail, e.Buffer);
                        _previousTail = new byte[0];
                    }
                    else
                    {
                        buffer = e.Buffer;
                    }

                    _packetLength = GetLength(buffer, e.Offset, out _packetLengthBytesRead);
                }
            }
            else
            {
#if LOG_TCP
                TLUtils.WriteLog("TCP OnReceived BeginRead with e.SocketError=" + e.SocketError);
#endif
                BeginRead();
                return;
            }


            // Check if the buffer is already full
#if LOG_TCP
            TLUtils.WriteLog("TCP OnReceived e.BytesTransferred=" + e.BytesTransferred);
#endif
            if (e.BytesTransferred > 0)
            {
                _stream.Write(e.Buffer, e.Offset, e.BytesTransferred);

                if (_bytesReceived >= _packetLength + _packetLengthBytesRead)
                {
                    var bytes = _stream.ToArray();

                    var data = bytes.SubArray(_packetLengthBytesRead, _packetLength);
                    _previousTail = new byte[] {};
                    if (_bytesReceived > _packetLength + _packetLengthBytesRead)
                    {
                        _previousTail = bytes.SubArray(_packetLengthBytesRead + _packetLength, _bytesReceived - (_packetLengthBytesRead + _packetLength));
                    }
                        
                    _stream.SetLength(0);
                    _stream.Write(_previousTail, 0, _previousTail.Length);
                    _bytesReceived = _previousTail.Length;

                    if (_previousTail.Length > 0)
                    {
                        if (_previousTail.Length >= 4)
                        {
                            _packetLength = GetLength(_previousTail, 0, out _packetLengthBytesRead);
                            //if (_previousTail.Length >= _packetLength + _packetLengthBytesRead)
                            //{
                            //    var package = _previousTail.SubArray(_packetLengthBytesRead, _packetLength);

                            //    _previousTail = _previousTail.SubArray(
                            //        _packetLengthBytesRead + _packetLength,
                            //        _previousTail.Length - (_packetLengthBytesRead + _packetLength));
                            //    try
                            //    {
                            //        //ThreadPool.QueueUserWorkItem(state =>
                            //        //{
                            //            if (ReceiveBytes != null)
                            //            {
                            //                ReceiveBytes(this, new DataEventArgs(package));
                            //            }
                            //        //});


                            //    }
                            //    catch (Exception ex)
                            //    {
                            //        TLUtils.WriteLine(ex.ToString(), LogSeverity.Error);
                            //    }


                            //    if (_previousTail.Length > 0)
                            //    {
                            //        if (_previousTail.Length >= 4)
                            //        {
                            //            _packetLength = GetLength(_previousTail, 0, out _packetLengthBytesRead);
                            //        }
                            //        else
                            //        {
                            //            _packetLengthBytesRead = 0;
                            //            _packetLength = 0;
                            //            _usePreviousTail = true;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        _packetLength = GetLength(_previousTail, 0, out _packetLengthBytesRead);
                            //    }
                            //}

                        }
                        else
                        {
                            _packetLengthBytesRead = 0;
                            _packetLength = 0;
                            _usePreviousTail = true;
                        }
                    }
                    else
                    {
                        _packetLength = GetLength(_previousTail, 0, out _packetLengthBytesRead);
                    }

                    try
                    {
                        ThreadPool.QueueUserWorkItem(state =>
                        {
#if LOG_TCP
                            TLUtils.WriteLog("TCP OnReceived InvokeDataReceived ReceiveBytes!=" + (ReceiveBytes != null) + " data.Length=" + data.Length);
#endif
                            RaiseReceiveBytes(new DataEventArgs(data));
                        });
                    }
                    catch (Exception ex)
                    {
#if LOG_TCP
                        TLUtils.WriteLog("TCP OnReceived EndWithException=" + ex);
#endif
                        TLUtils.WriteLine(ex.ToString(), LogSeverity.Error);
                    }  
                }
            }

            BeginRead();               
        }

        private void ConnectAsync(Action callback, Action<TcpTransportResult> faultCallback)
        {
            try
            {
                var args = new SocketAsyncEventArgs
                {
                    RemoteEndPoint = new DnsEndPoint(_host, _port)
                };

                args.Completed += (o, e) => OnConnected(e, callback, faultCallback);


                try
                {
                    RaiseConnectingAsync(EventArgs.Empty);
                    var result = _socket.ConnectAsync(args);
                }
                catch (ObjectDisposedException ex)
                {
                    faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Connect, ex));
#if LOG_REGISTRATION
                    TLUtils.WriteLog("ConnectAsync on DisposedSocket Invoke FaultCallback");
#endif
                    //TLUtils.WriteLine("ConnectAsync on disposed socket", LogSeverity.Error);
                }
            }
            catch (Exception e)
            {
#if LOG_REGISTRATION
                TLUtils.WriteLog("ConnectAsync exception Invoke FaultCallback");
#endif

                faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Connect, e));
            }
        }

        private readonly object _socketRoot = new object();

        private DateTime? _firstReceiveTime;

        public DateTime? FirstReceiveTime { get { return _firstReceiveTime; } }

        private DateTime? _lastReceiveTime;

        public DateTime? LastReceiveTime { get { return _lastReceiveTime; } }

        private DateTime? _firstSendTime;

        public DateTime? FirstSendTime { get { return _firstSendTime; } }

        private DateTime? _lastSendTime;

        public DateTime? LastSendTime { get { return _lastSendTime; } }

        public bool Connected{ get { return _socket.Connected; } }

        public void SendBytesAsync(string caption, byte[] data, Action<SocketError> callback, Action<TcpTransportResult> faultCallback = null)
        {
            var now = DateTime.Now;
            if (!_firstSendTime.HasValue)
            {
                _firstSendTime = now;
            }
            _lastSendTime = now;

            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    TLUtils.WriteLine("  TCP: Send " + caption);
                    var args = CreateArgs(data, callback);

                    lock (_socketRoot)
                    {
                        var manualResetEvent = new ManualResetEvent(false);
                        if (!_socket.Connected)
                        {
                            if (caption.StartsWith("msgs_ack"))
                            {
                                TLUtils.WriteLine("!!!!!!MSGS_ACK FAULT!!!!!!!", LogSeverity.Error);
                                Debug.WriteLine("!!!!!!!FAULT MSGS_ACK!!!!!!!");
                                faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Send, new Exception("MSGS_ACK_FAULT")));
                                return;
                            }
                            //TLUtils.WritePerformance("  TCP: Connect " + caption);
                            ConnectAsync(() =>
                                {
                                    manualResetEvent.Set();
                                    try
                                    {
                                        _socket.SendAsync(args);
                                    }
                                    catch (ObjectDisposedException ex)
                                    {
                                        //TLUtils.WriteLine("SendAsync on disposed socket", LogSeverity.Error);
                                    }

                                },
                                error =>
                                {
                                    manualResetEvent.Set();
                                    faultCallback.SafeInvoke(error);
                                });
                            var connected = manualResetEvent.WaitOne(25000);
                            if (!connected)
                            {
                                faultCallback.SafeInvoke(new TcpTransportResult(SocketAsyncOperation.Connect, new Exception("Connect timeout exception")));
                            }
                        }
                        else
                        {
                            try
                            {
                                var result = _socket.SendAsync(args);
                            }
                            catch (ObjectDisposedException ex)
                            {
                                //TLUtils.WriteLine("SendAsync on disposed socket", LogSeverity.Error);
                            }
                        }
                    }
                });
        }

        private static SocketAsyncEventArgs CreateArgs(byte[] buffer, Action<SocketError> callback = null)
        {
            const int maxShortLength = 0x7E;
            var shortLength = buffer.Length / 4;
            var length = (shortLength > maxShortLength) ? 4 + buffer.Length : 1 + buffer.Length;
            var bytes = new byte[length];

            if (shortLength > maxShortLength)
            {
                bytes[0] = 0x7F;
                var shortLengthBytes = BitConverter.GetBytes(shortLength);
                Array.Copy(shortLengthBytes, 0, bytes, 1, 3);
                Array.Copy(buffer, 0, bytes, 4, buffer.Length);
            }
            else
            {
                bytes[0] = (byte)shortLength;
                Array.Copy(buffer, 0, bytes, 1, buffer.Length);
            }

            var args = new SocketAsyncEventArgs();
            args.SetBuffer(bytes, 0, bytes.Length);
            args.Completed += (sender, eventArgs) =>
            {
                callback.SafeInvoke(eventArgs.SocketError);
            };
            return args;
        }

        public bool Closed { get; protected set; }

        public void Close()
        {
            //_packetLength = 0;

            //_packetLengthBytesRead = 0;

            //_previousTail = new byte[0];

            //_usePreviousTail = false;
            if (_socket != null)
            {
                _socket.Close();
#if LOG_REGISTRATION

                TLUtils.WriteLog(string.Format("Close socket {2} {0}:{1}", Host, Port, Id));
#endif
                Closed = true;
            }
        }

        public void StartListening()
        {
            try
            {
                var result = _socket.ReceiveAsync(_listener);
            }
            catch (Exception e)
            {
                TLUtils.WriteLine(e.ToString(), LogSeverity.Error);
            }
        }

        public event EventHandler<DataEventArgs> ReceiveBytes;

        private void RaiseReceiveBytes(DataEventArgs args)
        {
            var handler = ReceiveBytes;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public event EventHandler SocketConnecting;

        private void RaiseConnectingAsync(EventArgs args)
        {
            var handler = SocketConnecting;
            if (handler != null)
            {
                Helpers.Execute.BeginOnThreadPool(() => handler(this, args));
            }
        }

        public event EventHandler SocketConnected;

        private void RaiseConnectedAsync(EventArgs args)
        {
            var handler = SocketConnected;
            if (handler != null)
            {
                Helpers.Execute.BeginOnThreadPool(() => handler(this, args));
            }
        }
    }
}
