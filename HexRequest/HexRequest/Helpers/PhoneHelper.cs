﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Xml;

namespace Telegram.Api.Helpers
{
    public class PhoneHelper
    {
        const string AppManifestName = "WMAppManifest.xml";
        const string AppNodeName = "InnoShopHelper";
        public const string AppVersion = "0.1";


        public static bool IsLowMemoryDevice()
        {
            return false;
        }

        public static string GetOSVersion()
        {
            return Environment.OSVersion.Version.ToString();
        }

        public static string GetAppVersion()
        {
            return AppVersion;
        }

        /// <summary>
        /// Gets the value from the WMAppManifest in runtime
        /// Example: PhoneHelper.GetAppAttribute("Title");
        /// 
        /// http://stackoverflow.com/questions/3411377/get-the-windows-phone-7-application-title-from-code
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public static string GetAppAttribute(string attributeName)
        {
            
                return "";
        }

        public static string GetDeviceName()
        {
            return "pc";
        }

        public static string GetDeviceFullName()
        {
            return GetDeviceName();
        }

        public static string GetDeviceUniqueId()
        {
            return "asdkjasdbasdb";
        }

        public static string GetDeviceHardwareVersion()
        {
            return "asdasd";
        }

        public static string GetDeviceFirmwareVersion()
        {
            return "asdasdasd";
        }
    }
}
