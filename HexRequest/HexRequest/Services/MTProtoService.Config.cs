﻿using System;
using Telegram.Api.TL;
using Telegram.Api.Transport;

namespace Telegram.Api.Services
{
    public partial class MTProtoService
    {
        public void SaveConfig()
        {
            _cacheService.SetConfig(_config);
        }

        public void LoadConfig(TcpTransport transport)
        {
            _activeTransport = new TcpTransport(transport.Host, transport.Port);
            _activeTransport.AuthKey = transport.AuthKey;
            _activeTransport.Salt = transport.Salt;
            _activeTransport.DCId = transport.DCId;
        }
        public TLConfig LoadConfig()
        {
            throw new NotImplementedException();
        }
    }
}
