﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading;
using Caliburn.Micro;
using Telegram.Api.Extensions;
using Telegram.Api.TL;

namespace Telegram.Api.Services.FileManager
{
    public class DocumentFileManager : IDocumentFileManager
    {
        private readonly object _randomRoot = new object();

        private readonly Random _random = new Random();

        private readonly List<Worker> _workers = new List<Worker>(Constants.WorkersNumber);

        private readonly object _itemsSyncRoot = new object();

        private readonly List<DownloadableItem> _items = new List<DownloadableItem>();

        private readonly IEventAggregator _eventAggregator;

        private readonly IMTProtoService _mtProtoService;

        public DocumentFileManager(IEventAggregator eventAggregator, IMTProtoService mtProtoService)
        {
            _eventAggregator = eventAggregator;
            _mtProtoService = mtProtoService;


            var timer = Stopwatch.StartNew();
            for (int i = 0; i < Constants.VideoDownloadersCount; i++)
            {
                var worker = new Worker(OnDownloading, "documentDownloader" + i);
                _workers.Add(worker);
            }

            TLUtils.WritePerformance("Start workers timer: " + timer.Elapsed);

        }

        private void OnDownloading(object state)
        {
            DownloadablePart part = null;
            lock (_itemsSyncRoot)
            {
                for (var i = 0; i < _items.Count; i++)
                {
                    var item = _items[i];
                    if (item.Canceled)
                    {
                        _items.RemoveAt(i--);
                        try
                        {
                            //_eventAggregator.Publish(new UploadingCanceledEventArgs(item));
                        }
                        catch (Exception e)
                        {
                            TLUtils.WriteException(e);
                        }
                    }
                }

                foreach (var item in _items)
                {
                    part = item.Parts.FirstOrDefault(x => x.Status == PartStatus.Ready);
                    if (part != null)
                    {
                        part.Status = PartStatus.Processing;
                        break;
                    }
                }
            }

            if (part == null)
            {
                var currentWorker = (Worker)state;
                currentWorker.Stop();
                return;
            }

            var partName = string.Format("document{0}_{1}_{2}.dat", part.ParentItem.InputDocumentLocation.Id, part.ParentItem.InputDocumentLocation.AccessHash, part.Number);
            
            part.File = GetFile(part.ParentItem.DCId, part.ParentItem.InputDocumentLocation, part.Offset, part.Limit);
            while (part.File == null)
            {
                part.File = GetFile(part.ParentItem.DCId, part.ParentItem.InputDocumentLocation, part.Offset, part.Limit);
            }

            part.Status = PartStatus.Processed;

            if (part.Offset.Value == 0)
            {
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists(partName))
                    {
                        store.DeleteFile(partName);
                    }
                }
            }

            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                if (store.FileExists(partName))
                {
                    store.DeleteFile(partName);
                }

                using (var stream = new IsolatedStorageFileStream(partName, FileMode.OpenOrCreate, FileAccess.Write, store))
                {
                    var data = part.File.Bytes.Data;
                    part.File.Bytes = new TLString();
                    stream.Position = stream.Length;
                    stream.Write(data, 0, data.Length);

                    if (data.Length < part.Limit.Value && (part.Number + 1) != part.ParentItem.Parts.Count)
                    {
                        lock (_itemsSyncRoot)
                        {
                            var complete = part.ParentItem.Parts.All(x => x.Status == PartStatus.Processed);
                            if (!complete)
                            {
                                var emptyBufferSize = part.Limit.Value - data.Length;
                                var position = stream.Position;

                                var missingPart = new DownloadablePart(part.ParentItem, new TLInt((int)position), new TLInt(emptyBufferSize), -part.Number);

                                var currentItemIndex = part.ParentItem.Parts.IndexOf(part);
                                part.ParentItem.Parts.Insert(currentItemIndex + 1, missingPart);
                            }
                        }
                    }
                }
            }

            // indicate progress
            // indicate complete
            bool isComplete;
            bool isCanceled;
            var progress = 0.0;
            lock (_itemsSyncRoot)
            {
                part.Status = PartStatus.Processed;
                isCanceled = part.ParentItem.Canceled;

                isComplete = part.ParentItem.Parts.All(x => x.Status == PartStatus.Processed);
                if (!isComplete)
                {
                    var downloadedCount = part.ParentItem.Parts.Count(x => x.Status == PartStatus.Processed);
                    var count = part.ParentItem.Parts.Count;
                    progress = downloadedCount / (double)count;
                }
                else
                {
                    _items.Remove(part.ParentItem);
                }
            }

            if (!isCanceled)
            {
                if (isComplete)
                {
                    var fileExtension = Path.GetExtension(part.ParentItem.FileName.ToString());
                    var fileName = string.Format("document{0}_{1}{2}", part.ParentItem.InputDocumentLocation.Id, part.ParentItem.InputDocumentLocation.AccessHash, fileExtension);

                    MergePartsToFile(part.ParentItem.Parts, fileName);

                    part.ParentItem.IsoFileName = fileName;
                    _eventAggregator.Publish(part.ParentItem);
                }
                else
                {
                    _eventAggregator.Publish(new ProgressChangedEventArgs(part.ParentItem, progress));
                }
            }
        }

        private static void MergePartsToFile(IEnumerable<DownloadablePart> parts, string fileName)
        {
            //TLUtils.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " mergeDocParts " + fileName, LogSeverity.Error);
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                try
                {
                    using (var stream = store.OpenFile(fileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
                    {
                        foreach (var part in parts)
                        {
                            var partFileName = string.Format("document{0}_{1}_{2}.dat",
                                part.ParentItem.InputDocumentLocation.Id,
                                part.ParentItem.InputDocumentLocation.AccessHash,
                                part.Number);
                            try
                            {
                                using (var partStream = store.OpenFile(partFileName, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
                                {
                                    var bytes = new byte[partStream.Length];
                                    partStream.Read(bytes, 0, bytes.Length);
                                    stream.Position = stream.Length;
                                    stream.Write(bytes, 0, bytes.Length);
                                }
                                store.DeleteFile(partFileName);
                            }
                            catch (Exception ex)
                            {
                                Helpers.Execute.ShowDebugMessage(string.Format("part {0} ex\n", partFileName) + ex);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helpers.Execute.ShowDebugMessage(string.Format("doc {0} ex\n", fileName) + ex);
                }
            }
        }

        private TLFile GetFile(TLInt dcId, TLInputDocumentFileLocation location, TLInt offset, TLInt limit)
        {
            var manualResetEvent = new ManualResetEvent(false);
            TLFile result = null;

            _mtProtoService.GetFileAsync(dcId, location, offset, limit,
                file =>
                {
                    result = file;
                    manualResetEvent.Set();
                },
                error =>
                {
                    int delay;
                    lock (_randomRoot)
                    {
                        delay = _random.Next(1000, 3000);
                    }
                    Thread.Sleep(delay);
                    manualResetEvent.Set();
                });

            manualResetEvent.WaitOne();
            return result;
        }


        public void DownloadFileAsync(TLString originalFileName, TLInt dcId, TLInputDocumentFileLocation fileLocation, TLObject owner, TLInt fileSize, Action<double> callback)
        {
            Helpers.Execute.BeginOnThreadPool(() =>
            {
                var downloadableItem = GetDownloadableItem(originalFileName, dcId, fileLocation, owner, fileSize);

                var downloadedCount = downloadableItem.Parts.Count(x => x.Status == PartStatus.Processed);
                var count = downloadableItem.Parts.Count;
                var isComplete = downloadedCount == count;

                if (isComplete)
                {
                    var fileExtension = Path.GetExtension(downloadableItem.FileName.ToString());
                    var fileName = string.Format("document{0}_{1}{2}", downloadableItem.InputDocumentLocation.Id, downloadableItem.InputDocumentLocation.AccessHash, fileExtension);

                    MergePartsToFile(downloadableItem.Parts, fileName);

                    downloadableItem.IsoFileName = fileName;
                    _eventAggregator.Publish(downloadableItem);
                }
                else
                {
                    var progress = downloadedCount/(double) count;
                    callback.SafeInvoke(progress);

                    lock (_itemsSyncRoot)
                    {
                        bool addFile = true;
                        foreach (var item in _items)
                        {
                            if (item.InputDocumentLocation.AccessHash.Value == fileLocation.AccessHash.Value
                                && item.InputDocumentLocation.Id.Value == fileLocation.Id.Value
                                && item.Owner == owner)
                            {
                                addFile = false;
                                break;
                            }
                        }

                        if (addFile)
                        {
                            _items.Add(downloadableItem);
                        }
                    }

                    StartAwaitingWorkers();
                }
            });
        }

        private void StartAwaitingWorkers()
        {
        }

        private DownloadableItem GetDownloadableItem(TLString fileName, TLInt dcId, TLInputDocumentFileLocation location, TLObject owner, TLInt fileSize)
        {
            var item = new DownloadableItem
            {
                DCId = dcId,
                FileName = fileName,
                Owner = owner,
                InputDocumentLocation = location
            };
            item.Parts = GetItemParts(fileSize, item);

            return item;
        }

        private List<DownloadablePart> GetItemParts(TLInt size, DownloadableItem item)
        {
            const int chunkSize = 30 * 1024; // 30Kb, должен быть кратен 1 Kb
            var parts = new List<DownloadablePart>();
            var partsCount = size.Value / chunkSize + (size.Value % chunkSize > 0 ? 1 : 0);

            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                for (var i = 0; i < partsCount; i++)
                {
                    var part = new DownloadablePart(item, new TLInt(i * chunkSize), size.Value == 0 ? new TLInt(0) : new TLInt(chunkSize), i);
                    var partName = string.Format("document{0}_{1}_{2}.dat", item.InputDocumentLocation.Id, item.InputDocumentLocation.AccessHash, part.Number);

                    if (store.FileExists(partName))
                    {
                        using (var file = store.OpenFile(partName, FileMode.Open, FileAccess.Read))
                        {
                            var isCompletePart = (part.Number + 1 == partsCount) || file.Length == part.Limit.Value;
                            part.Status = isCompletePart ? PartStatus.Processed : PartStatus.Ready;
                        }
                    }

                    parts.Add(part);
                }
            }

            return parts;
        }

        public void CancelDownloadFileAsync(TLObject owner)
        {
            Helpers.Execute.BeginOnThreadPool(() =>
            {
                lock (_itemsSyncRoot)
                {
                    var items = _items.Where(x => x.Owner == owner);

                    foreach (var item in items)
                    {
                        item.Canceled = true;
                    }
                }
            });
        }
    }
}
