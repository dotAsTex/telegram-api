﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading;
using Telegram.Api.TL;

namespace Telegram.Api.Services.FileManager
{
    public class AudioFileManager : IAudioFileManager
    {
        private readonly object _randomRoot = new object();

        private readonly Random _random = new Random();

        private readonly List<Worker> _workers = new List<Worker>(Constants.WorkersNumber);

        private readonly object _itemsSyncRoot = new object();

        private readonly List<DownloadableItem> _items = new List<DownloadableItem>();


        private readonly IMTProtoService _mtProtoService;

        public AudioFileManager(IMTProtoService mtProtoService)
        {
            _mtProtoService = mtProtoService;


            var timer = Stopwatch.StartNew();
            for (int i = 0; i < Constants.AudioDownloadersCount; i++)
            {
                var worker = new Worker(OnDownloading, "audioDownloader" + i);
                _workers.Add(worker);
            }

            TLUtils.WritePerformance("Start workers timer: " + timer.Elapsed);

        }

        private void OnDownloading(object state)
        {
            DownloadablePart part = null;
            lock (_itemsSyncRoot)
            {
                for (var i = 0; i < _items.Count; i++)
                {
                    var item = _items[i];
                    if (item.Canceled)
                    {
                        _items.RemoveAt(i--);
                        try
                        {
                            //_eventAggregator.Publish(new UploadingCanceledEventArgs(item));
                        }
                        catch (Exception e)
                        {
                            TLUtils.WriteException(e);
                        }
                    }
                }

                foreach (var item in _items)
                {
                    part = item.Parts.FirstOrDefault(x => x.Status == PartStatus.Ready);
                    if (part != null)
                    {
                        part.Status = PartStatus.Processing;
                        break;
                    }
                }
            }

            if (part == null)
            {
                var currentWorker = (Worker)state;
                currentWorker.Stop();
                return;
            }

            var partName = string.Format("audio{0}_{1}_{2}.dat", part.ParentItem.InputAudioLocation.Id, part.ParentItem.InputAudioLocation.AccessHash, part.Number);
            var partExists = false;
            var partLength = 0;
            var isLastPart = part.Number + 1 == part.ParentItem.Parts.Count;
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                partExists = store.FileExists(partName);
                if (partExists)
                {
                    using (var file = store.OpenFile(partName, FileMode.Open, FileAccess.Read))
                    {
                        partLength = (int)file.Length;
                    }
                }
            }
            var isCorrectPartLength = isLastPart || partLength == part.Limit.Value;

            if (!partExists || !isCorrectPartLength)
            {
                part.File = GetFile(part.ParentItem.DCId, part.ParentItem.InputAudioLocation, part.Offset, part.Limit);
                while (part.File == null)
                {
                    part.File = GetFile(part.ParentItem.DCId, part.ParentItem.InputAudioLocation, part.Offset, part.Limit);
                }

                part.Status = PartStatus.Processed;

                if (part.Offset.Value == 0)
                {
                    using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        if (store.FileExists(partName))
                        {
                            store.DeleteFile(partName);
                        }
                    }
                }

                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists(partName))
                    {
                        store.DeleteFile(partName);
                    }

                    using (var stream = new IsolatedStorageFileStream(partName, FileMode.OpenOrCreate, FileAccess.Write, store))
                    {
                        var data = part.File.Bytes.Data;
                        part.File.Bytes = new TLString();
                        stream.Position = stream.Length;
                        stream.Write(data, 0, data.Length);

                        if (data.Length < part.Limit.Value && (part.Number + 1) != part.ParentItem.Parts.Count)
                        {
                            lock (_itemsSyncRoot)
                            {
                                var complete = part.ParentItem.Parts.All(x => x.Status == PartStatus.Processed);
                                if (!complete)
                                {
                                    var emptyBufferSize = part.Limit.Value - data.Length;
                                    var position = stream.Position;

                                    var missingPart = new DownloadablePart(part.ParentItem, new TLInt((int)position), new TLInt(emptyBufferSize), -part.Number);

                                    var currentItemIndex = part.ParentItem.Parts.IndexOf(part);
                                    part.ParentItem.Parts.Insert(currentItemIndex + 1, missingPart);
                                }
                            }
                        }
                    }
                }
            }

            // indicate progress
            // indicate complete
            bool isComplete;
            bool isCanceled;
            var progress = 0.0;
            lock (_itemsSyncRoot)
            {
                part.Status = PartStatus.Processed;
                isCanceled = part.ParentItem.Canceled;

                isComplete = part.ParentItem.Parts.All(x => x.Status == PartStatus.Processed);
                if (!isComplete)
                {
                    var downloadedCount = part.ParentItem.Parts.Count(x => x.Status == PartStatus.Processed);
                    var count = part.ParentItem.Parts.Count;
                    progress = downloadedCount / (double)count;
                }
                else
                {
                    _items.Remove(part.ParentItem);
                }
            }

            if (!isCanceled)
            {
                if (isComplete)
                {
                    var fileName = string.Format("audio{0}_{1}.mp3", part.ParentItem.InputAudioLocation.Id, part.ParentItem.InputAudioLocation.AccessHash);

                    MergePartsToFile(part.ParentItem.Parts, fileName);

                    part.ParentItem.IsoFileName = fileName;
                }
                else
                {
                }
            }
        }

        private TLFile GetFile(TLInt dcId, TLInputAudioFileLocation location, TLInt offset, TLInt limit)
        {
            var manualResetEvent = new ManualResetEvent(false);
            TLFile result = null;

            _mtProtoService.GetFileAsync(dcId, location, offset, limit,
                file =>
                {
                    result = file;
                    manualResetEvent.Set();
                },
                error =>
                {
                    int delay;
                    lock (_randomRoot)
                    {
                        delay = _random.Next(1000, 3000);
                    }
                    Thread.Sleep(delay);
                    manualResetEvent.Set();
                });

            manualResetEvent.WaitOne();
            return result;
        }

        private static void MergePartsToFile(IEnumerable<DownloadablePart> parts, string fileName)
        {
            using (var store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var stream = new IsolatedStorageFileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write, store))
                {
                    foreach (var part in parts)
                    {
                        var partFileName = string.Format("audio{0}_{1}_{2}.dat", part.ParentItem.InputAudioLocation.Id, part.ParentItem.InputAudioLocation.AccessHash, part.Number);
                        using (var partStream = new IsolatedStorageFileStream(partFileName, FileMode.OpenOrCreate, FileAccess.Read, store))
                        {
                            var bytes = new byte[partStream.Length];
                            partStream.Read(bytes, 0, bytes.Length);
                            stream.Position = stream.Length;
                            stream.Write(bytes, 0, bytes.Length);
                        }
                        store.DeleteFile(partFileName);
                    }
                }
            }
        }


        public void DownloadFile(TLInt dcId, TLInputAudioFileLocation fileLocation, TLObject owner, TLInt fileSize)
        {
            var downloadableItem = GetDownloadableItem(dcId, fileLocation, owner, fileSize);

            var downloadedCount = downloadableItem.Parts.Count(x => x.Status == PartStatus.Processed);
            var count = downloadableItem.Parts.Count;
            var isComplete = downloadedCount == count;

            if (isComplete)
            {
                var fileName = string.Format("audio{0}_{1}.mp3", downloadableItem.InputAudioLocation.Id, downloadableItem.InputAudioLocation.AccessHash);

                MergePartsToFile(downloadableItem.Parts, fileName);

                downloadableItem.IsoFileName = fileName;
            }
            else
            {
                lock (_itemsSyncRoot)
                {
                    bool addFile = true;
                    foreach (var item in _items)
                    {
                        if (item.InputAudioLocation.AccessHash.Value == fileLocation.AccessHash.Value
                            && item.InputAudioLocation.Id.Value == fileLocation.Id.Value
                            && item.Owner == owner)
                        {
                            addFile = false;
                            break;
                        }
                    }

                    if (addFile)
                    {
                        _items.Add(downloadableItem);
                    }
                }

                StartAwaitingWorkers();
            }
        }

        private void StartAwaitingWorkers()
        {
            var awaitingWorkers = _workers.Where(x => x.ThreadState == System.Threading.ThreadState.WaitSleepJoin);
            foreach (var awaitingWorker in awaitingWorkers)
            {
                awaitingWorker.Start();
            }
        }

        private DownloadableItem GetDownloadableItem(TLInt dcId, TLInputAudioFileLocation location, TLObject owner, TLInt fileSize)
        {
            var item = new DownloadableItem
            {
                Owner = owner,
                DCId = dcId,
                InputAudioLocation = location
            };
            item.Parts = GetItemParts(fileSize, item);

            return item;
        }

        private List<DownloadablePart> GetItemParts(TLInt size, DownloadableItem item)
        {
            const int chunkSize = 30 * 1024; // 30Kb, должен быть кратен 1 Kb
            var parts = new List<DownloadablePart>();
            var partsCount = size.Value / chunkSize + (size.Value % chunkSize > 0 ? 1 : 0);
            for (var i = 0; i < partsCount; i++)
            {
                var part = new DownloadablePart(item, new TLInt(i * chunkSize), size.Value == 0 ? new TLInt(0) : new TLInt(chunkSize), i);
                parts.Add(part);
            }

            return parts;
        }

        public void CancelDownloadFile(TLObject owner)
        {
            lock (_itemsSyncRoot)
            {
                var items = _items.Where(x => x.Owner == owner);

                foreach (var item in items)
                {
                    item.Canceled = true;
                }
            }
        }
    }
}
