﻿using Telegram.Api.TL;

namespace Telegram.Api.Services.Cache.EventArgs
{
    public class MessageRemovedEventArgs
    {
        public TLDialogBase Dialog { get; protected set; }

        public TLMessageBase Message { get; protected set; }

        public TLDecryptedMessageBase DecryptedMessage { get; protected set; }

        public MessageRemovedEventArgs(TLDialogBase dialog, TLMessageBase message)
        {
            Dialog = dialog;
            Message = message;
        }

        public MessageRemovedEventArgs(TLDialogBase dialog, TLDecryptedMessageBase message)
        {
            Dialog = dialog;
            DecryptedMessage = message;
        }
    }

    public class DialogAddedEventArgs
    {
        public TLDialogBase Dialog { get; protected set; }

        public DialogAddedEventArgs(TLDialogBase dialog)
        {
            Dialog = dialog;
        }
    }

    public class DialogRemovedEventArgs
    {
        public TLDialogBase Dialog { get; protected set; }

        public DialogRemovedEventArgs(TLDialogBase dialog)
        {
            Dialog = dialog;
        }
    }
}