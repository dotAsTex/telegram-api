﻿using System;
using System.Collections.Generic;
using Telegram.Api.Services.Cache;
using Telegram.Api.TL;

namespace Telegram.Api.Services.Updates
{
    public delegate void GetDifferenceAction(TLInt pts, TLInt date, TLInt qts, Action<TLDifferenceBase> callback, Action<TLRPCError> faultCallback);
    public delegate void GetDHConfigAction(TLInt version, TLInt randomLength, Action<TLDHConfigBase> callback, Action<TLRPCError> faultCallback);
    public delegate void AcceptEncryptionAction(TLInputEncryptedChat peer, TLString gb, TLLong keyFingerprint, Action<TLEncryptedChatBase> callback, Action<TLRPCError> faultCallback);
    public delegate void SendEncryptedServiceAction(TLInputEncryptedChat peer, TLLong randomkId, TLString data, Action<TLSentEncryptedMessage> callback, Action<TLRPCError> faultCallback);

    public interface IUpdatesService
    {
        IList<ExceptionInfo> SyncDifferenceExceptions { get; }
        //void IncrementClientSeq();

        Func<TLInt> GetCurrentUserId { get; set; }

        Action<Action<TLState>, Action<TLRPCError>> GetStateAsync { get; set; }

        GetDHConfigAction GetDHConfigAsync { get; set; }
        GetDifferenceAction GetDifferenceAsync { get; set; }
        AcceptEncryptionAction AcceptEncryptionAsync { get; set; }
        SendEncryptedServiceAction SendEncryptedServiceAsync { get; set; }

        void SetInitState();

        TLInt ClientSeq { get; }
        void SetState(TLInt seq, TLInt pts, TLInt qts, TLInt date, TLInt unreadCount, string caption, bool cleanupMissingSeq = false);
        void ProcessTransportMessage(TLTransportMessage transportMessage);
        
        void LoadStateAndUpdate(Action callback);
        void SaveState();
        void ClearState();

        event EventHandler<DCOptionsUpdatedEventArgs> DCOptionsUpdated;
    }
}
