﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto.Engines;
#if DEBUG
using System.Windows;
#endif
using Caliburn.Micro;
using Telegram.Api.Extensions;
using Telegram.Api.Services.Cache;
using Telegram.Api.TL; 

namespace Telegram.Api.Services.Updates
{
    [Serializable]
    public class UpdatesService : IUpdatesService
    {
        public TLUserBase CurrentUser { get; set; }

        private readonly ICacheService _cacheService;

        private readonly IEventAggregator _eventAggregator;

        public Func<TLInt> GetCurrentUserId { get; set; }

        public Action<Action<TLState>, Action<TLRPCError>> GetStateAsync { get; set; }
        public GetDHConfigAction GetDHConfigAsync { get; set; }
        public GetDifferenceAction GetDifferenceAsync { get; set; }
        public AcceptEncryptionAction AcceptEncryptionAsync { get; set; }
        public SendEncryptedServiceAction SendEncryptedServiceAsync { get; set; }

        private readonly Timer _lostSeqTimer; 

        public UpdatesService(ICacheService cacheService, IEventAggregator eventAggregator)
        {
            _lostSeqTimer = new Timer(OnCheckLostSeq, this, Timeout.Infinite, Timeout.Infinite);

            _cacheService = cacheService;
            _eventAggregator = eventAggregator;
        }

        private void StartLostSeqTimer()
        {
            _lostSeqTimer.Change(TimeSpan.FromSeconds(1.0), TimeSpan.FromSeconds(1.0));
            TLUtils.WriteLine(DateTime.Now.ToString("  HH:mm:ss.fff", CultureInfo.InvariantCulture) + " Start lostSeqTimer", LogSeverity.Error);

        }

        private void StopLostSeqTimer()
        {
            _lostSeqTimer.Change(Timeout.Infinite, Timeout.Infinite);
            TLUtils.WriteLine(DateTime.Now.ToString("  HH:mm:ss.fff", CultureInfo.InvariantCulture) + " Stop lostSeqTimer", LogSeverity.Error);
        }

        private void OnCheckLostSeq(object state)
        {
            TLUtils.WriteLine(DateTime.Now.ToString("  HH:mm:ss.fff", CultureInfo.InvariantCulture) + " OnCheck lostSeqTimer", LogSeverity.Error);
            var getDifference = false;
            var isLostSeqEmpty = true;
            var keyValuePair = default(KeyValuePair<int, WindowsPhone.Tuple<DateTime, TLState>>);
            lock (_clientSeqLock)
            {
                foreach (var keyValue in _lostSeq.OrderBy(x => x.Key))
                {
                    isLostSeqEmpty = false;
                    if (DateTime.Now > keyValue.Value.Item1.AddSeconds(3.0))
                    {
                        getDifference = true;
                        keyValuePair = keyValue;
                        break;
                    }
                }
            }

            if (isLostSeqEmpty)
            {
                StopLostSeqTimer();
            }

            if (getDifference)
            {
                var seq = keyValuePair.Key;
                var pts = keyValuePair.Value.Item2.Pts;
                var date = keyValuePair.Value.Item2.Date;
                var qts = keyValuePair.Value.Item2.Qts;

                Helpers.Execute.ShowDebugMessage(string.Format("lostSeqTimer.OnCheckLostSeq From=[seq={0}, pts={1}, date={2}, qts={3}] Current=[seq={4}, pts={5}, date={6}, qts={7}]", seq, pts, date, qts, ClientSeq, _pts, _date, _qts));
                StopLostSeqTimer();
                //GetDifference(() =>
                //{

                //});
            }
        }

        public TLInt ClientSeq { get; protected set; }

        private TLInt _date;

        private TLInt _pts;

        private TLInt _qts = new TLInt(1);

        private TLInt _unreadCount;

        public void SetState(TLInt seq, TLInt pts, TLInt qts, TLInt date, TLInt unreadCount, string caption, bool cleanupMissingSeq = false)
        {
            TLUtils.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " " + caption + " clientSeq=" + ClientSeq + " newSeq=" + seq, LogSeverity.Error);

            if (seq != null)
            {
                UpdateLostSeq(new List<TLInt>{seq}, cleanupMissingSeq);
            }
            
            _date = date ?? _date;
            _pts = pts ?? _pts;
            _qts = qts ?? _qts;
            _unreadCount = unreadCount ?? _unreadCount;
        }

        public void SetState(TLState state, string caption)
        {
            if (state == null) return;

            SetState(state.Seq, state.Pts, state.Qts, state.Date, state.UnreadCount, caption, true);
        }

        public void SetInitState()
        {
            GetStateAsync.SafeInvoke(
                result => SetState(result, "setInitState"),
                error =>
                {
                    Thread.Sleep(5000);
                    SetInitState();
                });
        }

        private void GetDifference(System.Action callback)
        {
            if (_pts != null && _date != null && _qts != null)
            {
                GetDifference(_pts, _date, _qts, callback);
            }
            else
            {
                SetInitState();
                callback();
            }
        }

        private void GetDifference(TLInt pts, TLInt date, TLInt qts, System.Action callback)
        {
                TLUtils.WritePerformance(string.Format("UpdatesService.GetDifference(Pts={0} Date={1} Qts={2})", _pts, _date, _qts));
                GetDifferenceAsync(pts, date, qts, 
                    diff =>
                    {
                        var differenceEmpty = diff as TLDifferenceEmpty;
                        if (differenceEmpty != null)
                        {
                            _date = differenceEmpty.Date;
                            lock (_clientSeqLock)
                            {
                                ClientSeq = differenceEmpty.Seq;
                            }
                            TLUtils.WritePerformance("UpdateService.GetDifference empty result=" + differenceEmpty.Seq);
                            callback();
                            return;
                        }

                        var difference = diff as TLDifference;
                        if (difference != null)
                        {
                            Helpers.Execute.BeginOnThreadPool(() =>
                            {
                                _eventAggregator.Publish(new UpdatingEventArgs());
                                //var mtProtoService = StaticExchanger.IoC.mtProtoService;
                                //if (mtProtoService != null)
                                //{

                                //    mtProtoService.SetMessageOnTime(3.0, "Updating...");
                                //}
                            });

                            var resetEvent = new ManualResetEvent(false);

                            TLUtils.WritePerformance(string.Format("UpdateService.GetDifference result=[Pts={0} Date={1} Qts={2}]", difference.State.Pts, difference.State.Date, difference.State.Qts));
                            lock (_clientSeqLock)
                            {
                                SetState(difference.State, "processDiff");
                            }
                            ProcessDifference(difference, () => resetEvent.Set());

#if DEBUG
                            resetEvent.WaitOne();
#else
                            resetEvent.WaitOne(10000);
#endif

                        }

                        var differenceSlice = diff as TLDifferenceSlice;
                        if (differenceSlice != null)
                        {
                            GetDifference(callback);
                        }
                        else
                        {
                            Helpers.Execute.BeginOnThreadPool(() => _eventAggregator.Publish(new UpdateCompletedEventArgs()));
                            callback();
                        }
                    },
                    error =>
                    {
                        Thread.Sleep(5000);
                        GetDifference(callback);
                    });
        }

        private readonly List<ExceptionInfo> _syncDifferenceExceptions = new List<ExceptionInfo>();

        public IList<ExceptionInfo> SyncDifferenceExceptions
        {
            get { return _syncDifferenceExceptions; }
        }

        private void ProcessDifference(TLDifference difference, System.Action callback)
        {
            // сначала получаем апдейты, а только потом синхронизируем новые сообщения
            // т.к. апдейт о создании секретного чата нада обрабатывать раньше, чем новые сообщения в нем
            foreach (var update in difference.OtherUpdates)
            {
                try
                {
                    ProcessUpdate(update);
                }
                catch (Exception ex)
                {
                    _syncDifferenceExceptions.Add(new ExceptionInfo
                    {
                        Caption = "UpdatesService.ProcessDifference OtherUpdates",
                        Exception = ex,
                        Timestamp = DateTime.Now
                    });

                    TLUtils.WriteException("UpdatesService.ProcessDifference OtherUpdates ex ", ex);
                }
            }

            _cacheService.SyncDifference(difference, 
                result =>
                {
                    callback.SafeInvoke();
                },
                _syncDifferenceExceptions);
        }
        public UpdatesService()
        {

        }
        private bool ProcessUpdates(TLUpdatesBase updates)
        {
            //ClientSeq = updates.GetSeq() ?? ClientSeq;

            // chat message
            var shortChatMessage = updates as TLUpdatesShortChatMessage;
            if (shortChatMessage != null)
            {
                _pts = shortChatMessage.Pts;
                //ClientSeq = shortChatMessage.Seq;

                // message from user that is missing on client
                if (_cacheService.GetUser(shortChatMessage.FromId) == null)
                {
                    return false;
                }

                var message = new TLMessage17
                {
                    Id = shortChatMessage.Id,
                    FromId = shortChatMessage.FromId,
                    ToId = new TLPeerChat{ Id = shortChatMessage.ChatId },
                    Out = new TLBool(false),
                    _date = shortChatMessage.Date,
                    Message = shortChatMessage.Message,
                    _media = new TLMessageMediaEmpty()
                };
                message.SetUnread(new TLBool(true));

                _cacheService.SyncMessage(message, new TLPeerChat { Id = shortChatMessage.ChatId }, cachedMessage => _eventAggregator.Publish(cachedMessage));
                return true;
            }

            // user message
            var shortMessage = updates as TLUpdatesShortMessage;
            if (shortMessage != null)
            {
                _pts = shortMessage.Pts;
                //ClientSeq = shortMessage.Seq;

                // message from user that is missing on client
                if (_cacheService.GetUser(shortMessage.FromId) == null)
                {
                    return false;
                }

                var message = new TLMessage17
                {
                    Id = shortMessage.Id,
                    FromId = shortMessage.FromId,
                    Out = new TLBool(false),
                    Date = shortMessage.Date,
                    Message = shortMessage.Message,
                    Media = new TLMessageMediaEmpty()
                };
                message.SetUnread(new TLBool(true));
                message.ToId = new TLPeerUser{ Id = GetCurrentUserId() };

                _cacheService.SyncMessage(message, new TLPeerUser { Id = shortMessage.FromId }, cachedMessage => _eventAggregator.Publish(cachedMessage));
                return true;
            }

            var @short = updates as TLUpdatesShort;
            if (@short != null)
            {
                _date = @short.Date;
                return ProcessUpdate(@short.Update);
            }

            var combined = updates as TLUpdatesCombined;
            if (combined != null)
            {
                var resetEvent = new ManualResetEvent(false);
                var returnValue = true;

                _cacheService.SyncUsersAndChats(combined.Users, combined.Chats,
                    result =>
                    {
                        _date = combined.Date;
                        //ClientSeq = combined.Seq;
                        foreach (var update in combined.Updates)
                        {
                            if (!ProcessUpdate(update))
                            {
                                returnValue = false;
                            }
                        }

                        resetEvent.Set();
                    });

                resetEvent.WaitOne(10000);

                return returnValue;
            }

            var updatesFull = updates as TLUpdates;
            if (updatesFull != null)
            {
                var resetEvent = new ManualResetEvent(false);
                var returnValue = true;

                _cacheService.SyncUsersAndChats(updatesFull.Users, updatesFull.Chats,
                    result =>
                    {
                        _date = updatesFull.Date;
                        //ClientSeq = updatesFull.Seq;
                        foreach (var update in updatesFull.Updates)
                        {
                            if (!ProcessUpdate(update))
                            {
                                returnValue = false;
                            }
                        }
                    });

                resetEvent.WaitOne(10000);
                return returnValue;
            }

            return false;
        }

        public event EventHandler<DCOptionsUpdatedEventArgs> DCOptionsUpdated;

        protected virtual void RaiseDCOptionsUpdated(DCOptionsUpdatedEventArgs e)
        {
            EventHandler<DCOptionsUpdatedEventArgs> handler = DCOptionsUpdated;
            if (handler != null) handler(this, e);
        }

        public static TLDecryptedMessageBase GetDecryptedMessage(TLEncryptedChat cachedChat, TLEncryptedMessageBase encryptedMessageBase, TLInt qts)
        {
            if (cachedChat == null) return null;
            if (cachedChat.Key == null) return null;

            TLDecryptedMessageBase decryptedMessage = null;
            try
            {
                decryptedMessage = TLUtils.DecryptMessage(encryptedMessageBase.Bytes, cachedChat);
            }
            catch (Exception e)
            {
#if DEBUG
                TLUtils.WriteException(e);
#endif
            }

            if (decryptedMessage == null) return null;

            var currentUserId = StaticExchanger.IoC.mtProtoService.CurrentUserId;
            var participantId = currentUserId.Value == cachedChat.ParticipantId.Value
                ? cachedChat.AdminId
                : cachedChat.ParticipantId;
            var cachedUser = StaticExchanger.IoC.cacheService.GetUser(participantId);
            if (cachedUser == null) return null;

            decryptedMessage.FromId = cachedUser.Id;
            decryptedMessage.Out = new TLBool(false);
            decryptedMessage.Unread = new TLBool(true);
            decryptedMessage.RandomId = encryptedMessageBase.RandomId;
            decryptedMessage.ChatId = encryptedMessageBase.ChatId;
            decryptedMessage.Date = encryptedMessageBase.Date;
            decryptedMessage.Qts = qts;

            var message = decryptedMessage as TLDecryptedMessage;
            if (message != null)
            {
                var encryptedMessage = encryptedMessageBase as TLEncryptedMessage;
                if (encryptedMessage != null)
                {
                    message.Media.File = encryptedMessage.File;
                    var document = message.Media as TLDecryptedMessageMediaDocument;
                    if (document != null)
                    {
                        var file = document.File as TLEncryptedFile;
                        if (file != null)
                        {
                            file.FileName = document.FileName;
                        }
                    }

                    var video = message.Media as TLDecryptedMessageMediaVideo;
                    if (video != null)
                    {
                        var file = video.File as TLEncryptedFile;
                        if (file != null)
                        {
                            file.Duration = video.Duration;
                        }
                    }

                    var audio = message.Media as TLDecryptedMessageMediaAudio;
                    if (audio != null)
                    {
                        audio.UserId = decryptedMessage.FromId;
                    }
                }
            }

            return decryptedMessage;
        }

        private bool ProcessUpdate(TLUpdateBase update)
        {
            var updatePrivacy = update as TLUpdatePrivacy;
            if (updatePrivacy != null)
            {
                Helpers.Execute.BeginOnThreadPool(() => _eventAggregator.Publish(updatePrivacy));
            }

            var updateUserBlocked = update as TLUpdateUserBlocked;
            if (updateUserBlocked != null)
            {
                Helpers.Execute.BeginOnThreadPool(() => _eventAggregator.Publish(updateUserBlocked));

                return true;
            }

            var updateEncryptedChatTyping = update as TLUpdateEncryptedChatTyping;
            if (updateEncryptedChatTyping != null)
            {
                _eventAggregator.Publish(updateEncryptedChatTyping);
            }

            var updateEncryptedMessagesRead = update as TLUpdateEncryptedMessagesRead;
            if (updateEncryptedMessagesRead != null)
            {
                var encryptedChat = _cacheService.GetEncryptedChat(updateEncryptedMessagesRead.ChatId) as TLEncryptedChat;

                if (encryptedChat != null)
                {
                    var items = _cacheService.GetDecryptedHistory(encryptedChat.Id.Value, 100);
                    for (var i = 0; i < items.Count; i++)
                    {
                        if (items[i].Status == MessageStatus.Confirmed)
                        //&& Items[i].Date.Value <= update.MaxDate.Value) // здесь надо учитывать смещение по времени
                        {
                            items[i].Status = MessageStatus.Read;
                            items[i].NotifyOfPropertyChange("Status");

                            if (items[i].TTL != null && items[i].TTL.Value > 0)
                            {
#if SECRET_CHAT_17
                                var decryptedMessage = items[i] as TLDecryptedMessage17;
                                if (decryptedMessage != null)
                                {
                                    var decryptedPhoto = decryptedMessage.Media as TLDecryptedMessageMediaPhoto;
                                    if (decryptedPhoto != null && items[i].TTL.Value <= 60.0)
                                    {
                                        continue;
                                    }

                                    var decryptedVideo17 = decryptedMessage.Media as TLDecryptedMessageMediaVideo17;
                                    if (decryptedVideo17 != null && items[i].TTL.Value <= 60.0)
                                    {
                                        continue;
                                    }

                                    var decryptedAudio17 = decryptedMessage.Media as TLDecryptedMessageMediaAudio17;
                                    if (decryptedAudio17 != null && items[i].TTL.Value <= 60.0)
                                    {
                                        continue;
                                    }
                                }
#endif

                                items[i].DeleteDate = new TLLong(DateTime.Now.Ticks + encryptedChat.MessageTTL.Value * TimeSpan.TicksPerSecond);
                            }
                        }
                        else if (items[i].Status == MessageStatus.Read)
                        {
                            break;
                        }
                    }

                    var dialog = _cacheService.GetEncryptedDialog(encryptedChat.Id) as TLEncryptedDialog;
                    if (dialog != null)
                    {
                        //dialog.UnreadCount = new TLInt(dialog.UnreadCount.Value - 1);
                        var topMessage = dialog.TopMessage;
                        if (topMessage != null)
                        {
                            dialog.NotifyOfPropertyChange(() => dialog.TopMessage);
                        }
                    }
                }

                _eventAggregator.Publish(updateEncryptedMessagesRead);

                return true;
            }

            var updateNewEncryptedMessage = update as TLUpdateNewEncryptedMessage;
            if (updateNewEncryptedMessage != null)
            {
                var encryptedMessageBase = updateNewEncryptedMessage.Message;
                if (encryptedMessageBase != null)
                {
                    var encryptedChat = _cacheService.GetEncryptedChat(encryptedMessageBase.ChatId) as TLEncryptedChat;
                    if (encryptedChat == null) return true;

                    TLDecryptedMessageBase decryptedMessage = null;
                    try
                    {
                        decryptedMessage = GetDecryptedMessage(encryptedChat, encryptedMessageBase, updateNewEncryptedMessage.Qts);
                    }
                    catch (Exception ex)
                    {
                        Helpers.Execute.ShowDebugMessage("ProcessUpdate(TLUpdateNewEncryptedMessage) ex " + ex);
                    }

                    if (decryptedMessage == null) return true;

#if SECRET_CHAT_17
                    var hasMessagesGap = true;
                    var decryptedMessage17 = decryptedMessage as ISeqNo;
                    var decryptedMessageService = decryptedMessage as TLDecryptedMessageService;
                    var encryptedChat17 = encryptedChat as TLEncryptedChat17;
                    var encryptedChat8 = encryptedChat; 
                    if (decryptedMessage17 != null)
                    {
                        // если чат уже обновлен до нового слоя, то проверяем rawInSeqNo
                        if (encryptedChat17 != null)
                        {
                            var chatRawInSeqNo = encryptedChat17.RawInSeqNo.Value;
                            var messageRawInSeqNo = GetRawInFromReceivedMessage(StaticExchanger.IoC.mtProtoService.CurrentUserId, encryptedChat17, decryptedMessage17);

                            if (messageRawInSeqNo == chatRawInSeqNo)
                            {
                                hasMessagesGap = false;
                                encryptedChat17.RawInSeqNo = new TLInt(encryptedChat17.RawInSeqNo.Value + 1);
                                _cacheService.SyncEncryptedChat(encryptedChat17, result => { });
                            }
                            else
                            {
                                Helpers.Execute.ShowDebugMessage(string.Format("TLUpdateNewEncryptedMessage messageRawInSeqNo != chatRawInSeqNo + 1 chatId={0} chatRawInSeqNo={1} messageRawInSeqNo={2}", encryptedChat17.Id, chatRawInSeqNo, messageRawInSeqNo));
                            }
                        }
                        // обновляем до нового слоя при получении любого сообщения с более высоким слоем
                        else if (encryptedChat8 != null)
                        {
                            hasMessagesGap = false;

                            var newLayer = Constants.SecretSupportedLayer;
                            if (decryptedMessageService != null)
                            {
                                var actionNotifyLayer = decryptedMessageService.Action as TLDecryptedMessageActionNotifyLayer;
                                if (actionNotifyLayer != null)
                                {
                                    if (actionNotifyLayer.Layer.Value <= Constants.SecretSupportedLayer)
                                    {
                                        newLayer = actionNotifyLayer.Layer.Value;
                                    }
                                }
                            }

                            var layer = new TLInt(newLayer);
                            var rawInSeqNo = new TLInt(1);      // только что получил сообщение по новому слою
                            var rawOutSeqNo = new TLInt(0);

                            UpgradeSecretChatLayerAndSendNotification(encryptedChat8, layer, rawInSeqNo, rawOutSeqNo);
                        }
                    }
                    else if (decryptedMessageService != null)
                    {
                        hasMessagesGap = false;
                        var notifyLayerAction = decryptedMessageService.Action as TLDecryptedMessageActionNotifyLayer;
                        if (notifyLayerAction != null)
                        {
                            if (encryptedChat17 != null)
                            {
                                // пропукаем апдейт, т.к. уже обновились
                            }
                            else if (encryptedChat8 != null)
                            {
                                var newLayer = Constants.SecretSupportedLayer;
                                if (notifyLayerAction.Layer.Value <= Constants.SecretSupportedLayer)
                                {
                                    newLayer = notifyLayerAction.Layer.Value;
                                }

                                var layer = new TLInt(newLayer);
                                var rawInSeqNo = new TLInt(0);
                                var rawOutSewNo = new TLInt(0);

                                UpgradeSecretChatLayerAndSendNotification(encryptedChat8, layer, rawInSeqNo, rawOutSewNo);
                            }
                        }
                    }
                    else
                    {
                        hasMessagesGap = false;
                    }

                    if (hasMessagesGap)
                    {
                        Helpers.Execute.ShowDebugMessage("catch gap " + decryptedMessage);
                        //return true;
                    }

                    
#endif
                    var decryptedMessageService17 = decryptedMessage as TLDecryptedMessageService17;
                    if (decryptedMessageService17 != null)
                    {
                        var resendAction = decryptedMessageService17.Action as TLDecryptedMessageActionResend;
                        if (resendAction != null)
                        {
                            Helpers.Execute.ShowDebugMessage(string.Format("TLDecryptedMessageActionResend start_seq_no={0} end_seq_no={1}", resendAction.StartSeqNo, resendAction.EndSeqNo));
                        
                            //_cacheService.GetDecryptedHistory()
                        }
                    }

                    var syncMessageFlag = IsSyncRequierd(decryptedMessage);

                    _eventAggregator.Publish(decryptedMessage);

                    if (syncMessageFlag)
                    {
                        _cacheService.SyncDecryptedMessage(decryptedMessage, encryptedChat, cachedMessage =>
                        {
                            SetState(null, null, updateNewEncryptedMessage.Qts, null, null, "TLUpdateNewEncryptedMessage");
                        });
                    }
                    else
                    {
                        SetState(null, null, updateNewEncryptedMessage.Qts, null, null, "TLUpdateNewEncryptedMessage");
                    }

                    return true;
                }
            }

            var updateEncryption = update as TLUpdateEncryption;
            if (updateEncryption != null)
            {
                var chatRequested = updateEncryption.Chat as TLEncryptedChatRequested;

                if (chatRequested != null)
                {
                    _cacheService.SyncEncryptedChat(updateEncryption.Chat, result => _eventAggregator.Publish(result));

                    var message = new TLDecryptedMessageService
                    {
                        RandomId = TLLong.Random(),                        
                        RandomBytes = new TLString(""),
                        ChatId = chatRequested.Id,
                        Action = new TLDecryptedMessageActionEmpty(),
                        FromId = StaticExchanger.IoC.mtProtoService.CurrentUserId,
                        Date = chatRequested.Date,
                        Out = new TLBool(false),
                        Unread = new TLBool(false),
                        Status = MessageStatus.Read
                    };

                    _cacheService.SyncDecryptedMessage(message, chatRequested, result => { });

                    GetDHConfigAsync(new TLInt(0), new TLInt(0),
                        result =>
                        {
                            var dhConfig = (TLDHConfig) result;

                            //TODO: precalculate gb to improve speed
                            var bBytes = new byte[256];
                            var random = new Random();
                            random.NextBytes(bBytes);
                            //var b = TLString.FromBigEndianData(bBytes);
                            var p = dhConfig.P;
                            var g = dhConfig.G;

                            updateEncryption.Chat.P = p;
                            updateEncryption.Chat.G = g;

                            var gbBytes = MTProtoService.GetGA(bBytes, dhConfig.G, dhConfig.P);
                            var gb = TLString.FromBigEndianData(gbBytes);

                            var key = MTProtoService.GetAuthKey(bBytes, chatRequested.GA.ToBytes(), dhConfig.P.ToBytes());
                            var keyHash = new SHA1Managed().ComputeHash(key);
                            var keyFingerprint = new TLLong(BitConverter.ToInt64(keyHash, 12));

                            AcceptEncryptionAsync(
                                new TLInputEncryptedChat
                                {
                                    AccessHash = chatRequested.AccessHash,
                                    ChatId = chatRequested.Id
                                },
                                gb,
                                keyFingerprint,
                                chat =>
                                {
                                    chat.P = p;
                                    chat.G = g;
                                    chat.Key = TLString.FromBigEndianData(key);
                                    chat.KeyFingerprint = keyFingerprint;

                                    _cacheService.SyncEncryptedChat(chat, r2 => _eventAggregator.Publish(r2));
                                },
                                er =>
                                {
                                    Helpers.Execute.ShowDebugMessage("messages.acceptEncryption " + er);
                                });

                        },
                        error =>
                        {
                            Helpers.Execute.ShowDebugMessage("messages.getDhConfig error " + error);
                        });
                }

                var encryptedChat = updateEncryption.Chat as TLEncryptedChat;
                if (encryptedChat != null)
                {
                    var waitingChat = _cacheService.GetEncryptedChat(encryptedChat.Id) as TLEncryptedChatWaiting;

                    if (waitingChat != null)
                    {
                        var dialog = _cacheService.GetEncryptedDialog(encryptedChat.Id) as TLEncryptedDialog;
                        if (dialog != null)
                        {
                            var serviceMessage = dialog.TopMessage as TLDecryptedMessageService;
                            if (serviceMessage != null)
                            {
                                var action = serviceMessage.Action as TLDecryptedMessageActionEmpty;
                                if (action != null)
                                {
                                    serviceMessage.Unread = new TLBool(true);
                                    serviceMessage.Status = MessageStatus.Confirmed;
                                }
                            }
                        }


                        // уведомление о слое, если начали сами чат
#if SECRET_CHAT_17
                        if (Constants.SecretSupportedLayer >= 17)
                        {
                            var notifyLayerAction = new TLDecryptedMessageActionNotifyLayer();
                            notifyLayerAction.Layer = new TLInt(Constants.SecretSupportedLayer);

                            // уведомляем в старом слое, чтобы не сломать предыдущие версии клиентов
                            var decryptedService = new TLDecryptedMessageService
                            {
                                Action = notifyLayerAction,
                                RandomId = TLLong.Random(),
                                RandomBytes = TLString.Empty,

                                FromId = StaticExchanger.IoC.mtProtoService.CurrentUserId,
                                Out = new TLBool(true),
                                Unread = new TLBool(false),
                                Date = TLUtils.DateToUniversalTimeTLInt(0, DateTime.Now),
                                Status = MessageStatus.Confirmed,

                                ChatId = encryptedChat.Id
                            };

                            _cacheService.SyncEncryptedChat(encryptedChat,
                                syncedChat =>
                                {
                                    _cacheService.SyncDecryptedMessage(decryptedService, syncedChat,
                                        messageResult =>
                                        {
                                            SendEncryptedServiceAsync(
                                                new TLInputEncryptedChat
                                                {
                                                    AccessHash = encryptedChat.AccessHash,
                                                    ChatId = encryptedChat.Id
                                                },
                                                decryptedService.RandomId,
                                                TLUtils.EncryptMessage(decryptedService, (TLEncryptedChat) syncedChat),
                                                sentEncryptedMessage =>
                                                {

                                                },
                                                error =>
                                                {
                                                    Helpers.Execute.ShowDebugMessage("messages.sendEncryptedService error " + error);
                                                });
                                        });
                                });
                        }
#endif
                    }

                    _cacheService.SyncEncryptedChat(updateEncryption.Chat, 
                        r =>
                        {
                            _eventAggregator.Publish(r);
                        });
                }
                else
                {
                    _cacheService.SyncEncryptedChat(updateEncryption.Chat, 
                        r =>
                        {
                            _eventAggregator.Publish(r);
                        });
                }
                
                return true;
            }

            var updateDCOptions = update as TLUpdateDCOptions;
            if (updateDCOptions != null)
            {
                RaiseDCOptionsUpdated(new DCOptionsUpdatedEventArgs{ Update = updateDCOptions });

                return true;
            }

            var newMessage = update as TLUpdateNewMessage;
            if (newMessage != null)
            {

                _pts = newMessage.Pts;
                var commonMessage = newMessage.Message as TLMessageCommon;
                if (commonMessage != null)
                {
                    TLPeerBase peer;
                    if (commonMessage.ToId is TLPeerChat)
                    {
                        peer = commonMessage.ToId;
                    }
                    else
                    {
                        peer = commonMessage.Out.Value ? commonMessage.ToId : new TLPeerUser { Id = commonMessage.FromId };
                    }

                    _cacheService.SyncMessage(newMessage.Message, peer, cachedMessage => Helpers.Execute.BeginOnThreadPool(() =>_eventAggregator.Publish(cachedMessage)));
                }

                return true;
            }

            var messageId = update as TLUpdateMessageId;
            if (messageId != null)
            {
                var sendingMessage = _cacheService.GetMessage(messageId.RandomId) as TLMessage;
                if (sendingMessage != null)
                {
                    sendingMessage.Id = messageId.Id;
                    _cacheService.SyncSendingMessage(sendingMessage, null, new TLPeerChat(), m => { });
                }

                return true;
            }

            var readMessages = update as TLUpdateReadMessages;
            if (readMessages != null)
            {
                _pts = readMessages.Pts;
                foreach (var readMessageId in readMessages.Messages)
                {
                    var message = _cacheService.GetMessage(readMessageId) as TLMessageCommon;
                    if (message != null)
                    {
                        message.SetUnread(new TLBool(false));
                        message.NotifyOfPropertyChange(() => message.Unread);
                        var dialog = _cacheService.GetDialog(message);
                        if (dialog != null && dialog.UnreadCount.Value > 0)
                        {
                            dialog.UnreadCount = new TLInt(dialog.UnreadCount.Value - 1);
                            var topMessage = dialog.TopMessage;
                            if (topMessage != null
                                && topMessage.Index == readMessageId.Value)
                            {
                                dialog.NotifyOfPropertyChange(() => dialog.TopMessage);
                            }
                        }
                    }
                }

                return true;
            }

            var deleteMessages = update as TLUpdateDeleteMessages;
            if (deleteMessages != null)
            {
                _pts = deleteMessages.Pts;
                _cacheService.DeleteMessages(deleteMessages.Messages);

                return true;
            }

            var restoreMessages = update as TLUpdateRestoreMessages;
            if (restoreMessages != null)
            {
                _pts = restoreMessages.Pts;
                return true;
            }

            var userTyping = update as TLUpdateUserTyping;
            if (userTyping != null)
            {
                _eventAggregator.Publish(userTyping);
                return true;
            }

            var chatUserTyping = update as TLUpdateChatUserTyping;
            if (chatUserTyping != null)
            {
                _eventAggregator.Publish(chatUserTyping);
                return true;
            }

            var chatParticipants = update as TLUpdateChatParticipants;
            if (chatParticipants != null)
            {
                return true;
            }

            var userStatus = update as TLUpdateUserStatus;
            if (userStatus != null)
            {
                var user = _cacheService.GetUser(userStatus.UserId);
                if (user == null)
                {
                    return false;
                }

                user.Status = userStatus.Status;

                Helpers.Execute.BeginOnThreadPool(() => _eventAggregator.Publish(user));
                //_cacheService.SyncUser(user, result => _eventAggregator.Publish(result));
                return true;
            }

            var userName = update as TLUpdateUserName;
            if (userName != null)
            {
                var user = _cacheService.GetUser(userName.UserId);
                if (user == null)
                {
                    return false;
                }

                user.FirstName = userName.FirstName;
                user.LastName = userName.LastName;

                var userWithUserName = user as IUserName;
                if (userWithUserName != null)
                {
                    userName.UserName = userName.UserName;
                }

                Helpers.Execute.BeginOnThreadPool(() => _eventAggregator.Publish(userName));
                return true;
            }

            var userPhoto = update as TLUpdateUserPhoto;
            if (userPhoto != null)
            {
                var user = _cacheService.GetUser(userPhoto.UserId);
                if (user == null)
                {
                    return false;
                }

                user.Photo = userPhoto.Photo;
                Helpers.Execute.BeginOnThreadPool(() => _eventAggregator.Publish(userPhoto));
                //_cacheService.SyncUser(user, result => _eventAggregator.Publish(result));

                return true;
            }

            var userPhone = update as TLUpdateUserPhone;
            if (userPhone != null)
            {
                var user = _cacheService.GetUser(userPhone.UserId);
                if (user == null)
                {
                    return false;
                }

                user.Phone = userPhone.Phone;
                Helpers.Execute.BeginOnThreadPool(() => user.NotifyOfPropertyChange(() => user.Phone));

                return true;
            }

            var contactRegistered = update as TLUpdateContactRegistered;
            if (contactRegistered != null)
            {
                return true;
            }

            var contactLink = update as TLUpdateContactLink;
            if (contactLink != null)
            {
                //var mtProtoService = StaticExchanger.IoC.mtProtoService;
                //mtProtoService.GetFullUserAsync(contactLink.UserId);
                //_eventAggregator.Publish(contactLink);
                return true;
            }

            var chatParticipantAdd = update as TLUpdateChatParticipantAdd;
            if (chatParticipantAdd != null)
            {
                return true;
            }

            var deleteParticipantAdd = update as TLUpdateChatParticipantDelete;
            if (deleteParticipantAdd != null)
            {
                return true;
            }

            return false;
        }

        private void UpgradeSecretChatLayerAndSendNotification(TLEncryptedChat encryptedChat, TLInt layer, TLInt rawInSeqNo, TLInt rawOutSeqNo)
        {
            var newEncryptedChat = new TLEncryptedChat17();
            newEncryptedChat.Layer = layer;
            newEncryptedChat.RawInSeqNo = rawInSeqNo;
            newEncryptedChat.RawOutSeqNo = rawOutSeqNo; 

            newEncryptedChat.Id = encryptedChat.Id;
            newEncryptedChat.AccessHash = encryptedChat.AccessHash;
            newEncryptedChat.Date = encryptedChat.Date;
            newEncryptedChat.AdminId = encryptedChat.AdminId;
            newEncryptedChat.ParticipantId = encryptedChat.ParticipantId;
            newEncryptedChat.GAorB = encryptedChat.GAorB;

            newEncryptedChat.Key = encryptedChat.Key;
            newEncryptedChat.KeyFingerprint = encryptedChat.KeyFingerprint;
            newEncryptedChat.P = encryptedChat.P;
            newEncryptedChat.G = encryptedChat.G;
            newEncryptedChat.A = encryptedChat.A;
            newEncryptedChat.MessageTTL = encryptedChat.MessageTTL;
            newEncryptedChat.FileName = encryptedChat.FileName;

            _cacheService.SyncEncryptedChat(newEncryptedChat,
                result =>
                {
                    _eventAggregator.Publish(newEncryptedChat);

                    Helpers.Execute.ShowDebugMessage("TLDecryptedMessageActionNotifyLayer Layer=" + newEncryptedChat.Layer);

                    var randomId = TLLong.Random();

                    var notifyLayerAction = new TLDecryptedMessageActionNotifyLayer();
                    notifyLayerAction.Layer = new TLInt(Constants.SecretSupportedLayer);

                    var currentUserId = StaticExchanger.IoC.mtProtoService.CurrentUserId;
                    var inSeqNo = TLUtils.GetInSeqNo(currentUserId, newEncryptedChat);
                    var outSeqNo = TLUtils.GetOutSeqNo(currentUserId, newEncryptedChat);

                    var newEncryptedChat17 = _cacheService.GetEncryptedChat(newEncryptedChat.Id) as TLEncryptedChat17;
                    if (newEncryptedChat17 != null)
                    {
                        newEncryptedChat17.RawOutSeqNo = new TLInt(newEncryptedChat17.RawOutSeqNo.Value + 1);
                    }

                    var decryptedMessageService17 = new TLDecryptedMessageService17
                    {
                        Action = notifyLayerAction,
                        RandomId = randomId,
                        RandomBytes = TLString.Empty,

                        ChatId = encryptedChat.Id,
                        FromId = currentUserId,
                        Out = new TLBool(true),
                        Unread = new TLBool(true),
                        Date = TLUtils.DateToUniversalTimeTLInt(0, DateTime.Now),
                        Status = MessageStatus.Sending,

                        TTL = new TLInt(0),
                        InSeqNo = inSeqNo,
                        OutSeqNo = outSeqNo
                    };

                    var decryptedMessageLayer17 = new TLDecryptedMessageLayer17();
                    decryptedMessageLayer17.Layer = new TLInt(Constants.SecretSupportedLayer);
                    decryptedMessageLayer17.InSeqNo = inSeqNo;
                    decryptedMessageLayer17.OutSeqNo = outSeqNo;
                    decryptedMessageLayer17.RandomBytes = TLString.Empty;
                    decryptedMessageLayer17.Message = decryptedMessageService17;

                    _cacheService.SyncDecryptedMessage(
                        decryptedMessageService17,
                        encryptedChat,
                        messageResult =>
                        {
                            SendEncryptedServiceAsync(
                                new TLInputEncryptedChat
                                {
                                    AccessHash = encryptedChat.AccessHash,
                                    ChatId = encryptedChat.Id
                                },
                                randomId,
                                TLUtils.EncryptMessage(decryptedMessageLayer17, encryptedChat),
                                sentEncryptedMessage =>
                                {
                                    decryptedMessageService17.Status = MessageStatus.Confirmed;
                                    decryptedMessageService17.NotifyOfPropertyChange(() => decryptedMessageService17.Status);
                                },
                                error =>
                                {
                                    Helpers.Execute.ShowDebugMessage("messages.sendEncryptedService error " + error);
                                });
                        });
                });
        }

        private static int GetRawInFromReceivedMessage(TLInt currentUserId, TLEncryptedChat17 chat, ISeqNo message)
        {
            var isAdmin = chat.AdminId.Value == currentUserId.Value;
            var x = isAdmin ? 0 : 1;
            return (message.OutSeqNo.Value - x) / 2;
        }

        public static TLDecryptedMessage ToDecryptedMessage17(TLDecryptedMessage decryptedMsg, TLEncryptedChat encryptedChat)
        {
#if SECRET_CHAT_17
            var decryptedMessage17 = new TLDecryptedMessage17();
            decryptedMessage17.RandomId = decryptedMsg.RandomId;
            decryptedMessage17.TTL = encryptedChat.MessageTTL;
            decryptedMessage17.Message = decryptedMsg.Message;
            decryptedMessage17.Media = decryptedMsg.Media;

            decryptedMessage17.FromId = decryptedMsg.FromId;
            decryptedMessage17.Out = decryptedMsg.Out;
            decryptedMessage17.Unread = decryptedMsg.Unread;
            decryptedMessage17.ChatId = decryptedMsg.ChatId;
            decryptedMessage17.Date = decryptedMsg.Date;
            decryptedMessage17.Qts = decryptedMsg.Qts;

            return decryptedMessage17;
#else
            return decryptedMsg;
#endif
        }

        public static bool IsSyncRequierd(TLDecryptedMessageBase decryptedMessage)
        {
#if SECRET_CHAT_17
            return true;
#endif

            var syncMessage = true;
            var decryptedMessageService = decryptedMessage as TLDecryptedMessageService;
            if (decryptedMessageService != null)
            {
                var deleteMessagesAction = decryptedMessageService.Action as TLDecryptedMessageActionDeleteMessages;
                if (deleteMessagesAction != null)
                {
                    syncMessage = false;
                }

                var readMessagesAction = decryptedMessageService.Action as TLDecryptedMessageActionReadMessages;
                if (readMessagesAction != null)
                {
                    syncMessage = false;
                }

                var flushHistoryAction = decryptedMessageService.Action as TLDecryptedMessageActionFlushHistory;
                if (flushHistoryAction != null)
                {
                    syncMessage = false;
                }

                var screenshotMessagesAction = decryptedMessageService.Action as TLDecryptedMessageActionScreenshotMessages;
                if (screenshotMessagesAction != null)
                {
                    syncMessage = false;
                }

                var notifyLayerAction = decryptedMessageService.Action as TLDecryptedMessageActionNotifyLayer;
                if (notifyLayerAction != null)
                {
                    syncMessage = false;
                }
            }
            return syncMessage;
        }

        private readonly object _clientSeqLock = new object();

        private readonly Dictionary<int, WindowsPhone.Tuple<DateTime, TLState>> _lostSeq = new Dictionary<int, WindowsPhone.Tuple<DateTime, TLState>>();

        private void UpdateLostSeq(IList<TLInt> seqList, bool cleanupMissingSeq = false)
        {
            lock (_clientSeqLock)
            {
                if (ClientSeq != null)
                {
                    // add missing items
                    if (seqList[0].Value > ClientSeq.Value + 1)
                    {
                        for (var i = ClientSeq.Value + 1; i < seqList[0].Value; i++)
                        {
                            _lostSeq[i] = new WindowsPhone.Tuple<DateTime, TLState>(DateTime.Now, new TLState{Seq = ClientSeq, Pts = _pts, Date = _date, Qts = _qts});
                        }
                    }

                    // remove received items
                    for (var i = 0; i < seqList.Count; i++)
                    {
                        if (_lostSeq.ContainsKey(seqList[i].Value))
                        {
                            TLUtils.WriteLine(
                                DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " remove from Missing Seq List seq=" +
                                seqList[i].Value + " time=" + (DateTime.Now - _lostSeq[seqList[i].Value].Item1), LogSeverity.Error);
                            _lostSeq.Remove(seqList[i].Value);
                        }
                    }

                    // cleanup (updates.getDifference, set initState, etc)
                    if (cleanupMissingSeq)
                    {
                        _lostSeq.Clear();
                    }
                }

                var lastSeqValue = seqList.Last().Value;
                var maxSeqValue = Math.Max(lastSeqValue, ClientSeq != null ? ClientSeq.Value : -1);
                ClientSeq = new TLInt(maxSeqValue);

                if (_lostSeq.Count > 0)
                {
                    var missingSeqInfo = new StringBuilder();
                    foreach (var keyValue in _lostSeq)
                    {
                        missingSeqInfo.AppendLine(string.Format("seq={0}, date={1}", keyValue.Key,
                            keyValue.Value.Item1.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture)));
                    }

                    StartLostSeqTimer();

                    TLUtils.WriteLine(
                        DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " Missing Seq List\n" +
                        missingSeqInfo, LogSeverity.Error);
                }
            }
        }

        public void ProcessTransportMessage(TLTransportMessage transportMessage)
        {
            try
            {
                var updateList = TLUtils.FindInnerObjects<TLUpdatesBase>(transportMessage).ToList();
#if DEBUG
                var updatesTooLong = TLUtils.FindInnerObjects<TLUpdatesTooLong>(transportMessage).ToList();
                if (updatesTooLong.Count > 0)
                {
                    TLUtils.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " updatesTooLong clientSeq=" + ClientSeq, LogSeverity.Error);
                }
#endif

                var seqList = updateList.SelectMany(updates => updates.GetSeq()).OrderBy(x => x.Value).ToList();

//#if DEBUG
//                if (seqList.Count > 0)
//                {
//                    var showDebugInfo = false;
//                    for (var i = 0; i < seqList.Count; i++)
//                    {
//                        if (seqList[i].Value == 0)
//                        {
//                            showDebugInfo = true;
//                            break;
//                        }
//                    }

//                    if (showDebugInfo)
//                    {
//                        var updateListInfo = new StringBuilder();
//                        foreach (var updatesBase in updateList)
//                        {
//                            updateListInfo.AppendLine(updatesBase.ToString());
//                        }
//                        Helpers.Execute.ShowDebugMessage("ProcessTransportMessage seqs=0 " + updateListInfo);
//                    }
//                }
//#endif

                TLUtils.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " clientSeq=" + ClientSeq + " seqs=" + string.Join(",", seqList), LogSeverity.Error);

                var processUpdates = false;
                if (updateList.Count > 0)
                {
                    if (seqList.Count > 0)
                    {
                        UpdateLostSeq(seqList);
                    }

                    processUpdates = true;
                }

                if (processUpdates)
                {
                    foreach (var updatesItem in updateList)
                    {
                        ProcessUpdates(updatesItem);
                    }
                }

                return;
            }
            catch (Exception e)
            {
                TLUtils.WriteLine("Error during processing update: ", LogSeverity.Error);
                TLUtils.WriteException(e);
            }
        }

        private readonly object _stateRoot = new object();

        //private readonly string StateFileName = "state.xml";

        public void LoadStateAndUpdate(System.Action callback)
        {
            TLUtils.WritePerformance(">>Loading current state and updating");

            if (_pts == null || _date == null || _qts == null)
            {
                var state = TLUtils.OpenObjectFromMTProtoFile<TLState>(_stateRoot, Constants.StateFileName);
#if DEBUG_UPDATES
                state.Pts = new TLInt(140000);
#endif

                SetState(state, "setFileState");
                TLUtils.WritePerformance("Current state: " + state);       
            }


            GetDifference(callback);
        }

        public void SaveState()
        {
            TLUtils.WritePerformance("<<Saving current state");
            TLUtils.SaveObjectToMTProtoFile(_stateRoot, Constants.StateFileName, new TLState { Date = _date, Pts = _pts, Qts = _qts, Seq = ClientSeq, UnreadCount = _unreadCount });
        }

        public void ClearState()
        {
            _date = null;
            _pts = null;
            _qts = null;
            ClientSeq = null;
            _unreadCount = null;
            TLUtils.DeleteFile(_stateRoot, Constants.StateFileName);
        }
    }

    public class DCOptionsUpdatedEventArgs : EventArgs
    {
        public TLUpdateDCOptions Update { get; set; }
    }

    public class UpdatingEventArgs : EventArgs { }

    public class UpdateCompletedEventArgs : EventArgs { }
}
