﻿using System;
using System.Threading;
using System.Windows;
using Telegram.Api.Extensions;
using Telegram.Api.Helpers;
using Telegram.Api.TL;
using Telegram.Api.TL.Account;
using Telegram.Api.TL.Functions.Account;
using TLUpdateUserName = Telegram.Api.TL.Account.TLUpdateUserName;

namespace Telegram.Api.Services
{
	public partial class MTProtoService
	{
	    public void GetWallpapersAsync(Action<TLVector<TLWallPaperBase>> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLGetWallPapers();

            SendInformativeMessage("account.getWallpapers", obj, callback, faultCallback);
	    }

        public void SendChangePhoneCodeAsync(TLString phoneNumber, Action<TLSentChangePhoneCode> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSendChangePhoneCode { PhoneNumber = phoneNumber };

            SendInformativeMessage("account.sendChangePhoneCode", obj, callback, faultCallback);
        }

        public void ChangePhoneAsync(TLString phoneNumber, TLString phoneCodeHash, TLString phoneCode, Action<TLUserBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLChangePhone { PhoneNumber = phoneNumber, PhoneCodeHash = phoneCodeHash, PhoneCode = phoneCode };

            SendInformativeMessage<TLUserBase>("account.changePhone", obj, 
                user =>
                {
                    _cacheService.SyncUser(user, 
                        result =>
                        {
                            callback.SafeInvoke(result);
                        });
                }, 
                faultCallback);
        }

        public void RegisterDeviceAsync(TLString token, TLString deviceModel, TLString systemVersion, TLString appVersion, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            if (_activeTransport.AuthKey == null)
            {
                faultCallback.SafeInvoke(new TLRPCError
                {
                    Code = new TLInt(404),
                    Message = new TLString("Service is not initialized to register device")
                });
                return;
            }
#if DEBUG
            //Execute.BeginOnUIThread(() => Clipboard.SetText(token.ToString()));
#endif

            var obj = new TLRegisterDevice
            {
                TokenType = new TLInt(3),
                Token = token,
                DeviceModel = new TLString("pc"),
                SystemVersion = systemVersion,
                AppVersion = appVersion,
                AppSandbox = new TLBool(false),
                LandCode = new TLString(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName)
            };

            SendInformativeMessage("account.registerDevice", obj, callback, faultCallback);
        }

        public void UnregisterDeviceAsync(TLString token, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLUnregisterDevice
            {
                TokenType = new TLInt(3),
                Token = token
            };

            SendInformativeMessage("account.unregisterDevice", obj, callback, faultCallback);
        }

        public void GetNotifySettingsAsync(TLInputNotifyPeerBase peer, Action<TLPeerNotifySettingsBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetNotifySettings{ Peer = peer };

            SendInformativeMessage("account.getNotifySettings", obj, callback, faultCallback);
        }

        public void ResetNotifySettingsAsync(Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLResetNotifySettings();

            SendInformativeMessage("account.resetNotifySettings", obj, callback, faultCallback);
        }

	    public void UpdateNotifySettingsAsync(TLInputNotifyPeerBase peer, TLInputPeerNotifySettings settings, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TL.Functions.Account.TLUpdateNotifySettings { Peer = peer, Settings = settings };

            SendInformativeMessage("account.updateNotifySettings", obj, callback, faultCallback);
        }

        public void UpdateProfileAsync(TLString firstName, TLString lastName, Action<TLUserBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLUpdateProfile { FirstName = firstName, LastName = lastName };

            SendInformativeMessage<TLUserBase>("account.updateProfile", obj, result =>
            {
                _cacheService.SyncUser(result, callback);
            }, faultCallback);
        }

        public void UpdateStatusAsync(TLBool offline, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            if (_activeTransport.AuthKey == null) return;

            var obj = new TLUpdateStatus { Offline = offline };

            SendInformativeMessage("account.updateStatus", obj, callback, faultCallback);
        }

	    public void CheckUsernameAsync(TLString username, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLCheckUsername { Username = username };

            SendInformativeMessage("account.checkUsername", obj, callback, faultCallback);
	    }

	    public void UpdateUsernameAsync(TLString username, Action<TLUserBase> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLUpdateUserName { Username = username };

            SendInformativeMessage("account.updateUsername", obj, callback, faultCallback);
	    }

	    public void GetAccountTTLAsync(Action<TLAccountDaysTTL> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLGetAccountTTL();

            SendInformativeMessage("account.getAccountTTL", obj, callback, faultCallback);
	    }

        public void SetAccountTTLAsync(TLAccountDaysTTL ttl, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSetAccountTTL{TTL = ttl};

            SendInformativeMessage("account.setAccountTTL", obj, callback, faultCallback);
        }

        public void DeleteAccountTTLAsync(TLString reason, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLDeleteAccount { Reason = reason };

            SendInformativeMessage("account.deleteAccount", obj, callback, faultCallback);
        }

        public void GetPrivacyAsync(TLInputPrivacyKeyBase key, Action<TLPrivacyRules> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetPrivacy { Key = key };

            SendInformativeMessage("account.getPrivacy", obj, callback, faultCallback);
        }

        public void SetPrivacyAsync(TLInputPrivacyKeyBase key, TLVector<TLInputPrivacyRuleBase> rules, Action<TLPrivacyRules> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSetPrivacy { Key = key, Rules = rules };

            SendInformativeMessage("account.setPrivacy", obj, callback, faultCallback);
        }

	}
}
