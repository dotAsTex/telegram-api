﻿using System;
using System.Globalization;
using System.Threading;
using Telegram.Api.Helpers;
using Telegram.Api.TL;

namespace Telegram.Api.Services.Connection
{
    public interface IConnectionService
    {
        void Initialize(IMTProtoService mtProtoService);
        event EventHandler ConnectionLost;
    }

    public class ConnectionService : IConnectionService
    {
        public event EventHandler ConnectionLost;

        protected virtual void RaiseConnectionFailed()
        {
            var handler = ConnectionLost;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        private IMTProtoService _mtProtoService;

        public void Initialize(IMTProtoService mtProtoService)
        {
            _mtProtoService = mtProtoService;
        }

        private Timer _connectionScheduler;

        public ConnectionService()
        {
            _connectionScheduler = new Timer(CheckConnectionState, this, TimeSpan.FromSeconds(0.0), TimeSpan.FromSeconds(5.0));
        }

        private void CheckConnectionState(object state)
        {
            if (_mtProtoService == null) return;
            
            var activeTransport = _mtProtoService.GetActiveTransport();
            if (activeTransport == null) return;
            if (activeTransport.AuthKey == null) return;

            var transportId = activeTransport.Id;


            var connectionFailed = false;
            var now = DateTime.Now;
            if (activeTransport.LastReceiveTime.HasValue)
            {
                connectionFailed = Math.Abs((now - activeTransport.LastReceiveTime.Value).TotalSeconds) > Constants.TimeoutInterval;
            }
            else
            {
                if (activeTransport.FirstSendTime.HasValue)
                {
                    connectionFailed = Math.Abs((now - activeTransport.FirstSendTime.Value).TotalSeconds) > Constants.TimeoutInterval;
                }
            }

            if (connectionFailed)
            {
                RaiseConnectionFailed();
                TLUtils.WriteLine(DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture) + " reconnect t" + transportId, LogSeverity.Error);
                return;
            }

            var pingRequired = false;
            var timeFromLastReceive = 0.0;
            var timeFromFirstSend = 0.0;
            if (activeTransport.LastReceiveTime.HasValue)
            {
                // что-то уже получали по соединению
                var lastReceiveTime = activeTransport.LastReceiveTime.Value;
                timeFromLastReceive = Math.Abs((now - lastReceiveTime).TotalSeconds);

                pingRequired = timeFromLastReceive > 15.0;
            }
            else
            {
                // ничего не получали, но что-то отправляли
                if (activeTransport.FirstSendTime.HasValue)
                {
                    var firstSendTime = activeTransport.FirstSendTime.Value;
                    timeFromFirstSend = Math.Abs((now - firstSendTime).TotalSeconds);

                    pingRequired = timeFromFirstSend > 15.0;
                }
                // хотя бы пинганем для начала
                else
                {
                    pingRequired = true;
                }
            }

            if (pingRequired)
            {
                var pingId = TLLong.Random();
                var pingIdHash = pingId.Value % 1000;

                var debugString = string.Format("{0} ping t{1} ({2}, {3}) [{4}]", 
                    DateTime.Now.ToString("HH:mm:ss.fff"),
                    transportId, 
                    timeFromFirstSend.ToString("N"), 
                    timeFromLastReceive.ToString("N"), 
                    pingIdHash);

                TLUtils.WriteLine(debugString, LogSeverity.Error);
                _mtProtoService.PingAsync(pingId, //new TLInt(35),
                    result =>
                    {
                        var resultDebugString = string.Format("{0} pong t{1} ({2}, {3}) [{4}]",
                            DateTime.Now.ToString("HH:mm:ss.fff"),
                            transportId,
                            timeFromFirstSend.ToString("N"),
                            timeFromLastReceive.ToString("N"),
                            pingIdHash);

                        TLUtils.WriteLine(resultDebugString, LogSeverity.Error);
                    },
                    error =>
                    {
                        var errorDebugString = string.Format("{0} pong error t{1} ({2}, {3}) [{4}] \nSocketError={5}",
                            DateTime.Now.ToString("HH:mm:ss.fff"),
                            transportId,
                            timeFromFirstSend.ToString("N"),
                            timeFromLastReceive.ToString("N"),
                            pingIdHash,
                            error.SocketError);

                        TLUtils.WriteLine(errorDebugString, LogSeverity.Error);
                    });
            }
            else
            {
                var checkDebugString = string.Format("{0} check t{1} ({2}, {3})",
                    DateTime.Now.ToString("HH:mm:ss.fff"),
                    transportId,
                    timeFromFirstSend.ToString("N"),
                    timeFromLastReceive.ToString("N"));

                //TLUtils.WriteLine(checkDebugString, LogSeverity.Error);
            }
        }
    }
}
