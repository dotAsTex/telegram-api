﻿using System;
using Telegram.Api.TL;
using Telegram.Api.TL.Functions.Updates;

namespace Telegram.Api.Services
{
	public partial class MTProtoService
	{
        public void GetStateAsync(Action<TLState> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetState();

            SendInformativeMessage("updates.getState", obj, callback, faultCallback);
        }

        public void GetDifferenceAsync(TLInt pts, TLInt date, TLInt qts, Action<TLDifferenceBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetDifference{ Date = date, Pts = pts, Qts = qts };

            SendInformativeMessage("updates.getDifference", obj, callback, faultCallback);
        }
	}
}
