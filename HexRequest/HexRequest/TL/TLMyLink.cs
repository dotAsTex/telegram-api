﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    [KnownType(typeof(TLMyLinkContact))]
    [KnownType(typeof(TLMyLinkEmpty))]
    [KnownType(typeof(TLMyLinkRequested))]
    [DataContract]
    public abstract class TLMyLinkBase : TLObject { }

    [DataContract]
    public class TLMyLinkEmpty : TLMyLinkBase
    {
        public const uint Signature = TLConstructors.TLMyLinkEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }

    [DataContract]
    public class TLMyLinkRequested : TLMyLinkBase
    {
        public const uint Signature = TLConstructors.TLMyLinkRequested;

        [DataMember]
        public TLBool Contact { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Contact = GetObject<TLBool>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Contact = GetObject<TLBool>(input);

            return this;
        }
        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Contact.ToStream(output);
        }
    }

    [DataContract]
    public class TLMyLinkContact : TLMyLinkBase
    {
        public const uint Signature = TLConstructors.TLMyLinkContact;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }
}
