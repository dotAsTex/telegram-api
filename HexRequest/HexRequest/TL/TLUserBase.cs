﻿using System;
using System.Collections.Generic;
using System.IO;
using Telegram.Api.TL.Interfaces;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public interface IUserName
    {
        TLString UserName { get; set; }
    }

    public class TLUserExtendedInfo : TLObject
    {
        public const uint Signature = TLConstructors.TLUserExtendedInfo;

        public TLString FirstName { get; set; }

        public TLString LastName { get; set; }

        public override TLObject FromStream(Stream input)
        {
            FirstName = GetObject<TLString>(input);
            LastName = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
        }
    }

    public class TLUserNotRegistered : TLUserBase
    {
        public IEnumerable<string> PhoneNumbers { get; set; } 

        public override TLInputUserBase ToInputUser()
        {
            return null;
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return null;
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }
    }
    [Serializable]

    public abstract class TLUserBase : TLObject, IInputPeer, ISelectable, IFullName, IVIsibility
    {
        public int Index
        {
            get { return Id.Value; }
            set { Id = new TLInt(value); }
        }

        public TLInt Id { get; set; }

        public TLString _firstName;

        public TLString FirstName
        {
            get { return _firstName; }
            set
            {
                SetField(ref _firstName, value, () => FirstName);
                NotifyOfPropertyChange(() => FullName);
            }
        }

        public TLString _lastName;

        public TLString LastName
        {
            get { return _lastName; }
            set
            {
                SetField(ref _lastName, value, () => LastName);
                NotifyOfPropertyChange(() => FullName);
            }
        }

        public TLString Phone { get; set; }

        public TLPhotoBase _photo;

        public TLPhotoBase Photo
        {
            get { return _photo; }
            set { SetField(ref _photo, value, () => Photo); }
        }

        public TLUserStatus _status;

        public TLUserStatus Status
        {
            get { return _status; }
            set { SetField(ref _status, value, () => Status); }
        }

        public int StatusValue
        {
            get
            {
                if (Status is TLUserStatusOnline)
                {
                    return int.MaxValue;
                }
                var offline = Status as TLUserStatusOffline;
                if (offline != null)
                {
                    return offline.WasOnline.Value;
                }
                
                return int.MinValue;
            }
        }

        #region Additional

        public bool RemoveUserAction { get; set; }

        public TLContact Contact { get; set; }

        public override string ToString()
        {
            return FullName;
        }

        public virtual string FullName
        {
            get
            {
                if (GetType() == typeof (TLUserEmpty))
                {
                    return "Deleted user";
                }

                if (ExtendedInfo != null)
                {
                    return string.Format("{0} {1}", ExtendedInfo.FirstName, ExtendedInfo.LastName);
                }

                var firstName = FirstName != null ? FirstName.ToString() : string.Empty;
                var lastName = LastName != null ? LastName.ToString() : string.Empty;
                
                if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName))
                {
                    return Phone != null ? "+" + Phone : string.Empty;
                }
                
                if (string.Equals(firstName, lastName, StringComparison.OrdinalIgnoreCase))
                {
                    return firstName;
                }

                if (string.IsNullOrEmpty(firstName))
                {
                    return lastName;
                }

                if (string.IsNullOrEmpty(lastName))
                {
                    return firstName;
                }

                return string.Format("{0} {1}", firstName, lastName);
            }
        }

        public virtual bool HasPhone { get { return Phone != null && !string.IsNullOrEmpty(Phone.ToString()); } }

        public abstract TLInputUserBase ToInputUser();

        public virtual void Update(TLUserBase user)
        {
            try
            {
                _firstName = user.FirstName;
                _lastName = user.LastName;
                Phone = user.Phone;

                if (Photo.GetType() != user.Photo.GetType())
                {
                    Photo = user.Photo;
                }
                else
                {
                    Photo.Update(user.Photo);
                }

                _status = user.Status;

                if (user.Contact != null)
                {
                    Contact = user.Contact;
                }

                if (user.Link != null)
                {
                    Link = user.Link;
                }

                if (user.ProfilePhoto != null)
                {
                    ProfilePhoto = user.ProfilePhoto;
                }

                if (user.NotifySettings != null)
                {
                    _notifySettings = user.NotifySettings;
                }

                if (user.ExtendedInfo != null)
                {
                    ExtendedInfo = user.ExtendedInfo;
                }

                if (user.Blocked != null)
                {
                    Blocked = user.Blocked;
                }

                if (user.RealFirstName != null)
                {
                    RealFirstName = user.RealFirstName;
                }

                if (user.RealLastName != null)
                {
                    RealLastName = user.RealLastName;
                }
            }
            catch (Exception e)
            {
                
            }

        }

        public abstract TLInputPeerBase ToInputPeer();

        public abstract string GetUnsendedTextFileName();

        #region UserFull information

        public TLLink Link { get; set; }

        public TLPhotoBase ProfilePhoto { get; set; }

        protected TLPeerNotifySettingsBase _notifySettings;

        public TLPeerNotifySettingsBase NotifySettings
        {
            get { return _notifySettings; }
            set { SetField(ref _notifySettings, value, () => NotifySettings); }
        }

        public TLUserExtendedInfo ExtendedInfo { get; set; }

        public TLBool Blocked { get; set; }

        public TLString RealFirstName { get; set; }

        public TLString RealLastName { get; set; }

        #endregion


        #endregion

        public TLInputNotifyPeerBase ToInputNotifyPeer()
        {
            return new TLInputNotifyPeer{Peer = ToInputPeer()};
        }

        private bool _isVisible;

        public bool IsVisible
        {
            get { return _isVisible; }
            set { SetField(ref _isVisible, value, () => IsVisible); }
        }

        public bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetField(ref _isSelected, value, () => IsSelected); }
        }
        
        private string _selectedText;

        public string SelectedText
        {
            get { return _selectedText; }
            set { SetField(ref _selectedText, value, () => SelectedText); }
        }

        #region PhoneBook
        public TLLong ClientId { get; set; }

        public string Mobile { get; set; }

        public string Mobile2 { get; set; }


        public string Home { get; set; }

        public string Home2 { get; set; }


        public string Work { get; set; }

        public string Work2 { get; set; }


        public string Company { get; set; }

        public string Pager { get; set; }

        public string HomeFax { get; set; }

        public string WorkFax { get; set; }


        #endregion

        public static string GetLastNameKey(TLUserBase person)
        {
            if (person.LastName == null) return ('#').ToString();

            char key = char.ToLower(person.LastName.Value[0]);

            if (key < 'a' || key > 'z')
            {
                if (key < 'а' || key > 'я')
                {
                    key = '#';
                }
            }

            return key.ToString();
        }

        public static int CompareByLastName(object obj1, object obj2)
        {
            var p1 = (TLUserBase)obj1;
            var p2 = (TLUserBase)obj2;

            if (p1.LastName == null && p2.LastName != null)
            {
                return -1;
            }

            if (p1.LastName != null && p2.LastName == null)
            {
                return 1;
            }

            if (p1.LastName == null && p2.LastName == null)
            {
                return 0;
            }


            int result = String.Compare(p1.LastName.Value, p2.LastName.Value, StringComparison.Ordinal);
            //if (result == 0)
            //{
            //    result = String.Compare(p1.FirstName.Value, p2.FirstName.Value, StringComparison.Ordinal);
            //}

            return result;
        }
    }

    public class TLUserEmpty : TLUserBase
    {
        public const uint Signature = TLConstructors.TLUserEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
        }

        public override void Update(TLUserBase user)
        {
            return;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override TLInputUserBase ToInputUser()
        {
            return new TLInputUserContact { UserId = Id };
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerContact{ UserId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }
    }

    public class TLUserSelf18 : TLUserSelf, IUserName
    {
        public new const uint Signature = TLConstructors.TLUserSelf18;

        public TLString UserName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            UserName = GetObject<TLString>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);
            Inactive = GetObject<TLBool>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                UserName.ToBytes(),
                Phone.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes(),
                Inactive.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            UserName = GetObject<TLString>(input);
            Phone = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);
            Inactive = GetObject<TLBool>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(UserName.ToBytes());
            output.Write(Phone.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);
            output.Write(Inactive.ToBytes());

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            var user18 = user as TLUserSelf18;
            if (user18 != null)
            {
                UserName = user18.UserName;
            }
        }

        public override string ToString()
        {
            var userNameString = UserName != null ? " @" + UserName : string.Empty;
            return base.ToString() + userNameString;
        }
    }

    public class TLUserSelf : TLUserBase
    {
        public const uint Signature = TLConstructors.TLUserSelf;

        public TLBool Inactive { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);
            Inactive = GetObject<TLBool>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                Phone.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes(),
                Inactive.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            Phone = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);
            Inactive = GetObject<TLBool>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(Phone.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);
            output.Write(Inactive.ToBytes());

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            Inactive = ((TLUserSelf)user).Inactive;
        }

        #region Additional
        public override bool HasPhone { get { return true; } }

        public override TLInputUserBase ToInputUser()
        {
            return new TLInputUserSelf();
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerSelf();
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }

        #endregion
    }

    public class TLUserContact18 : TLUserContact, IUserName
    {
        public new const uint Signature = TLConstructors.TLUserContact18;

        public TLString UserName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            UserName = GetObject<TLString>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                UserName.ToBytes(),
                AccessHash.ToBytes(),
                Phone.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            UserName = GetObject<TLString>(input);
            AccessHash = GetObject<TLLong>(input);
            Phone = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(UserName.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Phone.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            var user18 = user as TLUserContact18;
            if (user18 != null)
            {
                UserName = user18.UserName;
            }
        }

        public override string ToString()
        {
            var userNameString = UserName != null ? " @" + UserName : string.Empty;
            return base.ToString() + userNameString;
        }
    }

    public class TLUserContact : TLUserBase
    {
        public const uint Signature = TLConstructors.TLUserContact;

        public TLLong AccessHash { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                AccessHash.ToBytes(),
                Phone.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            AccessHash = GetObject<TLLong>(input);
            Phone = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Phone.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            AccessHash = ((TLUserContact) user).AccessHash;
        }

        public override TLInputUserBase ToInputUser()
        {
            return new TLInputUserContact{ UserId = Id };
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerContact { UserId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }
    }

    public class TLUserRequest18 : TLUserRequest, IUserName
    {
        public new const uint Signature = TLConstructors.TLUserRequest18;

        public TLString UserName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            UserName = GetObject<TLString>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                UserName.ToBytes(),
                AccessHash.ToBytes(),
                Phone.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            UserName = GetObject<TLString>(input);
            AccessHash = GetObject<TLLong>(input);
            Phone = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(UserName.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Phone.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            var user18 = user as TLUserRequest18;
            if (user18 != null)
            {
                UserName = user18.UserName;
            }
        }

        public override string ToString()
        {
            var userNameString = UserName != null ? " @" + UserName : string.Empty;
            return base.ToString() + userNameString;
        }
    }

    public class TLUserRequest : TLUserBase
    {
        public const uint Signature = TLConstructors.TLUserRequest;

        public TLLong AccessHash { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                AccessHash.ToBytes(),
                Phone.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            AccessHash = GetObject<TLLong>(input);
            Phone = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Phone.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override TLInputUserBase ToInputUser()
        {
            return new TLInputUserForeign { UserId = Id, AccessHash = AccessHash };
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            AccessHash = ((TLUserRequest)user).AccessHash;
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerForeign{ UserId = Id, AccessHash = AccessHash };
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }
    }

    public class TLUserForeign18 : TLUserForeign, IUserName
    {
        public new const uint Signature = TLConstructors.TLUserForeign18;

        public TLString UserName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            UserName = GetObject<TLString>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                UserName.ToBytes(),
                AccessHash.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            UserName = GetObject<TLString>(input);
            AccessHash = GetObject<TLLong>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(UserName.ToBytes());
            output.Write(AccessHash.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            var user18 = user as TLUserForeign18;
            if (user18 != null)
            {
                UserName = user18.UserName;
            }
        }

        public override string ToString()
        {
            var userNameString = UserName != null ? " @" + UserName : string.Empty;
            return base.ToString() + userNameString;
        }
    }

    public class TLUserForeign : TLUserBase
    {
        public const uint Signature = TLConstructors.TLUserForeign;

        public TLLong AccessHash { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            _status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                AccessHash.ToBytes(),
                Photo.ToBytes(),
                Status.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            AccessHash = GetObject<TLLong>(input);
            _photo = GetObject<TLPhotoBase>(input);
            _status = GetObject<TLUserStatus>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(AccessHash.ToBytes());
            Photo.ToStream(output);
            Status.ToStream(output);

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override TLInputUserBase ToInputUser()
        {
            return new TLInputUserForeign { UserId = Id, AccessHash = AccessHash };
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            AccessHash = ((TLUserForeign)user).AccessHash;
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerForeign{ UserId = Id, AccessHash = AccessHash };
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }
    }

    public class TLUserDeleted18 : TLUserDeleted, IUserName
    {
        public new const uint Signature = TLConstructors.TLUserDeleted18;

        public TLString UserName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            UserName = GetObject<TLString>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes(),
                UserName.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);
            UserName = GetObject<TLString>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());
            output.Write(UserName.ToBytes());

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override void Update(TLUserBase user)
        {
            base.Update(user);

            var user18 = user as TLUserDeleted18;
            if (user18 != null)
            {
                UserName = user18.UserName;
            }
        }

        public override string ToString()
        {
            var userNameString = UserName != null ? " @" + UserName : string.Empty;
            return base.ToString() + userNameString;
        }
    }

    public class TLUserDeleted : TLUserBase
    {
        public const uint Signature = TLConstructors.TLUserDeleted;
         
        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _firstName = GetObject<TLString>(bytes, ref position);
            _lastName = GetObject<TLString>(bytes, ref position);
            
            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _firstName = GetObject<TLString>(input);
            _lastName = GetObject<TLString>(input);

            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;
            ExtendedInfo = GetObject<TLObject>(input) as TLUserExtendedInfo;
            Contact = GetObject<TLObject>(input) as TLContact;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(FirstName.ToBytes());
            output.Write(LastName.ToBytes());

            NotifySettings.NullableToStream(output);
            ExtendedInfo.NullableToStream(output);
            Contact.NullableToStream(output);
        }

        public override TLInputUserBase ToInputUser()
        {
            throw new NotSupportedException();
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerContact{ UserId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "u" + Id + ".dat";
        }
    }
}
