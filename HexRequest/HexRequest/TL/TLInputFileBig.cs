﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telegram.Api.TL
{
    public class TLInputFileBig : TLObject
    {
        public const uint Signature = TLConstructors.TLInputFileBig;

        public TLLong Id { get; set; }

        public TLInt Parts { get; set; }

        public TLString Name { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLInputFileBig--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLLong>(bytes, ref position);
            Parts = GetObject<TLInt>(bytes, ref position);
            Name = GetObject<TLString>(bytes, ref position);

            return this;
        }
    }
}
