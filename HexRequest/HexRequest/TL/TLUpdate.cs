﻿namespace Telegram.Api.TL
{
    public abstract class TLUpdateBase : TLObject { }

    public class TLUpdateNewMessage : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateNewMessage;

        public TLMessageBase Message { get; set; }
        public TLInt Pts { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Message = GetObject<TLMessageBase>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateChatParticipantAdd : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateChatParticipantAdd;

        public TLInt ChatId { get; set; }
        public TLInt UserId { get; set; }
        public TLInt InviterId { get; set; }
        public TLInt Version { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            UserId = GetObject<TLInt>(bytes, ref position);
            InviterId = GetObject<TLInt>(bytes, ref position);
            Version = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateChatParticipantDelete : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateChatParticipantDelete;

        public TLInt ChatId { get; set; }
        public TLInt UserId { get; set; }
        public TLInt Version { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            UserId = GetObject<TLInt>(bytes, ref position);
            Version = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateNewEncryptedMessage : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateNewEncryptedMessage;

        public TLEncryptedMessageBase Message { get; set; }
        public TLInt Qts { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);
            
            Message = GetObject<TLEncryptedMessageBase>(bytes, ref position);
            Qts = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateEncryption : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateEncryption;

        public TLEncryptedChatBase Chat { get; set; }
        public TLInt Date { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Chat = GetObject<TLEncryptedChatBase>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateMessageId : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateMessageId;

        public TLInt Id { get; set; }
        public TLLong RandomId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            RandomId = GetObject<TLLong>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateReadMessages : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateReadMessages;

        public TLVector<TLInt> Messages { get; set; }
        public TLInt Pts { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Messages = GetObject<TLVector<TLInt>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateEncryptedMessagesRead : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateEncryptedMessagesRead;

        public TLInt ChatId { get; set; }
        public TLInt MaxDate { get; set; }
        public TLInt Date { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            MaxDate = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateDeleteMessages : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateDeleteMessages;

        public TLVector<TLInt> Messages { get; set; }
        public TLInt Pts { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Messages = GetObject<TLVector<TLInt>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateRestoreMessages : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateRestoreMessages;

        public TLVector<TLInt> Messages { get; set; }
        public TLInt Pts { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Messages = GetObject<TLVector<TLInt>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserTyping : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateUserTyping;

        public TLInt UserId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserTyping17 : TLUpdateUserTyping
    {
        public new const uint Signature = TLConstructors.TLUpdateUserTyping17;

        public TLSendMessageActionBase Action { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            Action = GetObject<TLSendMessageActionBase>(bytes, ref position);

            return this;
        }
    }


    public class TLUpdateChatUserTyping : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateChatUserTyping;

        public TLInt ChatId { get; set; }
        public TLInt UserId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            UserId = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateChatUserTyping17 : TLUpdateChatUserTyping
    {
        public new const uint Signature = TLConstructors.TLUpdateChatUserTyping17;

        public TLSendMessageActionBase Action { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            UserId = GetObject<TLInt>(bytes, ref position);
            Action = GetObject<TLSendMessageActionBase>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateEncryptedChatTyping : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateEncryptedChatTyping;

        public TLInt ChatId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateChatParticipants : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateChatParticipants;

        public TLChatParticipantsBase Participants { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Participants = GetObject<TLChatParticipantsBase>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserStatus : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateUserStatus;

        public TLInt UserId { get; set; }
        public TLUserStatus Status { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            Status = GetObject<TLUserStatus>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserName : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateUserName;

        public TLInt UserId { get; set; }
        public TLString FirstName { get; set; }
        public TLString LastName { get; set; }
        public TLString UserName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            FirstName = GetObject<TLString>(bytes, ref position);
            LastName = GetObject<TLString>(bytes, ref position);
            UserName = GetObject<TLString>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserPhoto : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateUserPhoto;

        public TLInt UserId { get; set; }

        public TLInt Date { get; set; }

        public TLPhotoBase Photo { get; set; }

        public TLBool Previous { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Photo = GetObject<TLPhotoBase>(bytes, ref position);
            Previous = GetObject<TLBool>(bytes, ref position);
            
            return this;
        }
    }

    public class TLUpdateContactRegistered : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateContactRegistered;

        public TLInt UserId { get; set; }
        public TLInt Date { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateContactLink : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateContactLink;

        public TLInt UserId { get; set; }
        public TLMyLinkBase MyLink { get; set; }
        public TLForeignLinkBase ForeignLink { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            MyLink = GetObject<TLMyLinkBase>(bytes, ref position);
            ForeignLink = GetObject<TLForeignLinkBase>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateActivation : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateActivation;

        public TLInt UserId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateNewAuthorization : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateNewAuthorization;

        public TLLong AuthKeyId { get; set; }
        public TLInt Date { get; set; }
        public TLString Device { get; set; }
        public TLString Location { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            AuthKeyId = GetObject<TLLong>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Device = GetObject<TLString>(bytes, ref position);
            Location = GetObject<TLString>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateDCOptions : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateDCOptions;

        public TLVector<TLDCOption> DCOptions { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            DCOptions = GetObject<TLVector<TLDCOption>>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateNotifySettings : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateNotifySettings;

        public TLNotifyPeerBase Peer { get; set; }

        public TLPeerNotifySettingsBase NotifySettings { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Peer = GetObject<TLNotifyPeerBase>(bytes, ref position);
            NotifySettings = GetObject<TLPeerNotifySettingsBase>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserBlocked : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateUserBlocked;

        public TLInt UserId { get; set; }

        public TLBool Blocked { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            Blocked = GetObject<TLBool>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdatePrivacy : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdatePrivacy;

        public TLPrivacyKeyBase Key { get; set; }

        public TLVector<TLPrivacyRuleBase> Rules { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Key = GetObject<TLPrivacyKeyBase>(bytes, ref position);
            Rules = GetObject<TLVector<TLPrivacyRuleBase>>(bytes, ref position);

            return this;
        }
    }

    public class TLUpdateUserPhone : TLUpdateBase
    {
        public const uint Signature = TLConstructors.TLUpdateUserPhone;

        public TLInt UserId { get; set; }

        public TLString Phone { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            Phone = GetObject<TLString>(bytes, ref position);

            return this;
        }
    }
}
