﻿namespace Telegram.Api.TL
{
    public class TLCheckedPhone : TLObject
    {
        public const uint Signature = TLConstructors.TLCheckedPhone;

        public TLBool PhoneRegistered { get; set; }

        public TLBool PhoneInvited { get; set; }

        /// <summary>
        /// Parses bytes arrays to TLObject from specific position, than adds count of readed bytes to position variable.
        /// </summary>
        /// <param name="bytes">    The bytes array. </param>
        /// <param name="position"> The position. </param>
        /// <returns>   The TLObject. </returns>
        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLCheckedPhone--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            PhoneRegistered = GetObject<TLBool>(bytes, ref position);
            TLUtils.WriteLine("PhoneRegistered: " + PhoneRegistered);

            PhoneInvited = GetObject<TLBool>(bytes, ref position);
            TLUtils.WriteLine("PhoneInvited: " + PhoneInvited);

            return this;
        }
    }
}
