﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLReadEncryptedHistory : TLObject
    {
        public const string Signature = "#7f4b690a";

        public TLInputEncryptedChat Peer { get; set; }

        public TLInt MaxDate { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                MaxDate.ToBytes());
        }
    }
}