﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLForwardMessage : TLObject
    {
        public const string Signature = "#3f3f4f2";

        public TLInputPeerBase Peer { get; set; }

        public TLInt Id { get; set; }

        public TLLong RandomId { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Id.ToBytes(),
                RandomId.ToBytes());
        }
    }
}