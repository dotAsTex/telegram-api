﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSendMedia : TLObject
    {
        public const string Signature = "#a3c85d76";

        public TLInputPeerBase Peer { get; set; }

        public TLInputMediaBase Media { get; set; }

        public TLLong RandomId { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Media.ToBytes(),
                RandomId.ToBytes());
        }
    }
}
