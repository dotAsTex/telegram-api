﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSearch : TLObject
    {
        public const string Signature = "#7e9f2ab";

        public TLInputPeerBase Peer { get; set; }

        public TLString Query { get; set; }

        public TLInputMessagesFilterBase Filter { get; set; }

        public TLInt MinDate { get; set; }

        public TLInt MaxDate { get; set; }

        public TLInt Offset { get; set; }

        public TLInt MaxId { get; set; }

        public TLInt Limit { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Query.ToBytes(),
                Filter.ToBytes(),
                MinDate.ToBytes(),
                MaxDate.ToBytes(),
                Offset.ToBytes(),
                MaxId.ToBytes(),
                Limit.ToBytes());
        }
    }
}
