﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLGetHistory : TLObject
    {
        public const string Signature = "#92a1df2f";

        public TLInputPeerBase Peer { get; set; }

        public TLInt Offset { get; set; }

        public TLInt MaxId { get; set; }

        public TLInt Limit { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Offset.ToBytes(),
                MaxId.ToBytes(),
                Limit.ToBytes());
        }
    }
}
