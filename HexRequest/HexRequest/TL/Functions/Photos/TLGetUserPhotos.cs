﻿namespace Telegram.Api.TL.Functions.Photos
{
    class TLGetUserPhotos : TLObject
    {
        public const string Signature = "#b7ee553c";

        public TLInputUserBase UserId { get; set; }

        public TLInt Offset { get; set; }

        public TLInt MaxId { get; set; }

        public TLInt Limit { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                UserId.ToBytes(),
                Offset.ToBytes(),
                MaxId.ToBytes(),
                Limit.ToBytes());
        }
    }
}
