﻿namespace Telegram.Api.TL
{
    public class TLStatedMessages : TLObject
    {
        public const uint Signature = TLConstructors.TLStatedMessages;

        public TLVector<TLMessageBase> Messages { get; set; }

        public TLVector<TLChatBase> Chats { get; set; }

        public TLVector<TLUserBase> Users { get; set; } 

        public TLInt Pts { get; set; }

        public TLInt Seq { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Messages = GetObject<TLVector<TLMessageBase>>(bytes, ref position);
            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public virtual TLStatedMessages GetEmptyObject()
        {
            return new TLStatedMessages
            {
                Chats = new TLVector<TLChatBase>(Chats.Count),
                Users = new TLVector<TLUserBase>(Users.Count),
                Messages = new TLVector<TLMessageBase>(Messages.Count),
                Pts = Pts,
                Seq = Seq
            };
        }
    }

    public class TLStatedMessagesLinks : TLStatedMessages
    {
        public new const uint Signature = TLConstructors.TLStatedMessagesLinks;

        public TLVector<TLLink> Links { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Messages = GetObject<TLVector<TLMessageBase>>(bytes, ref position);
            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);
            Links = GetObject<TLVector<TLLink>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override TLStatedMessages GetEmptyObject()
        {
            return new TLStatedMessagesLinks
            {
                Chats = new TLVector<TLChatBase>(Chats.Count),
                Users = new TLVector<TLUserBase>(Users.Count),
                Messages = new TLVector<TLMessageBase>(Messages.Count),
                Links = new TLVector<TLLink>(Links.Count),
                Pts = Pts,
                Seq = Seq
            };
        }
    }
}
