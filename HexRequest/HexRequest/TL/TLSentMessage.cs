﻿namespace Telegram.Api.TL
{
    public class TLSentMessage : TLObject
    {
        public const uint Signature = TLConstructors.TLSentMessage;

        public TLInt Id { get; set; }
        public TLInt Date { get; set; }
        public TLInt Pts { get; set; }
        public TLInt Seq { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                Date.ToBytes(),
                Pts.ToBytes(),
                Seq.ToBytes());
        }
    }

    public class TLSentMessageLink : TLSentMessage
    {
        public new const uint Signature = TLConstructors.TLSentMessageLink;

        public TLVector<TLLink> Links { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);
            Links = GetObject<TLVector<TLLink>>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                Date.ToBytes(),
                Pts.ToBytes(),
                Seq.ToBytes(),
                Links.ToBytes());
        }
    }
}
