﻿namespace Telegram.Api.TL
{
    public class TLChatFull : TLObject
    {
        public const uint Signature = TLConstructors.TLChatFull;

        public TLInt Id { get; set; }

        public TLChatParticipantsBase Participants { get; set; }

        public TLPhotoBase ChatPhoto { get; set; }

        public TLPeerNotifySettingsBase NotifySettings { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            Participants = GetObject<TLChatParticipantsBase>(bytes, ref position);
            ChatPhoto = GetObject<TLPhotoBase>(bytes, ref position);
            NotifySettings = GetObject<TLPeerNotifySettingsBase>(bytes, ref position);

            return this;
        }

        public TLChatBase ToChat(TLChatBase chat)
        {
            chat.NotifySettings = NotifySettings;
            chat.Participants = Participants;
            chat.ChatPhoto = ChatPhoto;

            return chat;
        }
    }
}
