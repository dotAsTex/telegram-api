﻿namespace Telegram.Api.TL
{
    public class TLAffectedHistory : TLObject
    {
        public const uint Signature = TLConstructors.TLAffectedHistory;

        public TLInt Pts { get; set; }

        public TLInt Seq { get; set; }

        public TLInt Offset { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);
            Offset = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }
}
