﻿//#if PRIVATE_BETA
//#define TEST_SERVER
//#endif

using System.Collections.Generic;

namespace Telegram.Api
{
    public static class Constants
    {
        public const int FirstServerDCId = 2; // from 1, 2, 3, 4, 5
        public const int FirstServerPort = 443;
        public const string FirstServerIpAddress =

#if TEST_SERVER //test
            "173.240.5.253"; //dc1
#else   //prod
        //"149.154.175.50";     // dc1
            "149.154.167.51";       // dc2
        //"174.140.142.6";      // dc3
        //"149.154.167.90";     // dc4
        //"149.154.171.5";      // dc5
#endif

        public const int ApiId = 44620;
        public const string ApiHash = "e82e2f68e6502c1fa9b49bdcd196fae8";
        public const int SupportedLayer = 22;
        public const int SecretSupportedLayer = 17;

        public static List<string> ServerIpAddresses = new List<string>
        {
            "149.154.175.50",   // dc1
            "149.154.167.51",   // dc2
            "174.140.142.6",    // dc3
            "149.154.167.91",   // dc4
            "149.154.171.5"     // dc5
        };

        public const int LongPollReattemptDelay = 5000;     //ms
        public const double MessageSendingInterval = 
#if DEBUG
            300;   //seconds (5 minutes - 30 seconds(max delay: 25))
#else
            180;   //seconds (5 minutes - 30 seconds(max delay: 25))
#endif
        public const double ResendMessageInterval = 5.0;    //seconds
        public const int CommitDBInterval = 3;              //seconds
        public const int GetConfigInterval = 60 * 60;       //seconds
        public const int TimeoutInterval = 25;              //seconds    

        public const bool IsLongPollEnabled = false;
        public const int CachedDialogsCount = 20;
        public const int CachedMessagesCount = 15;
        public const int WorkersNumber = 4;

        public const string ConfigKey = "Config";
        public static double CheckSendingMesagesInterval = 5.0;     //seconds
        public static double CheckGetConfigInterval = 30 * 60.0;    //seconds
        public static double CheckPingInterval = 20.0;              //seconds
        public static double UpdateStatusInterval = 2.0;
        public static int VideoDownloadersCount = 3;
        public static int VideoUploadersCount = 3;
        public static int DocumentUploadersCount = 3;
        public static int AudioDownloadersCount = 3;
        public static int MaximumChunksCount = 3000;
        public static ulong MaximumUploadedFileSize = 512 * 1024 * 2000;

        public static string StateFileName = "state.dat";

        public const string IsAuthorizedKey = "IsAuthorized";
        public const int StickerMaxSize = 128*1024;         // 128 KB
        public const int SmallFileMaxSize = 10*1024*1024;   // 10 MB
    }
}
