﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Org.BouncyCastle.Math;
using Telegram.Api.Helpers;
using Telegram.Api.Services;
using Telegram.Api.TL;
using Telegram.Api.Transport;

namespace Telegram.Api.Tests
{
    [TestClass]
    public class MTProtoTests
    {
        private static readonly ITransport Transport = new HttpTransport("http://95.142.192.65:80/api");
        //private static readonly IAuthorizationHelper AuthHelper = new AuthorizationHelper(Transport);

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            //AuthHelper.Init();
        }


        [TestMethod]
        public void SignInMove()
        {
            
        }

        [TestMethod]
        public void SignIn()
        {
            var response = Program.SendSendCodeRequest(1, new TLString("79996620000"), TLSmsType.AppName_Code);

            //response = Program.SendAcknowledgments(2, response);
            var phoneCodeHash = ((TLSentCode)((TLRPCResult)response.Data).Object).PhoneCodeHash;
            response = Program.SendSignInRequest(3, new TLString("79996620000"), phoneCodeHash, new TLString("22222"));

            //Program.SendAcknowledgments(4, response);
        }

        [TestMethod]
        public void GetDialogs()
        {
            var response = Program.SendSendCodeRequest(1, new TLString("79996620000"), TLSmsType.AppName_Code);

            //response = Program.SendAcknowledgments(2, response);
            var phoneCodeHash = ((TLSentCode)((TLRPCResult)response.Data).Object).PhoneCodeHash;
            response = Program.SendSignInRequest(3, new TLString("79996620000"), phoneCodeHash, new TLString("22222"));

            //Program.SendAcknowledgments(4, response);
            response = Program.SendGetDialogsRequest(5, new TLInt(0), new TLInt(-1), new TLInt(100));
            //Program.SendAcknowledgments(6, response);
        }

        [TestMethod]
        public void GetContacts()
        {
            var response = Program.SendSendCodeRequest(1, new TLString("79996620000"), TLSmsType.AppName_Code);

            //response = Program.SendAcknowledgments(2, response);
            var phoneCodeHash = ((TLSentCode)((TLRPCResult)response.Data).Object).PhoneCodeHash;
            response = Program.SendSignInRequest(3, new TLString("79996620000"), phoneCodeHash, new TLString("22222"));

            //Program.SendAcknowledgments(4, response);
            response = Program.SendGetContactsRequest(5, new TLString(""));
            //Program.SendAcknowledgments(6, response);
        }

        [TestMethod]
        public void Ping()
        {
            var response = Program.SendPingRequest(0);

            //response = Program.SendAcknowledgments(0, response);
        }

        [TestMethod]
        public void SaveDeveloperInfo()
        {
            var response = Program.SendSaveDeveloperInfoRequest(1);

            //response = Program.SendAcknowledgments(2, response);
        }

        [TestMethod]
        public void GetNearestDC()
        {
            var response = Program.SendGetNearestDCRequest(1);

            //response = Program.SendAcknowledgments(2, response);
        }

        [TestMethod]
        public void GetConfig()
        {
            var response = Program.SendGetConfigRequest(1);

            //response = Program.SendAcknowledgments(2, response);
        }

        [TestMethod]
        public void SendCode()
        {
            var response = Program.SendSendCodeRequest(1, new TLString("79996620000"), TLSmsType.AppName_Code);

            //response = Program.SendAcknowledgments(2, response);
            var phoneCodeHash = ((TLSentCode)((TLRPCResult)response.Data).Object).PhoneCodeHash;
            response = Program.SendSignInRequest(3, new TLString("79996620000"), phoneCodeHash, new TLString("22222"));

            //Program.SendAcknowledgments(4, response);
        }

        [TestMethod]
        public void SendCall()
        {
            var response = Program.SendSendCodeRequest(1, new TLString("79996620000"), TLSmsType.AppName_Code);

            //response = Program.SendAcknowledgments(2, response);
            var phoneCodeHash = ((TLSentCode)((TLRPCResult)response.Data).Object).PhoneCodeHash;
            response = Program.SendSendCallRequest(3, new TLString("79996620000"), phoneCodeHash);
            //Program.SendAcknowledgments(4, response);
        }

        [TestMethod]
        public void PollarRho()
        {
            long pq = 1699187538047316311;
            var timer = Stopwatch.StartNew();
            //var p_q = Utils.GetPQ((ulong)pq);
            //Console.WriteLine("Simple " + timer.Elapsed);
            //Console.WriteLine(p_q.Item1);
            //Console.WriteLine(p_q.Item2);
            timer.Restart();
            var p1 = new BigInteger("1699187538047316311");
            Console.WriteLine("Polard n1 " + timer.Elapsed);
            timer.Restart();
            var p2 = new BigInteger(BitConverter.GetBytes(pq).Reverse().ToArray());
            Console.WriteLine("Polard n2 " + timer.Elapsed);
            timer.Restart();
            var p_q2 = PollardRho.Factor(p2);
            Console.WriteLine("Polard " + timer.Elapsed);
            Console.WriteLine(p_q2.Item1);
            Console.WriteLine(p_q2.Item2);
            Console.WriteLine((UInt64)p_q2.Item1.LongValue);
            Console.WriteLine();
        }
    }
}
