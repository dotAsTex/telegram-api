﻿using Caliburn.Micro;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Telegram.Api;
using Telegram.Api.Services;
using Telegram.Api.Services.Cache;
using Telegram.Api.Services.Connection;
using Telegram.Api.Services.Messages;
using Telegram.Api.Services.Updates;
using Telegram.Api.TL;
using Telegram.Api.TL.Functions.Stuff;
using Telegram.Api.Transport;
using System.Threading;

namespace TelegramReady
{
    class Program
    {

        static string PhoneNumber = "+79397366681";
        static TLString PhoneCodeHash = new TLString("ac17712fe607564ab9");
        static string Code = "28309";
        static InMemoryCacheService cacheService = new InMemoryCacheService(null);
        static UpdatesService updateService = new UpdatesService(cacheService, null);
        static TransportService transportService = new TransportService();
        static ConnectionService connectionService = new ConnectionService();
        static MTProtoService mtProtoService;
        static ManualResetEvent resetEvent = new ManualResetEvent(true); 
        
        static TLLong Salt;
        static TLLong SessionId;
        static bool debugAuthKey = false;
        
        static void Main(string[] args)
        {
            // BootStrapper bs = new BootStrapper();
            //  bs.Start();
            Telegram.Api.StaticExchanger.IoC.cacheService = cacheService;
            Telegram.Api.StaticExchanger.IoC.cacheService.Init();
            mtProtoService = new MTProtoService(updateService, Telegram.Api.StaticExchanger.IoC.cacheService, transportService, connectionService);
            mtProtoService.Initialized += MtProtoService_Initialized;
            Telegram.Api.StaticExchanger.IoC.mtProtoService = mtProtoService;
            int pos = 0;
            //Salt = new TLLong(0);
            //Salt.FromBytes(File.ReadAllBytes("configs/salt.txt"), ref pos);
            pos = 0;
            //SessionId = new TLLong(0);
            byte[] authKey;
            TLInt CurrentUserId;
            //SessionId.FromBytes(File.ReadAllBytes("configs/sessionid.txt"), ref pos);
            if (!debugAuthKey)

            {
                resetEvent.WaitOne();

                mtProtoService.UpdateUsernameAsync(new TLString("AsTex2"), callback =>
                {
                    Console.WriteLine(callback.FirstName);
                });
                
            /*    mtProtoService.SendCodeAsync(new TLString(PhoneNumber), TLSmsType.Code, callback =>
                 {

                     PhoneCodeHash = callback.PhoneCodeHash;
                     Console.WriteLine("Code sent");
                 },
                 attemptFailed =>
                 {
                     Console.WriteLine(attemptFailed);
                 },
                 faultCallback =>
                 {
                     Console.WriteLine(faultCallback.Message);
                 });
                Code = Console.ReadLine();
                mtProtoService.SignInAsync(new TLString(PhoneNumber), PhoneCodeHash, new TLString(Code), callback =>
                {
                    authKey = mtProtoService.GetActiveTransport().AuthKey;
                    var authKeyId = TLUtils.GenerateLongAuthKeyId(authKey);
                    Console.WriteLine(authKeyId);
                    File.WriteAllBytes("ak.txt", authKey);
                    CurrentUserId = mtProtoService.CurrentUserId;
                    List<TLUserBase> users = new List<TLUserBase>();
                    cacheService.TryCommit();
                    resetEvent.Set();
                });
                */
                //Console.WriteLine(BitConverter.ToString(mtProtoService.GetActiveTransport().AuthKey));
              //  File.WriteAllBytes("configs/authkey.txt", mtProtoService.GetActiveTransport().AuthKey);
               // Console.WriteLine(mtProtoService.GetActiveTransport().SessionId);
               // File.WriteAllBytes("configs/sessionid.txt", mtProtoService.GetActiveTransport().SessionId.ToBytes());
               // Console.WriteLine(mtProtoService.GetActiveTransport().Salt);
                //File.WriteAllBytes("configs/salt.txt", mtProtoService.GetActiveTransport().Salt.ToBytes());
            }
            else
            {
                //mtProtoService.SetParams(File.ReadAllBytes("configs/authkey.txt"));
            }
            Console.ReadKey();

        }

        private static void MtProtoService_Initialized(object sender, EventArgs e)
        {
            mtProtoService.GetActiveTransport().AuthKey = File.ReadAllBytes("ak.txt");

            resetEvent.Reset();
        }

        /// Writes the given object instance to a Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [JsonIgnore] attribute.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToJsonFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite);
                writer = new StreamWriter(filePath, append);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        /// <summary>
        /// Reads an object instance from an Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the Json file.</returns>
        public static T ReadFromJsonFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(filePath);
                var fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(fileContents);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
    }


}
