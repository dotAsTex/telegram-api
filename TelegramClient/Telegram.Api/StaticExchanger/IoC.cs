﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Api.Services;
using Telegram.Api.Services.Cache;

namespace Telegram.Api.StaticExchanger
{
    public static class IoC
    {
        public static IMTProtoService mtProtoService;
        public static ICacheService cacheService;
    }
}
