﻿using System;
using System.Threading;
using System.Windows;
using Telegram.Api.Extensions;
using Telegram.Api.TL;

namespace Telegram.Api.Helpers
{
    public static class Execute
    {
        public static void BeginOnThreadPool(TimeSpan delay, Action action)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                Thread.Sleep(delay);
                try
                {
                    action.SafeInvoke();
                }
                catch (Exception ex)
                {
                    TLUtils.WriteException(ex);
                }
            });
        }

        public static void BeginOnThreadPool(Action action)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                try
                {
                    action.SafeInvoke();
                }
                catch (Exception ex)
                {
                    TLUtils.WriteException(ex);
                }
            });
        }

        public static void BeginOnUIThread(Action action)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    action.SafeInvoke();
                }
                catch (Exception ex)
                {
                    TLUtils.WriteException(ex);
                }
            });
        }

        public static void BeginOnUIThread(TimeSpan delay, Action action)
        {
            BeginOnThreadPool(() =>
            {
                Thread.Sleep(delay);
                BeginOnUIThread(action);
            });
        }

        public static void ShowDebugMessage(string message)
        {
#if DEBUG
            BeginOnUIThread(() => MessageBox.Show(message));
#endif
        }
    }
}
