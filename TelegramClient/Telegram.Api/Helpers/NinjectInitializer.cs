﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Api.Services;
using Telegram.Api.Services.Cache;
using Telegram.Api.Services.Updates;
using Telegram.Api.Transport;

namespace Telegram.Api.Helpers
{
    class NinjectInitializer : NinjectModule
    {
        public override void Load()
        {
            Bind<IMTProtoService>().To<MTProtoService>();
            Bind<ICacheService>().To<InMemoryCacheService>();
            Bind<ITransportService>().To<TransportService>();
            Bind<IUpdatesService>().To<UpdatesService>();
        }
    }
}
