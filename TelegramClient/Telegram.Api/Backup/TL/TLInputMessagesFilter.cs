﻿namespace Telegram.Api.TL
{
    public abstract class TLInputMessagesFilterBase : TLObject { }

    public class TLInputMessagesFilterEmpty : TLInputMessagesFilterBase
    {
        public const uint Signature = TLConstructors.TLInputMessageFilterEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLInputMessagesFilterEmpty--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);
            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLInputMessagesFilterPhoto : TLInputMessagesFilterBase
    {
        public const uint Signature = TLConstructors.TLInputMessageFilterPhoto;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLInputMessagesFilterPhoto--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);
            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLInputMessagesFilterVideo : TLInputMessagesFilterBase
    {
        public const uint Signature = TLConstructors.TLInputMessageFilterVideo;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLInputMessagesFilterVideo--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);
            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLInputMessagesFilterPhotoVideo : TLInputMessagesFilterBase
    {
        public const uint Signature = TLConstructors.TLInputMessageFilterPhotoVideo;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLInputMessagesFilterPhotoVideo--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);
            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }
}
