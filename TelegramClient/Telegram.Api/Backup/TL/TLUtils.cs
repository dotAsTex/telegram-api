﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
#if SILVERLIGHT
#endif
using Caliburn.Micro;
using Telegram.Api.Helpers;
using Telegram.Api.Services;


namespace Telegram.Api.TL
{
    public static partial class TLUtils
    {
        public static bool IsDisplayedDecryptedDialogMessage(TLDecryptedMessageBase cachedMessage)
        {
            if (cachedMessage == null) return false;

            var serviceMessage = cachedMessage as TLDecryptedMessageService;
            if (serviceMessage != null)
            {
                //var emptyAction = serviceMessage.Action as TLDecryptedMessageActionEmpty;
                //if (emptyAction != null)
                //{
                //    return false;
                //}

                var notifyLayerAction = serviceMessage.Action as TLDecryptedMessageActionNotifyLayer;
                if (notifyLayerAction != null)
                {
                    return false;
                }

                var deleteMessagesAction = serviceMessage.Action as TLDecryptedMessageActionDeleteMessages;
                if (deleteMessagesAction != null)
                {
                    return false;
                }

                var readMessagesAction = serviceMessage.Action as TLDecryptedMessageActionReadMessages;
                if (readMessagesAction != null)
                {
                    return false;
                }

                var flushHistoryAction = serviceMessage.Action as TLDecryptedMessageActionFlushHistory;
                if (flushHistoryAction != null)
                {
                    return false;
                }

                var resendAction = serviceMessage.Action as TLDecryptedMessageActionResend;
                if (resendAction != null)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsDisplayedDecryptedMessage(TLDecryptedMessageBase cachedMessage)
        {
            if (cachedMessage == null) return false;

            var serviceMessage = cachedMessage as TLDecryptedMessageService;
            if (serviceMessage != null)
            {
                var emptyAction = serviceMessage.Action as TLDecryptedMessageActionEmpty;
                if (emptyAction != null)
                {
                    return false;
                }

                var notifyLayerAction = serviceMessage.Action as TLDecryptedMessageActionNotifyLayer;
                if (notifyLayerAction != null)
                {
                    return false;
                }

                var deleteMessagesAction = serviceMessage.Action as TLDecryptedMessageActionDeleteMessages;
                if (deleteMessagesAction != null)
                {
                    return false;
                }

                var readMessagesAction = serviceMessage.Action as TLDecryptedMessageActionReadMessages;
                if (readMessagesAction != null)
                {
                    return false;
                }

                var flushHistoryAction = serviceMessage.Action as TLDecryptedMessageActionFlushHistory;
                if (flushHistoryAction != null)
                {
                    return false;
                }

                var resendAction = serviceMessage.Action as TLDecryptedMessageActionResend;
                if (resendAction != null)
                {
                    return false;
                }
            }

            return true;
        }

        public static TLInt GetOutSeqNo(TLInt currentUserId, TLEncryptedChat17 chat)
        {
            var isAdmin = chat.AdminId.Value == currentUserId.Value;
            var seqNo = 2 * chat.RawOutSeqNo.Value + (isAdmin ? 1 : 0);

            return new TLInt(seqNo);
        }

        public static TLInt GetInSeqNo(TLInt currentUserId, TLEncryptedChat17 chat)
        {
            var isAdmin = chat.AdminId.Value == currentUserId.Value;
            var seqNo = 2 * chat.RawInSeqNo.Value + (isAdmin ? 0 : 1);

            return new TLInt(seqNo);
        }

        public static TLString EncryptMessage(TLObject decryptedMessage, TLEncryptedChatCommon chat)
        {
            var sha1 = new SHA1Managed();
            var random = new Random();

            var key = chat.Key.Data;
            var keyFingerprint = chat.KeyFingerprint;
            var decryptedBytes = decryptedMessage.ToBytes();
            var bytes = Combine(BitConverter.GetBytes(decryptedBytes.Length), decryptedBytes);
            var sha1Hash = sha1.ComputeHash(bytes);
            var msgKey = sha1Hash.SubArray(sha1Hash.Length - 16, 16);

            var padding = (bytes.Length % 16 == 0) ? 0 : (16 - (bytes.Length % 16));
            var paddingBytes = new byte[padding];
            random.NextBytes(paddingBytes);
            var bytesWithPadding = Combine(bytes, paddingBytes);

            var x = 0;
            var sha1_a = sha1.ComputeHash(Combine(msgKey, key.SubArray(x, 32)));
            var sha1_b = sha1.ComputeHash(Combine(key.SubArray(32 + x, 16), msgKey, key.SubArray(48 + x, 16)));
            var sha1_c = sha1.ComputeHash(Combine(key.SubArray(64 + x, 32), msgKey));
            var sha1_d = sha1.ComputeHash(Combine(msgKey, key.SubArray(96 + x, 32)));

            var aesKey = Combine(sha1_a.SubArray(0, 8), sha1_b.SubArray(8, 12), sha1_c.SubArray(4, 12));
            var aesIV = Combine(sha1_a.SubArray(8, 12), sha1_b.SubArray(0, 8), sha1_c.SubArray(16, 4), sha1_d.SubArray(0, 8));

            var encryptedBytes = Utils.AesIge(bytesWithPadding, aesKey, aesIV, true);

            var resultBytes = Combine(keyFingerprint.ToBytes(), msgKey, encryptedBytes);

            return TLString.FromBigEndianData(resultBytes);
        }

        public static TLDecryptedMessageBase DecryptMessage(TLString data, TLEncryptedChat chat)
        {
            var sha1 = new SHA1Managed();

            var bytes = data.Data;

            var keyFingerprint = bytes.SubArray(0, 8);  // secret chat key fingerprint
            var msgKey = bytes.SubArray(8, 16);
            var authKey = chat.Key.Data;

            var x = 0;
            var sha1_a = sha1.ComputeHash(Combine(msgKey, authKey.SubArray(x, 32)));
            var sha1_b = sha1.ComputeHash(Combine(authKey.SubArray(32 + x, 16), msgKey, authKey.SubArray(48 + x, 16)));
            var sha1_c = sha1.ComputeHash(Combine(authKey.SubArray(64 + x, 32), msgKey));
            var sha1_d = sha1.ComputeHash(Combine(msgKey, authKey.SubArray(96 + x, 32)));

            var aesKey = Combine(sha1_a.SubArray(0, 8), sha1_b.SubArray(8, 12), sha1_c.SubArray(4, 12));
            var aesIV = Combine(sha1_a.SubArray(8, 12), sha1_b.SubArray(0, 8), sha1_c.SubArray(16, 4), sha1_d.SubArray(0, 8));

            var encryptedBytes = bytes.SubArray(24, bytes.Length - 24);
            var decryptedBytes = Utils.AesIge(encryptedBytes, aesKey, aesIV, false);

            //check msgKey
            var length = BitConverter.ToInt32(decryptedBytes, 0);
            var calculatedMsgKey = sha1.ComputeHash(decryptedBytes.SubArray(0, 4 + length));
            //var calculatedMsgKey2 = sha1.ComputeHash(decryptedBytes);


            var position = 4;
            var decryptedObject = TLObject.GetObject<TLObject>(decryptedBytes, ref position);
            var decryptedMessageLayer = decryptedObject as TLDecryptedMessageLayer;
            var decryptedMessageLayer17 = decryptedObject as TLDecryptedMessageLayer17;
            TLDecryptedMessageBase decryptedMessage = null;

            if (decryptedMessageLayer17 != null)
            {
                decryptedMessage = decryptedMessageLayer17.Message;
                var decryptedMessage17 = decryptedMessage as ISeqNo;
                if (decryptedMessage17 != null)
                {
                    decryptedMessage17.InSeqNo = decryptedMessageLayer17.InSeqNo;
                    decryptedMessage17.OutSeqNo = decryptedMessageLayer17.OutSeqNo;
                }
            }
            else if (decryptedMessageLayer != null)
            {
                decryptedMessage = decryptedMessageLayer.Message;
            }
            else if (decryptedObject is TLDecryptedMessageBase)
            {
                decryptedMessage = (TLDecryptedMessageBase)decryptedObject;
            }

            return decryptedMessage;
        }


        public static bool DeleteFile(object syncRoot, string fileName)
        {
            try
            {
                lock (syncRoot)
                {
                    var storage = IsolatedStorageFile.GetUserStoreForApplication();

                    if (storage.FileExists(fileName))
                    {
                        storage.DeleteFile(fileName);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("FILE ERROR: cannot delete file " + fileName, LogSeverity.Error);
                WriteException(e);

                return false;
            }
            return true;
        }

        public static T OpenObjectFromFile<T>(object syncRoot, string fileName)
            where T : class
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.OpenOrCreate, file))
                    {
                        if (fileStream.Length > 0)
                        {
                            var serializer = new DataContractSerializer(typeof(T));
                            return serializer.ReadObject(fileStream) as T;
                        }

                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("FILE ERROR: cannot read " + typeof(T) + " from file " + fileName, LogSeverity.Error);
                WriteException(e);
            }
            return default(T);
        }

        public static T OpenObjectFromMTProtoFile<T>(object syncRoot, string fileName)
            where T : TLObject
        {
            try
            {
                lock (syncRoot)
                {
                    using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.OpenOrCreate, store))
                        {
                            if (fileStream.Length > 0)
                            {
                                return TLObject.GetObject<T>(fileStream);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("MTPROTO FILE ERROR: cannot read " + typeof(T) + " from file " + fileName, LogSeverity.Error);
                WriteException(e);
            }
            return default(T);
        }

        public static void SaveObjectToFile<T>(object syncRoot, string fileName, T data)
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.Create, file))
                    {
                        var dcs = new DataContractSerializer(typeof(T));
                        dcs.WriteObject(fileStream, data);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("FILE ERROR: cannot write " + typeof(T) + " to file " + fileName, LogSeverity.Error);
                WriteException(e);
            }
        }

        public static void SaveObjectToMTProtoFile<T>(object syncRoot, string fileName, T data)
            where T: TLObject
        {
            try
            {
                lock (syncRoot)
                {
                    using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                    {
                        var tempFileName = fileName + ".temp";
                        using (var fileStream = new IsolatedStorageFileStream(tempFileName, FileMode.Create, store))
                        {
                            data.ToStream(fileStream);
                        }
                        //var stopwatch = Stopwatch.StartNew();
                        store.CopyFile(tempFileName, fileName, true);
                        //store.DeleteFile(fileName);
                        //store.MoveFile(tempFileName, fileName);
                        //store.DeleteFile(tempFileName);
                        //WritePerformance("MoveFile time: " + stopwatch.Elapsed);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("MTPROTO FILE ERROR: cannot write " + typeof(T) + " to file " + fileName, LogSeverity.Error);
                WriteException(e);
            }
        }

        public static TLPeerBase GetPeerFromMessage(TLDecryptedMessageBase message)
        {
            TLPeerBase peer = null;
            var commonMessage = message as TLDecryptedMessageBase;
            if (commonMessage != null)
            {
                if (commonMessage.ChatId != null)
                {
                    peer = new TLPeerEncryptedChat{ Id = commonMessage.ChatId };
                }
            }
            else
            {
                WriteLine("Cannot get peer from non TLDecryptedMessage", LogSeverity.Error);
            }

            return peer;
        }

        public static TLPeerBase GetPeerFromMessage(TLMessageBase message)
        {
            TLPeerBase peer = null;
            var commonMessage = message as TLMessageCommon;
            if (commonMessage != null)
            {
                if (commonMessage.ToId is TLPeerChat)
                {
                    peer = commonMessage.ToId;
                }
                else
                {
                    if (commonMessage.Out.Value)
                    {
                        peer = commonMessage.ToId;
                    }
                    else
                    {
                        peer = new TLPeerUser { Id = commonMessage.FromId };
                    }
                }
            }
            else
            {
                WriteLine("Cannot get peer from non TLMessageCommon", LogSeverity.Error);
            }

            return peer;
        }

        public static bool InsertItem<T>(IList<T> items, T item, Func<T, long> getField, Func<T, long> equalitysField = null)
            where T : TLObject
        {
            var fieldValue = getField(item);
            for (var i = 0; i < items.Count; i++)
            {
                if (getField(items[i]) > fieldValue)
                {
                    items.Insert(i, item);
                    return true;
                }
                if (getField(items[i]) == fieldValue
                    && equalitysField != null
                    && equalitysField(items[i]) == equalitysField(item))
                {
                    return false;
                }
            }

            items.Add(item);
            return true;
        }

        public static bool InsertItemByDesc<T>(IList<T> items, T item, Func<T, long> getField, Func<T, long> equalityField = null)
            where T : TLObject
        {
            var fieldValue = getField(item);
            for (var i = 0; i < items.Count; i++)
            {
                if (getField(items[i]) < fieldValue)
                {
                    items.Insert(i, item);
                    return true;
                }
                if (getField(items[i]) == fieldValue
                    && equalityField != null
                    && equalityField(items[i]) == equalityField(item))
                {
                    return false;
                }
            }

            items.Add(item);
            return true;
        }

        public static IEnumerable<T> FindInnerObjects<T>(TLTransportMessage obj)
            where T : TLObject
        {
            var result = obj.MessageData as T;
            if (result != null)
            {
                yield return (T)obj.MessageData;
            }
            else
            {
                var gzipData = obj.MessageData as TLGzipPacked;
                if (gzipData != null)
                {
                    result = gzipData.Data as T;
                    if (result != null)
                    {
                        yield return result;
                    }
                }

                var container = obj.MessageData as TLContainer;
                if (container != null)
                {
                    foreach (var message in container.Messages)
                    {
                        result = message.MessageData as T;
                        if (result != null)
                        {
                            yield return (T)message.MessageData;
                        }
                    }
                }
            }
        }

        public static int InputPeerToId(TLInputPeerBase inputPeer, TLInt selfId)
        {
            var chat = inputPeer as TLInputPeerChat;
            if (chat != null)
            {
                return chat.ChatId.Value;
            }

            var contact = inputPeer as TLInputPeerContact;
            if (contact != null)
            {
                return contact.UserId.Value;
            }

            var foreign = inputPeer as TLInputPeerForeign;
            if (foreign != null)
            {
                return foreign.UserId.Value;
            }

            var self = inputPeer as TLInputPeerSelf;
            if (self != null)
            {
                return selfId.Value;
            }

            return -1;
        }

        public static TLPeerBase InputPeerToPeer(TLInputPeerBase inputPeer, int selfId)
        {
            var broadcast = inputPeer as TLInputPeerBroadcast;
            if (broadcast != null)
            {
                return new TLPeerBroadcast { Id = broadcast.ChatId };
            }

            var chat = inputPeer as TLInputPeerChat;
            if (chat != null)
            {
                return new TLPeerChat { Id = chat.ChatId };
            }

            var contact = inputPeer as TLInputPeerContact;
            if (contact != null)
            {
                return new TLPeerUser { Id = contact.UserId };
            }

            var foreign = inputPeer as TLInputPeerForeign;
            if (foreign != null)
            {
                return new TLPeerUser { Id = foreign.UserId };
            }

            var self = inputPeer as TLInputPeerSelf;
            if (self != null)
            {
                return new TLPeerUser { Id = new TLInt(selfId) };
            }

            return null;
        }


        public static int MergeItemsDesc<T>(Func<T, int> indexFunc, IList<T> current, IList<T> updated, int offset, int maxId, int count, out IList<T> removedItems, Func<T, int> skipFunc = null)
        {
            removedItems = new List<T>();

            var currentIndex = 0;
            var updatedIndex = 0;


            //skip just added or sending items
            while (updatedIndex < updated.Count
                && currentIndex < current.Count
                && indexFunc(updated[updatedIndex]) < indexFunc(current[currentIndex]))
            {
                currentIndex++;
            }

            // insert before current items
            while (updatedIndex < updated.Count
                && (current.Count < currentIndex 
                    || (currentIndex < current.Count && indexFunc(updated[updatedIndex]) > indexFunc(current[currentIndex]))))
            {
                if (indexFunc(current[currentIndex]) == 0)
                {
                    currentIndex++;
                    continue;
                }

                current.Insert(currentIndex, updated[updatedIndex]);
                updatedIndex++;
                currentIndex++;
            }

            // update existing items
            if (updatedIndex < updated.Count)
            {
                for (; currentIndex < current.Count; currentIndex++)
                {
                    if (skipFunc != null
                        && skipFunc(current[currentIndex]) == 0)
                    {
                        currentIndex++;
                        continue;
                    }

                    for (; updatedIndex < updated.Count; updatedIndex++)
                    {
                        // missing item at current list
                        if (indexFunc(updated[updatedIndex]) > indexFunc(current[currentIndex]))
                        {
                            current.Insert(currentIndex, updated[updatedIndex]);
                            updatedIndex++;
                            break;
                        }
                        // equal item
                        if (indexFunc(updated[updatedIndex]) == indexFunc(current[currentIndex]))
                        {
                            updatedIndex++;
                            break;
                        }
                        // deleted item
                        if (indexFunc(updated[updatedIndex]) < indexFunc(current[currentIndex]))
                        {
                            var removedItem = current[currentIndex];
                            removedItems.Add(removedItem);
                            current.RemoveAt(currentIndex);
                            currentIndex--;
                            break;
                        }
                    }

                    // at the end of updated list
                    if (updatedIndex == updated.Count)
                    {
                        currentIndex++;
                        break;
                    }
                }
            }


            // all other items were deleted
            if (updated.Count < count && current.Count != currentIndex)
            {
                for (var i = current.Count - 1; i >= updatedIndex; i--)
                {
                    current.RemoveAt(i);
                }
                return currentIndex - 1;
            }

            // add after current items
            while (updatedIndex < updated.Count)
            {
                current.Add(updated[updatedIndex]);
                updatedIndex++;
                currentIndex++;
            }

            return currentIndex - 1;
        }

        public static TLInt DateToUniversalTimeTLInt(long clientDelta, DateTime date)
        {
            clientDelta = IoC.Get<IMTProtoService>().ClientTicksDelta;

            var unixTime = (long)(Utils.DateTimeToUnixTimestamp(date) * 4294967296) + clientDelta; //int * 2^32 + clientDelta

            return new TLInt((int)(unixTime / 4294967296));
        }

        public static byte[] GenerateAuthKeyId(byte[] authKey)
        {
            var sha = new SHA1Managed();
            var authKeyHash = sha.ComputeHash(authKey);
            var authKeyId = authKeyHash.SubArray(12, 8);

            return authKeyId;
        }

        public static long GenerateLongAuthKeyId(byte[] authKey)
        {
            var sha = new SHA1Managed();
            var authKeyHash = sha.ComputeHash(authKey);
            var authKeyId = authKeyHash.SubArray(12, 8);

            return BitConverter.ToInt64(authKeyId, 0);
        }

        public static byte[] GetMsgKey(byte[] saveDeveloperInfoData)
        {
            //TLUtils.WriteLine("--Get MsgKey--");
            var sha = new SHA1Managed();
            var bytes = sha.ComputeHash(saveDeveloperInfoData);
            //TLUtils.WriteLine("Sha1 data without padding: " + BitConverter.ToString(bytes));
            var last16Bytes = bytes.SubArray(4, 16);
            //TLUtils.WriteLine("Last 16 bytes            : " + BitConverter.ToString(last16Bytes));
            return last16Bytes;
        }


        public static byte[] Combine(params byte[][] arrays)
        {
            var length = 0;
            for (int i = 0; i < arrays.Length; i++)
            {
                length += arrays[i].Length;
            }

            var result = new byte[length]; ////[arrays.Sum(a => a.Length)];
            var offset = 0;
            foreach (var array in arrays)
            {
                Buffer.BlockCopy(array, 0, result, offset, array.Length);
                offset += array.Length;
            }
            return result;
        }

        public static string Signature<T>(this T obj)
            where T : TLObject
        {
            var attr = typeof(T).GetCustomAttributes(typeof(SignatureAttribute), true);
            return ((SignatureAttribute)attr[0]).Value;
        }

        public static string MessageIdString(byte[] bytes)
        {
            var ticks = BitConverter.ToInt64(bytes, 0);
            var date = Utils.UnixTimestampToDateTime(ticks >> 32);

            return BitConverter.ToString(bytes) + " " 
                + ticks + "%4=" + ticks % 4 + " " 
                + date;
        }

        public static string MessageIdString(TLLong messageId)
        {
            var bytes = BitConverter.GetBytes(messageId.Value);
            var ticks = BitConverter.ToInt64(bytes, 0);
            var date = Utils.UnixTimestampToDateTime(ticks >> 32);

            return BitConverter.ToString(bytes) + " "
                + ticks + "%4=" + ticks % 4 + " "
                + date;
        }

        public static string MessageIdString(TLInt messageId)
        {
            var ticks = messageId.Value;
            var date = Utils.UnixTimestampToDateTime(ticks >> 32);

            return BitConverter.ToString(BitConverter.GetBytes(messageId.Value)) + " "
                + ticks + "%4=" + ticks % 4 + " "
                + date;
        }

        public static DateTime ToDateTime(TLInt date)
        {
            var ticks = date.Value;
            return Utils.UnixTimestampToDateTime(ticks >> 32);
        }

        public static void ThrowNotSupportedException(this byte[] bytes, string objectType)
        {
            throw new NotSupportedException(String.Format("Not supported {0} signature: {1}", objectType, BitConverter.ToString(bytes.SubArray(0, 4))));
        }

        public static void ThrowNotSupportedException(this byte[] bytes, int position, string objectType)
        {
            throw new NotSupportedException(String.Format("Not supported {0} signature: {1}", objectType, BitConverter.ToString(bytes.SubArray(position, position + 4))));
        }

        public static void ThrowExceptionIfIncorrect(this byte[] bytes, ref int position, uint signature)
        {
            //if (!bytes.SubArray(position, 4).StartsWith(signature))
            //{
            //    throw new ArgumentException(String.Format("Incorrect signature: actual - {1}, expected - {0}", SignatureToBytesString(signature), BitConverter.ToString(bytes.SubArray(0, 4))));
            //}
            position += 4;
        }

        public static void ThrowExceptionIfIncorrect(this byte[] bytes, ref int position, string signature)
        {
            //if (!bytes.SubArray(position, 4).StartsWith(signature))
            //{
            //    throw new ArgumentException(String.Format("Incorrect signature: actual - {1}, expected - {0}", SignatureToBytesString(signature), BitConverter.ToString(bytes.SubArray(0, 4))));
            //}
            position += 4;
        }

        private static bool StartsWith(this byte[] array, byte[] startArray)
        {
            for (var i = 0; i < startArray.Length; i++)
            {
                if (array[i] != startArray[i]) return false;
            }
            return true;
        }

        private static bool StartsWith(this byte[] array, int position, byte[] startArray)
        {
            for (var i = 0; i < startArray.Length; i++)
            {
                if (array[position + i] != startArray[i]) return false;
            }
            return true;
        }

        public static bool StartsWith(this byte[] bytes, uint signature)
        {
            var sign = BitConverter.ToUInt32(bytes, 0);

            return sign == signature;
        }

        public static bool StartsWith(this Stream input, uint signature)
        {
            var bytes = new byte[4];
            input.Read(bytes, 0, 4);
            var sign = BitConverter.ToUInt32(bytes, 0);

            return sign == signature;
        }

        public static bool StartsWith(this byte[] bytes, string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            var signatureBytes = SignatureToBytes(signature);

            return bytes.StartsWith(signatureBytes);
        }

        public static bool StartsWith(this byte[] bytes, int position, uint signature)
        {
            var sign = BitConverter.ToUInt32(bytes, position);

            return sign == signature;
        }

        public static bool StartsWith(this byte[] bytes, int position, string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            var signatureBytes = SignatureToBytes(signature);

            return bytes.StartsWith(position, signatureBytes);
        }

        public static string SignatureToBytesString(string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            return BitConverter.ToString(SignatureToBytes(signature));
        }

        public static byte[] SignatureToBytes(uint signature)
        {
            return BitConverter.GetBytes(signature);
        }

        public static byte[] SignatureToBytes(string signature)
        {
            if (signature[0]!= '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);
            
            var bytesString = 
                signature.Length % 2 == 0?
                new string(signature.Replace("#", "0").ToArray()):
                new string(signature.Replace("#", String.Empty).ToArray());

            var bytes = Utils.StringToByteArray(bytesString);
            Array.Reverse(bytes);
            return bytes;
        }

    }
}
