﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Telegram.Api.TL
{
    class TLObjectGenerator
    {
        private static readonly Dictionary<Type, Func<TLObject>> _baredTypes =
            new Dictionary<Type, Func<TLObject>>
                {
                    {typeof (TLDouble), () => new TLDouble()},
                    {typeof (TLBool), () => new TLBool()},
                    {typeof (TLInt), () => new TLInt()},
                    {typeof (TLLong), () => new TLLong()},
                    {typeof (TLInt128), () => new TLInt128()},
                    {typeof (TLInt256), () => new TLInt256()},
                    {typeof (TLString), () => new TLString()},
                    {typeof (TLNonEncryptedMessage), () => new TLNonEncryptedMessage()},
                    {typeof (TLTransportMessage), () => new TLTransportMessage()},
                    {typeof (TLContainerTransportMessage), () => new TLContainerTransportMessage()},
                };

        private static readonly Dictionary<uint, Func<TLObject>> _clothedTypes =
            new Dictionary<uint, Func<TLObject>>
                {
                    {TL.TLUpdateUserBlocked.Signature, () => new TLUpdateUserBlocked()},
                    {TL.TLUpdateNotifySettings.Signature, () => new TLUpdateNotifySettings()},

                    {TL.TLNotifyPeer.Signature, () => new TLNotifyPeer()},
                    {TL.TLNotifyUsers.Signature, () => new TLNotifyUsers()},
                    {TL.TLNotifyChats.Signature, () => new TLNotifyChats()},
                    {TL.TLNotifyAll.Signature, () => new TLNotifyAll()},

                    {TL.TLDecryptedMessageLayer.Signature, () => new TLDecryptedMessageLayer()},
                    {TL.TLUpdateDCOptions.Signature, () => new TLUpdateDCOptions()},
                    
                    {TL.TLDecryptedMessageMediaAudio.Signature, () => new TLDecryptedMessageMediaAudio()},
                    {TL.TLDecryptedMessageMediaDocument.Signature, () => new TLDecryptedMessageMediaDocument()},

                    {TL.TLInputMediaDocument.Signature, () => new TLInputMediaDocument()},
                    {TL.TLInputMediaUploadedDocument.Signature, () => new TLInputMediaUploadedDocument()},
                    {TL.TLInputMediaUploadedThumbDocument.Signature, () => new TLInputMediaUploadedThumbDocument()},

                    {TL.TLInputMediaAudio.Signature, () => new TLInputMediaAudio()},
                    {TL.TLInputMediaUploadedAudio.Signature, () => new TLInputMediaUploadedAudio()}, 
                
                    {TL.TLInputDocument.Signature, () => new TLInputDocument()},
                    {TL.TLInputDocumentEmpty.Signature, () => new TLInputDocumentEmpty()}, 
                
                    {TL.TLInputAudio.Signature, () => new TLInputAudio()},
                    {TL.TLInputAudioEmpty.Signature, () => new TLInputAudioEmpty()},

                    {TL.TLMessageMediaAudio.Signature, () => new TLMessageMediaAudio()},
                    {TL.TLMessageMediaDocument.Signature, () => new TLMessageMediaDocument()},

                    {TL.TLAudioEmpty.Signature, () => new TLAudioEmpty()},
                    {TL.TLAudio.Signature, () => new TLAudio()},

                    {TL.TLDocumentEmpty.Signature, () => new TLDocumentEmpty()},
                    {TL.TLDocument10.Signature, () => new TLDocument10()},

                    {TL.TLUpdateChatParticipantAdd.Signature, () => new TLUpdateChatParticipantAdd()},
                    {TL.TLUpdateChatParticipantDelete.Signature, () => new TLUpdateChatParticipantDelete()},

                    {TL.TLInputEncryptedFileBigUploaded.Signature, () => new TLInputEncryptedFileBigUploaded()},
                    {TL.TLInputFileBig.Signature, () => new TLInputFileBig()},

                    {TL.TLDecryptedMessageActionSetMessageTTL.Signature, () => new TLDecryptedMessageActionSetMessageTTL()},
                    {TL.TLDecryptedMessageActionReadMessages.Signature, () => new TLDecryptedMessageActionReadMessages()},
                    {TL.TLDecryptedMessageActionDeleteMessages.Signature, () => new TLDecryptedMessageActionDeleteMessages()},
                    {TL.TLDecryptedMessageActionScreenshotMessages.Signature, () => new TLDecryptedMessageActionScreenshotMessages()},
                    {TL.TLDecryptedMessageActionFlushHistory.Signature, () => new TLDecryptedMessageActionFlushHistory()},
                    {TL.TLDecryptedMessageActionNotifyLayer.Signature, () => new TLDecryptedMessageActionNotifyLayer()},

                    {TL.TLDecryptedMessage.Signature, () => new TLDecryptedMessage()},
                    {TL.TLDecryptedMessageService.Signature, () => new TLDecryptedMessageService()},

                    {TL.TLUpdateNewEncryptedMessage.Signature, () => new TLUpdateNewEncryptedMessage()},
                    {TL.TLUpdateEncryptedChatTyping.Signature, () => new TLUpdateEncryptedChatTyping()},
                    {TL.TLUpdateEncryption.Signature, () => new TLUpdateEncryption()},
                    {TL.TLUpdateEncryptedMessagesRead.Signature, () => new TLUpdateEncryptedMessagesRead()},
                    
                    {TL.TLEncryptedChatEmpty.Signature, () => new TLEncryptedChatEmpty()},
                    {TL.TLEncryptedChatWaiting.Signature, () => new TLEncryptedChatWaiting()},
                    {TL.TLEncryptedChatRequested.Signature, () => new TLEncryptedChatRequested()},
                    {TL.TLEncryptedChat.Signature, () => new TLEncryptedChat()},
                    {TL.TLEncryptedChatDiscarded.Signature, () => new TLEncryptedChatDiscarded()},
                    
                    {TL.TLInputEncryptedChat.Signature, () => new TLInputEncryptedChat()},

                    {TL.TLInputEncryptedFileEmpty.Signature, () => new TLInputEncryptedFileEmpty()},
                    {TL.TLInputEncryptedFileUploaded.Signature, () => new TLInputEncryptedFileUploaded()},
                    {TL.TLInputEncryptedFile.Signature, () => new TLInputEncryptedFile()},

                    {TL.TLInputEncryptedFileLocation.Signature, () => new TLInputEncryptedFileLocation()},

                    {TL.TLEncryptedFileEmpty.Signature, () => new TLEncryptedFileEmpty()},
                    {TL.TLEncryptedFile.Signature, () => new TLEncryptedFile()},

                    {TL.TLEncryptedMessage.Signature, () => new TLEncryptedMessage()},
                    {TL.TLEncryptedMessageService.Signature, () => new TLEncryptedMessageService()},

                    {TL.TLDecryptedMessageMediaEmpty.Signature, () => new TLDecryptedMessageMediaEmpty()},
                    {TL.TLDecryptedMessageMediaPhoto.Signature, () => new TLDecryptedMessageMediaPhoto()},
                    {TL.TLDecryptedMessageMediaVideo.Signature, () => new TLDecryptedMessageMediaVideo()},
                    {TL.TLDecryptedMessageMediaGeoPoint.Signature, () => new TLDecryptedMessageMediaGeoPoint()},
                    {TL.TLDecryptedMessageMediaContact.Signature, () => new TLDecryptedMessageMediaContact()},
                    
                    {TL.TLDHConfig.Signature, () => new TLDHConfig()},
                    {TL.TLDHConfigNotModified.Signature, () => new TLDHConfigNotModified()},
                    
                    {TL.TLSentEncryptedMessage.Signature, () => new TLSentEncryptedMessage()},
                    {TL.TLSentEncryptedFile.Signature, () => new TLSentEncryptedFile()},

                    {TL.TLMessageDetailedInfo.Signature, () => new TLMessageDetailedInfo()},
                    {TL.TLMessageNewDetailedInfo.Signature, () => new TLMessageNewDetailedInfo()},
                    {TL.TLMessagesAllInfo.Signature, () => new TLMessagesAllInfo()},

                    {TL.TLUpdateNewMessage.Signature, () => new TLUpdateNewMessage()},
                    {TL.TLUpdateMessageId.Signature, () => new TLUpdateMessageId()},
                    {TL.TLUpdateReadMessages.Signature, () => new TLUpdateReadMessages()},
                    {TL.TLUpdateDeleteMessages.Signature, () => new TLUpdateDeleteMessages()},
                    {TL.TLUpdateRestoreMessages.Signature, () => new TLUpdateRestoreMessages()},
                    {TL.TLUpdateUserTyping.Signature, () => new TLUpdateUserTyping()},
                    {TL.TLUpdateChatUserTyping.Signature, () => new TLUpdateChatUserTyping()},
                    {TL.TLUpdateChatParticipants.Signature, () => new TLUpdateChatParticipants()},
                    {TL.TLUpdateUserStatus.Signature, () => new TLUpdateUserStatus()},
                    {TL.TLUpdateUserName.Signature, () => new TLUpdateUserName()},
                    {TL.TLUpdateUserPhoto.Signature, () => new TLUpdateUserPhoto()},
                    {TL.TLUpdateContactRegistered.Signature, () => new TLUpdateContactRegistered()},
                    {TL.TLUpdateContactLink.Signature, () => new TLUpdateContactLink()},
                    {TL.TLUpdateActivation.Signature, () => new TLUpdateActivation()},
                    {TL.TLUpdateNewAuthorization.Signature, () => new TLUpdateNewAuthorization()},

                    {TL.TLDifferenceEmpty.Signature, () => new TLDifferenceEmpty()},
                    {TL.TLDifference.Signature, () => new TLDifference()},
                    {TL.TLDifferenceSlice.Signature, () => new TLDifferenceSlice()},

                    {TL.TLUpdatesTooLong.Signature, () => new TLUpdatesTooLong()},
                    {TLUpdatesShortMessage.Signature, () => new TLUpdatesShortMessage()},
                    {TLUpdatesShortChatMessage.Signature, () => new TLUpdatesShortChatMessage()},
                    {TLUpdatesShort.Signature, () => new TLUpdatesShort()},
                    {TL.TLUpdatesCombined.Signature, () => new TLUpdatesCombined()},
                    {TL.TLUpdates.Signature, () => new TLUpdates()},

                    {TL.TLFutureSalt.Signature, () => new TLFutureSalt()},
                    {TL.TLFutureSalts.Signature, () => new TLFutureSalts()},

                    {TL.TLGzipPacked.Signature, () => new TLGzipPacked()},
                    {TL.TLState.Signature, () => new TLState()},

                    {TL.TLFileTypeUnknown.Signature, () => new TLFileTypeUnknown()},
                    {TL.TLFileTypeJpeg.Signature, () => new TLFileTypeJpeg()},
                    {TL.TLFileTypeGif.Signature, () => new TLFileTypeGif()},
                    {TL.TLFileTypePng.Signature, () => new TLFileTypePng()},
                    {TL.TLFileTypeMp3.Signature, () => new TLFileTypeMp3()},
                    {TL.TLFileTypeMov.Signature, () => new TLFileTypeMov()},
                    {TL.TLFileTypePartial.Signature, () => new TLFileTypePartial()},
                    {TL.TLFileTypeMp4.Signature, () => new TLFileTypeMp4()},
                    {TL.TLFileTypeWebp.Signature, () => new TLFileTypeWebp()},


                    {TL.TLFile.Signature, () => new TLFile()},
                    
                    {TL.TLInputFileLocation.Signature, () => new TLInputFileLocation()},
                    {TL.TLInputVideoFileLocation.Signature, () => new TLInputVideoFileLocation()},

                    {TL.TLInviteText.Signature, () => new TLInviteText()},

                    {TL.TLDHGenOk.Signature, () => new TLDHGenOk()},
                    {TL.TLDHGenRetry.Signature, () => new TLDHGenRetry()},
                    {TL.TLDHGenFail.Signature, () => new TLDHGenFail()},

                    {TL.TLServerDHInnerData.Signature, () => new TLServerDHInnerData()},
                    {TL.TLServerDHParamsFail.Signature, () => new TLServerDHParamsFail()},
                    {TL.TLServerDHParamsOk.Signature, () => new TLServerDHParamsOk()},
                    {TL.TLPQInnerData.Signature, () => new TLPQInnerData()},
                    {TL.TLResPQ.Signature, () => new TLResPQ()},

                    {TL.TLContactsBlocked.Signature, () => new TLContactsBlocked()},
                    {TL.TLContactsBlockedSlice.Signature, () => new TLContactsBlockedSlice()},
                    {TL.TLContactBlocked.Signature, () => new TLContactBlocked()},
                    
                    {TL.TLImportedContacts.Signature, () => new TLImportedContacts()},
                    {TL.TLImportedContact.Signature, () => new TLImportedContact()},

                    {TL.TLInputContact.Signature, () => new TLInputContact()},

                    {TL.TLContactStatus.Signature, () => new TLContactStatus()},

                    {TL.TLForeignLinkUnknown.Signature, () => new TLForeignLinkUnknown()},
                    {TL.TLForeignLinkRequested.Signature, () => new TLForeignLinkRequested()},
                    {TL.TLForeignLinkMutual.Signature, () => new TLForeignLinkMutual()},

                    {TL.TLMyLinkEmpty.Signature, () => new TLMyLinkEmpty()},
                    {TL.TLMyLinkContact.Signature, () => new TLMyLinkContact()},
                    {TL.TLMyLinkRequested.Signature, () => new TLMyLinkRequested()},

                    {TL.TLLink.Signature, () => new TLLink()},

                    {TL.TLUserFull.Signature, () => new TLUserFull()},
                    
                    {TL.TLPhotos.Signature, () => new TLPhotos()},
                    {TL.TLPhotosSlice.Signature, () => new TLPhotosSlice()},
                    {TL.TLPhotosPhoto.Signature, () => new TLPhotosPhoto()},

                    {TL.TLInputPeerNotifyEventsEmpty.Signature, () => new TLInputPeerNotifyEventsEmpty()},
                    {TL.TLInputPeerNotifyEventsAll.Signature, () => new TLInputPeerNotifyEventsAll()},

                    {TL.TLInputPeerNotifySettings.Signature, () => new TLInputPeerNotifySettings()},

                    {TL.TLInputNotifyPeer.Signature, () => new TLInputNotifyPeer()},
                    {TL.TLInputNotifyUsers.Signature, () => new TLInputNotifyUsers()},
                    {TL.TLInputNotifyChats.Signature, () => new TLInputNotifyChats()},
                    {TL.TLInputNotifyAll.Signature, () => new TLInputNotifyAll()},

                    {TL.TLInputUserEmpty.Signature, () => new TLInputUserEmpty()},
                    {TL.TLInputUserSelf.Signature, () => new TLInputUserSelf()},
                    {TL.TLInputUserContact.Signature, () => new TLInputUserContact()},
                    {TL.TLInputUserForeign.Signature, () => new TLInputUserForeign()},

                    {TL.TLInputPhotoCropAuto.Signature, () => new TLInputPhotoCropAuto()},
                    {TL.TLInputPhotoCrop.Signature, () => new TLInputPhotoCrop()},

                    {TL.TLInputChatPhotoEmpty.Signature, () => new TLInputChatPhotoEmpty()},
                    {TL.TLInputChatUploadedPhoto.Signature, () => new TLInputChatUploadedPhoto()},
                    {TL.TLInputChatPhoto.Signature, () => new TLInputChatPhoto()},

                    {TL.TLMessagesChatFull.Signature, () => new TLMessagesChatFull()},
                    {TL.TLChatFull.Signature, () => new TLChatFull()},

                    {TL.TLChatParticipant.Signature, () => new TLChatParticipant()},

                    {TL.TLChatParticipantsForbidden.Signature, () => new TLChatParticipantsForbidden()},
                    {TL.TLChatParticipants.Signature, () => new TLChatParticipants()},

                    {TL.TLPeerNotifySettingsEmpty.Signature, () => new TLPeerNotifySettingsEmpty()},
                    {TL.TLPeerNotifySettings.Signature, () => new TLPeerNotifySettings()},

                    {TL.TLPeerNotifyEventsEmpty.Signature, () => new TLPeerNotifyEventsEmpty()},
                    {TL.TLPeerNotifyEventsAll.Signature, () => new TLPeerNotifyEventsAll()},

                    {TL.TLChats.Signature, () => new TLChats()},

                    {TL.TLMessages.Signature, () => new TLMessages()},
                    {TL.TLMessagesSlice.Signature, () => new TLMessagesSlice()},

                    {TL.TLExportedAuthorization.Signature, () => new TLExportedAuthorization()},                   

                    {TL.TLInputFile.Signature, () => new TLInputFile()},
                    {TL.TLInputPhotoEmpty.Signature, () => new TLInputPhotoEmpty()},
                    {TL.TLInputPhoto.Signature, () => new TLInputPhoto()},
                    {TL.TLInputGeoPoint.Signature, () => new TLInputGeoPoint()}, 
                    {TL.TLInputGeoPointEmpty.Signature, () => new TLInputGeoPointEmpty()},
                    {TL.TLInputVideo.Signature, () => new TLInputVideo()}, 
                    {TL.TLInputVideoEmpty.Signature, () => new TLInputVideoEmpty()},

                    {TL.TLInputMediaEmpty.Signature, () => new TLInputMediaEmpty()},
                    {TL.TLInputMediaUploadedPhoto.Signature, () => new TLInputMediaUploadedPhoto()},
                    {TL.TLInputMediaPhoto.Signature, () => new TLInputMediaPhoto()},
                    {TL.TLInputMediaGeoPoint.Signature, () => new TLInputMediaGeoPoint()}, 
                    {TL.TLInputMediaContact.Signature, () => new TLInputMediaContact()},
                    {TL.TLInputMediaUploadedVideo.Signature, () => new TLInputMediaUploadedVideo()},
                    {TL.TLInputMediaUploadedThumbVideo.Signature, () => new TLInputMediaUploadedThumbVideo()},
                    {TL.TLInputMediaVideo.Signature, () => new TLInputMediaVideo()},

                    {TLInputMessagesFilterEmpty.Signature, () => new TLInputMessagesFilterEmpty()},
                    {TLInputMessagesFilterPhoto.Signature, () => new TLInputMessagesFilterPhoto()},
                    {TLInputMessagesFilterVideo.Signature, () => new TLInputMessagesFilterVideo()},
                    {TLInputMessagesFilterPhotoVideo.Signature, () => new TLInputMessagesFilterPhotoVideo()}, 

                    {TL.TLSentMessageLink.Signature, () => new TLSentMessageLink()},
                    {TL.TLStatedMessage.Signature, () => new TLStatedMessage()},
                    {TL.TLStatedMessageLink.Signature, () => new TLStatedMessageLink()},
                    {TL.TLStatedMessages.Signature, () => new TLStatedMessages()},
                    {TL.TLStatedMessagesLinks.Signature, () => new TLStatedMessagesLinks()},

                    {TL.TLAffectedHistory.Signature, () => new TLAffectedHistory()},

                    {TL.TLNull.Signature, () => new TLNull()},               

                    {TLBool.BoolTrue, () => new TLBool()},
                    {TLBool.BoolFalse, () => new TLBool()},
                    
                    {TL.TLChatEmpty.Signature, () => new TLChatEmpty()},
                    {TL.TLChat.Signature, () => new TLChat()},
                    {TL.TLChatForbidden.Signature, () => new TLChatForbidden()},

                    {TL.TLSentMessage.Signature, () => new TLSentMessage()},

                    {TL.TLMessageEmpty.Signature, () => new TLMessageEmpty()},
                    {TL.TLMessage.Signature, () => new TLMessage()},
                    {TL.TLMessageForwarded.Signature, () => new TLMessageForwarded()},
                    {TL.TLMessageService.Signature, () => new TLMessageService()},                   

                    {TL.TLMessageMediaEmpty.Signature, () => new TLMessageMediaEmpty()},
                    {TL.TLMessageMediaPhoto.Signature, () => new TLMessageMediaPhoto()},
                    {TL.TLMessageMediaVideo.Signature, () => new TLMessageMediaVideo()},
                    {TL.TLMessageMediaGeo.Signature, () => new TLMessageMediaGeo()},
                    {TL.TLMessageMediaContact.Signature, () => new TLMessageMediaContact()},
                    {TL.TLMessageMediaUnsupported.Signature, () => new TLMessageMediaUnsupported()},

                    {TL.TLMessageActionEmpty.Signature, () => new TLMessageActionEmpty()},
                    {TL.TLMessageActionChatCreate.Signature, () => new TLMessageActionChatCreate()},
                    {TL.TLMessageActionChatEditTitle.Signature, () => new TLMessageActionChatEditTitle()},
                    {TL.TLMessageActionChatEditPhoto.Signature, () => new TLMessageActionChatEditPhoto()},
                    {TL.TLMessageActionChatDeletePhoto.Signature, () => new TLMessageActionChatDeletePhoto()},
                    {TL.TLMessageActionChatAddUser.Signature, () => new TLMessageActionChatAddUser()},
                    {TL.TLMessageActionChatDeleteUser.Signature, () => new TLMessageActionChatDeleteUser()},

                    {TL.TLPhoto.Signature, () => new TLPhoto()},
                    {TL.TLPhotoEmpty.Signature, () => new TLPhotoEmpty()},

                    {TL.TLPhotoSize.Signature, () => new TLPhotoSize()},
                    {TL.TLPhotoSizeEmpty.Signature, () => new TLPhotoSizeEmpty()},
                    {TL.TLPhotoCachedSize.Signature, () => new TLPhotoCachedSize()},

                    {TL.TLVideoEmpty.Signature, () => new TLVideoEmpty()},
                    {TL.TLVideo.Signature, () => new TLVideo()},

                    {TL.TLGeoPointEmpty.Signature, () => new TLGeoPointEmpty()},
                    {TL.TLGeoPoint.Signature, () => new TLGeoPoint()},

                    {TL.TLDialog.Signature, () => new TLDialog()},
                    {TL.TLDialogs.Signature, () => new TLDialogs()},
                    {TL.TLDialogsSlice.Signature, () => new TLDialogsSlice()},

                    {TL.TLInputPeerEmpty.Signature, () => new TLInputPeerEmpty()},
                    {TL.TLInputPeerSelf.Signature, () => new TLInputPeerSelf()},
                    {TL.TLInputPeerContact.Signature, () => new TLInputPeerContact()},
                    {TL.TLInputPeerForeign.Signature, () => new TLInputPeerForeign()},
                    {TL.TLInputPeerChat.Signature, () => new TLInputPeerChat()},
                    
                    {TL.TLPeerUser.Signature, () => new TLPeerUser()},
                    {TL.TLPeerChat.Signature, () => new TLPeerChat()},

                    {TL.TLUserStatusEmpty.Signature, () => new TLUserStatusEmpty()},
                    {TL.TLUserStatusOnline.Signature, () => new TLUserStatusOnline()},
                    {TL.TLUserStatusOffline.Signature, () => new TLUserStatusOffline()},

                    {TL.TLChatPhotoEmpty.Signature, () => new TLChatPhotoEmpty()},
                    {TL.TLChatPhoto.Signature, () => new TLChatPhoto()},
                    {TL.TLUserProfilePhotoEmpty.Signature, () => new TLUserProfilePhotoEmpty()},
                    {TL.TLUserProfilePhoto.Signature, () => new TLUserProfilePhoto()},
                    
                    {TL.TLUserEmpty.Signature, () => new TLUserEmpty()},
                    {TL.TLUserSelf.Signature, () => new TLUserSelf()},
                    {TL.TLUserContact.Signature, () => new TLUserContact()},
                    {TL.TLUserRequest.Signature, () => new TLUserRequest()},
                    {TL.TLUserForeign.Signature, () => new TLUserForeign()},
                    {TL.TLUserDeleted.Signature, () => new TLUserDeleted()},

                    {TL.TLSentCode.Signature, () => new TLSentCode()},

                    {TL.TLRPCResult.Signature, () => new TLRPCResult()},

                    {TL.TLRPCError.Signature, () => new TLRPCError()},

                    {TL.TLNewSessionCreated.Signature, () => new TLNewSessionCreated()},

                    {TL.TLNearestDC.Signature, () => new TLNearestDC()},

                    {TL.TLMessagesAcknowledgment.Signature, () => new TLMessagesAcknowledgment()},

                    {TL.TLContainer.Signature, () => new TLContainer()},

                    {TL.TLFileLocationUnavailable.Signature, () => new TLFileLocationUnavailable()},
                    {TL.TLFileLocation.Signature, () => new TLFileLocation()},

                    {TL.TLDCOption.Signature, () => new TLDCOption()},

                    {TL.TLContacts.Signature, () => new TLContacts()},
                    {TL.TLContactsNotModified.Signature, () => new TLContactsNotModified()},

                    {TL.TLContact.Signature, () => new TLContact()},

                    {TL.TLConfig.Signature, () => new TLConfig()},

                    {TL.TLCheckedPhone.Signature, () => new TLCheckedPhone()},

                    {TL.TLBadServerSalt.Signature, () => new TLBadServerSalt()},
                    {TL.TLBadMessageNotification.Signature, () => new TLBadMessageNotification()},

                    {TL.TLAuthorization.Signature, () => new TLAuthorization()},

                    {TL.TLPong.Signature, () => new TLPong()},
                    {TLWallPaper.Signature, () => new TLWallPaper()},
                    {TLWallPaperSolid.Signature, () => new TLWallPaperSolid()},
                    
                    {TL.TLSupport.Signature, () => new TLSupport()},

                    //16 layer
                    {TLSentAppCode.Signature, () => new TLSentAppCode()},

                    //17 layer
                    {TLSendMessageTypingAction.Signature, () => new TLSendMessageTypingAction()},
                    {TLSendMessageCancelAction.Signature, () => new TLSendMessageCancelAction()},
                    {TLSendMessageRecordVideoAction.Signature, () => new TLSendMessageRecordVideoAction()},
                    {TLSendMessageUploadVideoAction.Signature, () => new TLSendMessageUploadVideoAction()},
                    {TLSendMessageRecordAudioAction.Signature, () => new TLSendMessageRecordAudioAction()},
                    {TLSendMessageUploadAudioAction.Signature, () => new TLSendMessageUploadAudioAction()},
                    {TLSendMessageUploadPhotoAction.Signature, () => new TLSendMessageUploadPhotoAction()},
                    {TLSendMessageUploadDocumentAction.Signature, () => new TLSendMessageUploadDocumentAction()},
                    {TLSendMessageGeoLocationAction.Signature, () => new TLSendMessageGeoLocationAction()},
                    {TLSendMessageChooseContactAction.Signature, () => new TLSendMessageChooseContactAction()},
                    
                    {TLUpdateUserTyping17.Signature, () => new TLUpdateUserTyping17()},
                    {TLUpdateChatUserTyping17.Signature, () => new TLUpdateChatUserTyping17()},
                    
                    {TLMessage17.Signature, () => new TLMessage17()},
                    {TLMessageForwarded17.Signature, () => new TLMessageForwarded17()},
                    {TLMessageService17.Signature, () => new TLMessageService17()},

                    //17 layer encrypted
                    {TLDecryptedMessage17.Signature, () => new TLDecryptedMessage17()},          
                    {TLDecryptedMessageService17.Signature, () => new TLDecryptedMessageService17()},
                    {TLDecryptedMessageMediaAudio17.Signature, () => new TLDecryptedMessageMediaAudio17()},
                    {TLDecryptedMessageMediaVideo17.Signature, () => new TLDecryptedMessageMediaVideo17()},
                    {TLDecryptedMessageLayer17.Signature, () => new TLDecryptedMessageLayer17()},
                    {TLDecryptedMessageActionResend.Signature, () => new TLDecryptedMessageActionResend()},
                    {TLDecryptedMessageActionTyping.Signature, () => new TLDecryptedMessageActionTyping()},

                    //18 layer
                    {TLContactFound.Signature, () => new TLContactFound()},
                    {TLContactsFound.Signature, () => new TLContactsFound()},
                    {TLUserSelf18.Signature, () => new TLUserSelf18()},
                    {TLUserContact18.Signature, () => new TLUserContact18()},
                    {TLUserRequest18.Signature, () => new TLUserRequest18()},
                    {TLUserForeign18.Signature, () => new TLUserForeign18()},
                    {TLUserDeleted18.Signature, () => new TLUserDeleted18()},

                    //19 layer
                    {TLUserStatusRecently.Signature, () => new TLUserStatusRecently()},
                    {TLUserStatusLastWeek.Signature, () => new TLUserStatusLastWeek()},
                    {TLUserStatusLastMonth.Signature, () => new TLUserStatusLastMonth()},
                    
                    {TLContactStatus19.Signature, () => new TLContactStatus19()},

                    {TLUpdatePrivacy.Signature, () => new TLUpdatePrivacy()},

                    {TLInputPrivacyKeyStatusTimestamp.Signature, () => new TLInputPrivacyKeyStatusTimestamp()},
                    
                    {TLPrivacyKeyStatusTimestamp.Signature, () => new TLPrivacyKeyStatusTimestamp()},

                    {TLInputPrivacyValueAllowContacts.Signature, () => new TLInputPrivacyValueAllowContacts()},
                    {TLInputPrivacyValueAllowAll.Signature, () => new TLInputPrivacyValueAllowAll()},
                    {TLInputPrivacyValueAllowUsers.Signature, () => new TLInputPrivacyValueAllowUsers()},
                    {TLInputPrivacyValueDisallowContacts.Signature, () => new TLInputPrivacyValueDisallowContacts()},
                    {TLInputPrivacyValueDisallowAll.Signature, () => new TLInputPrivacyValueDisallowAll()},
                    {TLInputPrivacyValueDisallowUsers.Signature, () => new TLInputPrivacyValueDisallowUsers()},

                    {TLPrivacyValueAllowContacts.Signature, () => new TLPrivacyValueAllowContacts()},
                    {TLPrivacyValueAllowAll.Signature, () => new TLPrivacyValueAllowAll()},
                    {TLPrivacyValueAllowUsers.Signature, () => new TLPrivacyValueAllowUsers()},
                    {TLPrivacyValueDisallowContacts.Signature, () => new TLPrivacyValueDisallowContacts()},
                    {TLPrivacyValueDisallowAll.Signature, () => new TLPrivacyValueDisallowAll()},
                    {TLPrivacyValueDisallowUsers.Signature, () => new TLPrivacyValueDisallowUsers()},
                    
                    {TLPrivacyRules.Signature, () => new TLPrivacyRules()},
                    
                    {TLAccountDaysTTL.Signature, () => new TLAccountDaysTTL()},

                    //20 layer
                    {TLSentChangePhoneCode.Signature, () => new TLSentChangePhoneCode()},
                    {TLUpdateUserPhone.Signature, () => new TLUpdateUserPhone()},

                    //21 layer
                    {TLPassword.Signature, () => new TLPassword()},
                    {TLNoPassword.Signature, () => new TLNoPassword()},

                    //22 layer

                    {TLInputMediaUploadedDocument22.Signature, () => new TLInputMediaUploadedDocument22()},
                    {TLInputMediaUploadedThumbDocument22.Signature, () => new TLInputMediaUploadedThumbDocument22()},
                    
                    {TLDocument22.Signature, () => new TLDocument22()},                  

                    {TLDocumentAttributeImageSize.Signature, () => new TLDocumentAttributeImageSize()},
                    {TLDocumentAttributeAnimated.Signature, () => new TLDocumentAttributeAnimated()},
                    {TLDocumentAttributeSticker.Signature, () => new TLDocumentAttributeSticker()},
                    {TLDocumentAttributeVideo.Signature, () => new TLDocumentAttributeVideo()},
                    {TLDocumentAttributeAudio.Signature, () => new TLDocumentAttributeAudio()},
                    {TLDocumentAttributeFileName.Signature, () => new TLDocumentAttributeFileName()},
                    
                    {TLStickersNotModified.Signature, () => new TLStickersNotModified()},
                    {TLStickers.Signature, () => new TLStickers()},
                    
                    {TLStickerPack.Signature, () => new TLStickerPack()},
                    
                    {TLAllStickersNotModified.Signature, () => new TLAllStickersNotModified()},
                    {TLAllStickers.Signature, () => new TLAllStickers()},

                    // additional sigantures
                    {TLEncryptedDialog.Signature, () => new TLEncryptedDialog()},                   
                    {TLUserExtendedInfo.Signature, () => new TLUserExtendedInfo()},                   
                    {TLDecryptedMessageActionEmpty.Signature, () => new TLDecryptedMessageActionEmpty()},
                    {TLPeerEncryptedChat.Signature, () => new TLPeerEncryptedChat()},
                    {TLBroadcastChat.Signature, () => new TLBroadcastChat()},
                    {TLPeerBroadcast.Signature, () => new TLPeerBroadcast()},
                    {TLBroadcastDialog.Signature, () => new TLBroadcastDialog()},
                    {TLInputPeerBroadcast.Signature, () => new TLInputPeerBroadcast()},
                    {TLServerFile.Signature, () => new TLServerFile()},
                    {TLEncryptedChat17.Signature, () => new TLEncryptedChat17()}
                };

        public static TimeSpan ElapsedClothedTypes;

        public static TimeSpan ElapsedBaredTypes;

        public static TimeSpan ElapsedVectorTypes;

        public static T GetObject<T>(byte[] bytes, int position) where T : TLObject
        {

            //var stopwatch = Stopwatch.StartNew();

            // bared types


            var stopwatch2 = Stopwatch.StartNew();
            try
            {

                if (_baredTypes.ContainsKey(typeof (T)))
                {
                    return (T) _baredTypes[typeof (T)].Invoke();
                }
            }
            finally
            {
                ElapsedBaredTypes += stopwatch2.Elapsed;
            }

            var stopwatch = Stopwatch.StartNew();
            uint signature;
            try
            {
                // clothed types
                //var signatureBytes = bytes.SubArray(position, 4);
                //Array.Reverse(signatureBytes);
                signature = BitConverter.ToUInt32(bytes, position);
                Func<TLObject> getInstance;


                // exact matching
                if (_clothedTypes.TryGetValue(signature, out getInstance))
                {
                    return (T)getInstance.Invoke();
                }


                //// matching with removed leading 0
                //while (signature.StartsWith("0"))
                //{
                //    signature = signature.Remove(0, 1);
                //    if (_clothedTypes.TryGetValue("#" + signature, out getInstance))
                //    {
                //        return (T)getInstance.Invoke();
                //    }
                //}
            }
            finally
            {
                ElapsedClothedTypes += stopwatch.Elapsed;
            }




            var stopwatch3 = Stopwatch.StartNew();
            //throw new Exception("Signature exception");
            try
            {
                // TLVector
                if (bytes.StartsWith(position, TLConstructors.TLVector))
                {
                    

                    //TODO: remove workaround for TLRPCRESULT: TLVECTOR<TLINT>
                    if (typeof (T) == typeof (TLObject))
                    {
                        if (bytes.StartsWith(position + 8, TLConstructors.TLContactStatus19))
                        {
                            return (T)Activator.CreateInstance(typeof(TLVector<TLContactStatusBase>));
                        }
                        else if (bytes.StartsWith(position + 8, TLConstructors.TLContactStatus))
                        {
                            return (T)Activator.CreateInstance(typeof(TLVector<TLContactStatusBase>));
                        }
                        else if (bytes.StartsWith(position + 8, TLConstructors.TLWallPaper)
                            || bytes.StartsWith(position + 8, TLConstructors.TLWallPaperSolid))
                        {
                            return (T)Activator.CreateInstance(typeof(TLVector<TLWallPaperBase>));
                        }


                        TLUtils.WriteLine("TLVecto<TLInt>  hack ", LogSeverity.Error);
                        return (T) Activator.CreateInstance(typeof(TLVector<TLInt>));
                    }
                    else
                    {
                        return (T) Activator.CreateInstance(typeof (T));
                    }
                }

            }
            finally
            {
                ElapsedVectorTypes += stopwatch3.Elapsed;
            }

            if (typeof (T) == typeof (TLObject))
            {
                TLUtils.WriteLine("  ERROR TLObjectGenerator: Cannot find signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")", LogSeverity.Error);
            }
            else
            {
                TLUtils.WriteLine("  ERROR TLObjectGenerator: Incorrect signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")" + " for type " + typeof(T), LogSeverity.Error);
            }
            TLUtils.WriteLine("Position", LogSeverity.Error);

            return null;
        }

        public static T GetNullableObject<T>(Stream input) where T : TLObject
        {
            // clothed types
            var signatureBytes = new byte[4];
            input.Read(signatureBytes, 0, 4);
            uint signature = BitConverter.ToUInt32(signatureBytes, 0);

            if (signature == TLNull.Signature) return null;

            input.Position = input.Position - 4;
            return GetObject<T>(input);
        }

        public static T GetObject<T>(Stream input) where T : TLObject
        {

            //var stopwatch = Stopwatch.StartNew();

            // bared types


            var stopwatch2 = Stopwatch.StartNew();
            try
            {

                if (_baredTypes.ContainsKey(typeof(T)))
                {
                    return (T)_baredTypes[typeof(T)].Invoke();
                }
            }
            finally
            {
                ElapsedBaredTypes += stopwatch2.Elapsed;
            }

            var stopwatch = Stopwatch.StartNew();
            uint signature;
            try
            {
                // clothed types
                var signatureBytes = new byte[4];
                input.Read(signatureBytes, 0, 4);
                signature = BitConverter.ToUInt32(signatureBytes, 0);
                Func<TLObject> getInstance;


                // exact matching
                if (_clothedTypes.TryGetValue(signature, out getInstance))
                {
                    return (T)getInstance.Invoke();
                }
            }
            finally
            {

                ElapsedClothedTypes += stopwatch.Elapsed;
            }




            var stopwatch3 = Stopwatch.StartNew();
            //throw new Exception("Signature exception");
            try
            {
                // TLVector
                if (signature == TLConstructors.TLVector)
                {
                    //TODO: remove workaround for TLRPCRESULT: TLVECTOR<TLINT>
                    if (typeof(T) == typeof(TLObject))
                    {
                        TLUtils.WriteLine("TLVecto<TLInt>  hack ", LogSeverity.Error);
                        return (T)Activator.CreateInstance(typeof(TLVector<TLInt>));
                    }
                    else
                    {
                        return (T)Activator.CreateInstance(typeof(T));
                    }
                }

            }
            finally
            {
                ElapsedVectorTypes += stopwatch3.Elapsed;
            }

            if (typeof(T) == typeof(TLObject))
            {
                TLUtils.WriteLine("  ERROR TLObjectGenerator: Cannot find signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")", LogSeverity.Error);
            }
            else
            {
                TLUtils.WriteLine("  ERROR TLObjectGenerator: Incorrect signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")" + " for type " + typeof(T), LogSeverity.Error);
            }
            TLUtils.WriteLine("Position", LogSeverity.Error);

            return null;
        }

        // Instead of visiting each field of stackFrame,
        // the StackFrame.ToString() method could be used, 
        // but the returned text would not include the class name.
        private static String MethodCallLog(System.Diagnostics.StackFrame p_MethodCall)
        {
            System.Text.StringBuilder l_MethodCallLog =
                        new System.Text.StringBuilder();

            var l_Method = p_MethodCall.GetMethod();
            l_MethodCallLog.Append(l_Method.DeclaringType.ToString());
            l_MethodCallLog.Append(".");
            l_MethodCallLog.Append(p_MethodCall.GetMethod().Name);

            var l_MethodParameters = l_Method.GetParameters();
            l_MethodCallLog.Append("(");
            for (Int32 x = 0; x < l_MethodParameters.Length; ++x)
            {
                if (x > 0)
                    l_MethodCallLog.Append(", ");
                var l_MethodParameter = l_MethodParameters[x];
                l_MethodCallLog.Append(l_MethodParameter.ParameterType.Name);
                l_MethodCallLog.Append(" ");
                l_MethodCallLog.Append(l_MethodParameter.Name);
            }
            l_MethodCallLog.Append(")");

            var l_SourceFileName = p_MethodCall.GetFileName();
            if (!String.IsNullOrEmpty(l_SourceFileName))
            {
                l_MethodCallLog.Append(" in ");
                l_MethodCallLog.Append(l_SourceFileName);
                l_MethodCallLog.Append(": line ");
                l_MethodCallLog.Append(p_MethodCall.GetFileLineNumber().ToString());
            }

            return l_MethodCallLog.ToString();
        }
    }
}
