﻿namespace Telegram.Api.TL
{
    public class TLChats : TLObject
    {
        public const uint Signature = TLConstructors.TLChats;

        public TLVector<TLChatBase> Chats { get; set; }

        public TLVector<TLUserBase> Users { get; set; } 

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLChats--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);

            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);

            return this;
        }
    }
}
