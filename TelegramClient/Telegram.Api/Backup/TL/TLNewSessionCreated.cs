namespace Telegram.Api.TL
{
    internal class TLNewSessionCreated : TLObject
    {
        public const uint Signature = TLConstructors.TLNewSessionCreated;

        public TLLong FirstMessageId { get; set; }

        public TLLong UniqueId { get; set; }

        public TLLong ServerSalt { get; set; }

        /// <summary>
        /// Parses bytes arrays to TLObject from specific position, than adds count of readed bytes to position variable.
        /// </summary>
        /// <param name="bytes">    The bytes array. </param>
        /// <param name="position"> The position. </param>
        /// <returns>   The TLObject. </returns>
        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse NewSessionCreated--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            FirstMessageId = GetObject<TLLong>(bytes, ref position);
            TLUtils.WriteLine("FirstMessageId: " + FirstMessageId);

            UniqueId = GetObject<TLLong>(bytes, ref position);
            TLUtils.WriteLine("UniqueId: " + UniqueId);

            ServerSalt = GetObject<TLLong>(bytes, ref position);
            TLUtils.WriteLine("ServerSalt: " + ServerSalt);


            return this;
        }
    }
}