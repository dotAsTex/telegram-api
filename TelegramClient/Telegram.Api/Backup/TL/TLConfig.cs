﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Telegram.Api.TL
{
    [DataContract]
    public class TLConfig : TLObject
    {
        public const uint Signature = TLConstructors.TLConfig;

        [DataMember]
        public TLInt Date { get; set; }

        [DataMember]
        public TLBool TestMode { get; set; }

        /// <summary>
        /// Номер датацентра, ему может соответствовать несколько записей в DCOptions
        /// </summary>
        [DataMember]
        public TLInt ThisDC { get; set; }

        [DataMember]
        public TLVector<TLDCOption> DCOptions { get; set; }

        [DataMember]
        public TLInt ChatMaxSize { get; set; }

        [DataMember]
        public TLInt BroadcastSizeMax { get; set; }
       
        #region Additional
        /// <summary>
        /// Время последней загрузки config
        /// </summary>
        [DataMember]
        public DateTime LastUpdate { get; set; }

        /// <summary>
        /// Номер конкретного датацентра внутри списка DCOptions, однозначно определяет текущий датацентр
        /// </summary>
        [DataMember]
        public int ActiveDCOptionIndex { get; set; }

        [DataMember]
        public string Country { get; set; }
        #endregion

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Date = GetObject<TLInt>(bytes, ref position);
            TestMode = GetObject<TLBool>(bytes, ref position);
            ThisDC = GetObject<TLInt>(bytes, ref position);
            DCOptions = GetObject<TLVector<TLDCOption>>(bytes, ref position);
            ChatMaxSize = GetObject<TLInt>(bytes, ref position);
            BroadcastSizeMax = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public static TLConfig Merge(TLConfig oldConfig, TLConfig newConfig)
        {
            if (oldConfig == null)
                return newConfig;

            if (newConfig == null)
                return oldConfig; 

            foreach (var dcOption in oldConfig.DCOptions)
            {
                if (dcOption.AuthKey != null)
                {
                    var option = dcOption;
                    foreach (var newDCOption in newConfig.DCOptions.Where(x => x.AreEquals(option)))
                    {
                        newDCOption.AuthKey = dcOption.AuthKey;
                        newDCOption.Salt = dcOption.Salt;
                        newDCOption.SessionId = dcOption.SessionId;
                        newDCOption.ClientTicksDelta = dcOption.ClientTicksDelta;
                    }
                }
            }
            if (!string.IsNullOrEmpty(oldConfig.Country))
            {
                newConfig.Country = oldConfig.Country;
            }
            if (oldConfig.ActiveDCOptionIndex != default(int))
            {
                newConfig.ActiveDCOptionIndex = oldConfig.ActiveDCOptionIndex;
            }
            if (oldConfig.LastUpdate != default(DateTime))
            {
                newConfig.LastUpdate = oldConfig.LastUpdate;
            }

            return newConfig;
        }
    }
}
