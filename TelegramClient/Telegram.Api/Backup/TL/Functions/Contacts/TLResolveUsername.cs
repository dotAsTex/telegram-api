﻿namespace Telegram.Api.TL.Functions.Contacts
{
    class TLResolveUsername : TLObject
    {
        public const string Signature = "#bf0131c";

        public TLString Username { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Username.ToBytes());
        }
    }
}
