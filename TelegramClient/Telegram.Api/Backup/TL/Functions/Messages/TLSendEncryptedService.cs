﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSendEncryptedService : TLObject
    {
        public const string Signature = "#32d439a4";

        public TLInputEncryptedChat Peer { get; set; }

        public TLLong RandomId { get; set; }

        public TLString Data { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                RandomId.ToBytes(),
                Data.ToBytes());
        }
    }
}