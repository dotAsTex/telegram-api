﻿namespace Telegram.Api.TL.Functions.Messages
{
    class TLDeleteHistory : TLObject
    {
        public const string Signature = "#f4f8fb61";

        public TLInputPeerBase Peer { get; set; }

        public TLInt Offset { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Offset.ToBytes());
        }
    }
}
