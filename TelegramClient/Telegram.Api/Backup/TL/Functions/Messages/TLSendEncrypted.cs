﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSendEncrypted : TLObject
    {
        public const string Signature = "#a9776773";

        public TLInputEncryptedChat Peer { get; set; }

        public TLLong RandomId { get; set; }

        public TLString Data { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                RandomId.ToBytes(),
                Data.ToBytes());
        }
    }
}