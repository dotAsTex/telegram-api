﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSendMessage : TLObject
    {
        public const string Signature = "#4cde0aab";

        public TLInputPeerBase Peer { get; set; }

        public TLString Message { get; set; }

        public TLLong RandomId { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Message.ToBytes(),
                RandomId.ToBytes());
        }
    }
}
