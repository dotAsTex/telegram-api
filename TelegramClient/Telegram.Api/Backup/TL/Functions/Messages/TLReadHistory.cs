﻿namespace Telegram.Api.TL.Functions.Messages
{
    class TLReadHistory : TLObject
    {
        public const string Signature = "#b04f2510";

        public TLInputPeerBase Peer { get; set; }

        public TLInt MaxId { get; set; }

        public TLInt Offset { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                MaxId.ToBytes(),
                Offset.ToBytes());
        }
    }
}
