﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSendEncryptedFile : TLObject
    {
        public const string Signature = "#9a901b66";

        public TLInputEncryptedChat Peer { get; set; }

        public TLLong RandomId { get; set; }

        public TLString Data { get; set; }

        public TLInputEncryptedFileBase File { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                RandomId.ToBytes(),
                Data.ToBytes(),
                File.ToBytes());
        }
    }
}