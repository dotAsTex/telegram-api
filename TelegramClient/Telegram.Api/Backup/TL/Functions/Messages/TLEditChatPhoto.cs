﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLEditChatPhoto : TLObject
    {
        public const string Signature = "#d881821d";

        public TLInt ChatId { get; set; }

        public TLInputChatPhotoBase Photo { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                ChatId.ToBytes(),
                Photo.ToBytes());
        }
    }
}
