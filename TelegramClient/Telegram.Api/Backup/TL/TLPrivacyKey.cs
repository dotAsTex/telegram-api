﻿namespace Telegram.Api.TL
{
    public abstract class TLPrivacyKeyBase : TLObject { }

    public class TLPrivacyKeyStatusTimestamp : TLPrivacyKeyBase
    {
        public const uint Signature = TLConstructors.TLPrivacyKeyStatusTimestamp;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }
    }
}
