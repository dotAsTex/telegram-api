﻿namespace Telegram.Api.TL
{
    public abstract class TLPasswordBase : TLObject
    {
        public TLString NewSalt { get; set; }
    }

    public class TLPassword : TLPasswordBase
    {
        public const uint Signature = TLConstructors.TLPassword;

        public TLString CurrentSalt { get; set; }

        public TLString Hint { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            CurrentSalt = GetObject<TLString>(bytes, ref position);
            NewSalt = GetObject<TLString>(bytes, ref position);
            Hint = GetObject<TLString>(bytes, ref position);

            return this;
        }
    }

    public class TLNoPassword : TLPasswordBase
    {
        public const uint Signature = TLConstructors.TLNoPassword;

        public TLUserStatus Status { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            NewSalt = GetObject<TLString>(bytes, ref position);

            return this;
        }
    }
}
