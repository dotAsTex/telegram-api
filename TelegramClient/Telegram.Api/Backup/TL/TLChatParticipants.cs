﻿using System.IO;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLChatParticipantsBase : TLObject
    {
        public TLInt ChatId { get; set; }
    }

    public class TLChatParticipantsForbidden : TLChatParticipantsBase
    {
        public const uint Signature = TLConstructors.TLChatParticipantsForbidden;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            ChatId = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(ChatId.ToBytes());
        }
    }

    public class TLChatParticipants : TLChatParticipantsBase
    {
        public const uint Signature = TLConstructors.TLChatParticipants;

        public TLInt AdminId { get; set; }

        public TLVector<TLChatParticipant> Participants { get; set; }

        public TLInt Version { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            AdminId = GetObject<TLInt>(bytes, ref position);
            Participants = GetObject<TLVector<TLChatParticipant>>(bytes, ref position);
            Version = GetObject<TLInt>(bytes, ref position);
            
            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            ChatId = GetObject<TLInt>(input);
            AdminId = GetObject<TLInt>(input);
            Participants = GetObject<TLVector<TLChatParticipant>>(input);
            Version = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(ChatId.ToBytes());
            output.Write(AdminId.ToBytes());
            Participants.ToStream(output);
            output.Write(Version.ToBytes());
        }
    }
}
