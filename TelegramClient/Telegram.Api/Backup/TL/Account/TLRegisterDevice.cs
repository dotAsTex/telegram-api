﻿namespace Telegram.Api.TL.Account
{
    public class TLRegisterDevice : TLObject
    {
        public const string Signature = "#446c712c";

        public TLInt TokenType { get; set; }

        public TLString Token { get; set; }

        public TLString DeviceModel { get; set; }

        public TLString SystemVersion { get; set; }

        public TLString AppVersion { get; set; }

        public TLBool AppSandbox { get; set; }

        public TLString LandCode { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                TokenType.ToBytes(),
                Token.ToBytes(),
                DeviceModel.ToBytes(),
                SystemVersion.ToBytes(),
                AppVersion.ToBytes(),
                AppSandbox.ToBytes(),
                LandCode.ToBytes());
        }
    }
}
