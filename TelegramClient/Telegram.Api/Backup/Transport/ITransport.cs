﻿using System;
using System.Net.Sockets;
using Telegram.Api.Services;
using Telegram.Api.TL;

namespace Telegram.Api.Transport
{
    public interface ITransport
    {
        event EventHandler SocketConnecting;
        event EventHandler SocketConnected;

        bool Connected { get; }
        DateTime? FirstSendTime { get; }
        DateTime? LastSendTime { get; }
        DateTime? FirstReceiveTime { get; }
        DateTime? LastReceiveTime { get; }

        WindowsPhone.Tuple<int, int, int> GetCurrentPacketInfo();
        string GetTransportInfo();

        int Id { get; }

        TLLong GenerateMessageId(bool checkPreviousMessageId = false);

        void EnqueueNonEncryptedItem(HistoryItem item);
        HistoryItem DequeueFirstNonEncryptedItem();
        void RemoveNonEncryptedItem(HistoryItem item);
        void ClearNonEncryptedHistory(Exception e = null);
        string PrintNonEncryptedHistory();

        object SyncRoot { get; }

        int DCId { get; set; }

        byte[] AuthKey { get; set; }
        TLLong SessionId { get; set; }
        TLLong Salt { get; set; }
        int SequenceNumber { get; set; }

        long ClientTicksDelta { get; set; }

        string Host { get; }

        int Port { get; }

        TransportType Type { get; }

        bool Closed { get; }

        //сделан initConnection
        bool Initiated { get; set; }

        //перенесена авторизация из активного dc
        bool Initialized { get; set; }

        bool IsInitializing { get; set; }

        bool IsAuthorized { get; set; }

        bool IsAuthorizing { get; set; }

        //void ConnectAsync(Action callback, Action<TcpTransportResult> faultCallback);

        void SendBytesAsync(string caption, byte[] data, Action<SocketError> callback, Action<TcpTransportResult> faultCallback = null);

        event EventHandler<DataEventArgs> ReceiveBytes;

        void Close();

        void StartListening();
    }
}
