﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Caliburn.Micro;
using Microsoft.Phone.Net.NetworkInformation;
using Telegram.Api.Services;
using Telegram.Api.TL;

namespace Telegram.Api.Transport
{
    public class TransportService : ITransportService
    {
        public TransportService()
        {
            DeviceNetworkInformation.NetworkAvailabilityChanged += (sender, args) =>
            {
                var networkString = string.Format("{0}, {1}, ", args.NotificationType,
                    args.NetworkInterface != null ? args.NetworkInterface.InterfaceState.ToString() : "none");

                var mtProtoService = IoC.Get<IMTProtoService>();
                if (mtProtoService != null)
                {
                    if (args.NotificationType == NetworkNotificationType.InterfaceDisconnected)
                    {
#if DEBUG
                        var interfaceSubtype = args.NetworkInterface != null
                            ? args.NetworkInterface.InterfaceSubtype.ToString()
                            : "Interface";
                        mtProtoService.SetMessageOnTime(2.0, interfaceSubtype + " disconnected...");
                        TLUtils.WriteLine(interfaceSubtype + " disconnected", LogSeverity.Error);
#else
                        //mtProtoService.SetMessageOnTime(2.0, "No Internet connection");
#endif
                    }
                    else if (args.NotificationType == NetworkNotificationType.InterfaceConnected)
                    {
#if DEBUG
                        var interfaceSubtype = args.NetworkInterface != null
                            ? args.NetworkInterface.InterfaceSubtype.ToString()
                            : "Interface";
                        mtProtoService.SetMessageOnTime(2.0, interfaceSubtype + " connected...");
                        TLUtils.WriteLine(interfaceSubtype + " connected", LogSeverity.Error);
#else
                        mtProtoService.SetMessageOnTime(0.0, string.Empty);
#endif
                    }
                    else if (args.NotificationType == NetworkNotificationType.CharacteristicUpdate)
                    {
//#if DEBUG

//                        mtProtoService.SetMessageOnTime(2.0, "Characteristic update...");
//                        var networkInterface = args.NetworkInterface;
//                        if (networkInterface != null)
//                        {
//                            var characteristics = new StringBuilder();
//                            characteristics.AppendLine();
//                            //characteristics.AppendLine("Description=" + networkInterface.Description);
//                            characteristics.AppendLine("InterfaceName=" + networkInterface.InterfaceName);
//                            characteristics.AppendLine("InterfaceState=" + networkInterface.InterfaceState);
//                            characteristics.AppendLine("InterfaceType=" + networkInterface.InterfaceType);
//                            characteristics.AppendLine("InterfaceSubtype=" + networkInterface.InterfaceSubtype);
//                            characteristics.AppendLine("Bandwidth=" + networkInterface.Bandwidth);
//                            //characteristics.AppendLine("Characteristics=" + networkInterface.Characteristics);
//                            TLUtils.WriteLine(characteristics.ToString(), LogSeverity.Error);
//                        }
//#endif
                    }
                }
                
            };
        }

        private readonly Dictionary<string, ITransport> _cache = new Dictionary<string, ITransport>();

        private readonly Dictionary<string, ITransport> _fileCache = new Dictionary<string, ITransport>();

        public ITransport GetFileTransport(string host, int port, TransportType type, out bool isCreated)
        {
            var key = string.Format("{0} {1} {2}", host, port, type);
            if (_fileCache.ContainsKey(key))
            {
                isCreated = false;
                return _fileCache[key];
            }

            if (type == TransportType.Http)
            {
                var transport = new HttpTransport(host);

                _fileCache.Add(key, transport);
                isCreated = true;
                return transport;
                //transport.SetAddress(host, port, () => callback(transport));
            }
            else
            {
                var transport = new TcpTransport(host, port);

                TLUtils.WritePerformance(string.Format("  TCP: New file transport {0}:{1}", host, port));

                _fileCache.Add(key, transport);
                isCreated = true;

                Debug.WriteLine("  TCP: New transport {0}:{1}", host, port);
                return transport;
                //trasport.SetAddress(host, port, () => callback(trasport));
            }
        }

        public ITransport GetTransport(string host, int port, TransportType type, out bool isCreated)
        {
            var key = string.Format("{0} {1} {2}", host, port, type);
            if (_cache.ContainsKey(key))
            {
                isCreated = false;

#if LOG_REGISTRATION
                TLUtils.WriteLog(string.Format("Old transport {2} {0}:{1}", host, port, _cache[key].Id));
#endif
                return _cache[key];
            }

            if (type == TransportType.Http)
            {
                var transport = new HttpTransport(host);

                _cache.Add(key, transport);
                isCreated = true;
                return transport;
                //transport.SetAddress(host, port, () => callback(transport));
            }
            else
            {
                var transport = new TcpTransport(host, port);

                transport.SocketConnecting += OnSocketConnecting;
                transport.SocketConnected += OnSocketConnected;

#if LOG_REGISTRATION
                TLUtils.WriteLog(string.Format("New transport {2} {0}:{1}", host, port, transport.Id));
#endif
                TLUtils.WritePerformance(string.Format("  TCP: New transport {0}:{1}", host, port));

                _cache.Add(key, transport);
                isCreated = true;

                Debug.WriteLine("  TCP: New transport {0}:{1}", host, port);
                return transport;
                //trasport.SetAddress(host, port, () => callback(trasport));
            }
        }

        public void Close()
        {
            var transports = new List<ITransport>(_cache.Values);

            foreach (var transport in transports)
            {
                transport.Close();
                transport.SocketConnecting -= OnSocketConnecting;
                transport.SocketConnected -= OnSocketConnected;
            }
            _cache.Clear();

            var fileTransports = new List<ITransport>(_fileCache.Values);

            foreach (var transport in fileTransports)
            {
                transport.Close();
                transport.SocketConnecting -= OnSocketConnecting;
                transport.SocketConnected -= OnSocketConnected;
            }
            _fileCache.Clear();
        }

        public void CloseTransport(ITransport transport)
        {
            var transports = new List<ITransport>(_cache.Values);

            foreach (var value in _cache.Values.Where(x => string.Equals(x.Host, transport.Host, StringComparison.OrdinalIgnoreCase)))
            {
                value.Close();
                transport.SocketConnecting -= OnSocketConnecting;
                transport.SocketConnected -= OnSocketConnected;
            }
            _cache.Remove(string.Format("{0} {1} {2}", transport.Host, transport.Port, transport.Type));

            var fileTransports = new List<ITransport>(_fileCache.Values);

            foreach (var value in _fileCache.Values.Where(x => string.Equals(x.Host, transport.Host, StringComparison.OrdinalIgnoreCase)))
            {
                value.Close();
                transport.SocketConnecting -= OnSocketConnecting;
                transport.SocketConnected -= OnSocketConnected;
            }
            _fileCache.Remove(string.Format("{0} {1} {2}", transport.Host, transport.Port, transport.Type));
        }

        public event EventHandler<TransportEventArgs> TransportConnecting;

        protected virtual void RaiseTransportConnecting(ITransport transport)
        {
            var handler = TransportConnecting;
            if (handler != null) handler(this, new TransportEventArgs{Transport = transport});
        }

        public void OnSocketConnecting(object sender, EventArgs args)
        {
            RaiseTransportConnecting(sender as ITransport);
            //return;

            //var mtProtoService = IoC.Get<IMTProtoService>();
            //if (mtProtoService != null)
            //{
            //    mtProtoService.SetMessageOnTime(25.0, string.Format("Connecting..."));
            //}
        }

        public event EventHandler<TransportEventArgs> TransportConnected;

        protected virtual void RaiseTransportConnected(ITransport transport)
        {
            var handler = TransportConnected;
            if (handler != null) handler(this, new TransportEventArgs{Transport = transport});
        }

        public void OnSocketConnected(object sender, EventArgs args)
        {
            RaiseTransportConnected(sender as ITransport);
            //return;

            //var mtProtoService = IoC.Get<IMTProtoService>();
            //if (mtProtoService != null)
            //{
            //    mtProtoService.SetMessageOnTime(0.0, string.Empty);
            //}
        }
    }

    public class TransportEventArgs : EventArgs
    {
        public ITransport Transport { get; set; }
    }
}
