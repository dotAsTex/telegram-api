﻿//#define DEBUG_READ_HISTORY

using System;
using System.Diagnostics;
using System.Linq;
using Telegram.Api.Extensions;
using Telegram.Api.TL;
using Telegram.Api.TL.Functions.Messages;

namespace Telegram.Api.Services
{
	public partial class MTProtoService
	{
        public void GetAllStickersAsync(TLString hash, Action<TLAllStickersBase> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLGetAllStickers { Hash = hash };

            const string caption = "messages.getAllStickers";
            SendInformativeMessage(caption, obj, callback, faultCallback);
	    }

        public void SendMessageAsync(TLInputPeerBase inputPeer, TLMessage message, TLPeerBase peer, Action<TLMessage> callback, Action fastCallback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSendMessage { Peer = inputPeer, Message = message.Message, RandomId = message.RandomId };

            const string caption = "messages.sendMessage";
            //TLUtils.WriteLine(caption + " " + message.RandomIndex, LogSeverity.Error);
            SendMessageAsyncInternal(obj,
                result =>
                {
                    //TLUtils.WriteLine(caption + " result " + message.RandomIndex + " " + result.Id.Value, LogSeverity.Error);

                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    message.Date = result.Date;
                    message.Id = result.Id;
                    message.Status = MessageStatus.Confirmed;

                    _cacheService.SyncSendingMessage(message, null, peer, callback);
                },
                () =>
                {
                    //TLUtils.WriteLine(caption + " fast result " + message.RandomIndex, LogSeverity.Error);
                    fastCallback();
                },
                error =>
                {
                    //TLUtils.WriteLine(caption + " error " + message.RandomIndex + " " + error, LogSeverity.Error);
                    faultCallback.SafeInvoke(error);
                });
        }

        public void SendMediaAsync(TLInputPeerBase inputPeer, TLInputMediaBase inputMedia, TLMessage message, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSendMedia { Peer = inputPeer, Media = inputMedia, RandomId = message.RandomId };

            const string caption = "messages.sendMedia";
            SendMediaAsyncInternal(obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    var sentMessage = (TLMessage)result.Message;
                    sentMessage.RandomId = message.RandomId;

                    TLUtils.WriteLine(caption + " result " + message.RandomIndex + " " + sentMessage.Id.Value, LogSeverity.Error);

                    message.Id = sentMessage.Id;
                    message.Date = sentMessage.Date;
                    message.Status = MessageStatus.Confirmed;
                    message.Media.LastProgress = 0.0;
                    message.Media.DownloadingProgress = 0.0;

                    _cacheService.SyncStatedMessage(result, callback);
                },
                () =>
                {
                    //TLUtils.WriteLine(caption + " fast result " + message.RandomIndex, LogSeverity.Error);
                    //fastCallback();
                },
                error =>
                {
                    //TLUtils.WriteLine(caption + " error " + message.RandomIndex + " " + error, LogSeverity.Error);
                    faultCallback.SafeInvoke(error);
                });
        }

        public void SendBroadcastAsync(TLVector<TLInputUserBase> contacts, TLInputMediaBase inputMedia, TLMessage message, Action<TLStatedMessages> callback, Action fastCallback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSendBroadcast { Contacts = contacts, Message = message.Message, Media = inputMedia };

            const string caption = "messages.sendMedia";
            SendInformativeMessage<TLStatedMessages>(caption,
                obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    message.Date = new TLInt(result.Messages.First().DateIndex - 1); // Делаем бродкаст после всех чатов, в которые отправили, в списке диалогов
                    //message.Id = result.Id;
                    message.Status = MessageStatus.Confirmed;
                    _cacheService.SyncStatedMessages(result, callback);
                },
                faultCallback);
        }

        public void SendEncryptedAsync(TLInputEncryptedChat peer, TLLong randomId, TLString data, Action<TLSentEncryptedMessage> callback, Action fastCallback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSendEncrypted { Peer = peer, RandomId = randomId, Data = data };

            SendEncryptedAsyncInternal(
                obj,
                result =>
                {
                    callback(result);
                },
                () =>
                {
                    
                },
                faultCallback);
        }


	    public void SendEncryptedFileAsync(TLInputEncryptedChat peer, TLLong randomId, TLString data, TLInputEncryptedFileBase file, Action<TLSentEncryptedFile> callback, Action fastCallback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLSendEncryptedFile { Peer = peer, RandomId = randomId, Data = data, File = file };

            SendEncryptedFileAsyncInternal(
                obj,
                result =>
                {
                    callback(result);
                },
                () =>
                {

                },
                faultCallback);
	    }

	    public void SendEncryptedServiceAsync(TLInputEncryptedChat peer, TLLong randomId, TLString data, Action<TLSentEncryptedMessage> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLSendEncryptedService { Peer = peer, RandomId = randomId, Data = data };

            SendEncryptedServiceAsyncInternal(
                obj,
                result =>
                {
                    callback(result);
                },
                () =>
                {

                },
                faultCallback);
	    }

	    public void ReadEncryptedHistoryAsync(TLInputEncryptedChat peer, TLInt maxDate, Action<TLBool> callback,
	        Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLReadEncryptedHistory { Peer = peer, MaxDate = maxDate };

            SendInformativeMessage<TLBool>("messages.reeadEncryptedHistory",
                obj,
                result =>
                {
                    callback(result);
                },
                faultCallback);
	    }

	    public void SetEncryptedTypingAsync(TLInputEncryptedChat peer, TLBool typing, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLSetEncryptedTyping { Peer = peer, Typing = typing };

            SendInformativeMessage("messages.setEncryptedTyping", obj, callback, faultCallback);
	    }
        public void SetTypingAsync(TLInputPeerBase peer, TLBool typing, Action<TLBool> callback, Action<TLRPCError> faultCallback = null)
        {
            var action = typing.Value ? (TLSendMessageActionBase) new TLSendMessageTypingAction() : new TLSendMessageCancelAction();
            var obj = new TLSetTyping { Peer = peer, Action = action };

            SendInformativeMessage("messages.setTyping", obj, callback, faultCallback);
        }

        public void GetMessagesAsync(TLVector<TLInt> id, Action<TLMessagesBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetMessages { Id = id };

            SendInformativeMessage("messages.getMessages", obj, callback, faultCallback);
        }

        public void GetDialogsAsync(TLInt offset, TLInt maxId, TLInt limit, Action<TLDialogsBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetDialogs { Offset = offset, MaxId = maxId, Limit = limit };

            SendInformativeMessage<TLDialogsBase>("messages.getDialogs", obj,
                result =>
                {
                    //callback(result);
                    _cacheService.SyncDialogs(result, callback);
                }, faultCallback);
        }

        public void GetHistoryAsync(TLInputPeerBase inputPeer, TLPeerBase peer, bool sync, TLInt maxId, TLInt limit, Action<TLMessagesBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetHistory { Peer = inputPeer, Offset = new TLInt(0), MaxId = maxId, Limit = limit };
            Stopwatch s = Stopwatch.StartNew();
            SendInformativeMessage<TLMessagesBase>("messages.getHistory", obj, result =>
            {

                //TLUtils.WritePerformance("GetHistory BeforeSyncTime " + s.Elapsed);
                s = Stopwatch.StartNew();
                //callback(result);

                if (sync)
                {
                    _cacheService.SyncMessages(result, peer, false, true, r =>
                    {
                        //TLUtils.WritePerformance("GetHistory AfterSyncTime " + s.Elapsed);
                        callback(r);
                    });
                }
                else
                {
                    callback(result);
                }

            }, faultCallback);
        }

        public void SearchAsync(TLInputPeerBase peer, TLString query, TLInputMessagesFilterBase filter, TLInt minDate, TLInt maxDate, TLInt offset, TLInt maxId, TLInt limit, Action<TLMessagesBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSearch { Peer = peer, Query = query, Filter = filter, MinDate = minDate, MaxDate = maxDate, Offset = offset, MaxId = maxId, Limit = limit };

            SendInformativeMessage("messages.search", obj, callback, faultCallback);
        }

        public void ReadHistoryAsync(TLInputPeerBase peer, TLInt maxId, TLInt offset, Action<TLAffectedHistory> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLReadHistory { Peer = peer, MaxId = maxId, Offset = offset };

            //_updatesService.IncrementClientSeq();

            const string caption = "messages.readHistory";
            SendInformativeMessage<TLAffectedHistory>(caption, obj,
                result =>
                {
                    
#if DEBUG && DEBUG_READ_HISTORY
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("ReadHistory");
                    });
#endif
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    callback(result);
                },
                faultCallback);
        }

        public void ReadMessageContentsAsync(TLVector<TLInt> id, Action<TLVector<TLInt>> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLReadMessageContents { Id = id };

            SendInformativeMessage("messages.readMessageContents", obj, callback, faultCallback);
        }

        public void DeleteHistoryAsync(TLInputPeerBase peer, TLInt offset, Action<TLAffectedHistory> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLDeleteHistory { Peer = peer, Offset = offset };

            const string caption = "messages.deleteHistory";
            SendInformativeMessage<TLAffectedHistory>(caption, obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    callback(result);
                },
                faultCallback);
        }

        public void DeleteMessagesAsync(TLVector<TLInt> id, Action<TLVector<TLInt>> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLDeleteMessages { Id = id };

            SendInformativeMessage("messages.deleteMessages", obj, callback, faultCallback);
        }

        public void RestoreMessagesAsync(TLVector<TLInt> id, Action<TLVector<TLInt>> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLRestoreMessages{ Id = id };

            SendInformativeMessage("messages.restoreMessages", obj, callback, faultCallback);
        }

        public void ReceivedMessagesAsync(TLInt maxId, Action<TLVector<TLInt>> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLReceivedMessages { MaxId = maxId };

            SendInformativeMessage("messages.receivedMessages", obj, callback, faultCallback);
        }

	    public void ForwardMessageAsync(TLInputPeerBase peer, TLInt fwdMessageId, TLMessageForwarded message, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
	    {
            var obj = new TLForwardMessage { Peer = peer, Id = fwdMessageId, RandomId = message.RandomId };

            const string caption = "messages.forwardMessage";
            SendInformativeMessage<TLStatedMessage>(caption, obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    var sentMessage = (TLMessage)result.Message;
                    sentMessage.RandomId = message.RandomId;

                    message.Id = sentMessage.Id;
                    message.Date = sentMessage.Date;
                    message.Status = MessageStatus.Confirmed;
                    message.Media.LastProgress = 0.0;
                    message.Media.DownloadingProgress = 0.0;

                    _cacheService.SyncStatedMessage(result, callback);

                },
                faultCallback);
	    }

        public void ForwardMessagesAsync(TLInputPeerBase peer, TLVector<TLInt> id, Action<TLStatedMessages> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLForwardMessages { Peer = peer, Id = id };

            const string caption = "messages.forwardMessages";
            SendInformativeMessage<TLStatedMessages>(caption, obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                },
                faultCallback);
        }

        public void GetChatsAsync(TLVector<TLInt> id, Action<TLChats> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetChats{ Id = id };

            SendInformativeMessage("messages.getChats", obj, callback, faultCallback);
        }

        public void GetFullChatAsync(TLInt chatId, Action<TLMessagesChatFull> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLGetFullChat { ChatId = chatId };

            SendInformativeMessage<TLMessagesChatFull>(
                "messages.getFullChat", obj,
                messagesChatFull =>
                {
                    callback(messagesChatFull);
                    _cacheService.SyncChat(messagesChatFull, null);
                },
                faultCallback);
        }

        public void EditChatTitleAsync(TLInt chatId, TLString title, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLEditChatTitle { ChatId = chatId, Title = title };

            const string caption = "messages.editChatTitle";
            SendInformativeMessage<TLStatedMessage>(caption, 
                obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    _cacheService.SyncStatedMessage(result, callback);
                }, 
                faultCallback);
        }

        public void EditChatPhotoAsync(TLInt chatId, TLInputChatPhotoBase photo, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLEditChatPhoto { ChatId = chatId, Photo = photo };

            const string caption = "messages.editChatPhoto";
            SendInformativeMessage<TLStatedMessage>(caption, 
                obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    _cacheService.SyncStatedMessage(result, callback);
                }, 
                faultCallback);
        }

        public void AddChatUserAsync(TLInt chatId, TLInputUserBase userId, TLInt fwdLimit, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLAddChatUser { ChatId = chatId, UserId = userId, FwdLimit = fwdLimit };

            const string caption = "messages.addChatUser";
            SendInformativeMessage<TLStatedMessage>(caption, 
                obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    _cacheService.SyncStatedMessage(result, callback);
                },
                faultCallback);
        }

        public void DeleteChatUserAsync(TLInt chatId, TLInputUserBase userId, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLDeleteChatUser { ChatId = chatId, UserId = userId };

            const string caption = "messages.deleteChatUser";
            SendInformativeMessage<TLStatedMessage>(caption, 
                obj,
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    _cacheService.SyncStatedMessage(result, callback);
                },
                faultCallback);
        }

        public void CreateChatAsync(TLVector<TLInputUserBase> users, TLString title, Action<TLStatedMessage> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLCreateChat { Users = users, Title = title };

            const string caption = "messages.createChat";
            SendInformativeMessage<TLStatedMessage>(caption,
                obj, 
                result =>
                {
                    _updatesService.SetState(result.Seq, result.Pts, null, null, null, caption);
                    _cacheService.SyncStatedMessage(result, callback);
                },
                faultCallback);
        }
	}
}
