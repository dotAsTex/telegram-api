﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Telegram.Api.Extensions;
using Telegram.Api.Services.Cache;
using Telegram.Api.TL;
using Telegram.Api.TL.Functions.Messages;
using Action = System.Action;

namespace Telegram.Api.Services
{
    public partial class MTProtoService
    {
        private readonly object _sendingQueueSyncRoot = new object();

        private readonly List<HistoryItem> _sendingQueue = new List<HistoryItem>(); 

        private static Timer _sendingTimer;

        private static void StartSendingTimer()
        {
            //Helpers.Execute.ShowDebugMessage("MTProtoService.StartSendingTimer");
            _sendingTimer.Change(TimeSpan.FromSeconds(Constants.ResendMessageInterval), TimeSpan.FromSeconds(Constants.ResendMessageInterval));
        }

        private static void StopSendingTimer()
        {
            //Helpers.Execute.ShowDebugMessage("MTProtoService.StoptSendingTimer");
            _sendingTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private static void CheckSendingMessages(object state)
        {
            var service = (MTProtoService) state;

            service.ProcessQueue();
        }

        private void SendEncryptedAsyncInternal(TLSendEncrypted message, Action<TLSentEncryptedMessage> callback, Action fastCallback, Action<TLRPCError> faultCallback) 
        {
            SendAsyncInternal("messages.sendEncrypted", message, callback, fastCallback, faultCallback);
        }

        private void SendEncryptedFileAsyncInternal(TLSendEncryptedFile message, Action<TLSentEncryptedFile> callback, Action fastCallback, Action<TLRPCError> faultCallback)
        {
            SendAsyncInternal("messages.sendEncryptedFile", message, callback, fastCallback, faultCallback);
        }

        private void SendEncryptedServiceAsyncInternal(TLSendEncryptedService message, Action<TLSentEncryptedMessage> callback, Action fastCallback, Action<TLRPCError> faultCallback)
        {
            SendAsyncInternal("messages.sendEncryptedService", message, callback, fastCallback, faultCallback);
        }

        private void SendMessageAsyncInternal(TLSendMessage message, Action<TLSentMessage> callback, Action fastCallback, Action<TLRPCError> faultCallback)
        {
            SendAsyncInternal("messages.sendMessage", message, callback, fastCallback, faultCallback);
        }

        private void SendMediaAsyncInternal(TLSendMedia message, Action<TLStatedMessage> callback, Action fastCallback, Action<TLRPCError> faultCallback)
        {
            SendAsyncInternal("messages.sendMedia", message, callback, fastCallback, faultCallback);
        }

        private void SendAsyncInternal<T>(string caption, TLObject obj, Action<T> callback, Action fastCallback, Action<TLRPCError> faultCallback) where T : TLObject
        {
            int sequenceNumber;
            TLLong messageId;
            lock (_activeTransportRoot)
            {
                sequenceNumber = _activeTransport.SequenceNumber * 2 + 1;
                _activeTransport.SequenceNumber++; 
                messageId = _activeTransport.GenerateMessageId(true);
            }

            var transportMessage = new TLContainerTransportMessage
            {
                MessageId = messageId,
                SeqNo = new TLInt(sequenceNumber),
                MessageData = obj
            };

            var now = DateTime.Now;
            var sendBeforeTime = now.AddSeconds(Constants.MessageSendingInterval);
            var sendingItem = new HistoryItem
            {
                SendTime = now,
                SendBeforeTime = sendBeforeTime,
                Caption = caption,
                Object = obj,
                Message = transportMessage,
                Callback = result => callback((T)result),
                FastCallback = fastCallback,
                FaultCallback = null, // чтобы не вылететь по таймауту не сохраняем сюда faultCallback, а просто запоминаем последнюю ошибку,
                FaultQueueCallback = faultCallback, // для MTProto.CleanupQueue
                InvokeAfter = null,   // устанвливаем в момент создания контейнера historyItems.LastOrDefault(),
                Status = RequestStatus.ReadyToSend,
            };

            //обрабатываем ошибки
            sendingItem.FaultCallback = error => ProcessFault(sendingItem, error);

            lock (_sendingQueueSyncRoot)
            {
                _sendingQueue.Add(sendingItem);

                StartSendingTimer();
            }

            ProcessQueue();
        }

        private void ProcessFault(HistoryItem item, TLRPCError error)
        {
            item.LastError = error;
            if (error != null
                && (error.CodeEquals(ErrorCode.BAD_REQUEST)
                    || error.CodeEquals(ErrorCode.FLOOD)
                    || error.CodeEquals(ErrorCode.UNAUTHORIZED)))
            {
                RemoveFromQueue(item);
                item.FaultQueueCallback.SafeInvoke(error);
            }
        }

        private void ProcessQueue()
        {
            CleanupQueue();

            SendQueue();
        }

        private void SendQueue()
        {
            List<HistoryItem> itemsSnapshort;
            lock (_sendingQueueSyncRoot)
            {
                itemsSnapshort = _sendingQueue.ToList();
            }
            if (itemsSnapshort.Count == 0) return;

            var historyItems = new List<HistoryItem>();
            for (var i = 0; i < itemsSnapshort.Count; i++)
            {
                itemsSnapshort[i].SendTime = DateTime.Now;
                itemsSnapshort[i].InvokeAfter = historyItems.LastOrDefault();
                historyItems.Add(itemsSnapshort[i]);
            }

#if DEBUG
            NotifyOfPropertyChange(() => History);
#endif

            lock (_historyRoot)
            {
                for (var i = 0; i < historyItems.Count; i++)
                {
                    _history[historyItems[i].Hash] = historyItems[i];
                }
            }

            var container = CreateContainer(historyItems);

            SendNonInformativeMessage<TLObject>(
                "container.sendMessages",
                container,
                result =>
                {
                    // переотправка сейчас по таймеру раз в 5 сек
                    // этот метод никогда не вызывается, т.к. не используется в SendNonInformativeMessage для container.sendMessages
                    //lock (_queueSyncRoot)
                    //{
                    //    // fast aknowledgments
                    //    _sendingQueue.Remove(item);
                    //}


                    //item.FastCallback.SafeInvoke();
                },
                error =>
                {
                    // переотправка сейчас по таймеру раз в 5 сек
                    //FaultSending(error, item);
                });
        }

        private void CleanupQueue()
        {
            var itemsToRemove = new List<HistoryItem>();

            lock (_sendingQueueSyncRoot)
            {
                var now = DateTime.Now;
                for (var i = 0; i < _sendingQueue.Count; i++)
                {
                    var historyItem = _sendingQueue[i];
                    if (historyItem.SendBeforeTime > now) continue;

                    itemsToRemove.Add(historyItem);
                    _sendingQueue.RemoveAt(i--);
                }

                if (_sendingQueue.Count == 0)
                {
                    StopSendingTimer();
                }
            }

            lock (_historyRoot)
            {
                for (var i = 0; i < itemsToRemove.Count; i++)
                {
                    _history.Remove(itemsToRemove[i].Hash);
                }
            }

            Helpers.Execute.BeginOnThreadPool(() =>
            {
                foreach (var item in itemsToRemove)
                {
                    item.FaultQueueCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("MTProtoService.CleanupQueue") });
                }
            });
        }

        private void RemoveFromQueue(HistoryItem item)
        {
            if (item == null)
            {
                Helpers.Execute.ShowDebugMessage("MTProtoService.RemoveFromQueue item=null");
                return;
            }

            lock (_sendingQueueSyncRoot)
            {
                _sendingQueue.Remove(item);
            }
        }

        private static TLContainer CreateContainer(IList<HistoryItem> items)
        {
            var messages = new List<TLContainerTransportMessage>();

            for (var i = 0; i < items.Count; i++)
            {
                var item = items[i];

                var transportMessage = (TLContainerTransportMessage)item.Message;
                if (item.InvokeAfter != null)
                {
                    transportMessage.MessageData = new TLInvokeAfterMsg
                    {
                        MsgId = item.InvokeAfter.Message.MessageId,
                        Object = item.Object
                    };
                }

                item.Status = RequestStatus.Sent;

                messages.Add(transportMessage);
            }

            var container = new TLContainer
            {
                Messages = new List<TLContainerTransportMessage> (messages)
            };

            return container;
        }


        public void GetSyncErrorsAsync(Action<ExceptionInfo, IList<ExceptionInfo>> callback)
        {
            Helpers.Execute.BeginOnThreadPool(() => callback.SafeInvoke(_cacheService.LastSyncMessageException, _updatesService.SyncDifferenceExceptions));
        }

        public void GetSendingQueueInfoAsync(Action<string> callback)
        {
            Helpers.Execute.BeginOnThreadPool(() =>
            {
                var info = new StringBuilder();
                lock (_sendingQueueSyncRoot)
                {
                    var count = 0;
                    foreach (var item in _sendingQueue)
                    {
                        var sendBeforeTimeString = item.SendBeforeTime.HasValue
                            ? item.SendBeforeTime.Value.ToString("H:mm:ss.fff")
                            : null;

                        var message = string.Empty;
                        try
                        {
                            var transportMessage = item.Message as TLContainerTransportMessage;
                            if (transportMessage != null)
                            {
                                var sendMessage = transportMessage.MessageData as TLSendMessage;
                                if (sendMessage != null)
                                {
                                    message = string.Format("{0} {1}", sendMessage.Message, sendMessage.RandomId);
                                }
                                else
                                {
                                    var invokeAfterMsg = transportMessage.MessageData as TLInvokeAfterMsg;
                                    if (invokeAfterMsg != null)
                                    {
                                        sendMessage = invokeAfterMsg.Object as TLSendMessage;
                                        if (sendMessage != null)
                                        {
                                            message = string.Format("{0} {1}", sendMessage.Message, sendMessage.RandomId);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        info.AppendLine(string.Format("{0} send={1} before={2} msg=[{3}] error=[{4}]", count++, item.SendTime.ToString("H:mm:ss.fff"), sendBeforeTimeString, message, item.LastError));
                    }
                }

                callback.SafeInvoke(info.ToString());
            });
        }

    }
}
