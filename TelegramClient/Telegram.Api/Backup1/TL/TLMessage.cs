﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using Caliburn.Micro;
using Telegram.Api.Services;
using Telegram.Api.Services.Cache;
using Telegram.Api.Extensions;
using Telegram.Api.TL.Interfaces;

namespace Telegram.Api.TL
{
    [Flags]
    public enum MessageFlags
    {
        Unread = 0x1,
        Out = 0x2,
    }

    public enum MessageStatus
    {
        Sending = 1,
        Confirmed = 0,
        Failed = 2,
        Read = 3,
        Broadcast = 4,
        Compressing = 5,
    }

    public abstract class TLMessageBase : TLObject, ISelectable
    {
        #region Flags

        protected static bool IsSet(TLInt flags, MessageFlags flag)
        {
            var isSet = false;

            if (flags != null)
            {
                var intFlag = (int)flag;
                isSet = (flags.Value & intFlag) == intFlag;
            }

            return isSet;
        }

        protected static void Set(ref TLInt flags, MessageFlags flag)
        {
            var intFlag = (int)flag;

            if (flags != null)
            {
                flags.Value |= intFlag;
            }
            else
            {
                flags = new TLInt(intFlag);
            }
        }

        protected static void Unset(ref TLInt flags, MessageFlags flag)
        {
            var intFlag = (int)flag;

            if (flags != null)
            {
                flags.Value &= ~intFlag;
            }
            else
            {
                flags = new TLInt(0);
            }
        }

        protected static void SetUnset(ref TLInt flags, bool set, MessageFlags flag)
        {
            if (set)
            {
                Set(ref flags, flag);
            }
            else
            {
                Unset(ref flags, flag);
            }
        }
        #endregion

        public abstract int DateIndex { get; set; }

        /// <summary>
        /// Message Random Id during sending to server
        /// </summary>
        private TLLong _randomId;

        public TLLong RandomId { get { return _randomId; } set { _randomId = value; } }

        public long RandomIndex
        {
            get { return RandomId != null ? RandomId.Value : 0; }
            set { RandomId = new TLLong(value); }
        }

        /// <summary>
        /// Message Id
        /// </summary>
        public TLInt Id { get; set; }
       
        public int Index
        {
            get { return Id != null ?  Id.Value : 0; }
            set { Id = new TLInt(value); }
        }

        public virtual void Update(TLMessageBase message)
        {
            Id = message.Id;
            Status = message.Status;
        }

        public override string ToString()
        {
            return "Id=" + Index + " RndId=" + RandomIndex;
        }

        #region Additional

        public MessageStatus _status;

        public virtual MessageStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public bool _isAnimated;

        public bool IsAnimated
        {
            get { return _isAnimated; }
            set { SetField(ref _isAnimated, value, () => IsAnimated); }
        }

        public double ScaleY { get { return IsAnimated ? 0.0 : 1.0; } }

        public virtual bool ShowFrom
        {
            get { return false; }
        }


        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetField(ref _isSelected, value, () => IsSelected); }
        }

        public virtual int MediaSize { get { return 0; } }

        public virtual Visibility MediaSizeVisibility { get { return Visibility.Collapsed; } }

        public virtual bool IsAudioVideoMessage()
        {
            return false;
        }

        public virtual bool IsSticker()
        {
            return false;
        }

        public static bool IsSticker(TLDocumentBase document)
        {
#if WP8
            var document22 = document as TLDocument22;
            if (document22 != null
                && document22.DocumentSize > 0
                && document22.DocumentSize < Constants.StickerMaxSize)
            {
                var documentStickerAttribute = document22.Attributes.FirstOrDefault(x => x is TLDocumentAttributeSticker);

                if (documentStickerAttribute != null
                    && string.Equals(document22.MimeType.ToString(), "image/webp", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
#endif

            return false;
        }

        public TLMessageBase Self { get { return this; } }
        #endregion

    }

    public abstract class TLMessageCommon : TLMessageBase
    {
        public TLInt FromId { get; set; }

        public TLPeerBase ToId { get; set; }

        public virtual TLBool Out { get; set; }

        private TLBool _unread;

        public virtual void SetUnread(TLBool value)
        {
            _unread = value;
        }

        public virtual TLBool Unread
        {
            get { return _unread; }
            set
            {
                SetField(ref _unread, value, () => Unread);
                NotifyOfPropertyChange(() => Status);
            }
        }

        public override MessageStatus Status
        {
            get 
            {
                if (_status == MessageStatus.Broadcast)
                {
                    return _status;
                }

                if (!Unread.Value)
                {
                    return MessageStatus.Read;
                }

                return _status;
            }
            set
            {
                if (_status == MessageStatus.Broadcast) return;

                SetField(ref _status, value, () => Status);
            }
        }

        public override int DateIndex
        {
            get { return Date.Value; }
            set { Date = new TLInt(value); }
        }

        public TLInt _date;

        public TLInt Date
        {
            get { return _date; }
            set { SetField(ref _date, value, () => Date); }
        }

        public override string ToString()
        {
            string dateTimeString = null;
            try
            {
                var clientDelta = IoC.Get<IMTProtoService>().ClientTicksDelta;
                var utc0SecsLong = Date.Value * 4294967296 - clientDelta;
                var utc0SecsInt = utc0SecsLong / 4294967296.0;
                DateTime? dateTime = Helpers.Utils.UnixTimestampToDateTime(utc0SecsInt);
                dateTimeString = dateTime.Value.ToString("H:mm:ss");
            }
            catch (Exception ex)
            {
                
            }

            return base.ToString() + string.Format(" [{0} {4}] FromId={1} ToId=[{2}] U={3} S={5}", Date, FromId, ToId, Unread, dateTimeString, Status);
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            FromId = GetObject<TLInt>(bytes, ref position);
            ToId = GetObject<TLPeerBase>(bytes, ref position);
            Out = GetObject<TLBool>(bytes, ref position);
            _unread = GetObject<TLBool>(bytes, ref position);
            _date = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            FromId = GetObject<TLInt>(input);
            ToId = GetObject<TLPeerBase>(input);
            Out = GetObject<TLBool>(input);
            _unread = GetObject<TLBool>(input);
            _date = GetObject<TLInt>(input);

            var randomId = GetObject<TLLong>(input);
            if (randomId.Value != 0)
            {
                RandomId = randomId;
            }
            var status = GetObject<TLInt>(input);
            Status = (MessageStatus) status.Value;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(FromId.ToBytes());
            ToId.ToStream(output);
            output.Write(Out.ToBytes());
            output.Write(Unread.ToBytes());
            output.Write(Date.ToBytes());

            RandomId = RandomId ?? new TLLong(0);
            RandomId.ToStream(output);
            var status = new TLInt((int) Status);
            output.Write(status.ToBytes());
        }


        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessageCommon) message;
            FromId = m.FromId;
            ToId = m.ToId;
            Out = m.Out;
            if (Unread.Value != m.Unread.Value)
            {
                if (Unread.Value)
                {
                    _unread = m.Unread;
                }
            }
            _date = m.Date;
        }

        #region Additional

        public TLUserBase From
        {
            get
            {
                var cacheService = IoC.Get<ICacheService>();
                return cacheService.GetUser(FromId);
            }
        }

        public override bool ShowFrom
        {
            get { return ToId is TLPeerChat && GetType() != typeof(TLMessageService); }
        }
        #endregion
    }

    public class TLMessageEmpty : TLMessageBase
    {
        public override int DateIndex { get; set; }

        public const uint Signature = TLConstructors.TLMessageEmpty;

        public override string ToString()
        {
            return base.ToString() + ", EmptyMessage";
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            }
            RandomId = GetObject<TLObject>(input) as TLLong;
            var status = GetObject<TLInt>(input);
            Status = (MessageStatus)status.Value;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id = Id ?? new TLInt(0);
            output.Write(Id.ToBytes());

            RandomId.NullableToStream(output);
            var status = new TLInt((int)Status);
            output.Write(status.ToBytes());
        }
    }

    public class TLMessage17 : TLMessage
    {
        public new const uint Signature = TLConstructors.TLMessage17;

        private TLInt _flags;

        public TLInt Flags
        {
            get { return _flags; }
            set { _flags = value; }
        }

        public override TLBool Out
        {
            get { return new TLBool(IsSet(_flags, MessageFlags.Out)); }
            set
            {
                if (value != null)
                {
                    SetUnset(ref _flags, value.Value, MessageFlags.Out);
                }
            }
        }

        public override void SetUnread(TLBool value)
        {
            Unread = value;
        }

        public override TLBool Unread
        {
            get { return new TLBool(IsSet(_flags, MessageFlags.Unread)); }
            set
            {
                if (value != null)
                {
                    SetUnset(ref _flags, value.Value, MessageFlags.Unread);
                    NotifyOfPropertyChange(() => Status);
                }
            }
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Flags = GetObject<TLInt>(bytes, ref position);
            Id = GetObject<TLInt>(bytes, ref position);
            FromId = GetObject<TLInt>(bytes, ref position);
            ToId = GetObject<TLPeerBase>(bytes, ref position);
            _date = GetObject<TLInt>(bytes, ref position);
            Message = GetObject<TLString>(bytes, ref position);
            _media = GetObject<TLMessageMediaBase>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Flags = GetObject<TLInt>(input);
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            }
            FromId = GetObject<TLInt>(input);
            ToId = GetObject<TLPeerBase>(input);
            _date = GetObject<TLInt>(input);
            Message = GetObject<TLString>(input);
            _media = GetObject<TLMessageMediaBase>(input);

            var randomId = GetObject<TLLong>(input);
            if (randomId.Value != 0)
            {
                RandomId = randomId;
            }
            var status = GetObject<TLInt>(input);
            Status = (MessageStatus)status.Value;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            
            Flags.ToStream(output);
            Id = Id ?? new TLInt(0);
            output.Write(Id.ToBytes());
            output.Write(FromId.ToBytes());
            ToId.ToStream(output);
            output.Write(Date.ToBytes());
            Message.ToStream(output);
            _media.ToStream(output);

            RandomId = RandomId ?? new TLLong(0);
            RandomId.ToStream(output);
            var status = new TLInt((int)Status);
            output.Write(status.ToBytes());
        }
        
        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessage17)message;
            Flags = m.Flags;
        }
    }

    public class TLMessage : TLMessageCommon
    {
        public const uint Signature = TLConstructors.TLMessage;

        public TLString Message { get; set; }

        public TLMessageMediaBase _media;

        public TLMessageMediaBase Media
        {
            get { return _media; }
            set { SetField(ref _media, value, () => Media); }
        }

        public override int MediaSize
        {
            get { return Media.MediaSize; }
        }

        public override Visibility MediaSizeVisibility
        {
            get { return _media is TLMessageMediaVideo ? Visibility.Visible : Visibility.Collapsed; }
        }

        public override bool IsAudioVideoMessage()
        {
            return _media is TLMessageMediaAudio || _media is TLMessageMediaVideo;
        }

        public override bool IsSticker()
        {
            var mediaDocument = _media as TLMessageMediaDocument;
            if (mediaDocument != null)
            {
                return IsSticker(mediaDocument.Document);
            }

            return false;
        }

        public override string ToString()
        {
            var messageString = Message.ToString();

            return base.ToString() + ((Media == null || Media is TLMessageMediaEmpty)? " Msg=" + messageString.Substring(0, Math.Min(messageString.Length, 5)) : " Media=" + Media.GetType().Name);
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            base.FromBytes(bytes, ref position);
            Message = GetObject<TLString>(bytes, ref position);
            _media = GetObject<TLMessageMediaBase>(bytes, ref position);          

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            }
            base.FromStream(input);
            Message = GetObject<TLString>(input);
            _media = GetObject<TLMessageMediaBase>(input); 

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id = Id ?? new TLInt(0);
            output.Write(Id.ToBytes());
            base.ToStream(output);
            Message.ToStream(output);
            _media.ToStream(output);
        }

        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessage)message;
            Message = m.Message;
            var oldMedia = Media;
            var newMedia = m.Media;
            if (oldMedia.GetType() != newMedia.GetType())
            {
                _media = m.Media;
            }
            else
            {
                var oldMediaDocument = oldMedia as TLMessageMediaDocument;
                var newMediaDocument = newMedia as TLMessageMediaDocument;
                if (oldMediaDocument != null && newMediaDocument != null)
                {
                    if (oldMediaDocument.Document.GetType() != newMediaDocument.Document.GetType())
                    {
                        _media = m.Media;
                    }
                    else
                    {
                        var oldDocument = oldMediaDocument.Document as TLDocument;
                        var newDocument = newMediaDocument.Document as TLDocument;
                        if (oldDocument != null
                            && newDocument != null
                            && (oldDocument.Id.Value != newDocument.Id.Value
                                || oldDocument.AccessHash.Value != newDocument.AccessHash.Value))
                        {
                            var isoFileName = Media.IsoFileName;
#if WP8
                            var file = Media.File;
#endif
                            _media = m.Media;
                            _media.IsoFileName = isoFileName;
#if WP8
                            _media.File = file;
#endif
                        }
                    }

                    return;
                }

                var oldMediaVideo = oldMedia as TLMessageMediaVideo;
                var newMediaVideo = newMedia as TLMessageMediaVideo;
                if (oldMediaVideo != null && newMediaVideo != null)
                {
                    if (oldMediaVideo.Video.GetType() != newMediaVideo.Video.GetType())
                    {
                        _media = m.Media;
                    }
                    else
                    {
                        var oldVideo = oldMediaVideo.Video as TLVideo;
                        var newVideo = newMediaVideo.Video as TLVideo;
                        if (oldVideo != null
                            && newVideo != null
                            && (oldVideo.Id.Value != newVideo.Id.Value
                                || oldVideo.AccessHash.Value != newVideo.AccessHash.Value))
                        {
                            var isoFileName = Media.IsoFileName;
                            _media = m.Media;
                            _media.IsoFileName = isoFileName;
                        }
                    }

                    return;
                }

                var oldMediaAudio = oldMedia as TLMessageMediaAudio;
                var newMediaAudio = newMedia as TLMessageMediaAudio;
                if (oldMediaAudio != null && newMediaAudio != null)
                {
                    if (oldMediaAudio.Audio.GetType() != newMediaAudio.Audio.GetType())
                    {
                        _media = m.Media;
                    }
                    else
                    {
                        var oldAudio = oldMediaAudio.Audio as TLAudio;
                        var newAudio = newMediaAudio.Audio as TLAudio;
                        if (oldAudio != null
                            && newAudio != null
                            && (oldAudio.Id.Value != newAudio.Id.Value
                                || oldAudio.AccessHash.Value != newAudio.AccessHash.Value))
                        {
                            var isoFileName = Media.IsoFileName;
                            _media = m.Media;
                            _media.IsoFileName = isoFileName;
                        }
                    }

                    return;
                }

                var oldMediaPhoto = oldMedia as TLMessageMediaPhoto;
                var newMediaPhoto = newMedia as TLMessageMediaPhoto;
                if (oldMediaPhoto == null || newMediaPhoto == null)
                {
                    _media = m.Media;
                }
                else
                {
                    var oldPhoto = oldMediaPhoto.Photo as TLPhoto;
                    var newPhoto = newMediaPhoto.Photo as TLPhoto;
                    if (oldPhoto == null || newPhoto == null)
                    {
                        _media = m.Media;
                    }
                    else
                    {
                        if (oldPhoto.AccessHash.Value != newPhoto.AccessHash.Value)
                        {
                            _media = m.Media;
                        }
                    }
                }
            }
        }

        #region Additional

        private TLInputMediaBase _inputMedia;

        /// <summary>
        /// To resend canceled message
        /// </summary>
        public TLInputMediaBase InputMedia
        {
            get { return _inputMedia; }
            set { SetField(ref _inputMedia, value, () => InputMedia); }
        }

        #endregion
    }

    public class TLMessageForwarded17 : TLMessageForwarded
    {
        public new const uint Signature = TLConstructors.TLMessageForwarded17;

        private TLInt _flags;

        public TLInt Flags
        {
            get { return _flags; }
            set { _flags = value; }
        }

        public override TLBool Out
        {
            get { return new TLBool(IsSet(_flags, MessageFlags.Out)); }
            set
            {
                if (value != null)
                {
                    SetUnset(ref _flags, value.Value, MessageFlags.Out);
                }
            }
        }

        public override void SetUnread(TLBool value)
        {
            Unread = value;
        }

        public override TLBool Unread
        {
            get { return new TLBool(IsSet(_flags, MessageFlags.Unread)); }
            set
            {
                if (value != null)
                {
                    SetUnset(ref _flags, value.Value, MessageFlags.Unread);
                    NotifyOfPropertyChange(() => Status);
                }
            }
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Flags = GetObject<TLInt>(bytes, ref position);
            Id = GetObject<TLInt>(bytes, ref position);
            FwdFromId = GetObject<TLInt>(bytes, ref position);
            FwdDate = GetObject<TLInt>(bytes, ref position);
            FromId = GetObject<TLInt>(bytes, ref position);
            ToId = GetObject<TLPeerBase>(bytes, ref position);
            _date = GetObject<TLInt>(bytes, ref position);
            Message = GetObject<TLString>(bytes, ref position);
            _media = GetObject<TLMessageMediaBase>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Flags = GetObject<TLInt>(input);
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            }
            FwdFromId = GetObject<TLInt>(input);
            FwdDate = GetObject<TLInt>(input);
            FromId = GetObject<TLInt>(input);
            ToId = GetObject<TLPeerBase>(input);
            _date = GetObject<TLInt>(input);
            Message = GetObject<TLString>(input);
            _media = GetObject<TLMessageMediaBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Flags.ToStream(output);
            Id = Id ?? new TLInt(0);
            Id.ToStream(output);
            FwdFromId.ToStream(output);
            FwdDate.ToStream(output);
            FromId.ToStream(output);
            ToId.ToStream(output);
            _date.ToStream(output);
            Message.ToStream(output);
            _media.ToStream(output);
        }

        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessageForwarded17)message;
            Flags = m.Flags;
        }
    }

    public class TLMessageForwarded : TLMessage
    {
        public new const uint Signature = TLConstructors.TLMessageForwarded;
        
        public TLInt FwdFromId { get; set; }

        public TLUserBase FwdFrom
        {
            get
            {
                var cacheService = IoC.Get<ICacheService>();
                return cacheService.GetUser(FwdFromId);
            }
        }

        public TLInt FwdDate { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            FwdFromId = GetObject<TLInt>(bytes, ref position);
            FwdDate = GetObject<TLInt>(bytes, ref position);
            FromId = GetObject<TLInt>(bytes, ref position);
            ToId = GetObject<TLPeerBase>(bytes, ref position);
            Out = GetObject<TLBool>(bytes, ref position);
            Unread = GetObject<TLBool>(bytes, ref position);
            _date = GetObject<TLInt>(bytes, ref position);
            Message = GetObject<TLString>(bytes, ref position);
            _media = GetObject<TLMessageMediaBase>(bytes, ref position);
            
            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            } 
            FwdFromId = GetObject<TLInt>(input);
            FwdDate = GetObject<TLInt>(input);
            FromId = GetObject<TLInt>(input);
            ToId = GetObject<TLPeerBase>(input);
            Out = GetObject<TLBool>(input);
            Unread = GetObject<TLBool>(input);
            _date = GetObject<TLInt>(input);
            Message = GetObject<TLString>(input);
            _media = GetObject<TLMessageMediaBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id = Id ?? new TLInt(0);
            Id.ToStream(output);
            FwdFromId.ToStream(output);
            FwdDate.ToStream(output);
            FromId.ToStream(output);
            ToId.ToStream(output);
            Out.ToStream(output);
            Unread.ToStream(output);
            _date.ToStream(output);
            Message.ToStream(output);
            _media.ToStream(output);
        }

        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessageForwarded)message;
            FwdFromId = m.FwdFromId;
            FwdDate = m.FwdDate;
        }
    }

    public class TLMessageService17 : TLMessageService
    {
        public new const uint Signature = TLConstructors.TLMessageService17;

        private TLInt _flags;

        public TLInt Flags
        {
            get { return _flags; }
            set { _flags = value; }
        }

        public override TLBool Out
        {
            get { return new TLBool(IsSet(_flags, MessageFlags.Out)); }
            set
            {
                if (value != null)
                {
                    SetUnset(ref _flags, value.Value, MessageFlags.Out);
                }
            }
        }

        public override void SetUnread(TLBool value)
        {
            Unread = value;
        }

        public override TLBool Unread
        {
            get { return new TLBool(IsSet(_flags, MessageFlags.Unread)); }
            set
            {
                if (value != null)
                {
                    SetUnset(ref _flags, value.Value, MessageFlags.Unread);
                    NotifyOfPropertyChange(() => Status);
                }
            }
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Flags = GetObject<TLInt>(bytes, ref position);
            Id = GetObject<TLInt>(bytes, ref position);
            FromId = GetObject<TLInt>(bytes, ref position);
            ToId = GetObject<TLPeerBase>(bytes, ref position);
            _date = GetObject<TLInt>(bytes, ref position);
            Action = GetObject<TLMessageActionBase>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Flags = GetObject<TLInt>(input);
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            }
            FromId = GetObject<TLInt>(input);
            ToId = GetObject<TLPeerBase>(input);
            _date = GetObject<TLInt>(input);
            Action = GetObject<TLMessageActionBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Flags.ToStream(output);
            Id = Id ?? new TLInt(0);
            Id.ToStream(output);
            FromId.ToStream(output);
            ToId.ToStream(output);
            _date.ToStream(output);
            Action.ToStream(output);
        }

        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessageService17)message;

            Flags = m.Flags;
        }
    }

    public class TLMessageService : TLMessageCommon
    {
        public const uint Signature = TLConstructors.TLMessageService;

        public TLMessageActionBase Action { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            base.FromBytes(bytes, ref position);
            Action = GetObject<TLMessageActionBase>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            var id = GetObject<TLInt>(input);
            if (id.Value != 0)
            {
                Id = id;
            }
            base.FromStream(input);
            Action = GetObject<TLMessageActionBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id = Id ?? new TLInt(0);
            Id.ToStream(output);
            base.ToStream(output);
            Action.ToStream(output);
        }

        public override void Update(TLMessageBase message)
        {
            base.Update(message);
            var m = (TLMessageService)message;

            if (Action != null)
            {
                Action.Update(m.Action);
            }
            else
            {
                Action = m.Action;
            }
        }
    }
}
