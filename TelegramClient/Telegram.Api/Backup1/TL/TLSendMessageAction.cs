﻿namespace Telegram.Api.TL
{
    public abstract class TLSendMessageActionBase : TLObject { }

    public class TLSendMessageTypingAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageTypingAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageCancelAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageCancelAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageRecordVideoAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageRecordVideoAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageUploadVideoAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageUploadVideoAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageRecordAudioAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageRecordAudioAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageUploadAudioAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageUploadAudioAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageUploadPhotoAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageUploadPhotoAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageUploadDocumentAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageUploadDocumentAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageGeoLocationAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageGeoLocationAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }

    public class TLSendMessageChooseContactAction : TLSendMessageActionBase
    {
        public const uint Signature = TLConstructors.TLSendMessageChooseContactAction;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }
    }
}
