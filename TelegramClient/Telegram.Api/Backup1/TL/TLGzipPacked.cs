﻿using System.Diagnostics;
using System.IO;
#if SILVERLIGHT

using SharpGIS;
#else
using System.IO.Compression;

#endif
using System.Linq;
using Telegram.Api.Helpers;

namespace Telegram.Api.TL
{
    public class TLGzipPacked : TLObject
    {
        public const uint Signature = TLConstructors.TLGzipPacked;

        public TLString PackedData { get; set; }

        public TLObject Data { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLGzipPacked--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

           // var stopwatch = Stopwatch.StartNew();

            PackedData = GetObject<TLString>(bytes, ref position);

            var decopressedData = new byte[] {};
            var compressedData = PackedData.Data;                   //Remove reverse here
            var buffer = new byte[4096];
            
#if SILVERLIGHT
            var hgs = new GZipInflateStream(new MemoryStream(compressedData));
#else
            var hgs = new GZipStream(new MemoryStream(compressedData), CompressionMode.Decompress); 
#endif

            var bytesRead = hgs.Read(buffer, 0, buffer.Length);
            while (bytesRead > 0)
            {
                decopressedData = TLUtils.Combine(decopressedData, buffer.SubArray(0, bytesRead));
                bytesRead = hgs.Read(buffer, 0, buffer.Length);
            }

            bytesRead = 0;

            //TLUtils.WritePerformance("GZIP decompress " + stopwatch.Elapsed);
            Data = GetObject<TLObject>(decopressedData, ref bytesRead);
            //string sample = "This is a compression test of microsoft .net gzip compression method and decompression methods";
            //System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            //byte[] data = encoding.GetBytes(sample);
            //bool result = false;

            ////Compress
            //MemoryStream cmpStream;
            //cmpStream = new MemoryStream();
            //GZipStream hgs = new GZipStream(cmpStream, CompressionMode.Compress);
            //hgs.Write(data, 0, data.Length);
            //byte[] cmpData = cmpStream.ToArray();

            //MemoryStream decomStream;
            //decomStream = new MemoryStream(cmpData);
            //hgs = new GZipStream(decomStream, CompressionMode.Decompress);
            //hgs.Read(data, 0, data.Length);

            //string sampleOut = System.BitConverter.ToString(data);

            //result = String.Equals(sample, sampleOut) ;
            //return result;

            
            return this;
        }
    }
}
