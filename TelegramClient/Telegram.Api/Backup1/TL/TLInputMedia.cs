﻿using System.IO;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLInputMediaBase : TLObject
    {
#region Additional
        public byte[] MD5Hash { get; set; }
#endregion
    }

    public class TLInputMediaEmpty : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaEmpty;

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }

    public class TLInputMediaUploadedDocument : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedDocument;

        public TLInputFileBase File { get; set; }

        public TLString FileName { get; set; }

        public TLString MimeType { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                FileName.ToBytes(),
                MimeType.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFile>(input);
            FileName = GetObject<TLString>(input);
            MimeType = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            FileName.ToStream(output);
            MimeType.ToStream(output);
        }
    }

    public class TLInputMediaUploadedDocument22 : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedDocument22;

        public TLInputFileBase File { get; set; }

        public TLString MimeType { get; set; }

        public TLVector<TLDocumentAttributeBase> Attributes { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                MimeType.ToBytes(),
                Attributes.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFile>(input);
            MimeType = GetObject<TLString>(input);
            Attributes = GetObject<TLVector<TLDocumentAttributeBase>>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            MimeType.ToStream(output);
            Attributes.ToStream(output);
        }
    }

    public class TLInputMediaUploadedThumbDocument : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedThumbDocument;

        public TLInputFileBase File { get; set; }

        public TLInputFileBase Thumb { get; set; }

        public TLString FileName { get; set; }

        public TLString MimeType { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                Thumb.ToBytes(),
                FileName.ToBytes(),
                MimeType.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFileBase>(input);
            Thumb = GetObject<TLInputFileBase>(input);
            FileName = GetObject<TLString>(input);
            MimeType = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            Thumb.ToStream(output);
            FileName.ToStream(output);
            MimeType.ToStream(output);
        }
    }

    public class TLInputMediaUploadedThumbDocument22 : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedThumbDocument22;

        public TLInputFileBase File { get; set; }

        public TLInputFileBase Thumb { get; set; }

        public TLString MimeType { get; set; }

        public TLVector<TLDocumentAttributeBase> Attributes { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                Thumb.ToBytes(),
                MimeType.ToBytes(),
                Attributes.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFileBase>(input);
            Thumb = GetObject<TLInputFileBase>(input);
            MimeType = GetObject<TLString>(input);
            Attributes = GetObject<TLVector<TLDocumentAttributeBase>>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            Thumb.ToStream(output);
            MimeType.ToStream(output);
            Attributes.ToStream(output);
        }
    }

    public class TLInputMediaDocument : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaDocument;

        public TLInputDocumentBase Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInputDocumentBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id.ToStream(output);
        }
    }

    public class TLInputMediaUploadedAudio : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedAudio;

        public TLInputFile File { get; set; }

        public TLInt Duration { get; set; }

        public TLString MimeType { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                Duration.ToBytes(),
                MimeType.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFile>(input);
            Duration = GetObject<TLInt>(input);
            MimeType = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            Duration.ToStream(output);
            MimeType.ToStream(output);
        }
    }

    public class TLInputMediaAudio : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaAudio;

        public TLInputAudioBase Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes());
        }
    }

    public class TLInputMediaUploadedPhoto : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedPhoto;

        public TLInputFileBase File { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFile>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
        }
    }

    public class TLInputMediaPhoto : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaPhoto;

        public TLInputPhotoBase Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInputPhotoBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id.ToStream(output);
        }
    }

    public class TLInputMediaGeoPoint : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaGeoPoint;

        public TLInputGeoPointBase GeoPoint { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                GeoPoint.ToBytes());
        }
    }

    public class TLInputMediaContact : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaContact;

        public TLString PhoneNumber { get; set; }
        public TLString FirstName { get; set; }
        public TLString LastName { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                PhoneNumber.ToBytes(),
                FirstName.ToBytes(),
                LastName.ToBytes());
        }
    }

    public class TLInputMediaUploadedVideo : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedVideo;

        public TLInputFileBase File { get; set; }
        public TLInt Duration { get; set; }
        public TLInt W { get; set; }
        public TLInt H { get; set; }
        public TLString MimeType { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                Duration.ToBytes(),
                W.ToBytes(),
                H.ToBytes(),
                MimeType.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFile>(input);
            Duration = GetObject<TLInt>(input);
            W = GetObject<TLInt>(input);
            H = GetObject<TLInt>(input);
            MimeType = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            Duration.ToStream(output);
            W.ToStream(output);
            H.ToStream(output);
            MimeType.ToStream(output);
        }
    }

    public class TLInputMediaUploadedThumbVideo : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaUploadedThumbVideo;

        public TLInputFileBase File { get; set; }
        public TLInputFile Thumb { get; set; }
        public TLInt Duration { get; set; }
        public TLInt W { get; set; }
        public TLInt H { get; set; }
        public TLString MimeType { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                File.ToBytes(),
                Thumb.ToBytes(),
                Duration.ToBytes(),
                W.ToBytes(),
                H.ToBytes(),
                MimeType.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            File = GetObject<TLInputFile>(input);
            Thumb = GetObject<TLInputFile>(input);
            Duration = GetObject<TLInt>(input);
            W = GetObject<TLInt>(input);
            H = GetObject<TLInt>(input);
            MimeType = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            File.ToStream(output);
            Thumb.ToStream(output);
            Duration.ToStream(output);
            W.ToStream(output);
            H.ToStream(output);
            MimeType.ToStream(output);
        }
    }

    public class TLInputMediaVideo : TLInputMediaBase
    {
        public const uint Signature = TLConstructors.TLInputMediaVideo;

        public TLInputVideoBase Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes());
        }
    }
}
