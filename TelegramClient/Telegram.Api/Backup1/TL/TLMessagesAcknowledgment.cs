namespace Telegram.Api.TL
{
    public class TLMessagesAcknowledgment : TLObject
    {
        public const uint Signature = TLConstructors.TLMessagesAcknowledgment; 

        public TLVector<TLLong> MessageIds { get; set; } 

        /// <summary>
        /// Parses bytes arrays to TLObject from specific position, than adds count of readed bytes to position variable.
        /// </summary>
        /// <param name="bytes">    The bytes array. </param>
        /// <param name="position"> The position. </param>
        /// <returns>   The TLObject. </returns>
        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLMessagesAcknowledgment--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            MessageIds = GetObject<TLVector<TLLong>>(bytes, ref position);

            return this;
        }
    }
}