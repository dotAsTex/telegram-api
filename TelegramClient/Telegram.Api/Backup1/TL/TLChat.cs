﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Telegram.Api.Extensions;
using Telegram.Api.TL.Interfaces;

namespace Telegram.Api.TL
{
    public abstract class TLChatBase : TLObject, IInputPeer, IFullName
    {
        public int Index
        {
            get { return Id.Value; }
            set { Id = new TLInt(value); }
        }

        public TLInt Id { get; set; }

        public virtual void Update(TLChatBase chat)
        {
            Id = chat.Id;

            if (chat.Participants != null)
            {
                Participants = chat.Participants;
            }

            if (chat.ChatPhoto != null)
            {
                ChatPhoto = chat.ChatPhoto;
            }

            if (chat.NotifySettings != null)
            {
                _notifySettings = chat.NotifySettings;
            }
        }

        public abstract TLInputPeerBase ToInputPeer();

        public abstract string GetUnsendedTextFileName();

        #region Full chat information
        
        public TLChatParticipantsBase Participants { get; set; }

        public TLPhotoBase ChatPhoto { get; set; }

        protected TLPeerNotifySettingsBase _notifySettings;

        public TLPeerNotifySettingsBase NotifySettings
        {
            get { return _notifySettings; }
            set { SetField(ref _notifySettings, value, () => NotifySettings); }
        }

        public int UsersOnline { get; set; }
        #endregion

        public TLInputNotifyPeerBase ToInputNotifyPeer()
        {
            return new TLInputNotifyPeer { Peer = ToInputPeer() };
        }

        public abstract string FullName { get; }

        public abstract bool IsForbidden { get; }

        #region Additional
        public IList<string> FullNameWords { get; set; }
        #endregion
    }

    public class TLChatEmpty : TLChatBase
    {
        public const uint Signature = TLConstructors.TLChatEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            
            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
        }


        public override string FullName
        {
            get { return string.Empty; }
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerChat { ChatId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "c" + Id + ".dat";
        }

        public override bool IsForbidden
        {
            get { return true; }
        }
    }

    public class TLChat : TLChatBase
    {
        public const uint Signature = TLConstructors.TLChat;

        private TLString _title;

        public TLString Title
        {
            get { return _title; }
            set
            {
                SetField(ref _title, value, () => Title);
                NotifyOfPropertyChange(() => FullName);
            }
        }

        private TLPhotoBase _photo;

        public TLPhotoBase Photo
        {
            get { return _photo; }
            set { SetField(ref _photo, value, () => Photo); }
        }

        public TLInt ParticipantsCount { get; set; }

        public TLInt Date { get; set; }

        public TLBool Left { get; set; }

        public TLInt Version { get; set; }

        public override string ToString()
        {
            return Title.ToString();
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            _title = GetObject<TLString>(bytes, ref position);
            _photo = GetObject<TLPhotoBase>(bytes, ref position);
            ParticipantsCount = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Left = GetObject<TLBool>(bytes, ref position);
            Version = GetObject<TLInt>(bytes, ref position);
            
            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _title = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            ParticipantsCount = GetObject<TLInt>(input);
            Date = GetObject<TLInt>(input);
            Left = GetObject<TLBool>(input);
            Version = GetObject<TLInt>(input);

            Participants = GetObject<TLObject>(input) as TLChatParticipants;
            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(Title.ToBytes());
            Photo.ToStream(output);
            output.Write(ParticipantsCount.ToBytes());
            output.Write(Date.ToBytes());
            output.Write(Left.ToBytes());
            output.Write(Version.ToBytes());

            Participants.NullableToStream(output);
            NotifySettings.NullableToStream(output);
        }

        public override string FullName
        {
            get { return Title != null ? Title.ToString() : string.Empty; }
        }

        public override void Update(TLChatBase chat)
        {
            base.Update(chat);
            var c = chat as TLChat;
            if (c != null)
            {
                _title = c.Title;
                if (Photo.GetType() != c.Photo.GetType())
                {
                    Photo = c.Photo;    // при удалении фото чата не обновляется UI при _photo = c.Photo
                }
                else
                {
                    Photo.Update(c.Photo);
                }
                ParticipantsCount = c.ParticipantsCount;
                Date = c.Date;
                Left = c.Left;
                Version = c.Version;
            }
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerChat { ChatId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "c" + Id + ".dat";
        }

        public override bool IsForbidden
        {
            get { return Left.Value; }
        }
    }

    public class TLChatForbidden : TLChatBase
    {
        public const uint Signature = TLConstructors.TLChatForbidden;

        public TLString Title { get; set; }

        public TLInt Date { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            Title = GetObject<TLString>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            
            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            Title = GetObject<TLString>(input);
            Date = GetObject<TLInt>(input);

            Participants = GetObject<TLObject>(input) as TLChatParticipantsBase;
            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(Title.ToBytes());
            output.Write(Date.ToBytes());

            Participants.NullableToStream(output);
            NotifySettings.NullableToStream(output);
        }

        public override void Update(TLChatBase chat)
        {
            base.Update(chat);
            var c = (TLChatForbidden)chat;
            Title = c.Title;
            Date = c.Date;
        }

        public override string FullName
        {
            get
            {
                return Title != null ? Title.ToString() : string.Empty;
            }
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerChat { ChatId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "c" + Id + ".dat";
        }

        public override bool IsForbidden
        {
            get { return true; }
        }
    }

    public class TLBroadcastChat : TLChatBase
    {
        public const uint Signature = TLConstructors.TLBroadcastChat;

        public TLVector<TLInt> ParticipantIds { get; set; }

        private TLString _title;

        public TLString Title
        {
            get { return _title; }
            set
            {
                SetField(ref _title, value, () => Title);
                NotifyOfPropertyChange(() => FullName);
            }
        }

        private TLPhotoBase _photo;

        public TLPhotoBase Photo
        {
            get { return _photo; }
            set { SetField(ref _photo, value, () => Photo); }
        }

        public override string FullName
        {
            get { return Title != null ? Title.ToString() : string.Empty; }
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            _title = GetObject<TLString>(input);
            _photo = GetObject<TLPhotoBase>(input);
            ParticipantIds = GetObject<TLVector<TLInt>>(input);

            Participants = GetObject<TLObject>(input) as TLChatParticipants;
            _notifySettings = GetObject<TLObject>(input) as TLPeerNotifySettingsBase;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(Title.ToBytes());
            Photo.ToStream(output);
            ParticipantIds.ToStream(output);

            Participants.NullableToStream(output);
            NotifySettings.NullableToStream(output);
        }

        public override void Update(TLChatBase chat)
        {
            base.Update(chat);
            var c = chat as TLBroadcastChat;
            if (c != null)
            {
                _title = c.Title;
                if (Photo.GetType() != c.Photo.GetType())
                {
                    _photo = c.Photo;
                }
                else
                {
                    Photo.Update(c.Photo);
                }
                ParticipantIds = c.ParticipantIds;
            }
        }

        public override TLInputPeerBase ToInputPeer()
        {
            return new TLInputPeerBroadcast { ChatId = Id };
        }

        public override string GetUnsendedTextFileName()
        {
            return "b" + Id + ".dat";
        }

        public override bool IsForbidden
        {
            get { return false; }
        }
    }
}
