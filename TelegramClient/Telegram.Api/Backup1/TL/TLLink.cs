﻿using System.IO;
using System.Runtime.Serialization;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    [DataContract]
    public class TLLink : TLObject
    {
        public const uint Signature = TLConstructors.TLLink;

        [DataMember]
        public TLMyLinkBase MyLink { get; set; }

        [DataMember]
        public TLForeignLinkBase ForeignLink { get; set; }

        [DataMember]
        public TLUserBase User { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            MyLink = GetObject<TLMyLinkBase>(bytes, ref position);
            ForeignLink = GetObject<TLForeignLinkBase>(bytes, ref position);
            User = GetObject<TLUserBase>(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            MyLink = GetObject<TLMyLinkBase>(input);
            ForeignLink = GetObject<TLForeignLinkBase>(input);
            User = GetObject<TLUserBase>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));

            MyLink.ToStream(output);
            ForeignLink.ToStream(output);
            User.ToStream(output);
        }
    }
}
