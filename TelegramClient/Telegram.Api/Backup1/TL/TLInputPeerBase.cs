﻿using System.Linq;

namespace Telegram.Api.TL
{
    public abstract class  TLInputPeerBase : TLObject { }

    public class TLInputPeerEmpty : TLInputPeerBase
    {
        public const uint Signature = TLConstructors.TLInputPeerEmpty;

#region Additional
        public TLInt UserId { get; set; }
#endregion

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override string ToString()
        {
            return "TLInputPeerEmpty";
        }
    }

    public class TLInputPeerSelf : TLInputPeerBase
    {
        public const uint Signature = TLConstructors.TLInputPeerSelf;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override string ToString()
        {
            return "TLInputPeerSelf";
        }

        #region Additional
        public TLInt SelfId { get; set; }
        #endregion
    }

    public class TLInputPeerContact : TLInputPeerBase
    {
        public const uint Signature = TLConstructors.TLInputPeerContact;

        public TLInt UserId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature)
                .Concat(UserId.ToBytes())
                .ToArray();
        }

        public override string ToString()
        {
            return "UserId " + UserId;
        }
    }

    public class TLInputPeerForeign : TLInputPeerBase
    {
        public const uint Signature = TLConstructors.TLInputPeerForeign;

        public TLInt UserId { get; set; }

        public TLLong AccessHash { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            UserId = GetObject<TLInt>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature)
                .Concat(UserId.ToBytes())
                .Concat(AccessHash.ToBytes())
                .ToArray();
        }

        public override string ToString()
        {
            return "UserId " + UserId + " AccessHash " + AccessHash;
        }
    }

    public class TLInputPeerChat : TLInputPeerBase
    {
        public const uint Signature = TLConstructors.TLInputPeerChat;

        public TLInt ChatId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature)
                .Concat(ChatId.ToBytes())
                .ToArray();
        }

        public override string ToString()
        {
            return "ChatId " + ChatId;
        }
    }

    public class TLInputPeerBroadcast : TLInputPeerBase
    {
        public const uint Signature = TLConstructors.TLInputPeerBroadcast;

        public TLInt ChatId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature)
                .Concat(ChatId.ToBytes())
                .ToArray();
        }

        public override string ToString()
        {
            return "ChatId " + ChatId;
        }
    }
}
