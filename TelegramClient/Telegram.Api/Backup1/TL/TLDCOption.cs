using System;
using System.Runtime.Serialization;

namespace Telegram.Api.TL
{
    [DataContract]
    public class TLDCOption : TLObject
    {
        public const uint Signature = TLConstructors.TLDCOption;

        [DataMember]
        public TLInt Id { get; set; }

        [DataMember]
        public TLString Hostname { get; set; }

        [DataMember]
        public TLString IpAddress { get; set; }

        [DataMember]
        public TLInt Port { get; set; }

        #region Additional
        [DataMember]
        public byte[] AuthKey { get; set; }

        [DataMember]
        public TLLong Salt { get; set; }

        [DataMember]
        public long ClientTicksDelta { get; set; }

        //[DataMember] //Important this field initialize with random value on each app startup to avoid TLBadMessage result with 32, 33 code (incorrect MsgSeqNo)
        public TLLong SessionId { get; set; }
        #endregion

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            Hostname = GetObject<TLString>(bytes, ref position);
            IpAddress = GetObject<TLString>(bytes, ref position);
            Port = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public bool AreEquals(TLDCOption dcOption)
        {
            if (dcOption == null) return false;

            return Id.Value == dcOption.Id.Value;
        }

        public override string ToString()
        {
            return string.Format("{0}) {1}:{2} (AuthKey {3})\n  Salt {4} TicksDelta {5}", Id, IpAddress, Port, AuthKey != null, Salt, ClientTicksDelta);
        }
    }
}