﻿namespace Telegram.Api.TL.Functions.Messages
{
    class TLDeleteMessages : TLObject
    {
        public const string Signature = "#14f2dd0a";

        public TLVector<TLInt> Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes());
        }
    }
}
