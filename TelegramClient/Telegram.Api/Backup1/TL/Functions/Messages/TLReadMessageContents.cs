﻿namespace Telegram.Api.TL.Functions.Messages
{
    class TLReadMessageContents : TLObject
    {
        public const string Signature = "#354b5bc2";

        public TLVector<TLInt> Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes());
        }
    }
}
