﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLForwardMessages : TLObject
    {
        public const string Signature = "#514cd10f";

        public TLInputPeerBase Peer { get; set; }

        public TLVector<TLInt> Id { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Peer.ToBytes(),
                Id.ToBytes());
        }
    }
}
