﻿using System.Collections.Generic;
using System.Text;

namespace Telegram.Api.TL
{
    public abstract class TLUpdatesBase : TLObject
    {
        public abstract IList<TLInt> GetSeq();
    }


    public class TLUpdatesTooLong : TLUpdatesBase
    {
        public const uint Signature = TLConstructors.TLUpdatesTooLong;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override IList<TLInt> GetSeq()
        {
            return new List<TLInt>();
        }
    }

    public class TLUpdatesShortMessage : TLUpdatesBase
    {
        public const uint Signature = TLConstructors.TLUpdateShortMessage;

        public TLInt Id { get; set; }

        public TLInt FromId { get; set; }

        public TLString Message { get; set; }

        public TLInt Pts { get; set; }

        public TLInt Date { get; set; }

        public TLInt Seq { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            TLUtils.WriteLine("Id: " + Id.Value);
            FromId = GetObject<TLInt>(bytes, ref position);
            Message = GetObject<TLString>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override IList<TLInt> GetSeq()
        {
            return new List<TLInt>{Seq};
        }

        public override string ToString()
        {
            return string.Format("UserMessage: FromId: {0} Message: {1}", FromId, Message);
        }
    }

    public class TLUpdatesShortChatMessage : TLUpdatesShortMessage
    {
        public new const uint Signature = TLConstructors.TLUpdateShortChatMessage;

        public TLInt ChatId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            FromId = GetObject<TLInt>(bytes, ref position);
            ChatId = GetObject<TLInt>(bytes, ref position);
            Message = GetObject<TLString>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override string ToString()
        {
            return string.Format("ChatMessage: ChatId: {0} FromId: {1} Message: {2}", ChatId, FromId, Message);
        }
    }

    public class TLUpdatesShort : TLUpdatesBase
    {
        public const uint Signature = TLConstructors.TLUpdateShort;

        public TLUpdateBase Update { get; set; }

        public TLInt Date { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Update = GetObject<TLUpdateBase>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override IList<TLInt> GetSeq()
        {
            return new List<TLInt>();
        }

        public override string ToString()
        {
            return "TLUpdatesShort Update: " + Update;
        }
    }

    public class TLUpdates : TLUpdatesBase
    {
        public const uint Signature = TLConstructors.TLUpdates;

        public TLVector<TLUpdateBase> Updates { get; set; }

        public TLVector<TLUserBase> Users { get; set; }

        public TLVector<TLChatBase> Chats { get; set; }

        public TLInt Date { get; set; }

        public TLInt Seq { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Updates = GetObject<TLVector<TLUpdateBase>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);
            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override string ToString()
        {
            var info = new StringBuilder();

            info.AppendLine("TLUpdates");
            for (var i = 0; i < Updates.Count; i++)
            {
                info.AppendLine(Updates[i].ToString());
            }

            return info.ToString();
        }

        public override IList<TLInt> GetSeq()
        {
            return new List<TLInt>{Seq};
        }
    }

    public class TLUpdatesCombined : TLUpdates
    {
        public new const uint Signature = TLConstructors.TLUpdatesCombined;

        public TLInt SeqStart { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Updates = GetObject<TLVector<TLUpdateBase>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);
            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            SeqStart = GetObject<TLInt>(bytes, ref position);               // seq младший
            Seq = GetObject<TLInt>(bytes, ref position);                    // seq старший

            return this;
        }

        public override IList<TLInt> GetSeq()
        {
            var list = new List<TLInt>();

            for (var i = SeqStart.Value; i <= Seq.Value; i++)
            {
                list.Add(new TLInt(i));
            }

            return list;
        }
    }
}