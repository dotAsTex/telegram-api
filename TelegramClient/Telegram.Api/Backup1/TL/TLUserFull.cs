﻿namespace Telegram.Api.TL
{
    public class TLUserFull : TLObject
    {
        public const uint Signature = TLConstructors.TLUserFull;

        public TLUserBase User { get; set; }

        public TLLink Link { get; set; }

        public TLPhotoBase ProfilePhoto { get; set; }

        public TLPeerNotifySettingsBase NotifySettings { get; set; }

        public TLBool Blocked { get; set; }

        public TLString RealFirstName { get; set; }

        public TLString RealLastName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            User = GetObject<TLUserBase>(bytes, ref position);
            Link = GetObject<TLLink>(bytes, ref position);
            ProfilePhoto = GetObject<TLPhotoBase>(bytes, ref position);
            NotifySettings = GetObject<TLPeerNotifySettingsBase>(bytes, ref position);
            Blocked = GetObject<TLBool>(bytes, ref position);
            RealFirstName = GetObject<TLString>(bytes, ref position);
            RealLastName = GetObject<TLString>(bytes, ref position);

            return this;
        }

        public TLUserBase ToUser()
        {
            User.Link = Link;
            User.ProfilePhoto = ProfilePhoto;
            User.NotifySettings = NotifySettings;
            User.Blocked = Blocked;
            User.RealFirstName = RealFirstName;
            User.RealLastName = RealLastName;

            return User;
        }
    }
}
