﻿namespace Telegram.Api.TL
{
    public class TLStatedMessage : TLObject
    {
        public const uint Signature = TLConstructors.TLStatedMessage;

        public TLMessageBase Message { get; set; }

        public TLVector<TLChatBase> Chats { get; set; }

        public TLVector<TLUserBase> Users { get; set; }

        public TLInt Pts { get; set; }

        public TLInt Seq { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Message = GetObject<TLMessageBase>(bytes, ref position);
            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public virtual TLStatedMessage GetEmptyObject()
        {
            return new TLStatedMessage
            {
                Chats = new TLVector<TLChatBase>(Chats.Count),
                Users = new TLVector<TLUserBase>(Users.Count),
                Pts = Pts,
                Seq = Seq
            };
        }
    }

    public class TLStatedMessageLink : TLStatedMessage
    {
        public new const uint Signature = TLConstructors.TLStatedMessageLink;

        public TLVector<TLLink> Links { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Message = GetObject<TLMessageBase>(bytes, ref position);
            Chats = GetObject<TLVector<TLChatBase>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);
            Links = GetObject<TLVector<TLLink>>(bytes, ref position);
            Pts = GetObject<TLInt>(bytes, ref position);
            Seq = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override TLStatedMessage GetEmptyObject()
        {
            return new TLStatedMessageLink
            {
                Chats = new TLVector<TLChatBase>(Chats.Count),
                Users = new TLVector<TLUserBase>(Users.Count),
                Links = new TLVector<TLLink>(Links.Count),
                Pts = Pts,
                Seq = Seq
            };
        }
    }
}
