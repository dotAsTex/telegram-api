namespace Telegram.Api.TL
{
    public class TLBadMessageNotification : TLObject
    {
        public const uint Signature = TLConstructors.TLBadMessageNotification;

        public TLLong BadMessageId { get; set; }

        public TLInt BadMessageSequenceNumber { get; set; }

        public TLInt ErrorCode { get; set; }

        /// <summary>
        /// Parses bytes arrays to TLObject from specific position, than adds count of readed bytes to position variable.
        /// </summary>
        /// <param name="bytes">    The bytes array. </param>
        /// <param name="position"> The position. </param>
        /// <returns>   The TLObject. </returns>
        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLBadMessageNotification--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            BadMessageId = GetObject<TLLong>(bytes, ref position);
            TLUtils.WriteLine("BadMessageId: " + TLUtils.MessageIdString(BadMessageId));

            BadMessageSequenceNumber = GetObject<TLInt>(bytes, ref position);
            TLUtils.WriteLine("BadMessageSequenceNumber: " + BadMessageSequenceNumber);

            ErrorCode = GetObject<TLInt>(bytes, ref position);
            TLUtils.WriteLine("ErrorCode: " + ErrorCode);
            
            return this;
        }
    }
}