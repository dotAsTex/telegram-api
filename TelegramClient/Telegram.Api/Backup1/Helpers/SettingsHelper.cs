﻿using System;
using System.IO.IsolatedStorage;
using Telegram.Api.TL;

namespace Telegram.Api.Helpers
{
#if SILVERLIGHT
    public static class SettingsHelper
    {
        private static readonly object SyncLock = new object();

        public static void CrossThreadAccess(Action<IsolatedStorageSettings> action)
        {
            lock (SyncLock)
            {
                try
                {
                    action(IsolatedStorageSettings.ApplicationSettings);
                }
                catch (Exception e)
                {
                    Execute.ShowDebugMessage("SettingsHelper.CrossThreadAccess" + e);
                }
            }
        }

        public static T GetValue<T>(string key)
        {
            T result;
            lock (SyncLock)
            {
                try
                {
                    result = (T)IsolatedStorageSettings.ApplicationSettings[key];
                }
                catch (Exception e)
                {
                    result = default(T);
                }
            }
            return result;
        }

        public static object GetValue(string key)
        {
            object result;
            lock (SyncLock)
            {
                try
                {
                    result = IsolatedStorageSettings.ApplicationSettings[key];
                }
                catch (Exception e)
                {
                    result = null;
                }
                
            }
            return result;
        }

        public static void SetValue(string key, object value)
        {
            lock (SyncLock)
            {
                IsolatedStorageSettings.ApplicationSettings[key] = value;
                IsolatedStorageSettings.ApplicationSettings.Save();
            }
        }

        public static void RemoveValue(string key)
        {
            lock (SyncLock)
            {
                IsolatedStorageSettings.ApplicationSettings.Remove(key);
            }
        }
    }
#endif
}
