﻿using System;
using System.Collections.Generic;
using Telegram.Api.TL;

namespace Telegram.Api.Services.Cache
{
    public interface ICacheService
    {
        ExceptionInfo LastSyncMessageException { get; }

        void Commit();
        bool TryCommit();
        //event EventHandler<DialogAddedEventArgs> DialogAdded;
        //event EventHandler<TopMessageUpdatedEventArgs> TopMessageUpdated;

        TLUserBase GetUser(TLInt id);
        TLUserBase GetUser(TLUserProfilePhoto photo);
        TLMessageBase GetMessage(TLInt id); 
        TLMessageBase GetMessage(TLLong randomId);
        TLDialog GetDialog(TLMessageCommon message);
        TLDialog GetDialog(TLPeerBase peer);
        TLDialogBase GetEncryptedDialog(TLInt chatId);

        TLChat GetChat(TLChatPhoto chatPhoto);
        TLChatBase GetChat(TLInt id);
        TLBroadcastChat GetBroadcast(TLInt id);

        IList<TLMessageBase> GetMessages();
        IList<TLMessageBase> GetSendingMessages();
        IList<TLMessageBase> GetResendingMessages(); 

        void GetHistoryAsync(TLInt currentUserId, TLPeerBase peer, Action<IList<TLMessageBase>> callback, int limit = Constants.CachedMessagesCount);
        IList<TLMessageBase> GetHistory(TLInt currentUserId, TLPeerBase peer, int limit = Constants.CachedMessagesCount);
        IList<TLMessageBase> GetHistory(int dialogId);
        IList<TLDecryptedMessageBase> GetDecryptedHistory(int dialogId, int limit = Constants.CachedMessagesCount);
        IList<TLDecryptedMessageBase> GetDecryptedHistory(int dialogId, long randomId, int limit = Constants.CachedMessagesCount);
        void GetDialogsAsync(Action<IList<TLDialogBase>> callback);
        IList<TLDialogBase> GetDialogs();
        void GetContactsAsync(Action<IList<TLUserBase>> callback);

        List<TLUserBase> GetContacts();
        List<TLUserBase> GetUsers();
        List<TLChatBase> GetChats();
        void GetChatsAsync(Action<IList<TLChatBase>> callback);


        void ClearAsync(Action callback = null);
        void SyncMessage(TLMessageBase message, TLPeerBase peer, Action<TLMessageBase> callback);
        void SyncSendingMessage(TLMessage message, TLMessageBase previousMessage, TLPeerBase inputPeerToPeer, Action<TLMessage> action);
        void SyncMessages(TLMessagesBase messages, TLPeerBase peer, bool notifyNewDialog, bool notifyTopMessageUpdated, Action<TLMessagesBase> callback);
        void SyncDialogs(TLDialogsBase dialogs, Action<TLDialogsBase> callback);
        void SyncUser(TLUserBase user, Action<TLUserBase> callback);
        void SyncUser(TLUserFull userFull, Action<TLUserFull> callback);
        void SyncUsers(TLVector<TLUserBase> users, Action<TLVector<TLUserBase>> callback);
        void SyncUsersAndChats(TLVector<TLUserBase> users, TLVector<TLChatBase> chats, Action<WindowsPhone.Tuple<TLVector<TLUserBase>, TLVector<TLChatBase>>> callback);
        void SyncUserLink(TLLink link, Action<TLLink> callback);
        void SyncContacts(TLContactsBase contacts, Action<TLContactsBase> callback);
        void SyncContacts(TLImportedContacts contacts, Action<TLImportedContacts> callback);

        void DeleteDialog(TLDialogBase dialog);
        void DeleteMessages(TLVector<TLInt> ids);
        void DeleteMessages(TLVector<TLLong> ids);
        void DeleteDecryptedMessages(TLVector<TLLong> ids);
        void ClearDecryptedHistoryAsync(TLInt chatId);
        void ClearBroadcastHistoryAsync(TLInt chatId);

        void SyncStatedMessage(TLStatedMessage statedMessage, Action<TLStatedMessage> callback);
        void SyncStatedMessages(TLStatedMessages statedMessages, Action<TLStatedMessages> callback);

        void GetConfigAsync(Action<TLConfig> config);
        void SetConfig(TLConfig config);
        void SyncChat(TLMessagesChatFull messagesChatFull, Action<TLMessagesChatFull> callback);
        void SyncBroadcast(TLBroadcastChat broadcast, Action<TLBroadcastChat> callback);

        TLEncryptedChatBase GetEncryptedChat(TLInt id);
        void SyncEncryptedChat(TLEncryptedChatBase encryptedChat, Action<TLEncryptedChatBase> callback);
        void SyncDecryptedMessage(TLDecryptedMessageBase message, TLEncryptedChatBase peer, Action<TLDecryptedMessageBase> callback);
        
        void Init();

        void SyncDifference(TLDifference difference, Action<TLDifference> result, IList<ExceptionInfo> exceptions);
        void SyncStatuses(TLVector<TLContactStatusBase> contacts, Action<TLVector<TLContactStatusBase>> callback);
    }

    public class ExceptionInfo
    {
        public Exception Exception { get; set; }

        public DateTime Timestamp { get; set; }

        public string Caption { get; set; }

        public override string ToString()
        {
            return string.Format("Caption={2}\nTimestamp={0}\nException={1}", Timestamp, Exception, Caption);
        }
    }
}
