﻿
//#define DEBUG_CONTAINER_MSGID

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Telegram.Api.Extensions;
using Telegram.Api.Helpers;
using Telegram.Api.TL;
using Telegram.Api.TL.Functions.Help;
using Telegram.Api.Transport;
using Action = System.Action;

namespace Telegram.Api.Services
{
	public partial class MTProtoService
    {

        private void PrintCaption(string caption)
        {
            TLUtils.WriteLine(" ");
            //TLUtils.WriteLine("------------------------");
            TLUtils.WriteLine(String.Format("-->>{0}", caption));
            TLUtils.WriteLine("------------------------");
        }

        private void SendNonInformativeMessage<T>(string caption, TLObject obj, Action<T> callback, Action<TLRPCError> faultCallback = null) where T : TLObject
        {
            PrintCaption(caption);

            int sequenceNumber;
            TLLong messageId;
            lock (_activeTransportRoot)
            {
                sequenceNumber = _activeTransport.SequenceNumber * 2;
                messageId = _activeTransport.GenerateMessageId(true);
            }
            var authKey = _activeTransport.AuthKey;
            var salt = _activeTransport.Salt;
            var sessionId = _activeTransport.SessionId;
            var clientsTicksDelta = _activeTransport.ClientTicksDelta;
            var transportMessage = CreateTLTransportMessage(salt, sessionId, new TLInt(sequenceNumber), messageId, obj);
            var encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);

#if DEBUG_CONTAINER_MSGID
            var dbgMessage = new StringBuilder();
            dbgMessage.AppendLine("Container:");
            dbgMessage.AppendLine("MsgId " + TLUtils.MessageIdString(transportMessage.MessageId));
            foreach (var m in ((TLContainer)obj).Messages)
            {
                dbgMessage.AppendLine(TLUtils.MessageIdString(m.MessageId));
            }

            TLUtils.WriteLineAtBegin("DEBUG MSGQUEUE:\n" + dbgMessage.ToString());
#endif
            lock (_activeTransportRoot)
            {
                if (_activeTransport.Closed)
                {
                    var transportDCId = _activeTransport.DCId;
                    var transportKey = _activeTransport.AuthKey;
                    var transportSalt = _activeTransport.Salt;
                    var transportSessionId = _activeTransport.SessionId;
                    var transportSequenceNumber = _activeTransport.SequenceNumber;
                    var transportClientTicksDelta = _activeTransport.ClientTicksDelta;
                    bool isCreated;
                    _activeTransport = _transportService.GetTransport(_activeTransport.Host, _activeTransport.Port, Type, out isCreated);
                    if (isCreated)
                    {
                        _activeTransport.DCId = transportDCId;
                        _activeTransport.AuthKey = transportKey;
                        _activeTransport.Salt = transportSalt;
                        _activeTransport.SessionId = transportSessionId; //TLLong.Random(); 
                        _activeTransport.SequenceNumber = transportSequenceNumber;
                        _activeTransport.ClientTicksDelta = transportClientTicksDelta;
                        _activeTransport.ReceiveBytes += OnReceiveBytes;
                    }
                }
            }

            HistoryItem historyItem = null;
            if (string.Equals(caption, "ping", StringComparison.OrdinalIgnoreCase)
                || string.Equals(caption, "ping_delay_disconnect", StringComparison.OrdinalIgnoreCase))
            {
                //save items to history
                historyItem = new HistoryItem
                {
                    SendTime = DateTime.Now,
                    //SendBeforeTime = sendBeforeTime,
                    Caption = caption,
                    Object = obj,
                    Message = transportMessage,
                    Callback = t => callback((T)t),
                    AttemptFailed = null,
                    FaultCallback = faultCallback,
                    ClientTicksDelta = clientsTicksDelta,
                    Status = RequestStatus.Sent,
                };

                lock (_historyRoot)
                {
                    _history[historyItem.Hash] = historyItem;
                }
#if DEBUG
                NotifyOfPropertyChange(() => History);
#endif
            }

            //Debug.WriteLine(">>{0} MsgId{1}", caption, transportMessage.MessageId.Value);
            Debug.WriteLine(">>{0, -30} MsgId {1} SeqNo {2, -4} SessionId {3}", caption, transportMessage.MessageId.Value, transportMessage.SeqNo.Value, transportMessage.SessionId.Value);    
            _activeTransport.SendBytesAsync(
                caption + " " + transportMessage.MessageId, 
                encryptedMessage.ToBytes(),
                result =>
                {
                    if (result != SocketError.Success)
                    {
                        if (historyItem != null)
                        {
                            lock (_historyRoot)
                            {
                                _history.Remove(historyItem.Hash);
                            }
#if DEBUG
                            NotifyOfPropertyChange(() => History);
#endif
                        }
                        // connection is unsuccessfully
                        faultCallback.SafeInvoke(new TLRPCError(404){ SocketError = result, Message = new TLString("FastCallback SocketError=" + result)});
                    }
                },
                error =>
                {
                    if (historyItem != null)
                    {
                        lock (_historyRoot)
                        {
                            _history.Remove(historyItem.Hash);
                        }
#if DEBUG
                        NotifyOfPropertyChange(() => History);
#endif
                    }
                    faultCallback.SafeInvoke(new TLRPCError(404) {SocketError = error.Error, Exception = error.Exception});
                });
        }

        private readonly object _historyRoot = new object();

        private void SendInformativeMessage<T>(string caption, TLObject obj, Action<T> callback, Action<TLRPCError> faultCallback = null, 
            int? maxAttempt = null,                 // to send delayed items
            Action<int> attemptFailed = null)       // to send delayed items
            where T : TLObject
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                //if (!IsInitialized && (caption != "help.getConfig" || caption != "help.getNearestDc"))
                if (_activeTransport.AuthKey == null)
                {
                    var delayedItem = new DelayedItem
                    {
                        SendTime = DateTime.Now,
                        //SendBeforeTime = sendBeforeTime,
                        Caption = caption,
                        Object = obj,
                        Callback = t => callback((T) t),
                        AttemptFailed = attemptFailed,
                        FaultCallback = faultCallback,
                        MaxAttempt = maxAttempt
                    };
#if LOG_REGISTRATION
                    TLUtils.WriteLog(DateTime.Now.ToLocalTime() + ": Enqueue delayed item\n " + delayedItem); 
#endif
                    lock (_delayedItemsRoot)
                    {
                        _delayedItems.Add(delayedItem);
                    }

                    return;
                }

                lock (_activeTransportRoot)
                {
                    if (_activeTransport.Closed)
                    {
                        var transportDCId = _activeTransport.DCId;
                        var transportKey = _activeTransport.AuthKey;
                        var transportSalt = _activeTransport.Salt;
                        var transportSessionId = _activeTransport.SessionId;
                        var transportSequenceNumber = _activeTransport.SequenceNumber;
                        var transportClientTicksDelta = _activeTransport.ClientTicksDelta;
                        bool isCreated;
                        _activeTransport = _transportService.GetTransport(_activeTransport.Host, _activeTransport.Port, Type, out isCreated);
                        if (isCreated)
                        {
                            _activeTransport.DCId = transportDCId;
                            _activeTransport.AuthKey = transportKey;
                            _activeTransport.Salt = transportSalt;
                            _activeTransport.SessionId = transportSessionId; //TLLong.Random();
                            _activeTransport.SequenceNumber = transportSequenceNumber;
                            _activeTransport.ClientTicksDelta = transportClientTicksDelta;
                            _activeTransport.ReceiveBytes += OnReceiveBytes;
                        }
                    }
                }

                PrintCaption(caption);

                TLObject data;
                lock (_activeTransportRoot)
                {
                    if (!_activeTransport.Initiated || caption == "auth.sendCode")
                    {
                        var deviceModel = PhoneHelper.GetDeviceFullName();
                        var shortModel = PhoneHelper.GetShortPhoneModel(deviceModel);
                        if (!string.IsNullOrEmpty(shortModel))
                        {
                            deviceModel = shortModel;
                        }

                        //SystemEvents.TimeChanged
                        var initConnection = new TLInitConnection
                        {
                            AppId = new TLInt(Constants.ApiId),
                            AppVersion = new TLString(PhoneHelper.GetAppVersion()),
                            Data = obj,
                            DeviceModel = new TLString(deviceModel),
                            LangCode = new TLString(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName),
                            SystemVersion = new TLString(PhoneHelper.GetOSVersion())
                        };
                        var withLayerN = new TLInvokeWithLayerN{ Data = initConnection };
                        data = withLayerN;
                        _activeTransport.Initiated = true;
                    }
                    else
                    {
                        data = obj;
                    }
                }

                int sequenceNumber;
                TLLong messageId;
                lock (_activeTransportRoot)
                {
                    sequenceNumber = _activeTransport.SequenceNumber * 2 + 1;
                    _activeTransport.SequenceNumber++;
                    messageId = _activeTransport.GenerateMessageId(true);
                }
                var authKey = _activeTransport.AuthKey;
                var salt = _activeTransport.Salt;
                var sessionId = _activeTransport.SessionId;
                var clientsTicksDelta = _activeTransport.ClientTicksDelta;
                var transportMessage = CreateTLTransportMessage(salt, sessionId, new TLInt(sequenceNumber), messageId, data);
                var encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);

                //save items to history
                var historyItem = new HistoryItem
                {
                    SendTime = DateTime.Now,
                    //SendBeforeTime = sendBeforeTime,
                    Caption = caption,
                    Object = obj,
                    Message = transportMessage,
                    Callback = t => callback((T)t),
                    AttemptFailed = attemptFailed,
                    FaultCallback = faultCallback,
                    ClientTicksDelta = clientsTicksDelta,
                    Status = RequestStatus.Sent,
                };

                lock (_historyRoot)
                {
                    _history[historyItem.Hash] = historyItem;
                }
#if DEBUG
                NotifyOfPropertyChange(() => History);
#endif

                Debug.WriteLine(">>{0, -30} MsgId {1} SeqNo {2, -4} SessionId {3}", caption, transportMessage.MessageId.Value, transportMessage.SeqNo.Value, transportMessage.SessionId.Value);
                _activeTransport.SendBytesAsync(caption + " " + transportMessage.SessionId.Value + " " + transportMessage.MessageId.Value,
                    encryptedMessage.ToBytes(),
                    result =>
                    {
                        if (result != SocketError.Success)
                        {
                            lock (_historyRoot)
                            {
                                _history.Remove(historyItem.Hash);
                            }
#if DEBUG
                            NotifyOfPropertyChange(() => History);
#endif
                            // connection is unsuccessfully
                            faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), SocketError = result, Message=new TLString("FastCallback SocketError=" + result)});
                        }
                        //Debug.WriteLine("@{0} {1} result {2}", caption, transportMessage.MessageId.Value, result);
                    },
                    error =>
                    {
                        lock (_historyRoot)
                        {
                            _history.Remove(historyItem.Hash);
                        }
#if DEBUG
                        NotifyOfPropertyChange(() => History);
#endif
                        faultCallback.SafeInvoke(new TLRPCError {Code = new TLInt(404), SocketError = error.Error, Exception = error.Exception});
                    });
            });
        }

        private void SendNonEncryptedMessage<T>(string caption, TLObject obj, Action<T> callback, Action<TLRPCError> faultCallback = null)
            where T : TLObject
        {
            PrintCaption(caption);
            TLLong messageId;
            lock (_activeTransportRoot)
            {
                messageId = _activeTransport.GenerateMessageId();
            }
            var message = CreateTLNonEncryptedMessage(messageId, obj);

            var historyItem = new HistoryItem
            {
                Caption = caption,
                Message = message,
                Callback = t => callback((T) t),
                FaultCallback = faultCallback,
                Status = RequestStatus.Sent
            };
            
            var guid = message.MessageId;
            lock (_activeTransportRoot)
            {
                if (_activeTransport.Closed)
                {
                    var transportDCId = _activeTransport.DCId;
                    var transportKey = _activeTransport.AuthKey;
                    var transportSalt = _activeTransport.Salt;
                    var transportSessionId = _activeTransport.SessionId;
                    var transportSequenceNumber = _activeTransport.SequenceNumber;
                    var transportClientTicksDelta = _activeTransport.ClientTicksDelta;
                    bool isCreated;
                    _activeTransport = _transportService.GetTransport(_activeTransport.Host, _activeTransport.Port, Type, out isCreated);
                    if (isCreated)
                    {
                        _activeTransport.DCId = transportDCId;
                        _activeTransport.AuthKey = transportKey;
                        _activeTransport.Salt = transportSalt;
                        _activeTransport.SessionId = transportSessionId;
                        _activeTransport.SequenceNumber = transportSequenceNumber;
                        _activeTransport.ClientTicksDelta = transportClientTicksDelta;
                        _activeTransport.ReceiveBytes += OnReceiveBytes;
                    }
                }
            }

            // Сначала создаем или получаем транспорт, а потом добавляем в его историю.
            // Если сначала добавить в историю транспорта, то потом можем получить новый и не найдем запрос
            _activeTransport.EnqueueNonEncryptedItem(historyItem);

            _activeTransport.SendBytesAsync(caption + " " + guid, message.ToBytes(),
                socketError =>
                {
#if LOG_REGISTRATION
                    TLUtils.WriteLog(caption + " SocketError=" + socketError);
#endif
                    if (socketError != SocketError.Success)
                    {
                        _activeTransport.RemoveNonEncryptedItem(historyItem);

                        // connection is unsuccessfully
                        faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("FastCallback SocketError=" + socketError) });
                    }
                },
                error =>
                {
#if LOG_REGISTRATION
                    TLUtils.WriteLog(caption + " error " + error);
#endif

                    _activeTransport.RemoveNonEncryptedItem(historyItem);

                    faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("FaltCallback") });
                });
        }

        private static TLEncryptedTransportMessage CreateTLEncryptedMessage(byte[] authKey, params TLContainerTransportMessage[] messages)
        {
            var message = new TLEncryptedTransportMessage { Data = messages[0].ToBytes() };

            return message.Encrypt(authKey);
        }

        private TLTransportMessage CreateTLTransportMessage(TLLong salt, TLLong sessionId, TLInt seqNo, TLLong messageId, TLObject obj)
        {
            var message = new TLTransportMessage();
            message.Salt = salt;
            message.SessionId = sessionId;
            message.MessageId = messageId;  //TLUtils.GenerateMessageId(ClientTicksDelta, true);
            message.SeqNo = seqNo;
            message.MessageData = obj;

            TLUtils.WriteLine("->MESSAGEID: " + TLUtils.MessageIdString(message.MessageId));
            TLUtils.WriteLine("  SEQUENCENUMBER: " + message.SeqNo);
            TLUtils.WriteLine("Salt: " + message.Salt);
            TLUtils.WriteLine("SessionId: " + message.SessionId);

            return message;
        }

        public static TLNonEncryptedMessage CreateTLNonEncryptedMessage(TLLong messageId, TLObject obj)
        {
            var message = new TLNonEncryptedMessage();
            message.AuthKeyId = new TLLong(0);
            message.MessageId = messageId;//TLUtils.GenerateMessageId(ClientTicksDelta);
            message.Data = obj;

            TLUtils.WriteLine("AuthKeyId: " + message.AuthKeyId);
            TLUtils.WriteLine("->MESSAGEID: " + TLUtils.MessageIdString(message.MessageId));
            //TLUtils.WriteLine("  SEQUENCENUMBER: " + message.SeqNo);

            return message;
        }

	    public void StartListening()
	    {
            _activeTransport.StartListening();
	    }
    }

    
}
