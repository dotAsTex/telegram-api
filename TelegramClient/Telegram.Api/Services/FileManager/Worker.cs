using System;
using System.Threading;
using Telegram.Api.TL;

namespace Telegram.Api.Services.FileManager
{
    public class Worker
    {
        private readonly Thread _thread;

        private bool _isCancelled;

        public bool IsCancelled
        {
            get { return _isCancelled; }
        }

        private readonly ManualResetEvent _resetEvent = new ManualResetEvent(true);

        public ThreadState ThreadState
        {
            get { return _thread.ThreadState; }
        }

        public Worker(ParameterizedThreadStart start, string name)
        {
            _thread = new Thread(state => OnThreadStartInternal(start));
            _thread.Name = name;
            //_thread.IsBackground = true;
            _thread.Start(this);
        }

        private void OnThreadStartInternal(ParameterizedThreadStart start)
        {
            while (true)
            {
                try
                {
                    start(this);
                }
                catch (Exception e)
                {
                    TLUtils.WriteException(e);
                }
                _resetEvent.WaitOne();
            }
        }

        public void Start()
        {
            _isCancelled = false;
            _resetEvent.Set();
        }

        public void Stop()
        {
            _resetEvent.Reset();
        }

        public void Cancel()
        {
            _isCancelled = true;
        }
    }
}