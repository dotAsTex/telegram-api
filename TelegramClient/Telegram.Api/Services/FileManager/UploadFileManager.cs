﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Threading;
using Caliburn.Micro;
using Telegram.Api.Helpers;
using Telegram.Api.TL;

namespace Telegram.Api.Services.FileManager
{
    public class UploadProgressChangedEventArgs
    {
        public double Progress { get; protected set; }

        public UploadableItem Item { get; protected set; }

        public UploadProgressChangedEventArgs(UploadableItem item, double progress)
        {
            Item = item;
            Progress = progress;
        }
    }

    public class UploadablePart
    {
        public UploadableItem ParentItem { get; protected set; }

        public TLInt FilePart { get; protected set; }

        public PartStatus Status { get; set; }

        public byte[] Bytes { get; protected set; }

        public long Position { get; protected set; }

        public long Count { get; protected set; }

        public void ClearBuffer()
        {
            Bytes = null;
        }

        public UploadablePart(UploadableItem item, TLInt filePart, byte[] bytes)
        {
            ParentItem = item;
            FilePart = filePart;
            Bytes = bytes;
        }

        public UploadablePart(UploadableItem item, TLInt filePart, long position, long count)
        {
            ParentItem = item;
            FilePart = filePart;
            Position = position;
            Count = count;
        }

        public UploadablePart(UploadableItem item, TLInt filePart, byte[] bytes, long position, long count)
        {
            ParentItem = item;
            FilePart = filePart;
            Bytes = bytes;
            Position = position;
            Count = count;
        }

        public override string ToString()
        {
            return string.Format("Part={0}, Status={1}, Position={2}, Count={3}", FilePart, Status, Position, Count);
        }

        public void CreateBuffer()
        {
            Bytes = new byte[Count];
        }

        public void SetParentItem(UploadableItem item)
        {
            ParentItem = item;
        }
    }

    public class UploadableItem
    {
        public TLLong FileId { get; protected set; }

        public string IsoFileName { get; protected set; }

        public long IsoFileLength { get; protected set; }

        public TLObject Owner { get; protected set; }

        public byte[] Bytes { get; protected set; }

        public List<UploadablePart> Parts { get; set; }

        public bool Canceled { get; set; }

        public UploadableItem(TLLong fileId, TLObject owner, byte[] bytes)
        {
            FileId = fileId;
            Owner = owner;
            Bytes = bytes;
        }

        public UploadableItem(TLLong fileId, TLObject owner, string isoFileName, long isoFileLength)
        {
            FileId = fileId;
            Owner = owner;
            IsoFileName = isoFileName;
            IsoFileLength = isoFileLength;
        }
    }


    public class UploadFileManager : IUploadFileManager
    {
        private readonly object _itemsSyncRoot = new object();

        private readonly List<UploadableItem> _items = new List<UploadableItem>();

        private readonly List<Worker> _workers = new List<Worker>(Constants.WorkersNumber); 

        private readonly IEventAggregator _eventAggregator;

        private readonly IMTProtoService _mtProtoService;

        public UploadFileManager(IEventAggregator eventAggregator, IMTProtoService mtProtoService)
        {
            _eventAggregator = eventAggregator;
            _mtProtoService = mtProtoService;


            var timer = Stopwatch.StartNew();
            for (int i = 0; i < Constants.WorkersNumber; i++)
            {
                var worker = new Worker(OnUploading, "uploader"+i);
                _workers.Add(worker);
            }

            TLUtils.WritePerformance("Start workers timer: " + timer.Elapsed);
            
        }

        private void OnUploading(object state)
        {
            UploadablePart part = null;
            lock (_itemsSyncRoot)
            {
                for (var i = 0; i < _items.Count; i++)
                {
                    var item = _items[i];
                    if (item.Canceled)
                    {
                        _items.RemoveAt(i--);
                        try
                        {
                            _eventAggregator.Publish(new UploadingCanceledEventArgs(item));
                        }
                        catch (Exception e)
                        {
                            TLUtils.WriteException(e);
                        }
                    }
                }


                foreach (var item in _items)
                {
                    part = item.Parts.FirstOrDefault(x => x.Status == PartStatus.Ready);
                    if (part != null)
                    {
                        part.Status = PartStatus.Processing;
                        break;
                    }
                }
            }

            if (part != null)
            {

                bool result = PutFile(part.ParentItem.FileId, part.FilePart, part.Bytes);
                while (!result)
                {
                    result = PutFile(part.ParentItem.FileId, part.FilePart, part.Bytes);
                }

                // indicate progress
                // indicate complete
                bool isComplete = false;
                bool isCanceled;
                var progress = 0.0;
                lock (_itemsSyncRoot)
                {
                    part.Status = PartStatus.Processed;
                    isCanceled = part.ParentItem.Canceled;
                    if (!isCanceled)
                    {
                        isComplete = part.ParentItem.Parts.All(x => x.Status == PartStatus.Processed);
                        if (!isComplete)
                        {
                            double uploadedCount = part.ParentItem.Parts.Count(x => x.Status == PartStatus.Processed);
                            double totalCount = part.ParentItem.Parts.Count;
                            progress = uploadedCount / totalCount;
                        }
                        else
                        {
                            _items.Remove(part.ParentItem);
                        }
                    }
                }

                if (!isCanceled)
                {
                    if (isComplete)
                    {
                        try
                        {
                            _eventAggregator.Publish(part.ParentItem);
                        }
                        catch (Exception e)
                        {
                            TLUtils.WriteLine(e.ToString(), LogSeverity.Error);
                        }
                    }
                    else
                    {
                        try
                        {
                            _eventAggregator.Publish(new UploadProgressChangedEventArgs(part.ParentItem, progress));
                        }
                        catch (Exception e)
                        {
                            TLUtils.WriteLine(e.ToString(), LogSeverity.Error);
                        }
                    }
                }
            }
            else
            {

                var currentWorker = (Worker)state;
                currentWorker.Stop();
            }
        }

        private bool PutFile(TLLong fileId, TLInt filePart, byte[] bytes)
        {
            var manualResetEvent = new ManualResetEvent(false);
            var result = false;

            _mtProtoService.SaveFilePartAsync(fileId, filePart, TLString.FromBigEndianData(bytes),
                savingResult =>
                {
                    result = true;
                    manualResetEvent.Set();
                },
                error =>
                {
                    Thread.Sleep(1000);
                    manualResetEvent.Set();
                });

            manualResetEvent.WaitOne();
            return result;
        }

        public void UploadFile(TLLong fileId, TLObject owner, byte[] bytes)
        {
            var item = GetUploadableItem(fileId, owner, bytes);

            lock (_itemsSyncRoot)
            {
                _items.Add(item);
            }

            StartAwaitingWorkers();
        }

        private UploadableItem GetUploadableItem(TLLong fileId, TLObject owner, byte[] bytes)
        {
            var item = new UploadableItem(fileId, owner, bytes);
            item.Parts = GetItemParts(item);
            return item;
        }

        private List<UploadablePart> GetItemParts(UploadableItem item)
        {
            const int chunkSize = 30 * 1024; // 512Kb
            var parts = new List<UploadablePart>();
            var partsCount = item.Bytes.Length / chunkSize + 1;
            for (var i = 0; i < partsCount; i++)
            {
                var part = new UploadablePart(item, new TLInt(i), item.Bytes.SubArray(i * chunkSize, Math.Min(chunkSize, item.Bytes.Length - i * chunkSize)));
                parts.Add(part);
            }

            return parts;
        }

        private void StartAwaitingWorkers()
        {
            var awaitingWorkers = _workers.Where(x => x.ThreadState == ThreadState.WaitSleepJoin);
            foreach (var awaitingWorker in awaitingWorkers)
            {
                awaitingWorker.Start();
            }
        }

        public void CancelUploadFile(TLLong fileId)
        {
            lock (_itemsSyncRoot)
            {
                var item = _items.FirstOrDefault(x => x.FileId.Value == fileId.Value);
                
                if (item != null)
                {
                    item.Canceled = true;
                    //_items.Remove(item);
                }
            }
        }
    }

    public class UploadingCanceledEventArgs
    {
        public UploadableItem Item { get; protected set; }

        public UploadingCanceledEventArgs(UploadableItem item)
        {
            Item = item;
        }
    }
}
