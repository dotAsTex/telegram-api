﻿using Telegram.Api.TL;

namespace Telegram.Api.Services.FileManager
{
    public interface IUploadFileManager
    {
        void UploadFile(TLLong fileId, TLObject owner, byte[] bytes);
        void CancelUploadFile(TLLong fileId);
    }
}
