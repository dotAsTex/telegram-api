﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows;
using Telegram.Api.Extensions;
using Telegram.Api.Helpers;
using Telegram.Api.TL;
using Telegram.Api.TL.Functions.Auth;
using Telegram.Api.TL.Functions.DHKeyExchange;
using Telegram.Api.TL.Functions.Help;
using Telegram.Api.TL.Functions.Stuff;
using Telegram.Api.TL.Functions.Upload;
using Telegram.Api.Transport;

namespace Telegram.Api.Services
{
    public partial class MTProtoService
    {
        private void ReqPQByTransportAsync(ITransport transport, TLInt128 nonce, Action<TLResPQ> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLReqPQ { Nonce = nonce };

            SendNonEncryptedMessageByTransport(transport, "req_pq", obj, callback, faultCallback);
        }

        private void ReqDHParamsByTransportAsync(ITransport transport, TLInt128 nonce, TLInt128 serverNonce, TLString p, TLString q, TLLong publicKeyFingerprint, TLString encryptedData, Action<TLServerDHParamsBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLReqDHParams { Nonce = nonce, ServerNonce = serverNonce, P = p, Q = q, PublicKeyFingerprint = publicKeyFingerprint, EncryptedData = encryptedData };

            SendNonEncryptedMessageByTransport(transport, "req_DH_params", obj, callback, faultCallback);
        }

        public void SetClientDHParamsByTransportAsync(ITransport transport, TLInt128 nonce, TLInt128 serverNonce, TLString encryptedData, Action<TLDHGenBase> callback, Action<TLRPCError> faultCallback = null)
        {
            var obj = new TLSetClientDHParams { Nonce = nonce, ServerNonce = serverNonce, EncryptedData = encryptedData };

            SendNonEncryptedMessageByTransport(transport, "set_client_DH_params", obj, callback, faultCallback);
        }

        public void InitTransportAsync(ITransport transport, Action<Tuple<byte[], TLLong, TLLong>> callback, Action<TLRPCError> faultCallback = null)
        {
            var authTime = Stopwatch.StartNew();
            var newNonce = TLInt256.Random();

#if LOG_REGISTRATION
            TLUtils.WriteLog("Start ReqPQ");
#endif
            ReqPQByTransportAsync(
                transport, 
                TLInt128.Random(),
                resPQ =>
                {
#if LOG_REGISTRATION
                    TLUtils.WriteLog("Stop ReqPQ");
#endif
                    TimeSpan calcTime;
                    Tuple<ulong, ulong> pqPair;
                    var innerData = GetInnerData(resPQ, newNonce, out calcTime, out pqPair);
                    var encryptedInnerData = GetEncryptedInnerData(innerData);

#if LOG_REGISTRATION
                    TLUtils.WriteLog("Start ReqDHParams");
#endif
                    ReqDHParamsByTransportAsync(
                        transport,
                        resPQ.Nonce,
                        resPQ.ServerNonce,
                        innerData.P,
                        innerData.Q,
                        resPQ.ServerPublicKeyFingerprints[0],
                        encryptedInnerData,
                        serverDHParams =>
                        {
#if LOG_REGISTRATION
                            TLUtils.WriteLog("Stop ReqDHParams");
#endif
                            var random = new Random();

                            var serverDHParamsOk = serverDHParams as TLServerDHParamsOk;
                            if (serverDHParamsOk == null)
                            {
                                var error = new TLRPCError{Code = new TLInt(404), Message = new TLString("Incorrect serverDHParams")};
                                if (faultCallback != null) faultCallback(error);
                                TLUtils.WriteLine(error.ToString());
#if LOG_REGISTRATION
                            
                                TLUtils.WriteLog("ServerDHParams " + serverDHParams);  
#endif
                                return;
                            }

                            var aesParams = GetAesKeyIV(resPQ.ServerNonce.ToBytes(), newNonce.ToBytes());

                            var decryptedAnswerWithHash = Utils.AesIge(serverDHParamsOk.EncryptedAnswer.Data, aesParams.Item1, aesParams.Item2, false);     //NOTE: Remove reverse here

                            var position = 0;
                            var serverDHInnerData = (TLServerDHInnerData)new TLServerDHInnerData().FromBytes(decryptedAnswerWithHash.Skip(20).ToArray(), ref position);

                            var bBytes = new byte[256]; //big endian B
                            random.NextBytes(bBytes);

                            var gbBytes = GetGB(bBytes, serverDHInnerData.G, serverDHInnerData.DHPrime);

                            var clientDHInnerData = new TLClientDHInnerData
                            {
                                Nonce = resPQ.Nonce,
                                ServerNonce = resPQ.ServerNonce,
                                RetryId = new TLLong(0),
                                GB = TLString.FromBigEndianData(gbBytes)
                            };

                            var encryptedClientDHInnerData = GetEncryptedClientDHInnerData(clientDHInnerData, aesParams);
#if LOG_REGISTRATION                 
                            TLUtils.WriteLog("Start SetClientDHParams");  
#endif
                            SetClientDHParamsByTransportAsync(
                                transport,
                                resPQ.Nonce, 
                                resPQ.ServerNonce, 
                                encryptedClientDHInnerData,
                                dhGen =>
                                {
#if LOG_REGISTRATION
                                    TLUtils.WriteLog("Stop SetClientDHParams");
#endif
                                    var getKeyTimer = Stopwatch.StartNew();
                                    var authKey = GetAuthKey(bBytes, serverDHInnerData.GA.ToBytes(), serverDHInnerData.DHPrime.ToBytes());

#if LOG_REGISTRATION
                                    var logCountersString = new StringBuilder();

                                    logCountersString.AppendLine("Auth Counters");
                                    logCountersString.AppendLine();
                                    logCountersString.AppendLine("pq factorization time: " + calcTime);
                                    logCountersString.AppendLine("calc auth key time: " + getKeyTimer.Elapsed);
                                    logCountersString.AppendLine("auth time: " + authTime.Elapsed);

                                    TLUtils.WriteLog(logCountersString.ToString());
#endif
                                    //newNonce - little endian
                                    //authResponse.ServerNonce - little endian
                                    var salt = GetSalt(newNonce.ToBytes(), resPQ.ServerNonce.ToBytes());
                                    var sessionId = new byte[8];
                                    random.NextBytes(sessionId);

                                    // authKey, salt, sessionId
                                    callback(new Tuple<byte[], TLLong, TLLong>(authKey, new TLLong(BitConverter.ToInt64(salt, 0)), new TLLong(BitConverter.ToInt64(sessionId, 0))));
                                },
                                error =>
                                {
#if LOG_REGISTRATION
                                    TLUtils.WriteLog("Stop SetClientDHParams with error " + error.ToString());
#endif
                                    if (faultCallback != null) faultCallback(error);
                                    TLUtils.WriteLine(error.ToString());
                                });
                        },
                        error =>
                        {
#if LOG_REGISTRATION
                            TLUtils.WriteLog("Stop ReqDHParams with error " + error.ToString());
#endif
                            if (faultCallback != null) faultCallback(error);
                            TLUtils.WriteLine(error.ToString());
                        });
                },
                error =>
                {
#if LOG_REGISTRATION
                    TLUtils.WriteLog("Stop ReqPQ with error " + error.ToString());
#endif
                    if (faultCallback != null) faultCallback(error);
                    TLUtils.WriteLine(error.ToString());
                });
        }

        public void GetFileAsync(TLInt dcId, TLInputFileLocationBase location, TLInt offset, TLInt limit, Action<TLFile> callback, Action<TLRPCError> faultCallback = null)
        {
            //GetFileAsync(location, offset, limit, callback, faultCallback);
            //return;

            var obj = new TLGetFile { Location = location, Offset = offset, Limit = limit };

            lock (_activeTransportRoot)
            {
                if (_activeTransport.DCId == dcId.Value)
                {
                    if (_activeTransport.DCId == 0)
                    {
                        TLUtils.WriteException(new Exception("_activeTransport.DCId==0"));
                    }

                    SendInformativeMessage("upload.getFile", obj, callback, faultCallback);
                    return;
                }
            }

            var transport = GetTransportByDCId(dcId);

            if (transport == null)
            {
                faultCallback.SafeInvoke(new TLRPCError{Code = new TLInt(404), Message = new TLString("GetFileAsync: Empty option for dc id " + dcId)});

                return;
            }

            if (transport.AuthKey == null)
            {
                var cancelInitializing = false;
                lock (transport.SyncRoot)
                {
                    if (transport.IsInitializing)
                    {
                        cancelInitializing = true;
                    }
                    else
                    {
                        transport.IsInitializing = true;
                    }
                }

                if (cancelInitializing)
                {
                    faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("DC " + dcId + " is already initializing") });
                    return;
                }

                InitTransportAsync(
                    transport,
                    tuple =>
                    {
                        lock (transport.SyncRoot)
                        {
                            transport.AuthKey = tuple.Item1;
                            transport.Salt = tuple.Item2;
                            transport.SessionId = tuple.Item3;

                            transport.IsInitializing = false;
                        }
                        var authKeyId = TLUtils.GenerateLongAuthKeyId(tuple.Item1);

                        lock (_authKeysRoot)
                        {
                            if (!_authKeys.ContainsKey(authKeyId))
                            {
                                _authKeys.Add(authKeyId, new AuthKeyItem { AuthKey = tuple.Item1, AutkKeyId = authKeyId });
                            }
                        }

                        ExportImportAuthorizationAsync(
                            transport,
                            () =>
                            {
                                foreach (var dcOption in _config.DCOptions)
                                {
                                    if (dcOption.Id.Value == transport.DCId)
                                    {
                                        dcOption.AuthKey = tuple.Item1;
                                        dcOption.Salt = tuple.Item2;
                                        dcOption.SessionId = tuple.Item3;
                                    }
                                }

                                _cacheService.SetConfig(_config);

                                SendInformativeMessageByTransport(transport, "upload.getFile", obj, callback, faultCallback);
                            },
                            error =>
                            {
#if DEBUG
                                if (!error.CodeEquals(ErrorCode.NOT_FOUND) &&
                                    !error.Message.ToString().Contains("is already authorizing"))
                                {
                                    Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("ExportImportAuthorization error " + error));
                                }
#endif
                                faultCallback.SafeInvoke(error);
                            });
                    },
                    error =>
                    {
                        lock (transport.SyncRoot)
                        {
                            transport.IsInitializing = false;
                        }

                        faultCallback.SafeInvoke(error);
                    });
            }
            else
            {
                ExportImportAuthorizationAsync(
                    transport,
                    () =>
                    {
                        SendInformativeMessageByTransport(transport, "upload.getFile", obj, callback, faultCallback);
                    },
                    error =>
                    {
                        if (!error.CodeEquals(ErrorCode.NOT_FOUND) 
                            && !error.Message.ToString().Contains("is already authorizing"))
                        {
                            Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show("ExportImportAuthorization error " + error));
                        }

                        faultCallback.SafeInvoke(error);
                    });
            }
        }

        private void ExportImportAuthorizationAsync(ITransport toTransport, Action callback, Action<TLRPCError> faultCallback = null)
        {
            if (!toTransport.IsAuthorized)
            {
                bool authorizing = false;
                lock (toTransport.SyncRoot)
                {
                    if (toTransport.IsAuthorizing)
                    {
                        authorizing = true;
                    }

                    toTransport.IsAuthorizing = true;
                }

                if (authorizing)
                {
                    faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("DC " + toTransport.DCId + " is already authorizing") });
                    return;
                }

                ExportAuthorizationAsync(
                    new TLInt(toTransport.DCId),
                    exportedAuthorization =>
                    {
                        ImportAuthorizationByTransportAsync(
                            toTransport,
                            exportedAuthorization.Id,
                            exportedAuthorization.Bytes,
                            authorization =>
                            {
                                lock (toTransport.SyncRoot)
                                {
                                    toTransport.IsAuthorized = true; 
                                    toTransport.IsAuthorizing = false;
                                }
                                callback.SafeInvoke();
                            },
                            error =>
                            {
                                lock (toTransport.SyncRoot)
                                {
                                    toTransport.IsAuthorizing = false;
                                }
                                faultCallback.SafeInvoke(error);
                            });
                        ;
                    },
                    error =>
                    {
                        lock (toTransport.SyncRoot)
                        {
                            toTransport.IsAuthorizing = false;
                        }
                        faultCallback.SafeInvoke(error);
                    });
            }
            else
            {
                callback.SafeInvoke();
            }
        }

        private ITransport GetTransportByDCId(TLInt dcId)
        {
            ITransport transport;
            lock (_activeTransportRoot)
            {
                var dcOption = _config.DCOptions.FirstOrDefault(x => x.Id != null && x.Id.Value == dcId.Value);

                if (dcOption == null) return null;

                bool isCreated;
                transport = _transportService.GetFileTransport(dcOption.IpAddress.Value, dcOption.Port.Value, Type, out isCreated);
                if (isCreated)
                {
                    transport.DCId = dcId.Value;
                    transport.AuthKey = dcOption.AuthKey;
                    transport.Salt = dcOption.Salt;
                    transport.SessionId = TLLong.Random();
                    transport.SequenceNumber = 0;
                    transport.ClientTicksDelta = dcOption.ClientTicksDelta;
                    transport.ReceiveBytes += OnReceiveBytesByTransport;
                }
            }

            return transport;
        }


        private void SendInformativeMessageByTransport<T>(ITransport transport, string caption, TLObject obj, Action<T> callback, Action<TLRPCError> faultCallback = null,
            int? maxAttempt = null,                 // to send delayed items
            Action<int> attemptFailed = null)       // to send delayed items
            where T : TLObject
        {
            bool isInitialized;
            lock (transport.SyncRoot)
            {
                isInitialized = transport.AuthKey != null;
            }

            if (!isInitialized)
            {
                var delayedItem = new DelayedItem
                {
                    SendTime = DateTime.Now,
                    Caption = caption,
                    Object = obj,
                    Callback = t => callback((T)t),
                    AttemptFailed = attemptFailed,
                    FaultCallback = faultCallback,
                    MaxAttempt = maxAttempt
                };
#if LOG_REGISTRATION
                    TLUtils.WriteLog(DateTime.Now.ToLocalTime() + ": Enqueue delayed item\n " + delayedItem); 
#endif
                lock (_delayedItemsRoot)
                {
                    _delayedItems.Add(delayedItem);
                }

                return;
            }

            lock (transport.SyncRoot)
            {
                if (transport.Closed)
                {
                    var transportDCId = transport.DCId;
                    var transportKey = transport.AuthKey;
                    var transportSalt = transport.Salt;
                    var transportSessionId = transport.SessionId;
                    var transportSequenceNumber = transport.SequenceNumber;
                    var transportClientTicksDelta = transport.ClientTicksDelta;
                    bool isCreated;
                    transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                    if (isCreated)
                    {
                        transport.DCId = transportDCId;
                        transport.AuthKey = transportKey;
                        transport.Salt = transportSalt;
                        transport.SessionId = transportSessionId;
                        transport.SequenceNumber = transportSequenceNumber;
                        transport.ClientTicksDelta = transportClientTicksDelta;
                        transport.ReceiveBytes += OnReceiveBytesByTransport;
                    }
                }
            }

            PrintCaption(caption);

            TLObject data;
            int sequenceNumber;
            lock (transport.SyncRoot)
            {
                if (!transport.Initiated || caption == "auth.sendCode")
                {
                    var initConnection = new TLInitConnection
                    {
                        AppId = new TLInt(Constants.ApiId),
                        AppVersion = new TLString(PhoneHelper.GetAppVersion()),
                        Data = obj,
                        DeviceModel = new TLString(PhoneHelper.GetDeviceFullName()),
                        LangCode = new TLString(Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName),
                        SystemVersion = new TLString("7.0.0.0")
                    };
                    var withLayerN = new TLInvokeWithLayerN { Data = initConnection };
                    data = withLayerN;
                    transport.Initiated = true;
                }
                else
                {
                    data = obj;
                }

                sequenceNumber = transport.SequenceNumber * 2 + 1;
                transport.SequenceNumber++;
            }

            var authKey = transport.AuthKey;
            var salt = transport.Salt;
            var sessionId = transport.SessionId;
            var clientsTicksDelta = transport.ClientTicksDelta;
            var dcId = transport.DCId;
            var messageId = transport.GenerateMessageId(true);
            var transportMessage = CreateTLTransportMessage(salt, sessionId, new TLInt(sequenceNumber), messageId, data);
            var encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);

            var historyItem = new HistoryItem
            {
                SendTime = DateTime.Now,
                Caption = caption,
                Object = obj,
                Message = transportMessage,
                Callback = t => callback((T)t),
                FaultCallback = faultCallback,
                ClientTicksDelta = clientsTicksDelta,
                Status = RequestStatus.Sent,

                DCId = dcId
            };

            lock (_historyRoot)
            {
                _history[historyItem.Hash] = historyItem;
            }
#if DEBUG
            NotifyOfPropertyChange(() => History);
#endif

            Debug.WriteLine(">> {4} {0, -30} MsgId {1} SeqNo {2, -4} SessionId {3}", caption, transportMessage.MessageId, transportMessage.SeqNo, transportMessage.SessionId, historyItem.DCId);
            transport.SendBytesAsync(caption + " " + transportMessage.SessionId + " " + transportMessage.MessageId,
                encryptedMessage.ToBytes(),
                fastResult =>
                {
                    if (fastResult != SocketError.Success)
                    {
                        lock (_historyRoot)
                        {
                            _history.Remove(historyItem.Hash);
                        }
#if DEBUG
                        NotifyOfPropertyChange(() => History);
#endif
                        faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("FastCallback SocketError=" + fastResult) });
                    }
                },
                error =>
                {
                    lock (_historyRoot)
                    {
                        _history.Remove(historyItem.Hash);
                    }
#if DEBUG
                    NotifyOfPropertyChange(() => History);
#endif
                    faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404) });
                });
        }

        private void SendNonEncryptedMessageByTransport<T>(ITransport transport, string caption, TLObject obj, Action<T> callback, Action<TLRPCError> faultCallback = null) where T : TLObject
        {
            PrintCaption(caption);
            var messageId = transport.GenerateMessageId();
            var message = CreateTLNonEncryptedMessage(messageId, obj);

            var historyItem = new HistoryItem
            {
                Caption = caption,
                Message = message,
                Callback = t => callback((T)t),
                FaultCallback = faultCallback,
                Status = RequestStatus.Sent
            };

            var guid = message.MessageId;
            lock (transport.SyncRoot)
            {
                if (transport.Closed)
                {
                    var transportDCId = transport.DCId;
                    var transportKey = transport.AuthKey;
                    var transportSalt = transport.Salt;
                    var transportSessionId = transport.SessionId;
                    var transportSequenceNumber = transport.SequenceNumber;
                    var transportClientTicksDelta = transport.ClientTicksDelta;
                    bool isCreated;
                    lock (_activeTransportRoot)
                    {
                        transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                    }
                    if (isCreated)
                    {
                        transport.DCId = transportDCId;
                        transport.AuthKey = transportKey;
                        transport.Salt = transportSalt;
                        transport.SessionId = transportSessionId;
                        transport.SequenceNumber = transportSequenceNumber;
                        transport.ClientTicksDelta = transportClientTicksDelta;
                        transport.ReceiveBytes += OnReceiveBytesByTransport;
                    }
                }
            }

            transport.EnqueueNonEncryptedItem(historyItem);

            transport.SendBytesAsync(caption + " " + guid, message.ToBytes(),
                socketError =>
                {
#if LOG_REGISTRATION
                    TLUtils.WriteLog(caption + " SocketError=" + socketError);
#endif
                    if (socketError != SocketError.Success)
                    {
                        transport.RemoveNonEncryptedItem(historyItem);

                        // connection is unsuccessfully
                        faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("FastCallback SocketError=" + socketError) });
                    }
                },
                error =>
                {
                    transport.RemoveNonEncryptedItem(historyItem);

                    faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404) });
                });
        }



        public void MessageAcknowledgmentsByTransport(ITransport transport, TLVector<TLLong> ids)
        {
            PrintCaption("msgs_ack");
            TLUtils.WriteLine("ids");
            foreach (var id in ids)
            {
                TLUtils.WriteLine(TLUtils.MessageIdString(id));
            }
            var obj = new TLMessageAcknowledgments { MsgIds = ids };

            var authKey = transport.AuthKey;
            var sesseionId = transport.SessionId;
            var salt = transport.Salt;
            var sequenceNumber = transport.SequenceNumber * 2;
            var messageId = transport.GenerateMessageId(true);
            var transportMessage = CreateTLTransportMessage(salt, sesseionId, new TLInt(sequenceNumber), messageId, obj);
            var encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);

            lock (transport.SyncRoot)
            {
                if (transport.Closed)
                {
                    var transportDCId = transport.DCId;
                    var transportKey = transport.AuthKey;
                    var transportSalt = transport.Salt;
                    var transportSessionId = transport.SessionId;
                    var transportSequenceNumber = transport.SequenceNumber;
                    var transportClientTicksDelta = transport.ClientTicksDelta;
                    bool isCreated;
                    lock (_activeTransport)
                    {
                        transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                    }
                    if (isCreated)
                    {
                        transport.DCId = transportDCId;
                        transport.AuthKey = transportKey;
                        transport.Salt = transportSalt;
                        transport.SessionId = transportSessionId;
                        transport.SequenceNumber = transportSequenceNumber;
                        transport.ClientTicksDelta = transportClientTicksDelta;
                        transport.ReceiveBytes += OnReceiveBytesByTransport;
                    }
                }
            }

            lock (_debugRoot)
            {
                Debug.WriteLine(">>{0, -30} MsgId {1} SeqNo {2, -4} SessionId {3}\nids:", "msgs_ack", transportMessage.MessageId.Value, transportMessage.SeqNo.Value, transportMessage.SessionId.Value);
                foreach (var id in ids)
                {
                    Debug.WriteLine(id.Value);
                }
            }

            transport.SendBytesAsync("msgs_ack " + transportMessage.MessageId, encryptedMessage.ToBytes(),
                result =>
                {
                    Debug.WriteLine("@msgs_ack {0} result {1}", transportMessage.MessageId, result);
                    //ReceiveBytesAsync(result, authKey);
                },
                error =>
                {
                    Debug.WriteLine("<<msgs_ack failed " + transportMessage.MessageId);
                });
        }

        private void ProcessBadMessageByTransport(ITransport transport, TLTransportMessage message, TLBadMessageNotification badMessage, HistoryItem historyItem)
        {
            if (historyItem == null) return;

            switch (badMessage.ErrorCode.Value)
            {
                case 16:
                case 17:
                    var errorInfo = new StringBuilder();
                    errorInfo.AppendLine("0. CORRECT TIME DELTA by Transport " + transport.DCId);
                    errorInfo.AppendLine(historyItem.Caption);

                    lock (_historyRoot)
                    {
                        _history.Remove(historyItem.Hash);
                    }
#if DEBUG
                    NotifyOfPropertyChange(() => History);
#endif
                    var serverTime = message.MessageId.Value;
                    //TLUtils.WriteLine("Server time: " + TLUtils.MessageIdString(BitConverter.GetBytes(serverTime)));
                    var serverDateTime = Utils.UnixTimestampToDateTime(serverTime >> 32);
                    errorInfo.AppendLine("Server time: " + serverDateTime);
                    var clientTime = transport.GenerateMessageId().Value;
                    //TLUtils.WriteLine("Client time: " + TLUtils.MessageIdString(BitConverter.GetBytes(clientTime)));
                    var clientDateTime = Utils.UnixTimestampToDateTime(clientTime >> 32);
                    errorInfo.AppendLine("Client time: " + clientDateTime);
                    var saveConfig = false;
                    lock (transport.SyncRoot)
                    {
                        if (historyItem.ClientTicksDelta == transport.ClientTicksDelta)
                        {
                            transport.ClientTicksDelta += serverTime - clientTime;
                            saveConfig = true;
                            errorInfo.AppendLine("Set ticks delta: " + transport.ClientTicksDelta + "(" + (serverDateTime-clientDateTime).TotalSeconds + " seconds)");
                        }
                    }

                    if (saveConfig && _config != null)
                    {
                        var dcOption = _config.DCOptions.FirstOrDefault(x => string.Equals(x.IpAddress.ToString(), transport.Host, StringComparison.OrdinalIgnoreCase));
                        if (dcOption != null)
                        {
                            dcOption.ClientTicksDelta = transport.ClientTicksDelta;
                            _cacheService.SetConfig(_config);
                        }
                    }

                    TLUtils.WriteLine(errorInfo.ToString(), LogSeverity.Error);
                    
                
                    // TODO: replace with SendInformativeMessage
                    var transportMessage = (TLContainerTransportMessage)historyItem.Message;
                    int sequenceNumber;
                    lock (transport.SyncRoot)
                    {
                        if (transportMessage.SeqNo.Value % 2 == 0)
                        {
                            sequenceNumber = 2 * transport.SequenceNumber;
                        }
                        else
                        {
                            sequenceNumber = 2 * transport.SequenceNumber + 1;
                            transport.SequenceNumber++;
                        }
                    }
                    transportMessage.SeqNo = new TLInt(sequenceNumber);
                    transportMessage.MessageId = transport.GenerateMessageId(true);
                    TLUtils.WriteLine("Corrected client time: " + TLUtils.MessageIdString(transportMessage.MessageId));
                    var authKey = transport.AuthKey;
                    var encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);

                    lock (_historyRoot)
                    {
                        _history[historyItem.Hash] = historyItem;
                    }

                    var faultCallback = historyItem.FaultCallback;

                    lock (transport.SyncRoot)
                    {
                        if (transport.Closed)
                        {
                            var transportDCId = transport.DCId;
                            var transportKey = transport.AuthKey;
                            var transportSalt = transport.Salt;
                            var transportSessionId = transport.SessionId;
                            var transportSequenceNumber = transport.SequenceNumber;
                            var transportClientTicksDelta = transport.ClientTicksDelta;
                            bool isCreated;
                            lock (_activeTransportRoot)
                            {
                                transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                            }
                            if (isCreated)
                            {
                                transport.DCId = transportDCId;
                                transport.AuthKey = transportKey;
                                transport.Salt = transportSalt;
                                transport.SessionId = transportSessionId;
                                transport.SequenceNumber = transportSequenceNumber;
                                transport.ClientTicksDelta = transportClientTicksDelta;
                                transport.ReceiveBytes += OnReceiveBytesByTransport;
                            }
                        }
                    }
                    Debug.WriteLine(">>{0, -30} MsgId {1} SeqNo {2,-4} SessionId {3} BadMsgId {4}", string.Format("{0}: {1}", historyItem.Caption, "time"), transportMessage.MessageId.Value, transportMessage.SeqNo.Value, message.SessionId.Value, badMessage.BadMessageId.Value);

                    transport.SendBytesAsync(historyItem.Caption + " " + message.SessionId.Value + " " + transportMessage.MessageId.Value,
                    encryptedMessage.ToBytes(),
                        result =>
                        {
                            Debug.WriteLine("@{0} {1} result {2}", string.Format("{0}: {1}", historyItem.Caption, "time"), transportMessage.MessageId.Value, result);

                        },//ReceiveBytesAsync(result, authKey),
                    error =>
                    {
                        lock (_historyRoot)
                        {
                            _history.Remove(historyItem.Hash);
                        }
#if DEBUG
                        NotifyOfPropertyChange(() => History);
#endif
                        faultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404) });
                    });

                    //_activeTransport.SendBytesAsync(historyItem.Caption + " " + transportMessage.MessageId, 
                    //    encryptedMessage.ToBytes(), result => ReceiveBytesAsync(result, authKey), 
                    //    () => { if (faultCallback != null) faultCallback(null); });

                    break;

                case 32:
                case 33:
                    TLUtils.WriteLine("INCORRECT MSGSEQNO ByTransport, CREATE NEW SESSION " + historyItem.Caption, LogSeverity.Error);


                    // fix seqNo with creating new Session
                    lock (transport.SyncRoot)
                    {
                        transport.SessionId = TLLong.Random();
                        transport.SequenceNumber = 0;
                        transportMessage = (TLTransportMessage)historyItem.Message;
                        if (transportMessage.SeqNo.Value % 2 == 0)
                        {
                            sequenceNumber = 2 * transport.SequenceNumber;
                        }
                        else
                        {
                            sequenceNumber = 2 * transport.SequenceNumber + 1;
                            transport.SequenceNumber++;
                        }
                    }
                    transportMessage.SeqNo = new TLInt(sequenceNumber);
                    ((TLTransportMessage)transportMessage).SessionId = transport.SessionId;


                    // TODO: replace with SendInformativeMessage

                    transportMessage.MessageId = transport.GenerateMessageId(true);
                    TLUtils.WriteLine("Corrected client time: " + TLUtils.MessageIdString(transportMessage.MessageId));
                    authKey = transport.AuthKey;
                    encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);

                    lock (_historyRoot)
                    {
                        _history[historyItem.Hash] = historyItem;
                    }

                    faultCallback = historyItem.FaultCallback;

                    lock (transport.SyncRoot)
                    {
                        if (transport.Closed)
                        {
                            var transportDCId = transport.DCId;
                            var transportKey = transport.AuthKey;
                            var transportSalt = transport.Salt;
                            var transportSessionId = transport.SessionId;
                            var transportSequenceNumber = transport.SequenceNumber;
                            var transportClientTicksDelta = transport.ClientTicksDelta;
                            bool isCreated;
                            lock (_activeTransportRoot)
                            {
                                transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                            }
                            if (isCreated)
                            {
                                transport.DCId = transportDCId;
                                transport.AuthKey = transportKey;
                                transport.Salt = transportSalt;
                                transport.SessionId = transportSessionId;
                                transport.SequenceNumber = transportSequenceNumber;
                                transport.ClientTicksDelta = transportClientTicksDelta;
                                transport.ReceiveBytes += OnReceiveBytesByTransport;
                            }
                        }
                    }
                    Debug.WriteLine(">>{0, -30} MsgId {1} SeqNo {2,-4} SessionId {3} BadMsgId {4}", string.Format("{0}: {1}", historyItem.Caption, "seqNo"), transportMessage.MessageId.Value, transportMessage.SeqNo.Value, message.SessionId.Value, badMessage.BadMessageId.Value);
                    transport.SendBytesAsync(historyItem.Caption + " " + transportMessage.MessageId, encryptedMessage.ToBytes(),
                        result =>
                        {
                            Debug.WriteLine("@{0} {1} result {2}", string.Format("{0}: {1}", historyItem.Caption, "seqNo"), transportMessage.MessageId.Value, result);

                        },//ReceiveBytesAsync(result, authKey)}, 
                        error => { if (faultCallback != null) faultCallback(null); });

                    break;
            }
        }

        private void ProcessBadServerSaltByTransport(ITransport transport, TLTransportMessage message, TLBadServerSalt badServerSalt, HistoryItem historyItem)
        {
            if (historyItem == null)
            {
                return;
            }

            var transportMessage = (TLContainerTransportMessage)historyItem.Message;
            lock (_historyRoot)
            {
                _history.Remove(historyItem.Hash);
            }
#if DEBUG
            NotifyOfPropertyChange(() => History);
#endif

            TLUtils.WriteLine("CORRECT SERVER SALT:");
            ((TLTransportMessage)transportMessage).Salt = badServerSalt.NewServerSalt;
            //Salt = badServerSalt.NewServerSalt;
            TLUtils.WriteLine("New salt: " + transport.Salt);

            switch (badServerSalt.ErrorCode.Value)
            {
                case 16:
                case 17:
                    TLUtils.WriteLine("1. CORRECT TIME DELTA with salt by transport " + transport.DCId);

                    var serverTime = message.MessageId.Value;
                    TLUtils.WriteLine("Server time: " + TLUtils.MessageIdString(BitConverter.GetBytes(serverTime)));
                    var clientTime = transport.GenerateMessageId().Value;
                    TLUtils.WriteLine("Client time: " + TLUtils.MessageIdString(BitConverter.GetBytes(clientTime)));
                    var saveConfig = false;
                    lock (transport.SyncRoot)
                    {
                        if (historyItem.ClientTicksDelta == transport.ClientTicksDelta)
                        {
                            saveConfig = true;
                            transport.ClientTicksDelta += serverTime - clientTime;
                        }
                    }

                    if (saveConfig && _config != null)
                    {
                        var dcOption = _config.DCOptions.FirstOrDefault(x => string.Equals(x.IpAddress.ToString(), transport.Host, StringComparison.OrdinalIgnoreCase));
                        if (dcOption != null)
                        {
                            dcOption.ClientTicksDelta += serverTime - clientTime;
                            _cacheService.SetConfig(_config);
                        }
                    }

                    transportMessage.MessageId = transport.GenerateMessageId(true);
                    TLUtils.WriteLine("Corrected client time: " + TLUtils.MessageIdString(transportMessage.MessageId));

                    break;
                case 48:
                    break;
            }

            if (transportMessage == null) return;

            var authKey = transport.AuthKey;
            var encryptedMessage = CreateTLEncryptedMessage(authKey, transportMessage);
            lock (_historyRoot)
            {
                _history[historyItem.Hash] = historyItem;
            }
            var faultCallback = historyItem.FaultCallback;

            lock (transport.SyncRoot)
            {
                if (transport.Closed)
                {
                    var transportDCId = transport.DCId;
                    var transportKey = transport.AuthKey;
                    var transportSalt = transport.Salt;
                    var transportSessionId = transport.SessionId;
                    var transportSequenceNumber = transport.SequenceNumber;
                    var transportClientTicksDelta = transport.ClientTicksDelta;
                    bool isCreated;
                    lock (_activeTransportRoot)
                    {
                        transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                    }
                    if (isCreated)
                    {
                        transport.DCId = transportDCId;
                        transport.AuthKey = transportKey;
                        transport.Salt = transportSalt;
                        transport.SessionId = transportSessionId;
                        transport.SequenceNumber = transportSequenceNumber;
                        transport.ClientTicksDelta = transportClientTicksDelta;
                        transport.ReceiveBytes += OnReceiveBytesByTransport;
                    }
                }
            }


            transport.SendBytesAsync(historyItem.Caption + " " + transportMessage.MessageId, encryptedMessage.ToBytes(),
                result =>
                {
                    Debug.WriteLine("@{0} {1} result {2}", historyItem.Caption, transportMessage.MessageId.Value, result);

                },//ReceiveBytesAsync(result, authKey)}, 
                error => { if (faultCallback != null) faultCallback(new TLRPCError()); });
        }

        private void ProcessRPCErrorByTransport(ITransport transport, TLRPCError error, HistoryItem historyItem)
        {
            if (error.CodeEquals(ErrorCode.UNAUTHORIZED))
            {
                if (historyItem != null
                    && historyItem.Caption != "account.updateStatus"
                    && historyItem.Caption != "account.registerDevice")
                {
#if DEBUG
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("Auth required: " + error.Code + " " + historyItem.Caption);
                    });
#endif

                    RaiseAuthorizationRequired();
                }
            }
            else if (error.CodeEquals(ErrorCode.ERROR_SEE_OTHER)
                && (error.TypeStarsWith(ErrorType.NETWORK_MIGRATE)
                    || error.TypeStarsWith(ErrorType.PHONE_MIGRATE)
                //|| error.TypeStarsWith(ErrorType.FILE_MIGRATE)
                    ))
            {
                var serverNumber = Convert.ToInt32(
                    error.GetErrorTypeString()
                    .Replace(ErrorType.NETWORK_MIGRATE.ToString(), string.Empty)
                    .Replace(ErrorType.PHONE_MIGRATE.ToString(), string.Empty)
                    //.Replace(ErrorType.FILE_MIGRATE.ToString(), string.Empty)
                    .Replace("_", string.Empty));

                if (_config == null
                    || _config.DCOptions.FirstOrDefault(x => x.Id != null && x.Id.Value == serverNumber) == null)
                {
                    GetConfigAsync(config =>
                    {
                        _config = TLConfig.Merge(_config, config);
                        SaveConfig();
                        if (historyItem.Object.GetType() == typeof(TLSendCode))
                        {
                            var dcOption = _config.DCOptions.Items.First(x => x.Id.Value == serverNumber);

                            lock (transport.SyncRoot)
                            {
                                var transportDCId = dcOption.Id.Value;
                                var transportKey = dcOption.AuthKey;
                                var transportSalt = dcOption.Salt;
                                var transportSessionId = TLLong.Random();
                                var transportSequenceNumber = 0;
                                var transportClientTicksDelta = dcOption.ClientTicksDelta;
                                bool isCreated;
                                lock (_activeTransportRoot)
                                {
                                    transport = _transportService.GetTransport(dcOption.IpAddress.ToString(), dcOption.Port.Value, Type, out isCreated);
                                }
                                if (isCreated)
                                {
                                    transport.DCId = transportDCId;
                                    transport.AuthKey = transportKey;
                                    transport.Salt = transportSalt;
                                    transport.SessionId = transportSessionId;
                                    transport.SequenceNumber = transportSequenceNumber;
                                    transport.ClientTicksDelta = transportClientTicksDelta;
                                    transport.ReceiveBytes += OnReceiveBytesByTransport;
                                }
                            }
                            lock (transport.SyncRoot)
                            {
                                transport.Initialized = false;
                            }
                            InitTransportAsync(transport, tuple =>
                            {
                                lock (transport.SyncRoot)
                                {
                                    transport.DCId = serverNumber;
                                    transport.AuthKey = tuple.Item1;
                                    transport.Salt = tuple.Item2;
                                    transport.SessionId = tuple.Item3;
                                }
                                var authKeyId = TLUtils.GenerateLongAuthKeyId(tuple.Item1);

                                lock (_authKeysRoot)
                                {
                                    if (!_authKeys.ContainsKey(authKeyId))
                                    {
                                        _authKeys.Add(authKeyId, new AuthKeyItem { AuthKey = tuple.Item1, AutkKeyId = authKeyId });
                                    }
                                }

                                dcOption.AuthKey = tuple.Item1;
                                dcOption.Salt = tuple.Item2;
                                dcOption.SessionId = tuple.Item3;

                                _config.ActiveDCOptionIndex = _config.DCOptions.IndexOf(dcOption);
                                _cacheService.SetConfig(_config);

                                lock (transport.SyncRoot)
                                {
                                    transport.Initialized = true;
                                }
                                RaiseInitialized();

                                SendInformativeMessage(historyItem.Caption, historyItem.Object, historyItem.Callback, historyItem.FaultCallback);
                            },
                            er =>
                            {
                                lock (transport.SyncRoot)
                                {
                                    transport.Initialized = false;
                                }
                                historyItem.FaultCallback.SafeInvoke(er);
                            });
                        }
                        else
                        {
                            MigrateAsync(serverNumber, auth => SendInformativeMessage(historyItem.Caption, historyItem.Object, historyItem.Callback, historyItem.FaultCallback));
                        }
                    });

                }
                else
                {
                    if (historyItem.Object.GetType() == typeof(TLSendCode)
                        || historyItem.Object.GetType() == typeof(TLGetFile))
                    {
                        var activeDCOption = _config.DCOptions.Items.First(x => x.Id.Value == serverNumber);

                        lock (transport.SyncRoot)
                        {
                            var transportDCId = activeDCOption.Id.Value;
                            var transportKey = activeDCOption.AuthKey;
                            var transportSalt = activeDCOption.Salt;
                            var transportSessionId = TLLong.Random();
                            var transportSequenceNumber = 0;
                            var transportClientTicksDelta = activeDCOption.ClientTicksDelta;
                            bool isCreated;
                            lock (_activeTransportRoot)
                            {
                                _activeTransport = _transportService.GetTransport(activeDCOption.IpAddress.ToString(), activeDCOption.Port.Value, Type, out isCreated);
                            }
                            if (isCreated)
                            {
                                transport.DCId = transportDCId;
                                transport.AuthKey = transportKey;
                                transport.Salt = transportSalt;
                                transport.SessionId = transportSessionId;
                                transport.SequenceNumber = transportSequenceNumber;
                                transport.ClientTicksDelta = transportClientTicksDelta;
                                transport.ReceiveBytes += OnReceiveBytesByTransport;
                            }
                        }

                        if (activeDCOption.AuthKey == null)
                        {
                            lock (transport.SyncRoot)
                            {
                                transport.Initialized = false;
                            }
                            InitTransportAsync(transport, tuple =>
                            {
                                lock (transport.SyncRoot)
                                {
                                    transport.DCId = serverNumber;
                                    transport.AuthKey = tuple.Item1;
                                    transport.Salt = tuple.Item2;
                                    transport.SessionId = tuple.Item3;
                                }

                                var authKeyId = TLUtils.GenerateLongAuthKeyId(tuple.Item1);

                                lock (_authKeysRoot)
                                {
                                    if (!_authKeys.ContainsKey(authKeyId))
                                    {
                                        _authKeys.Add(authKeyId, new AuthKeyItem { AuthKey = tuple.Item1, AutkKeyId = authKeyId });
                                    }
                                }

                                activeDCOption.AuthKey = tuple.Item1;
                                activeDCOption.Salt = tuple.Item2;
                                activeDCOption.SessionId = tuple.Item3;

                                _config.ActiveDCOptionIndex = _config.DCOptions.IndexOf(activeDCOption);
                                _cacheService.SetConfig(_config);

                                lock (transport.SyncRoot)
                                {
                                    transport.Initialized = true;
                                }

                                RaiseInitialized();
                                SendInformativeMessage(historyItem.Caption, historyItem.Object, historyItem.Callback, historyItem.FaultCallback);
                            },
                            er =>
                            {
                                lock (transport.SyncRoot)
                                {
                                    transport.Initialized = false;
                                }
                                historyItem.FaultCallback.SafeInvoke(er);
                            });
                        }
                        else
                        {
                            lock (transport.SyncRoot)
                            {
                                transport.AuthKey = activeDCOption.AuthKey;
                                transport.Salt = activeDCOption.Salt;
                                transport.SessionId = TLLong.Random();
                            }
                            var authKeyId = TLUtils.GenerateLongAuthKeyId(activeDCOption.AuthKey);

                            lock (_authKeysRoot)
                            {
                                if (!_authKeys.ContainsKey(authKeyId))
                                {
                                    _authKeys.Add(authKeyId, new AuthKeyItem { AuthKey = activeDCOption.AuthKey, AutkKeyId = authKeyId });
                                }
                            }


                            _config.ActiveDCOptionIndex = _config.DCOptions.IndexOf(activeDCOption);
                            _cacheService.SetConfig(_config);

                            lock (transport.SyncRoot)
                            {
                                transport.Initialized = true;
                            }
                            RaiseInitialized();

                            SendInformativeMessage(historyItem.Caption, historyItem.Object, historyItem.Callback, historyItem.FaultCallback);
                        }
                    }
                    else
                    {
                        MigrateAsync(serverNumber, auth => SendInformativeMessage(historyItem.Caption, historyItem.Object, historyItem.Callback, historyItem.FaultCallback));
                    }
                }
            }
            else if (historyItem.FaultCallback != null)
            {
                historyItem.FaultCallback(error);
            }
        }

        private void OnReceiveBytesByTransport(object sender, DataEventArgs e)
        {
            var transport = (ITransport)sender;
            bool isInitialized;
            lock (transport.SyncRoot)
            {
                isInitialized = transport.AuthKey != null;
            }

            var position = 0;
            var handled = false;

            if (!isInitialized)
            {
                try
                {
                    var message = TLObject.GetObject<TLNonEncryptedMessage>(e.Data, ref position);

                    var item = transport.DequeueFirstNonEncryptedItem();
                    if (item != null)
                    {
#if LOG_REGISTRATION
                            TLUtils.WriteLog("OnReceivedBytes !IsInitialized try historyItem " + item.Caption);
#endif
                        item.Callback.SafeInvoke(message.Data);
                    }
                    else
                    {
#if LOG_REGISTRATION
                        TLUtils.WriteLog("OnReceivedBytes !IsInitialized cannot try historyItem ");
#endif
                    }

                    handled = true;
                }
                catch (Exception ex)
                {
#if LOG_REGISTRATION

                    var sb = new StringBuilder();
                    sb.AppendLine("OnReceiveBytes !IsInitialized catch Exception: \n" + ex);
                    sb.AppendLine(transport.PrintNonEncryptedHistory());                   
                    TLUtils.WriteLog(sb.ToString());
#endif
                }

                if (!handled)
                {
#if LOG_REGISTRATION
                    TLUtils.WriteLog("OnReceiveBytes !IsInitialized !handled invoke ReceiveBytesAsync");
#endif
                    ReceiveBytesByTransportAsync(transport, e.Data);
                }
            }
            else
            {
#if LOG_REGISTRATION
                TLUtils.WriteLog("OnReceiveBytes IsInitialized invoke ReceiveBytesAsync");
#endif
                ReceiveBytesByTransportAsync(transport, e.Data);
            }
        }

        private void ReceiveBytesByTransportAsync(ITransport transport, byte[] bytes)
        {
            try
            {
                var position = 0;
                var encryptedMessage = (TLEncryptedTransportMessage)new TLEncryptedTransportMessage().FromBytes(bytes, ref position);

                encryptedMessage.Decrypt(transport.AuthKey);

                position = 0;
                TLTransportMessage transportMessage;
                transportMessage = TLObject.GetObject<TLTransportMessage>(encryptedMessage.Data, ref position);

                // get acknowledgments
                foreach (var acknowledgment in TLUtils.FindInnerObjects<TLMessagesAcknowledgment>(transportMessage))
                {
                    var ids = acknowledgment.MessageIds.Items;
                    lock (_historyRoot)
                    {
                        foreach (var id in ids)
                        {
                            if (_history.ContainsKey(id.Value))
                            {
                                _history[id.Value].Status = RequestStatus.Confirmed;
                            }
                        }
                    }

                }

                // send acknowledgments
                SendAcknowledgmentsByTransport(transport, transportMessage);

                // updates
                _updatesService.ProcessTransportMessage(transportMessage);

                // bad messages
                foreach (var badMessage in TLUtils.FindInnerObjects<TLBadMessageNotification>(transportMessage))
                {
                    HistoryItem item = null;
                    lock (_historyRoot)
                    {
                        if (_history.ContainsKey(badMessage.BadMessageId.Value))
                        {
                            item = _history[badMessage.BadMessageId.Value];
                        }
                    }

                    ProcessBadMessageByTransport(transport, transportMessage, badMessage, item);
                }

                // bad server salts
                foreach (var badServerSalt in TLUtils.FindInnerObjects<TLBadServerSalt>(transportMessage))
                {
                    lock (transport.SyncRoot)
                    {
                        transport.Salt = badServerSalt.NewServerSalt;
                    }
                    HistoryItem item = null;
                    lock (_historyRoot)
                    {
                        if (_history.ContainsKey(badServerSalt.BadMessageId.Value))
                        {
                            item = _history[badServerSalt.BadMessageId.Value];
                        }
                    }

                    ProcessBadServerSaltByTransport(transport, transportMessage, badServerSalt, item);
                }

                // new session created
                foreach (var newSessionCreated in TLUtils.FindInnerObjects<TLNewSessionCreated>(transportMessage))
                {
                    TLUtils.WritePerformance(string.Format("NEW SESSION CREATED: {0} (old {1})", transportMessage.SessionId, _activeTransport.SessionId));
                    lock (transport.SyncRoot)
                    {
                        transport.SessionId = transportMessage.SessionId;
                        transport.Salt = newSessionCreated.ServerSalt;
                    }
                }

                // rpcresults
                foreach (var result in TLUtils.FindInnerObjects<TLRPCResult>(transportMessage))
                {
                    HistoryItem historyItem = null;

                    lock (_historyRoot)
                    {
                        if (_history.ContainsKey(result.RequestMessageId.Value))
                        {
                            historyItem = _history[result.RequestMessageId.Value];
                            _history.Remove(result.RequestMessageId.Value);

                            //correct client ticks delta on getting response
                            var serverTime = transportMessage.MessageId.Value;
                            //TLUtils.WriteLine("Server time: " + TLUtils.MessageIdString(BitConverter.GetBytes(serverTime)));
                            var clientTime = transport.GenerateMessageId().Value;
                            //TLUtils.WriteLine("Client time: " + TLUtils.MessageIdString(BitConverter.GetBytes(clientTime)));


                            lock (transport.SyncRoot)
                            {
                                if (0 == transport.ClientTicksDelta)
                                {
                                    transport.ClientTicksDelta += serverTime - clientTime;
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
#if DEBUG
                    NotifyOfPropertyChange(() => History);
#endif

                    //RemoveItemFromSendingQueue(result.RequestMessageId.Value);

                    var error = result.Object as TLRPCError;
                    if (error != null)
                    {
                        ProcessRPCErrorByTransport(transport, error, historyItem);
                        Debug.WriteLine("RPCError: " + error.Code + " " + error.Message + " MsgId " +
                                        result.RequestMessageId.Value);
                        TLUtils.WriteLine("RPCError: " + error.Code + " " + error.Message);
                    }
                    else
                    {
                        var messageData = result.Object;
                        if (messageData is TLGzipPacked)
                        {
                            messageData = ((TLGzipPacked)messageData).Data;
                        }

                        if (messageData is TLSentMessage)
                        {
                            lock (_sendingQueue)
                            {
                                for (var i = 0; i < _sendingQueue.Count; i++)
                                {
                                    if (_sendingQueue[i].Hash == historyItem.Hash)
                                    {
                                        _sendingQueue.RemoveAt(i--);
                                    }
                                }
                            }
                        }

                        try
                        {
                            historyItem.Callback(messageData);
                        }
                        catch (Exception e)
                        {
#if LOG_REGISTRATION
                                TLUtils.WriteLog(e.ToString());
#endif
                            TLUtils.WriteException(e);
                        }
                    }
                }
            }
            catch (Exception e)
            {
#if LOG_REGISTRATION
                TLUtils.WriteLog("ReceiveBytesAsyncException:\n" + e);
#endif
                TLUtils.WriteException(e);
                ClearHistoryByTransport(transport);

                lock (transport.SyncRoot)
                {
                    // continue listening on fault
                    var transportDCId = transport.DCId;
                    var transportKey = transport.AuthKey;
                    var transportSalt = transport.Salt;
                    var transportSessionId = transport.SessionId;
                    var transportSequenceNumber = transport.SequenceNumber;
                    var transportClientTicksDelta = transport.ClientTicksDelta;
                    bool isCreated;
                    lock (_activeTransportRoot)
                    {
                        transport = _transportService.GetTransport(transport.Host, transport.Port, Type, out isCreated);
                    }
                    if (isCreated)
                    {
                        transport.DCId = transportDCId;
                        transport.AuthKey = transportKey;
                        transport.Salt = transportSalt;
                        transport.SessionId = transportSessionId;
                        transport.SequenceNumber = transportSequenceNumber;
                        transport.ClientTicksDelta = transportClientTicksDelta;
                        transport.ReceiveBytes += OnReceiveBytesByTransport;
                    }
                }
                // to bind authKey to current TCPTransport
                UpdateStatusAsync(new TLBool(false), result => { });
            }
        }

        public void ClearHistoryByTransport(ITransport transport)
        {
            _transportService.CloseTransport(transport);

            lock (_historyRoot)
            {
                var keysToRemove = new List<long>();
                foreach (var keyValue in _history)
                {
                    if (keyValue.Value.Caption.StartsWith("msgs_ack"))
                    {
                        TLUtils.WriteLine("!!!!!!MSGS_ACK FAULT!!!!!!!", LogSeverity.Error);
                        Debug.WriteLine("!!!!!!MSGS_ACK FAULT!!!!!!!");
                    }
                    if (transport.DCId == keyValue.Value.DCId)
                    {
                        keyValue.Value.FaultCallback.SafeInvoke(new TLRPCError { Code = new TLInt(404), Message = new TLString("Clear History") });
                        keysToRemove.Add(keyValue.Key);
                    }
                }
                foreach (var key in keysToRemove)
                {
                    _history.Remove(key);
                }
            }

            transport.ClearNonEncryptedHistory();
        }
    }
}
