﻿using System.IO;
using System.Windows;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLDecryptedMessageBase : TLObject
    {
        public TLLong RandomId { get; set; }

        public long RandomIndex
        {
            get { return RandomId != null ? RandomId.Value : 0; }
            set { RandomId = new TLLong(value); }
        }

        public TLString RandomBytes { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            RandomId = GetObject<TLLong>(bytes, ref position);
            RandomBytes = GetObject<TLString>(bytes, ref position);

            return this;
        }

        #region Additional 
        public TLInt ChatId { get; set; }
        public TLInputEncryptedFileUploaded InputFile { get; set; }     // to send media

        public TLInt FromId { get; set; }
        public TLBool Out { get; set; }
        public TLBool Unread { get; set; }
        
        public TLInt Date { get; set; }
        public int DateIndex
        {
            get { return Date != null ? Date.Value : 0; }
            set { Date = new TLInt(value); }
        }

        public TLInt Qts { get; set; }
        public int QtsIndex
        {
            get { return Qts != null ? Qts.Value : 0; }
            set { Qts = new TLInt(value); }
        }

        public TLLong DeleteDate { get; set; }
        public long DeleteIndex
        {
            get { return DeleteDate != null ? DeleteDate.Value : 0; }
            set { DeleteDate = new TLLong(value); }
        }

        public MessageStatus Status { get; set; }

        public virtual bool ShowFrom
        {
            get { return false; }
        }

        private bool _isSelected;

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetField(ref _isSelected, value, () => IsSelected); }
        }

        public TLInt TTL { get; set; }
        #endregion

        public virtual void Update(TLDecryptedMessageBase message)
        {
            ChatId = message.ChatId ?? ChatId;
            InputFile = message.InputFile ?? InputFile;
            FromId = message.FromId ?? FromId;
            Out = message.Out ?? Out;
            Unread = message.Unread ?? Unread;
            Date = message.Date ?? Date;
            Qts = message.Qts ?? Qts;
            DeleteDate = message.DeleteDate ?? DeleteDate;
            Status = message.Status;
            TTL = message.TTL ?? TTL;
        }
    }

    public class TLDecryptedMessage : TLDecryptedMessageBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessage;

        public TLString Message { get; set; }

        public TLDecryptedMessageMediaBase Media { get; set; }

        #region Additional

        public Visibility MessageVisibility
        {
            get { return Message == null || string.IsNullOrEmpty(Message.ToString()) ? Visibility.Collapsed : Visibility.Visible; }
        }

        #endregion

        public override string ToString()
        {
            if (Media is TLDecryptedMessageMediaEmpty)
            {
                return string.Format("Qts={0}, Date={1}: {2}", QtsIndex, DateIndex, Message);
            }

            return string.Format("Qts={0}, Date={1}: {2}", QtsIndex, DateIndex, Media); ;
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            Message = GetObject<TLString>(bytes, ref position);
            Media = GetObject<TLDecryptedMessageMediaBase>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                RandomId.ToBytes(),
                RandomBytes.ToBytes(),
                Message.ToBytes(),
                Media.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            RandomId = GetObject<TLLong>(input);
            RandomBytes = GetObject<TLString>(input);
            Message = GetObject<TLString>(input);
            Media = GetObject<TLDecryptedMessageMediaBase>(input);

            ChatId = GetNullableObject<TLInt>(input);
            InputFile = GetNullableObject<TLInputEncryptedFileUploaded>(input);
            FromId = GetNullableObject<TLInt>(input);
            Out = GetNullableObject<TLBool>(input);
            Unread = GetNullableObject<TLBool>(input);
            Date = GetNullableObject<TLInt>(input);
            DeleteDate = GetNullableObject<TLLong>(input);
            Qts = GetNullableObject<TLInt>(input);

            var status = GetObject<TLInt>(input);
            Status = (MessageStatus)status.Value;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(RandomId.ToBytes());
            output.Write(RandomBytes.ToBytes());
            output.Write(Message.ToBytes());
            Media.ToStream(output);

            ChatId.NullableToStream(output);
            InputFile.NullableToStream(output);
            FromId.NullableToStream(output);
            Out.NullableToStream(output);
            Unread.NullableToStream(output);
            Date.NullableToStream(output);
            DeleteDate.NullableToStream(output);
            Qts.NullableToStream(output);

            var status = new TLInt((int)Status);
            output.Write(status.ToBytes());
        }
    }

    public class TLDecryptedMessageService : TLDecryptedMessageBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageService;

        public TLDecryptedMessageActionBase Action { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            Action = GetObject<TLDecryptedMessageActionBase>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                RandomId.ToBytes(),
                RandomBytes.ToBytes(),
                Action.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            RandomId = GetObject<TLLong>(input);
            RandomBytes = GetObject<TLString>(input);
            Action = GetObject<TLDecryptedMessageActionBase>(input);

            ChatId = GetNullableObject<TLInt>(input);
            FromId = GetNullableObject<TLInt>(input);
            Out = GetNullableObject<TLBool>(input);
            Unread = GetNullableObject<TLBool>(input);
            Date = GetNullableObject<TLInt>(input);
            DeleteDate = GetNullableObject<TLLong>(input);
            Qts = GetNullableObject<TLInt>(input);

            var status = GetObject<TLInt>(input);
            Status = (MessageStatus)status.Value;

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(RandomId.ToBytes());
            output.Write(RandomBytes.ToBytes());
            output.Write(Action.ToBytes());

            ChatId.NullableToStream(output);
            FromId.NullableToStream(output);
            Out.NullableToStream(output);
            Unread.NullableToStream(output);
            Date.NullableToStream(output);
            DeleteDate.NullableToStream(output);
            Qts.NullableToStream(output);

            var status = new TLInt((int)Status);
            output.Write(status.ToBytes());
        }
    }
}
