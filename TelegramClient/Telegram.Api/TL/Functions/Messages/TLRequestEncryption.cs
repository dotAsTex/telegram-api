﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLRequestEncryption : TLObject
    {
        public const string Signature = "#f64daf43";

        public TLInputUserBase UserId { get; set; }

        public TLInt RandomId { get; set; }

        public TLString G_A { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                UserId.ToBytes(),
                RandomId.ToBytes(),
                G_A.ToBytes());
        }
    }
}
