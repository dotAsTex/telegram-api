﻿namespace Telegram.Api.TL.Functions.Messages
{
    public class TLSendBroadcast : TLObject
    {
        public const string Signature = "#41bb0972";

        public TLVector<TLInputUserBase> Contacts { get; set; }

        public TLString Message { get; set; }

        public TLInputMediaBase Media { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Contacts.ToBytes(),
                Message.ToBytes(),
                Media.ToBytes());
        }
    }
}
