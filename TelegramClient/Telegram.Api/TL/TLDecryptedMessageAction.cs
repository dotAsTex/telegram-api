﻿using System.IO;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLDecryptedMessageActionBase : TLObject { }

    #region Additional
    public class TLDecryptedMessageActionEmpty : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }
    #endregion

    public class TLDecryptedMessageActionSetMessageTTL : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionSetMessageTTL;

        public TLInt TTLSeconds { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            TTLSeconds = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                TTLSeconds.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            TTLSeconds = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(TTLSeconds.ToBytes());
        }
    }

    public class TLDecryptedMessageActionReadMessages : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionReadMessages;

        public TLVector<TLLong> RandomIds { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            RandomIds = GetObject<TLVector<TLLong>>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                RandomIds.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            RandomIds = GetObject<TLVector<TLLong>>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(RandomIds.ToBytes());
        }
    }

    public class TLDecryptedMessageActionDeleteMessages : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionDeleteMessages;

        public TLVector<TLLong> RandomIds { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            RandomIds = GetObject<TLVector<TLLong>>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                RandomIds.ToBytes());
        }
        
        public override TLObject FromStream(Stream input)
        {
            RandomIds = GetObject<TLVector<TLLong>>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(RandomIds.ToBytes());
        }
    }

    public class TLDecryptedMessageActionScreenshotMessages : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionScreenshotMessages;

        public TLVector<TLLong> RandomIds { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            RandomIds = GetObject<TLVector<TLLong>>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                RandomIds.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            RandomIds = GetObject<TLVector<TLLong>>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(RandomIds.ToBytes());
        }
    }

    public class TLDecryptedMessageActionFlushHistory : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionFlushHistory;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }

    public class TLDecryptedMessageActionNotifyLayer : TLDecryptedMessageActionBase
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageActionNotifyLayer;

        public TLInt Layer { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Layer = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Layer.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Layer = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Layer.ToBytes());
        }
    }
}
