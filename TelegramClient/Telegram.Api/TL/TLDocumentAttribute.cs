﻿using System.IO;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLDocumentAttributeBase  : TLObject { }

    public class TLDocumentAttributeImageSize : TLDocumentAttributeBase
    {
        public const uint Signature = TLConstructors.TLDocumentAttributeImageSize;

        public TLInt W { get; set; }

        public TLInt H { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            W = GetObject<TLInt>(bytes, ref position);
            H = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                W.ToBytes(),
                H.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            W = GetObject<TLInt>(input);
            H = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            W.ToStream(output);
            H.ToStream(output);
        }
    }

    public class TLDocumentAttributeAnimated : TLDocumentAttributeBase
    {
        public const uint Signature = TLConstructors.TLDocumentAttributeAnimated;
        
        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }

    public class TLDocumentAttributeSticker : TLDocumentAttributeBase
    {
        public const uint Signature = TLConstructors.TLDocumentAttributeSticker;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.SignatureToBytes(Signature);
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
        }
    }

    public class TLDocumentAttributeVideo : TLDocumentAttributeBase
    {
        public const uint Signature = TLConstructors.TLDocumentAttributeVideo;

        public TLInt Duration { get; set; }

        public TLInt W { get; set; }

        public TLInt H { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Duration = GetObject<TLInt>(bytes, ref position);
            W = GetObject<TLInt>(bytes, ref position);
            H = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Duration.ToBytes(),
                W.ToBytes(),
                H.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Duration = GetObject<TLInt>(input);
            W = GetObject<TLInt>(input);
            H = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Duration.ToStream(output);
            W.ToStream(output);
            H.ToStream(output);
        }
    }

    public class TLDocumentAttributeAudio : TLDocumentAttributeBase
    {
        public const uint Signature = TLConstructors.TLDocumentAttributeAudio;

        public TLInt Duration { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Duration = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Duration.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Duration = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Duration.ToStream(output);
        }
    }

    public class TLDocumentAttributeFileName : TLDocumentAttributeBase
    {
        public const uint Signature = TLConstructors.TLDocumentAttributeFileName;

        public TLString FileName { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            FileName = GetObject<TLString>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                FileName.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            FileName = GetObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            FileName.ToStream(output);
        }
    }
}
