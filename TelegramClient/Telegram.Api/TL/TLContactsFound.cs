﻿namespace Telegram.Api.TL
{
    public class TLContactsFound : TLObject
    {
        public const uint Signature = TLConstructors.TLContactsFound;

        public TLVector<TLContactFound> Results { get; set; }
 
        public TLVector<TLUserBase> Users { get; set; } 

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Results = GetObject<TLVector<TLContactFound>>(bytes, ref position);
            Users = GetObject<TLVector<TLUserBase>>(bytes, ref position);

            return this;
        }
    }
}
