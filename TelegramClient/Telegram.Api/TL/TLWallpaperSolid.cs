﻿namespace Telegram.Api.TL
{
    public class TLWallPaperSolid : TLObject
    {
        public const uint Signature = TLConstructors.TLWallPaperSolid;

        public TLInt Id { get; set; }

        public TLString Title { get; set; }

        public TLInt BgColor { get; set; }

        public TLInt Color { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLWallPaperSolid--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLInt>(bytes, ref position);
            Title = GetObject<TLString>(bytes, ref position);
            BgColor = GetObject<TLInt>(bytes, ref position);
            Color = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }
}
