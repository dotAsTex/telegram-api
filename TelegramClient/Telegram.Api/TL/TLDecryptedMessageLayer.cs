﻿namespace Telegram.Api.TL
{
    public class TLDecryptedMessageLayer : TLObject
    {
        public const uint Signature = TLConstructors.TLDecryptedMessageLayer;

        public TLInt Layer { get; set; }

        public TLDecryptedMessageBase Message { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Layer.ToBytes(),
                Message.ToBytes());
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Layer = GetObject<TLInt>(bytes, ref position);
            Message = GetObject<TLDecryptedMessageBase>(bytes, ref position);

            return this;
        }
    }
}
