﻿using System.IO;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLDocumentBase : TLObject
    {
        public TLLong Id { get; set; }

        public virtual int DocumentSize { get { return 0; } }

        public static bool DocumentEquals(TLDocumentBase document1, TLDocumentBase document2)
        {
            var doc1 = document1 as TLDocument;
            var doc2 = document2 as TLDocument;

            if (doc1 == null || doc2 == null) return false;

            return doc1.Id.Value == doc2.Id.Value
                   && doc1.DCId.Value == doc2.DCId.Value
                   && doc1.AccessHash.Value == doc2.AccessHash.Value;
        }
    }

    public class TLDocumentEmpty : TLDocumentBase
    {
        public const uint Signature = TLConstructors.TLDocumentEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLLong>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(TLUtils.SignatureToBytes(Signature), Id.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLLong>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));

            Id.ToStream(output);
        }
    }

    public class TLDocument : TLDocumentBase
    {
        public const uint Signature = TLConstructors.TLDocument;

        public TLLong AccessHash { get; set; }

        public TLInt UserId { get; set; }

        public TLInt Date { get; set; }

        public TLString FileName { get; set; }

        public string FileExt
        {
            get { return Path.GetExtension(FileName.ToString()).Replace(".", string.Empty); }
        }

        public TLString MimeType { get; set; }

        public TLInt Size { get; set; }

        public override int DocumentSize
        {
            get
            {
                return Size != null ? Size.Value : 0;
            }
        }

        public TLPhotoSizeBase Thumb { get; set; }

        public TLInt DCId { get; set; }

        public string GetFileName()
        {
            return string.Format("document{0}_{1}.{2}", Id, AccessHash, FileExt);
        }


        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLLong>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);
            UserId = GetObject<TLInt>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            FileName = GetObject<TLString>(bytes, ref position);
            MimeType = GetObject<TLString>(bytes, ref position);
            Size = GetObject<TLInt>(bytes, ref position);
            Thumb = GetObject<TLPhotoSizeBase>(bytes, ref position);
            DCId = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                Id.ToBytes(),
                AccessHash.ToBytes(),
                UserId.ToBytes(),
                Date.ToBytes(),
                FileName.ToBytes(),
                MimeType.ToBytes(),
                Size.ToBytes(),
                Thumb.ToBytes(),
                DCId.ToBytes());
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLLong>(input);
            AccessHash = GetObject<TLLong>(input);
            UserId = GetObject<TLInt>(input);
            Date = GetObject<TLInt>(input);
            FileName = GetObject<TLString>(input);
            MimeType = GetObject<TLString>(input);
            Size = GetObject<TLInt>(input);
            Thumb = GetObject<TLPhotoSizeBase>(input);
            DCId = GetObject<TLInt>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            Id.ToStream(output);
            AccessHash.ToStream(output);
            UserId.ToStream(output);
            Date.ToStream(output);
            FileName.ToStream(output);
            MimeType.ToStream(output);
            Size.ToStream(output);
            Thumb.ToStream(output);
            DCId.ToStream(output);
        }

        public byte[] Buffer { get; set; }
        public TLInputFile ThumbInputFile { get; set; }

        public TLInputDocumentFileLocation ToInputFileLocation()
        {
            return new TLInputDocumentFileLocation { AccessHash = AccessHash, Id = Id };
        }
    }
}
