﻿using System.IO;
using Telegram.Api.Extensions;

namespace Telegram.Api.TL
{
    public abstract class TLEncryptedChatBase : TLObject
    {
        public int Index { get { return Id.Value; } }

        public TLInt Id { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            Id = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        #region Additional
        public TLString Key { get; set; }

        public TLLong KeyFingerprint { get; set; }

        public TLString P { get; set; }

        public TLInt G { get; set; }

        public TLString A { get; set; }

        public TLInt MessageTTL { get; set; }

        public TLString FileName { get; set; }
        #endregion

        public virtual void Update(TLEncryptedChatBase chat)
        {
            Id = chat.Id;
            if (chat.Key != null) Key = chat.Key;
            if (chat.KeyFingerprint != null) KeyFingerprint = chat.KeyFingerprint;
            if (chat.P != null) P = chat.P;
            if (chat.G != null) G = chat.G;
            if (chat.A != null) A = chat.A;
        }
    }

    public class TLEncryptedChatEmpty : TLEncryptedChatBase
    {
        public const uint Signature = TLConstructors.TLEncryptedChatEmpty;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            return this;
        }

        public override void Update(TLEncryptedChatBase chat)
        {
            base.Update(chat);
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);

            Key = GetNullableObject<TLString>(input);
            KeyFingerprint = GetNullableObject<TLLong>(input);
            P = GetNullableObject<TLString>(input);
            G = GetNullableObject<TLInt>(input);
            A = GetNullableObject<TLString>(input);
            MessageTTL = GetNullableObject<TLInt>(input);
            FileName = GetNullableObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());

            Key.NullableToStream(output);
            KeyFingerprint.NullableToStream(output);
            P.NullableToStream(output);
            G.NullableToStream(output);
            A.NullableToStream(output);
            MessageTTL.NullableToStream(output);
            FileName.NullableToStream(output);
        }
    }

    public abstract class TLEncryptedChatCommon : TLEncryptedChatBase
    {
        public TLLong AccessHash { get; set; }

        public TLInt Date { get; set; }

        public TLInt AdminId { get; set; }

        public TLInt ParticipantId { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            base.FromBytes(bytes, ref position);

            AccessHash = GetObject<TLLong>(bytes, ref position);
            Date = GetObject<TLInt>(bytes, ref position);
            AdminId = GetObject<TLInt>(bytes, ref position);
            ParticipantId = GetObject<TLInt>(bytes, ref position);

            return this;
        }

        public override void Update(TLEncryptedChatBase chat)
        {
            base.Update(chat);

            var chatCommon = chat as TLEncryptedChatCommon;
            if (chatCommon != null)
            {
                AccessHash = chatCommon.AccessHash;
                Date = chatCommon.Date;
                AdminId = chatCommon.AdminId;
                ParticipantId = chatCommon.ParticipantId;
            }
        }
    }

    public class TLEncryptedChatWaiting : TLEncryptedChatCommon
    {
        public const uint Signature = TLConstructors.TLEncryptedChatWaiting;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            AccessHash = GetObject<TLLong>(input);
            Date = GetObject<TLInt>(input);
            AdminId = GetObject<TLInt>(input);
            ParticipantId = GetObject<TLInt>(input);

            Key = GetNullableObject<TLString>(input);
            KeyFingerprint = GetNullableObject<TLLong>(input);
            P = GetNullableObject<TLString>(input);
            G = GetNullableObject<TLInt>(input);
            A = GetNullableObject<TLString>(input);
            MessageTTL = GetNullableObject<TLInt>(input);
            FileName = GetNullableObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Date.ToBytes());
            output.Write(AdminId.ToBytes());
            output.Write(ParticipantId.ToBytes());

            Key.NullableToStream(output);
            KeyFingerprint.NullableToStream(output);
            P.NullableToStream(output);
            G.NullableToStream(output);
            A.NullableToStream(output);
            MessageTTL.NullableToStream(output);
            FileName.NullableToStream(output);
        }
    }

    public class TLEncryptedChatRequested : TLEncryptedChatCommon
    {
        public const uint Signature = TLConstructors.TLEncryptedChatRequested;

        public TLString GA { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            GA = GetObject<TLString>(bytes, ref position);

            return this;
        }

        public override void Update(TLEncryptedChatBase chat)
        {
            base.Update(chat);

            var chatRequested = chat as TLEncryptedChatRequested;
            if (chatRequested != null)
            {
                GA = chatRequested.GA;
            }
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            AccessHash = GetObject<TLLong>(input);
            Date = GetObject<TLInt>(input);
            AdminId = GetObject<TLInt>(input);
            ParticipantId = GetObject<TLInt>(input);
            GA = GetObject<TLString>(input);

            Key = GetNullableObject<TLString>(input);
            KeyFingerprint = GetNullableObject<TLLong>(input);
            P = GetNullableObject<TLString>(input);
            G = GetNullableObject<TLInt>(input);
            A = GetNullableObject<TLString>(input);
            MessageTTL = GetNullableObject<TLInt>(input);
            FileName = GetNullableObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Date.ToBytes());
            output.Write(AdminId.ToBytes());
            output.Write(ParticipantId.ToBytes());
            output.Write(GA.ToBytes());

            Key.NullableToStream(output);
            KeyFingerprint.NullableToStream(output);
            P.NullableToStream(output);
            G.NullableToStream(output);
            A.NullableToStream(output);
            MessageTTL.NullableToStream(output);
            FileName.NullableToStream(output);
        }
    }

    public class TLEncryptedChat : TLEncryptedChatCommon
    {
        public const uint Signature = TLConstructors.TLEncryptedChat;

        public TLString GAorB { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            GAorB = GetObject<TLString>(bytes, ref position);
            KeyFingerprint = GetObject<TLLong>(bytes, ref position);

            return this;
        }

        public override void Update(TLEncryptedChatBase chat)
        {
            base.Update(chat);

            var encryptedChat = chat as TLEncryptedChat;
            if (encryptedChat != null)
            {
                GAorB = encryptedChat.GAorB;
                KeyFingerprint = encryptedChat.KeyFingerprint;
            }
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);
            AccessHash = GetObject<TLLong>(input);
            Date = GetObject<TLInt>(input);
            AdminId = GetObject<TLInt>(input);
            ParticipantId = GetObject<TLInt>(input);
            GAorB = GetObject<TLString>(input);

            Key = GetNullableObject<TLString>(input);
            KeyFingerprint = GetNullableObject<TLLong>(input);
            P = GetNullableObject<TLString>(input);
            G = GetNullableObject<TLInt>(input);
            A = GetNullableObject<TLString>(input);
            MessageTTL = GetNullableObject<TLInt>(input);
            FileName = GetNullableObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());
            output.Write(AccessHash.ToBytes());
            output.Write(Date.ToBytes());
            output.Write(AdminId.ToBytes());
            output.Write(ParticipantId.ToBytes());
            output.Write(GAorB.ToBytes());

            Key.NullableToStream(output);
            KeyFingerprint.NullableToStream(output);
            P.NullableToStream(output);
            G.NullableToStream(output);
            A.NullableToStream(output);
            MessageTTL.NullableToStream(output);
            FileName.NullableToStream(output);
        }
    }

    public class TLEncryptedChatDiscarded : TLEncryptedChatBase
    {
        public const uint Signature = TLConstructors.TLEncryptedChatDiscarded;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            base.FromBytes(bytes, ref position);

            return this;
        }

        public override TLObject FromStream(Stream input)
        {
            Id = GetObject<TLInt>(input);

            Key = GetNullableObject<TLString>(input);
            KeyFingerprint = GetNullableObject<TLLong>(input);
            P = GetNullableObject<TLString>(input);
            G = GetNullableObject<TLInt>(input);
            A = GetNullableObject<TLString>(input);
            MessageTTL = GetNullableObject<TLInt>(input);
            FileName = GetNullableObject<TLString>(input);

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(TLUtils.SignatureToBytes(Signature));
            output.Write(Id.ToBytes());

            Key.NullableToStream(output);
            KeyFingerprint.NullableToStream(output);
            P.NullableToStream(output);
            G.NullableToStream(output);
            A.NullableToStream(output);
            MessageTTL.NullableToStream(output);
            FileName.NullableToStream(output);
        }
    }
}
