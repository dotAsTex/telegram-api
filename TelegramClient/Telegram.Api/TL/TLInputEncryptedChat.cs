﻿namespace Telegram.Api.TL
{
    public class TLInputEncryptedChat : TLObject
    {
        public const uint Signature = TLConstructors.TLInputEncryptedChat;

        public TLInt ChatId { get; set; }

        public TLLong AccessHash { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            ChatId = GetObject<TLInt>(bytes, ref position);
            AccessHash = GetObject<TLLong>(bytes, ref position);

            return this;
        }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                ChatId.ToBytes(),
                AccessHash.ToBytes());
        }
    }
}
