﻿namespace Telegram.Api.TL
{
    public class TLInitConnection : TLObject
    {
        public const string Signature = "#69796de9";

        public TLInt AppId { get; set; }

        public TLString DeviceModel { get; set; }

        public TLString SystemVersion { get; set; }

        public TLString AppVersion { get; set; }

        public TLString LangCode { get; set; }

        public TLObject Data { get; set; }

        public override byte[] ToBytes()
        {
            return TLUtils.Combine(
                TLUtils.SignatureToBytes(Signature),
                AppId.ToBytes(),
                DeviceModel.ToBytes(),
                SystemVersion.ToBytes(),
                AppVersion.ToBytes(),
                LangCode.ToBytes(),
                Data.ToBytes());
        }
    }
}
