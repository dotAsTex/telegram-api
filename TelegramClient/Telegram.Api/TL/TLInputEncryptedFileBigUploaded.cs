﻿namespace Telegram.Api.TL
{
    public class TLInputEncryptedFileBigUploaded : TLObject
    {
        public const uint Signature = TLConstructors.TLInputEncryptedFileBigUploaded;

        public TLLong Id { get; set; }

        public TLInt Parts { get; set; }

        public TLInt KeyFingerprint { get; set; }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            TLUtils.WriteLine("--Parse TLInputEncryptedFileBigUploaded--");
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            Id = GetObject<TLLong>(bytes, ref position);
            Parts = GetObject<TLInt>(bytes, ref position);
            KeyFingerprint = GetObject<TLInt>(bytes, ref position);

            return this;
        }
    }
}
