﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Telegram.Api.Transport
{
    public class TCPTransportEx
    {
        private const int TimeoutInterval = 25000;

        private string _host;

        private int _port;

        private byte[] _buffer;

        private const int BufferSize = 100;

        private readonly Socket _socket;

        private readonly SocketAsyncEventArgs _listener = new SocketAsyncEventArgs();

        private readonly SocketAsyncEventArgs _connector = new SocketAsyncEventArgs();

        public TCPTransportEx(string host, int port)
        {
            _host = host;
            _port = port;
            _buffer = new byte[BufferSize];

            //ConnectAsync(host, port, null);

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            _listener.SetBuffer(_buffer, 0, _buffer.Length);
            _listener.Completed += OnReceived;

            _connector.RemoteEndPoint = new DnsEndPoint(_host, _port);
            _connector.Completed += OnConnected;
        }

        private void OnConnected(object sender, SocketAsyncEventArgs e)
        {
            _connectEvent.Set();
        }

        private void OnReceived(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                ProcessResponse(e.Buffer);
            }

            ReceiveAsync();
        }

        private void ReceiveAsync()
        {
            _socket.ReceiveAsync(_listener);
        }

        public bool Connected { get { return _socket.Connected; } }

        private readonly object _connectingRoot = new object();

        public bool Connecting { get; protected set; }

        private readonly AutoResetEvent _connectEvent = new AutoResetEvent(false);

        public void ConnectAsync(int maxAttempt, Action callback, Action faultCallback)
        {
            int attempt = 0;
            while (true)
            {
                bool succeded;
                lock (_connectingRoot)
                {
                    if (Connected) return;

                    Connecting = true;

                    _connectEvent.Reset();
                    try
                    {
                        _socket.SendAsync(_connector);
                    }
                    catch (Exception e)
                    {
                        faultCallback();
                        return;
                    }

                    succeded = _connectEvent.WaitOne(TimeoutInterval);

                    Connecting = false;
                }


                if (!succeded)
                {
                    if (maxAttempt < attempt)
                    {
                        Thread.Sleep(1000);
                        attempt++;
                        continue;
                    }
                }
                else
                {
                    callback();
                }
                break;
            }
        }

        public void SendAsync(byte[] data, Action faultCallback)
        {
            if (!_socket.Connected) ConnectAsync(5, () => SendAsync(data, faultCallback), faultCallback);

            var manualResetEvent = new ManualResetEvent(false);
            var args = new SocketAsyncEventArgs();
            args.SetBuffer(data, 0, data.Length);
            args.Completed += (sender, eventArgs) => manualResetEvent.Set();
            _socket.SendAsync(args);

            var susseded = manualResetEvent.WaitOne(TimeoutInterval);

            if (!susseded)
            {
                faultCallback();
            }
        }


        private void ProcessResponse(byte[] response)
        {
            
        }

        public void Close()
        {
            _socket.Close();
        }
    }
}
